<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 流年 <liu21st@gmail.com>
// +----------------------------------------------------------------------

use think\Db;

/**
 * 判断是否为合法的身份证号码
 * @param $mobile
 * @return int
 */
function isCreditNo($vStr){
    $vCity = array(
        '11','12','13','14','15','21','22',
        '23','31','32','33','34','35','36',
        '37','41','42','43','44','45','46',
        '50','51','52','53','54','61','62',
        '63','64','65','71','81','82','91'
    );
    if (!preg_match('/^([\d]{17}[xX\d]|[\d]{15})$/', $vStr)) return false;
    if (!in_array(substr($vStr, 0, 2), $vCity)) return false;
    $vStr = preg_replace('/[xX]$/i', 'a', $vStr);
    $vLength = strlen($vStr);
    if ($vLength == 18) {
        $vBirthday = substr($vStr, 6, 4) . '-' . substr($vStr, 10, 2) . '-' . substr($vStr, 12, 2);
    } else {
        $vBirthday = '19' . substr($vStr, 6, 2) . '-' . substr($vStr, 8, 2) . '-' . substr($vStr, 10, 2);
    }
    if (date('Y-m-d', strtotime($vBirthday)) != $vBirthday) return false;
    if ($vLength == 18) {
        $vSum = 0;
        for ($i = 17 ; $i >= 0 ; $i--) {
            $vSubStr = substr($vStr, 17 - $i, 1);
            $vSum += (pow(2, $i) % 11) * (($vSubStr == 'a') ? 10 : intval($vSubStr , 11));
        }
        if($vSum % 11 != 1) return false;
    }
    return true;
}

// CURL
function https_request($url,$header=NULL,$data=null){
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL,$url);
    if(!empty($header)){
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
    }
    if (!empty($data)){
        curl_setopt($curl,CURLOPT_POST,1);
        curl_setopt($curl,CURLOPT_POSTFIELDS,$data);
    }
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    $output = curl_exec($curl);
    curl_close($curl);
    return $output;
}




//搜索界面查询数据
function getOneStock($gid) {
  
  	$stock = new Stock;
  	$list = $stock->Hq_real($gid);
    //$arr=explode('~',iconv("gb2312", "utf-8//IGNORE",$arr));
	//返回的是json字符串
    return $list;
}


//短信验证码
function sendMessage($rand,$mobile){
    $title=Db::name('hide')->where(['id' => 40])->value('value');
    //参数赋值
    $name = 'skcl';//账号
    $password = 'x4hbr81q';//密码
    $seed = date("YmdHis");//当前时间
    $content = '【'.$title.'】您的验证码是'.$rand.'。如非本人操作，请忽略本短信';//短信内容
    $ext = '';//扩展号码
    $reference = '';//参考信息
    $enCode = 'UTF-8';//编码（UTF-8、GBK）
    $method = 'GET';//请求方式（POST、GET）
    $url = '';
    if ($enCode=='UTF-8'){
        $url = 'http://160.19.212.218:8080/eums/utf8/send_strong.do';//UTF-8编码接口地址
    }else if ($enCode=='GBK'){
        $url = 'http://160.19.212.218:8080/eums/send_strong.do';//GBK编码接口地址
    }
    $content  = encoding($content,$enCode);//注意编码，字段编码要和接口所用编码一致，有可能出现汉字之类的记得转换编码

    //请求参数
    //utf8和gbk编码请自行转换
    $params = array(
        'name' => encoding($name,$enCode),//帐号，由网关分配
        'seed' => $seed,//当前时间，格式：YYYYMMDD HHMMSS，例如：20130806102030。客户时间早于或晚于网关时间超过30分钟，则网关拒绝提交。
        //从php5.1.0开始，PHP.ini里加了date.timezone这个选项，并且默认情况下是关闭的也就是显示的时间（无论用什么php命令）都是格林威治标准时间和我们的时间（北京时间）差了正好8个小时。
        //找到php.ini中的“;date.timezone =”这行，将“;”去掉，改成“date.timezone = PRC”（PRC：People's Republic of China 中华人民共和国），重启Apache，问题解决。
        'key' => md5(md5($password).$seed),//md5( md5(password)  +  seed) )
        //其中“+”表示字符串连接。即：先对密码进行md5加密，将结果与seed值合并，再进行一次md5加密。
        //两次md5加密后字符串都需转为小写。
        //例如：若当前时间为2013-08-06 10:20:30，密码为123456，
        //则：key=md5(md5(“123456”) + “20130806102030” )
        //则：key=md5(e10adc3949ba59abbe56e057f20f883e20130806102030)
        //则：key= cd6e1aa6b89e8e413867b33811e70153
        'dest' => $mobile,//手机号码（多个号码用“半角逗号”分开），GET方式每次最多100个号码，POST方式号码个数不限制，但建议不超过3万个
        'content' => $content,//短信内容。最多500个字符。
        'ext' => $ext,//扩展号码（视通道是否支持扩展，可以为空或不填）
        'reference' => $reference//参考信息（最多50个字符，在推送状态报告、推送上行时，推送给合作方，本参数不参与任何下行控制，仅为合作方提供方便，可以为空或不填）如果不知道如何使用，请忽略该参数，不能含有半角的逗号和分号。
    );
    //echo $method.'请求如下：<br/>';
    if ($method=='POST'){
        $resp =  send_post_curl($url, $params);//POST请求，数据返回格式为error:xxx,success:xxx
    }else if ($method=='GET'){
        $resp =  send_get($url, $params);//GET请求，数据返回格式为error:xxx,success:xxx
    }
    $response = explode(':', $resp);
    $code = $response[1];//响应代码

    //if ($response[0]=='success'){
    //   return '成功代码:'.$code.'<br/>';
    // }else{
    //  return '错误代码:'.$code.'<br/>';
    // }
}
function encoding($str,$urlCode){
    if( !empty($str) ){
        $fileType = mb_detect_encoding($str , array('UTF-8','GBK','LATIN1','BIG5')) ;
    }
    return mb_convert_encoding($str, $urlCode, $fileType);
}
 function send_get($url,$params){
    $getdata = http_build_query($params);
    $content = file_get_contents($url.'?'.$getdata);
    return $content;
}
 function send_post_curl($url,$params){

    $postdata = http_build_query($params);
    $length = strlen($postdata);
    $cl = curl_init($url);//①：初始化
    curl_setopt($cl, CURLOPT_POST, true);//②：设置属性
    curl_setopt($cl,CURLOPT_HTTP_VERSION,CURL_HTTP_VERSION_1_1);
    curl_setopt($cl,CURLOPT_HTTPHEADER,array("Content-Type: application/x-www-form-urlencoded","Content-length: ".$length));
    curl_setopt($cl,CURLOPT_POSTFIELDS,$postdata);
    curl_setopt($cl,CURLOPT_RETURNTRANSFER,true);
    $content = curl_exec($cl);//③：执行并获取结果
    curl_close($cl);//④：释放句柄
    return $content;
}

// 将UNICODE编码后的内容进行解码，编码后的内容格式：\u56fe\u7247
function unicode_decode($name) {
    // 转换编码，将Unicode编码转换成可以浏览的utf-8编码
    $pattern = '/([\w]+)|(\\\u([\w]{4}))/i';
    preg_match_all($pattern, $name, $matches);
    if (!empty($matches)) {
        $name = '';
        for ($j = 0; $j < count($matches[0]); $j++) {
            $str = $matches[0][$j];
            if (strpos($str, '\\u') === 0) {
                $code = base_convert(substr($str, 2, 2), 16, 10);
                $code2 = base_convert(substr($str, 4), 16, 10);
                $c = chr($code).chr($code2);
                $c = iconv('UCS-2', 'UTF-8', $c);
                $name .= $c;
            } else {
                $name .= $str;
            }
        }
    }
    return $name;
}


//导出excel
function daochu($title,$arrHeader,$data){
    Vendor('PHPExcel.PHPExcel');//调用类库,路径是基于vendor文件夹的
    Vendor('PHPExcel.PHPExcel.Worksheet.Drawing');
    Vendor('PHPExcel.PHPExcel.Writer.Excel2007');
   	Vendor('PHPExcel.PHPExcel.Writer.Excel5');
    $objExcel = new \PHPExcel();
    //set document Property
    $objWriter = \PHPExcel_IOFactory::createWriter($objExcel, 'Excel5');

    $objActSheet = $objExcel->getActiveSheet();
    $key = ord("A");
    $letter =config('ABCD');
    //填充表头信息
    $lenth =  count($arrHeader);
    for($i = 0;$i < $lenth;$i++) {
        $objActSheet->setCellValue("$letter[$i]1","$arrHeader[$i]");
    };

    //填充表格信息
    foreach($data as $k=>$v){
        $yuank=0;
        $k +=2;
        foreach($v as $kk=>$vv){
            $objActSheet->setCellValue(config('ABCD')[$yuank].$k,$vv);
            $yuank+=1;
        }
//            $objActSheet->setCellValue('A'.$k,$v['id']);
//            $objActSheet->setCellValue('B'.$k, $v['account']);
//            // // 图片生成
//            // $objDrawing[$k] = new \PHPExcel_Worksheet_Drawing();
//            // $objDrawing[$k]->setPath('public/static/admin/images/profile_small.jpg');
//            // // 设置宽度高度
//            // $objDrawing[$k]->setHeight(40);//照片高度
//            // $objDrawing[$k]->setWidth(40); //照片宽度
//            // /*设置图片要插入的单元格*/
//            // $objDrawing[$k]->setCoordinates('C'.$k);
//            // // 图片偏移距离
//            // $objDrawing[$k]->setOffsetX(30);
//            // $objDrawing[$k]->setOffsetY(12);
//            // $objDrawing[$k]->setWorksheet($objPHPExcel->getActiveSheet());
//            // 表格内容

        // 表格高度
        $objActSheet->getRowDimension($k)->setRowHeight(20);
    }

//        $width = array(20,20,20,20,20,20,20,20,20,20,20);
    //设置表格的宽度
    foreach(config('ABCD') as $k=>$v){
        $objActSheet->getColumnDimension($v)->setWidth(20);
    }
//        $objActSheet->getColumnDimension('A')->setWidth($width[0]);
//        $objActSheet->getColumnDimension('B')->setWidth($width[1]);
//        $objActSheet->getColumnDimension('C')->setWidth($width[2]);


    $outfile = $title.date("Y-m-d").".xls";
    ob_end_clean();
  	header("Content-type: application/vnd.ms-excel;charset=UTF-8");
    header("Content-Type: application/force-download");
    header("Content-Type: application/octet-stream");
    header("Content-Type: application/download");
    header('Content-Disposition:inline;filename="'.$outfile.'"');
    header("Content-Transfer-Encoding: binary");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Pragma: no-cache");
    $objWriter->save('php://output');
}



function json($list, $status, $msg,$para='') {
    $arr['status'] = $status;
    $arr['msg'] = $msg;
    $arr['list'] = $list;
    if($para){
        $arr['para'] = $para;
    }
    return json_encode($arr,JSON_UNESCAPED_UNICODE);
}




//获取是否是节假日
//工作日对应结果为 0, 休息日对应结果为 1, 节假日对应的结果为 2；
function isHoliday($i=0)
{

    $today = date('Ymd',time()-$i*24*3600);

    if (cache($today) !== false) {
        return cache($today);
    } else {
        $api2 = json_decode(https_request('http://api.goseek.cn/Tools/holiday?date='.$today),true)['data'];
        if (is_numeric($api2)) {
            if(!$api2&&(date('w',time()-$i*24*3600)<6&&date('w',time()-$i*24*3600)>0)){
                cache($today, $api2, 86400);
            }else{
                cache($today, 1, 86400);
            }
            return cache($today);
        } else {
            $api1 = https_request('http://tool.bitefu.net/jiari/?d='.$today);
            if (is_numeric($api1)) {
                if(!$api1&&(date('w',time()-$i*24*3600)<6&&date('w',time()-$i*24*3600)>0)){
                    cache($today, $api1, 86400);
                }else{
                    cache($today, 1, 86400);
                }
                return cache($today);
            } else {
                return -1;
            }
        }
    }
}

//推送(内部调用)
function push($account='',$content='',$push_type=1,$true='') {
	Db::name('push')->insert(['push_type'=>$push_type,'account'=>$account,'content'=>$content,'status'=>1,'is_del'=>1,'create_time'=>time()]);
}

?>
