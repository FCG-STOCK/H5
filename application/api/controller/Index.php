<?php
namespace app\API\controller;
use think\Db;
use think\Request;
use think\Controller;
use think\Validate;
use Stock;
class Index extends controller
{

	public function login() {
        $info = Request::instance()->param();
        $data['account'] = $info['account'];
        $data['pwd'] = $info['pwd'];

        if (!$data['account']) {
            return json('', 0, '用户名不能为空');
        }
        $rule = [
            'account' => '^1[345789]\d{9}$',
            'pwd'     => 'require',
        ];
        $msg = [
            'account' => '用户名必须是手机号',
            'pwd'     => '密码不能为空',
        ];
        $validate = new Validate($rule, $msg);
        if (!$validate->check($data)) {
            return json('', 0, $validate->getError());
        }
        $data['pwd'] = md5($data['pwd']);
      
      	$if_register = Db::name('admin')->where('account',$data['account'])->find();
      	if(!$if_register){
        	return json('', 1100, '用户名不存在');
        }

        $admin = Db::name('admin')->where($data)->find();
        if (!$admin) {
            return json('', 1100, '用户名或密码错误');
        }elseif($admin['is_del']==2){
            return json('', 0, '此账号已禁用');
        }

        //修改最后登陆时间,（毫秒级）     
      	//$time=microtime();
		//$time=explode(' ',$time)[1].substr(explode('.',$time)[1],0,3);
        $res=Db::name('admin')->where(['account'=>$data['account']])->update(['lastlogin_time'=>time()]);     	
        session('username',$admin['account']);
      	//设置cookie有效时间为12小时
        cookie('username',$admin['account'],43200);
        cookie('nick_name',$admin['nick_name'],43200);
      	cookie('lastlogin_time',time(),43200);

        if($res==1){
            //推广人数
            $admin['extend_num'] = Db::name('admin')->where(['inviter'=>$admin['account'],'is_del'=>1])->count();

            //更新用户银行卡信息
            //$this->confirmbankcard($data['account']);

            //是否隐藏策略 1隐藏 2不隐藏
            $hide=Db::name('hide')->where(['id' => 1])->value('value');
            //策略可进行翻倍的倍数
            $times=Db::name('hide')->where(['id' => 2])->value('value');
            //买卖的手续费比例
            $poundage=Db::name('hide')->where(['id' => 4])->value('value');
            //递延费比例
            $defer=Db::name('hide')->where(['id' => 5])->value('value');
            $arr['option'] = [
                'hide'=>$hide,
                'times'=>$times,
                'poundage'=>$poundage,
                'defer'=>$defer,
            ];

            //会员创建时间
            $admin['create_time']=strval(date('YmdHis',$admin['create_time']));
            return json($admin, 1, '登录成功',$arr);
        }else{
            return json('', 0, '修改最后登陆时间失败');
        }

    }

   
    //退出登录
    public function loginout(){
        cookie('username',null);
        cookie('nick_name',null);
      	cookie('lastlogin_time',null);
      	cookie('search_history',null);
      	
        return json('', 1, '退出登录成功');
    }


    //注册
    public function register() {
        $info = Request::instance()->param();

        $user=Db::name('admin')->where(['account'=>$info['Account']])->find();
        if($user){return json('', 1103, '用户名已存在');}

        // $data['regid'] = $info['regid'];
        $data['account'] = $info['Account'];
        $data['nick_name'] = $info['Account'];
        $data['pwd'] = $info['pwd'];
        $data['is_del'] = 1;
        $data['inviter'] = $info['inviter'];//邀请码

        $yzm = $info['yzm'];

        $yzmse = session('yzm');

        $tel = session('mobile');

        if (!$data['account']) {
            return json('', 0, '用户名不能为空');
        }
        if (!$yzm) {
            return json('', 1100, '验证码不能为空');
        }

        if($yzmse != $yzm){
            return json('',1101,"验证码不正确或已过期");
        }
        
        if($data['account'] != $tel){
            return json('',1102,"输入手机号和发送验证码手机不同");
        }

        $data['lastlogin_time'] = time();
        $data['create_time'] = time();
        

        //是否有邀请人,可填手机号或分销商工作人员编号,根据邀请人获得不同id
        if(array_key_exists('inviter',$info)&&$info['inviter']){
            //判断工作人员表中是否有此分销商工作人员编号
            $marketuser=Db::name('marketer_user')->where(['id'=>$info['inviter'],'is_del'=>1])->find();
            //判断会员表中是否有此手机号
            $inviter=Db::name('admin')->where(['account'=>$info['inviter'],'is_del'=>1])->find();
            //判断邀请人
            if($marketuser){
                //最近一次受此工作人员推广的用户  例8010012
                $list=Db::name('admin')->where(['marketer_user'=>$info['inviter'],'is_del'=>1])->select();
                array_multisort(array_column($list,'id'),SORT_DESC,$list);
                if($list){
                    $id=$list[0]['id'];
                    $data['id']=substr($id,0,6).(substr($id,6)+1);
                }else{
                    $data['id']=$info['inviter'].'1';
                }
                //添加分销商id和工作人员id
                $data['marketer_id']=substr($info['inviter'],0,3);
                $data['marketer_user']=$info['inviter'];
                //添加工作人员id到邀请人
                $data['inviter']=$info['inviter'];
            }elseif($inviter){
                //最近一次没有分销商的普通用户  例0000002
                $list=Db::name('admin')->where(['marketer_id'=>0,'is_del'=>1])->select();
                array_multisort(array_column($list,'id'),SORT_DESC,$list);
                if($list){
                    $id=$list[0]['id'];
                    $data['id']='000000'.($id+1);
                }else{
                    $data['id']='0000001';
                }
                //添加手机号到邀请人
                $data['inviter']=$info['inviter'];
            }else{
                return json('', 0, '邀请人不存在');
            }
        }else{
            //最近一次没有分销商的普通用户  例0000002
            $list=Db::name('admin')->field('id')->where(['marketer_id'=>0,'is_del'=>1])->select();
            array_multisort(array_column($list,'id'),SORT_DESC,$list);
            if($list){
                $id=$list[0]['id'];
                $data['id']='000000'.($id+1);
            }else{
                $data['id']='0000001';
            }
        }
        

        $rule = [
            'account' => '^1[345789]\d{9}$',
            'pwd'     => 'require',
            'inviter' => 'number',         
        ];
        $msg = [
            'account' => '用户名必须是手机号',
            'pwd'     => '密码不能为空',
            'inviter' => '邀请码必须是数字',
        ];
        $validate = new Validate($rule, $msg);
        if (!$validate->check($data)) {
            return json('', 0, $validate->getError());
        }

        $data['pwd'] = md5($data['pwd']);
      	//注册时，每个人默认有100000元炒股大赛的模拟资金
        $data['analog_money']=100000;
        //注册时，每个人默认有100000元的策略余额
        $data['tactics_balance']=0;
      	//注册时，用户的默认头像
        $apiurl=Db::name('hide')->where(['id'=>25])->value('value');//API接口地址
        $data['headurl']=$apiurl.'/public/static/img/grt.png';
        $res = Db::name('admin')->insert($data);
        if (!$res) {
            return json('', 0, '注册失败');
        }
		
      
      	//红包有效期
		$redday=Db::name('hide')->where(['id'=>11])->value('value');
        //发送红包
        if(array_key_exists('inviter',$info)&&$info['inviter']){
            //判断会员表中是否有此手机号
            $inviter=Db::name('admin')->where(['account'=>$info['inviter'],'is_del'=>1])->find();
            if($inviter){
                //给这个邀请人发放1个红包
                $para=[
                    'account'=>$info['inviter'],
                    'packet_name'=>'邀请奖励红包',
                    'packet_money'=>10,
                    'is_use'=>2,
                    'create_time'=>time(),
                   'daoqi_time'=>strtotime(date('Ymd',time()+$redday*24*3600)),
                ];
                Db::name('packet')->insert($para);
            }
        }

        //新注册用户获得5个红包
        $para=[
            'account'=>$info['Account'],
            'packet_name'=>'新人红包',
            'packet_money'=>10,
            'is_use'=>2,
            'create_time'=>time(),
          	 'daoqi_time'=>strtotime(date('Ymd',time()+$redday*24*3600)),
        ];
        for($i=0;$i<5;$i++){
            Db::name('packet')->insert($para);
        }

        //添加个人设置
        Db::name('settings')->insert(['account'=>$info['Account']]);

        session('username',$data['account']);
        cookie('username',$data['account']);
        cookie('nick_name',$data['nick_name']);        
        return json('', 1, '注册成功');
    }



    //修改登陆密码
    public function modifyPwd() {
        $info = Request::instance()->param();
        if (!$info['account']) {
            return json('', 0, '手机号不能为空');
        }

        $yzm = $info['yzm'];

        $yzmse = session('yzm');

        $tel = session('mobile');
        if (!$yzm) {
            return json('', 0, '验证码不能为空');
        }

        if($yzmse != $yzm){
            return json('',0,"验证码不正确或已过期");
        }
        
        if($info['account'] != $tel){
            return json('',0,"输入手机号和发送验证码手机不同");
        }

        if (!$info['pwd']) {
            return json('', 0, '密码不能为空');
        }
        if (!$info['repwd']) {
            return json('', 0, '确认密码不能为空');
        }
        if ($info['pwd'] != $info['repwd']) {
            return json('', 0, '两次输入密码不一致');
        }
        $res = Db::name('admin')->where(['account' => $info['account']])->update(['pwd' => md5($info['pwd'])]);
        // if (!$res) {
        //     return json('', 0, '密码修改失败');
        // }
        return json('', 1, '成功');
    }



    //PC修改交易密码
    public function setpay(){
        $info = Request::instance()->param();
        if (!$info['account']) {
            return json('', 0, '手机号不能为空');
        }

        $yzm = $info['yzm'];

        $yzmse = session('yzm');

        $tel = session('mobile');
        if (!$yzm) {
            return json('', 0, '验证码不能为空');
        }

        if($yzmse != $yzm){
            return json('',0,"验证码不正确或已过期");
        }
        
        if($info['account'] != $tel){
            return json('',0,"输入手机号和发送验证码手机不同");
        }

        if (!$info['trade_pwd']) {
            return json('', 0, '交易密码不能为空');
        }
        if (!$info['trade_repwd']) {
            return json('', 0, '确认密码不能为空');
        }
        if ($info['trade_pwd'] != $info['trade_repwd']) {
            return json('', 0, '两次输入密码不一致');
        }
        $res = Db::name('admin')->where(['account' => $info['account']])->update(['trade_pwd' => md5($info['trade_pwd'])]);
        // if (!$res) {
        //     return json('', 0, '密码修改失败');
        // }
        return json('', 1, '成功');
    }


    //h5修改交易密码
    public function setTradePwd() {
        
        $info = Request::instance()->param();
        if(!$info['trade_pwd']){
            return json('', 0, '交易密码为空');
        }
        $data['trade_pwd'] = md5($info['trade_pwd']);
        Db::name('admin')->where(['account' => $info['account'],'is_del'=>1])->update($data);
        return json('', 1, '修改成功');
    }



    //大盘股指实时行情_批量 new
    public function grailindex() {
        $shipanstock=new Stock();
        if(date('Hi')>=930){
            $res1=$shipanstock->grailindex1();
            $res2=$shipanstock->grailindex2();
            $res3=$shipanstock->grailindex3();

            $arr1=explode(",",explode("\"",$res1)[1]);
            $arr2=explode(",",explode("\"",$res2)[1]);
            $arr3=explode(",",explode("\"",$res3)[1]);

            $stock=[
                'showapi_res_body'=>[
                    'indexList'=>[
                        [
                            'name'=>'上证指数',
                            'nowPoint'=>round($arr1[1],2),
                            'nowPrice'=>round($arr1[2],2),
                            'diff_rate'=>round($arr1[3],2),
                        ],[
                            'name'=>'深证成指',
                            'nowPoint'=>round($arr2[1],2),
                            'nowPrice'=>round($arr2[2],2),
                            'diff_rate'=>round($arr2[3],2),
                        ],[
                            'name'=>'创业板指',
                            'nowPoint'=>round($arr3[1],2),
                            'nowPrice'=>round($arr3[2],2),
                            'diff_rate'=>round($arr3[3],2),
                        ]
                    ]
                ]
            ];
        }else{
            $res1=$shipanstock->Hq_real('sh000001');
            $res2=$shipanstock->Hq_real('sz399001');
            $res3=$shipanstock->Hq_real('sz399006');

            $arr1=json_decode($res1,true)['result'][0]['data'];
            $arr2=json_decode($res2,true)['result'][0]['data'];
            $arr3=json_decode($res3,true)['result'][0]['data'];

            $stock=[
                'showapi_res_body'=>[
                    'indexList'=>[
                        [
                            'name'=>'上证指数',
                            'nowPoint'=>round($arr1['nowPri'],2),
                            'nowPrice'=>round($arr1['increase'],2),
                            'diff_rate'=>round($arr1['increPer']*100,2),
                        ],[
                            'name'=>'深证成指',
                            'nowPoint'=>round($arr2['nowPri'],2),
                            'nowPrice'=>round($arr2['increase'],2),
                            'diff_rate'=>round($arr2['increPer']*100,2),
                        ],[
                            'name'=>'创业板指',
                            'nowPoint'=>round($arr3['nowPri'],2),
                            'nowPrice'=>round($arr3['increase'],2),
                            'diff_rate'=>round($arr3['increPer']*100,2),
                        ]
                    ]
                ]
            ]; 
        }
        
        return json($stock, 1, '查询成功');
    }


    //判断股票是否停盘（股票页面）
    public function checkstockstop() {
      	$upstop=Db::name('hide')->where(['id' => 22])->value('value');
      	$downstop=Db::name('hide')->where(['id' => 47])->value('value');
      	$dattime=Db::name('hide')->where(['id' => 48])->value('value');
        $time=1;
        for($i=2;$i<12;$i++){
            if(isHoliday($i)!=0){
                $time+=1;
            }else{
                break;
            }
            continue;
        }
        $times=1;
        for($i=1;$i<12;$i++){
            if(isHoliday($i)!=0){
                $times+=1;
            }else{
                break;
            }
            continue;
        }
        $arr=explode('~',iconv("gb2312", "utf-8//IGNORE",file_get_contents('http://qt.gtimg.cn/q='.input('gid'))));
        if($arr[31]!='0.00'&&$arr[3]){
            if(strpos($arr[1],'ST')===0||strpos($arr[1],'ST')){
                return json('', 201, '已停盘');
            }else{
                $str=file_get_contents('http://money.finance.sina.com.cn/quotes_service/api/json_v2.php/CN_MarketData.getKLineData?symbol='.input('gid').'&scale=60&ma=25&datalen=9');
                $list=explode(',',substr($str,1,-1));
                foreach($list as $k=>$v){
                    if(strpos($v,date('Y-m-d',time()-($time+1)*3600*24).' 15:00:00')){
                        $qclose=substr($list[$k+4],7,5);
                    }
                    if(strpos($v,date('Y-m-d',time()-$times*3600*24).' 15:00:00')){
                        $yclose=substr($list[$k+4],7,5);
                    }
                }
                $now=$arr[3];
                $today=($now-$yclose)/$yclose;
                $yesterday=($yclose-$qclose)/$qclose;
                if($today>$upstop){
                    return json('', 201, '涨停');
                }
              	if($today<$downstop){
                    return json('', 201, '跌停');
                }
              	if($dattime!=1){
                	if($yesterday>$upstop){
                        return json('', 201, '涨停');
                    }
                  	if($yesterday<$downstop){
                        return json('', 201, '跌停');
                    }
                }
                return json('', 200, '查询成功');
            }
        }else{
            return json('', 201, '已停盘');
        }
    }


    //忘记密码
    public function forgetPwd() {
        $info = Request::instance()->param();
        if (!$info['account']) {
            return json('', 0, '手机号不能为空');
        }
        $yzm = $info['yzm'];

        $yzmse = session('yzm');

        $tel = session('mobile');
        if (!$yzm) {
            return json('', 0, '验证码不能为空');
        }

        if($yzmse != $yzm){
            return json('',0,"验证码不正确或已过期");
        }
        
        if($info['account'] != $tel){
            return json('',0,"输入手机号和发送验证码手机不同");
        }
        if (!$info['pwd']) {
            return json('', 0, '密码不能为空');
        }
      
      	$if_register = Db::name('admin')->where(['account' => $info['account'],'is_del'=>1])->find();
      	if(!$if_register){
        	return json('', 0, '用户名不存在');
        
        }
        cookie('username','');
        $res = Db::name('admin')->where(['account' => $info['account']])->update(['pwd' => md5($info['pwd'])]);
        // if (!$res) {
        //     return json('', 0, '密码修改失败');
        // }
        return json('', 1, '提交成功');
    }

    //发送短信
    public function sendMessage() {
        $info = Request::instance()->param();
        $mobile = $info['mobile'];
      
      	$user=Db::name('admin')->where(['account'=>$mobile])->find();
        if($user){return json('', 1103, '用户名已存在');}
      
        $rand = rand(1000, 9999);
        session('yzm',$rand);
        session('mobile',$mobile);
        $json_data=sendMessage($rand,$mobile);
        $array = json_decode($json_data, true);
        if ($array['code'] == 0) {
            return json($list['rand'] = strval(md5($rand)), 1, '发送成功');
        } else {
            return json('', 0, '发送失败');
        }
    }
  
  
  	//发送短信
    public function sendMessageforgetpw() {
        $info = Request::instance()->param();
        $mobile = $info['mobile'];
                 
        $rand = rand(1000, 9999);
        session('yzm',$rand);
        session('mobile',$mobile);
        $json_data=sendMessage($rand,$mobile);
        $array = json_decode($json_data, true);


	if ($array['code'] == 0) {
            return json($list['rand'] = strval(md5($rand)), 1, '发送成功');
        } else {
            return json('', 0, '发送失败');
        }
    }
  
  
  
  
  


    //获取股市所有股票
    public function getallstock() {
        //获取深圳股票数据列表
        $sz = $this->szstock(1, 4);
        $page = ceil($sz['result']['totalCount'] / $sz['result']['num']);
        for ($i = 1; $i <= $page; $i++) {
            $szlist = $this->szstock($i, 4);
            foreach ($szlist['result']['data'] as $k => $v) {
                $list[] = $v;
            }
        }
        //获取上海股票数据列表
        $sh = $this->shstock(1, 4);
        $page = ceil($sh['result']['totalCount'] / $sh['result']['num']);
        for ($i = 1; $i <= $page; $i++) {
            $shlist = $this->shstock($i, 4);
            foreach ($shlist['result']['data'] as $k => $v) {
                $list[] = $v;
            }
        }

        //将股票数据写入js文件
        foreach ($list as $key => $value) {            
            $stock[$key] = "stock[".$key."]={ c: '".$value['symbol']."', n: '".$value['name']."' };";            
        }

        // foreach ($stock as $key => $value) {
            
        // }
        file_put_contents("test.js","var stock = new Array();");
        //在文件里追加内容
        echo file_put_contents("test.js",$stock,FILE_APPEND);
        exit;

//         echo "<pre>";

// print_r($stock);
// echo "</pre>";die;


        return json($list, 1, '查询成功');

    }



    //测试接口上线删除 获取股票 
    public function getallstock2() {
        //获取深圳股票数据列表
        $sz = $this->szstock(1, 4);
        $page = 1;
        for ($i = 1; $i <= $page; $i++) {
            $szlist = $this->szstock($i, 4);
            foreach ($szlist['result']['data'] as $k => $v) {
                $list[] = $v;
            }
        }
        //获取上海股票数据列表
        $sh = $this->shstock(1, 4);
        $page = 1;
        for ($i = 1; $i <= $page; $i++) {
            $shlist = $this->shstock($i, 4);
            foreach ($shlist['result']['data'] as $k => $v) {
                $list[] = $v;
            }
        }
        return json($list, 1, '查询成功');

    }





  
  
  
  	//获取持仓股票单价 获取单个股票数据 订单号
    public function getOneStocknum() {
      	$strategy_num = input('strategy_num');
      	$trues = input('trues');
      	if($trues==1){
        	$gid = Db::name('strategy')->where('strategy_num',$strategy_num)->value('stock_code'); 
        }else{
        	$gid = Db::name('strategy_contest')->where('strategy_num',$strategy_num)->value('stock_code'); 
        }
      	       
        $url = 'http://web.juhe.cn:8080/finance/stock/hs';
        $data['key'] = '7fdd610575a30a240bba1b9cd7529896';
        $data['gid'] = $gid;
        $res = https_request($url, '', $data);
        $stock = json_decode($res, true);
        return json($stock, 1, '查询成功');
    }
  

    //获取单个股票数据
    public function getOneStock2() {
        $arr=explode('~',iconv("gb2312", "utf-8//IGNORE",file_get_contents('http://qt.gtimg.cn/q=sh000001')));

        $list['result'][0]['data']['gid']=input('gid');
        $list['result'][0]['data']['increPer']=$arr[32];
        $list['result'][0]['data']['increase']=$arr[31];
        $list['result'][0]['data']['name']=$arr[1];
        $list['result'][0]['data']['todayStartPri']=$arr[5];
        $list['result'][0]['data']['yestodEndPri']=$arr[4];
        $list['result'][0]['data']['nowPri']=$arr[3];
        $list['result'][0]['data']['todayMax']=$arr[33];
        $list['result'][0]['data']['todayMin']=$arr[34];
        $list['result'][0]['data']['traNumber']=$arr[36];
        $list['result'][0]['data']['traAmount']=$arr[37];
        $list['result'][0]['data']['buyOne']=$arr[10];
        $list['result'][0]['data']['buyOnePri']=$arr[9];
        $list['result'][0]['data']['buyTwo']=$arr[12];
        $list['result'][0]['data']['buyTwoPri']=$arr[11];
        $list['result'][0]['data']['buyThree']=$arr[14];
        $list['result'][0]['data']['buyThreePri']=$arr[13];
        $list['result'][0]['data']['buyFour']=$arr[16];
        $list['result'][0]['data']['buyFourPri']=$arr[15];
        $list['result'][0]['data']['buyFive']=$arr[18];
        $list['result'][0]['data']['buyFivePri']=$arr[17];
        $list['result'][0]['data']['sellOne']=$arr[20];
        $list['result'][0]['data']['sellOnePri']=$arr[19];
        $list['result'][0]['data']['sellTwo']=$arr[22];
        $list['result'][0]['data']['sellTwoPri']=$arr[21];
        $list['result'][0]['data']['sellThree']=$arr[24];
        $list['result'][0]['data']['sellThreePri']=$arr[23];
        $list['result'][0]['data']['sellFour']=$arr[26];
        $list['result'][0]['data']['sellFourPri']=$arr[25];
        $list['result'][0]['data']['sellFive']=$arr[28];
        $list['result'][0]['data']['sellFivePri']=$arr[27];
        $list['result'][0]['data']['date']=substr($arr[30],0,4).'-'.substr($arr[30],4,2).'-'.substr($arr[30],6,2);
        $list['result'][0]['data']['time']=substr($arr[30],8,2).':'.substr($arr[30],10,2).':'.substr($arr[30],12,2);

//        $url = 'http://web.juhe.cn:8080/finance/stock/hs';
//        $data['key'] = '7fdd610575a30a240bba1b9cd7529896';
//        $data['gid'] = input('gid');
//        $res = https_request($url, '', $data);
//        $stock = json_decode($res, true);
        return json($list, 1, '查询成功');
    }
  
  
  	//获取单个股票数据
    public function getOneStock() {
        $arr=explode('~',iconv("gb2312", "utf-8//IGNORE",file_get_contents('http://qt.gtimg.cn/q='.input('gid'))));

        $list['result'][0]['data']['gid']=input('gid');
        $list['result'][0]['data']['increPer']=$arr[32];
        $list['result'][0]['data']['increase']=$arr[31];
        $list['result'][0]['data']['name']=$arr[1];
        $list['result'][0]['data']['todayStartPri']=$arr[5];
        $list['result'][0]['data']['yestodEndPri']=$arr[4];
        $list['result'][0]['data']['nowPri']=$arr[3];
        $list['result'][0]['data']['todayMax']=$arr[33];
        $list['result'][0]['data']['todayMin']=$arr[34];
        $list['result'][0]['data']['traNumber']=$arr[36];
        $list['result'][0]['data']['traAmount']=$arr[37];
        $list['result'][0]['data']['buyOne']=$arr[10];
        $list['result'][0]['data']['buyOnePri']=$arr[9];
        $list['result'][0]['data']['buyTwo']=$arr[12];
        $list['result'][0]['data']['buyTwoPri']=$arr[11];
        $list['result'][0]['data']['buyThree']=$arr[14];
        $list['result'][0]['data']['buyThreePri']=$arr[13];
        $list['result'][0]['data']['buyFour']=$arr[16];
        $list['result'][0]['data']['buyFourPri']=$arr[15];
        $list['result'][0]['data']['buyFive']=$arr[18];
        $list['result'][0]['data']['buyFivePri']=$arr[17];
        $list['result'][0]['data']['sellOne']=$arr[20];
        $list['result'][0]['data']['sellOnePri']=$arr[19];
        $list['result'][0]['data']['sellTwo']=$arr[22];
        $list['result'][0]['data']['sellTwoPri']=$arr[21];
        $list['result'][0]['data']['sellThree']=$arr[24];
        $list['result'][0]['data']['sellThreePri']=$arr[23];
        $list['result'][0]['data']['sellFour']=$arr[26];
        $list['result'][0]['data']['sellFourPri']=$arr[25];
        $list['result'][0]['data']['sellFive']=$arr[28];
        $list['result'][0]['data']['sellFivePri']=$arr[27];
        $list['result'][0]['data']['date']=substr($arr[30],0,4).'-'.substr($arr[30],4,2).'-'.substr($arr[30],6,2);
        $list['result'][0]['data']['time']=substr($arr[30],8,2).':'.substr($arr[30],10,2).':'.substr($arr[30],12,2);

//        $url = 'http://web.juhe.cn:8080/finance/stock/hs';
//        $data['key'] = '7fdd610575a30a240bba1b9cd7529896';
//        $data['gid'] = input('gid');
//        $res = https_request($url, '', $data);
//        $stock = json_decode($res, true);
        return json($list, 1, '查询成功');
    }
  
  
  
  	
  	//获取单个股票数据
    public function getOneStock3($gid) {
        $arr=explode('~',iconv("gb2312", "utf-8//IGNORE",file_get_contents('http://qt.gtimg.cn/q='.$gid)));

        $list['result'][0]['data']['gid']=input('gid');
        $list['result'][0]['data']['increPer']=$arr[32];
        $list['result'][0]['data']['increase']=$arr[31];
        $list['result'][0]['data']['name']=$arr[1];
        $list['result'][0]['data']['todayStartPri']=$arr[5];
        $list['result'][0]['data']['yestodEndPri']=$arr[4];
        $list['result'][0]['data']['nowPri']=$arr[3];
        $list['result'][0]['data']['todayMax']=$arr[33];
        $list['result'][0]['data']['todayMin']=$arr[34];
        $list['result'][0]['data']['traNumber']=$arr[36];
        $list['result'][0]['data']['traAmount']=$arr[37];
        $list['result'][0]['data']['buyOne']=$arr[10];
        $list['result'][0]['data']['buyOnePri']=$arr[9];
        $list['result'][0]['data']['buyTwo']=$arr[12];
        $list['result'][0]['data']['buyTwoPri']=$arr[11];
        $list['result'][0]['data']['buyThree']=$arr[14];
        $list['result'][0]['data']['buyThreePri']=$arr[13];
        $list['result'][0]['data']['buyFour']=$arr[16];
        $list['result'][0]['data']['buyFourPri']=$arr[15];
        $list['result'][0]['data']['buyFive']=$arr[18];
        $list['result'][0]['data']['buyFivePri']=$arr[17];
        $list['result'][0]['data']['sellOne']=$arr[20];
        $list['result'][0]['data']['sellOnePri']=$arr[19];
        $list['result'][0]['data']['sellTwo']=$arr[22];
        $list['result'][0]['data']['sellTwoPri']=$arr[21];
        $list['result'][0]['data']['sellThree']=$arr[24];
        $list['result'][0]['data']['sellThreePri']=$arr[23];
        $list['result'][0]['data']['sellFour']=$arr[26];
        $list['result'][0]['data']['sellFourPri']=$arr[25];
        $list['result'][0]['data']['sellFive']=$arr[28];
        $list['result'][0]['data']['sellFivePri']=$arr[27];
        $list['result'][0]['data']['date']=substr($arr[30],0,4).'-'.substr($arr[30],4,2).'-'.substr($arr[30],6,2);
        $list['result'][0]['data']['time']=substr($arr[30],8,2).':'.substr($arr[30],10,2).':'.substr($arr[30],12,2);

//        $url = 'http://web.juhe.cn:8080/finance/stock/hs';
//        $data['key'] = '7fdd610575a30a240bba1b9cd7529896';
//        $data['gid'] = input('gid');
//        $res = https_request($url, '', $data);
//        $stock = json_decode($res, true);
        return json($list, 1, '查询成功');
    }
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  

    //获取深圳股票数据列表
    public function szstock($page = 1, $type = 4) {
        $url = 'http://web.juhe.cn:8080/finance/stock/szall';
        $data['key'] = '7fdd610575a30a240bba1b9cd7529896';
        $data['page'] = $page;
        $data['type'] = $type;
        $res = https_request($url, '', $data);
        $list = json_decode($res, true);

        // return json($list, 1, '查询成功');
        return $list;
    }

    //获取上海股票数据列表
    public function shstock($page = 1, $type = 4) {
        $url = 'http://web.juhe.cn:8080/finance/stock/shall';
        $data['key'] = '7fdd610575a30a240bba1b9cd7529896';
        $data['page'] = $page;
        $data['type'] = $type;
        $res = https_request($url, '', $data);
        $list = json_decode($res, true);
        return $list;
    }

    //获取香港股票数据列表
    public function hkstock($page = 1, $type = 4) {
        $url = 'http://web.juhe.cn:8080/finance/stock/shall';
        $data['key'] = '7fdd610575a30a240bba1b9cd7529896';
        $data['page'] = $page;
        $data['type'] = $type;
        $res = https_request($url, '', $data);
        $list = json_decode($res, true);
        return $list;
    }


    //股票实时分时图
    // public function Tstock() {
    //     $url = 'http://route.showapi.com/131-49';
    //     $data['showapi_appid'] = '60189';
    //     $data['showapi_sign'] = "45667606118447cb83eaed229be03570";
    //     $data['code'] = substr(input('code'),2);
    //     $res = https_request($url, '', $data);
    //     $stock = json_decode($res, true);
    //     return json($stock, 1, '查询成功');
    // }

    //股票实时k线图
    public function Kstock() {
        $url = 'http://route.showapi.com/131-50';
        $data['showapi_appid'] = '60189';
        $data['showapi_sign'] = "45667606118447cb83eaed229be03570";
        $data['code'] = substr('sz000001',2);
        $data['time'] = 'day';
        $data['beginDay'] = date('Ym',time()).'01';
        $res = https_request($url, '', $data);
        $stock = json_decode($res, true);
        return json($stock, 1, '查询成功');
    }

    //修改昵称
    public function nickname() {
        $info = Request::instance()->param();
        $data['nick_name'] = $info['nickname'];
        $res = Db::name('admin')->where(['account' => $info['username']])->update($data);
        cookie('nick_name',$info['nickname']);
        // if ($res == 1) {
            return json('', 1, '修改成功');
        // } else {
        //     return json('', 0, '修改失败');
        // }
    }


    /*
    2018/8/21 周二
    */



    //首页banner图
    public function bannerList() {
        $list=Db::name('banner')->where(['is_del' => 1])->select();
        return json($list, 1, '查询成功');
    }
    //新闻列表页，传分类id
    public function newsCateList() {
        $info = Request::instance()->param();
        $list = Db::name('news')->where(['cid' => $info['Cid'],'is_del'=>1])->order('create_time desc')->select();
        foreach($list as $k => $v){
            $list[$k]['create_time'] = date('Y-m-d', $v['create_time']);
            $arr=explode(',',substr($v['picurl'],0,-1));
            $list[$k]['picurl']=[];
            foreach($arr as $kk=>$vv){
                $list[$k]['picurl'][$kk]=$vv;
            }
            $list[$k]['content']=str_replace('"','\'',htmlspecialchars($v['content'],ENT_NOQUOTES));
            $list[$k]['is_new']='';
            if($k<=4){
                $list[$k]['is_new']=1;
            }else{
                $list[$k]['is_new']=2;
            }
        }
        return json($list, 1, '查询成功');
    }

    //新闻分类
    public function newsCategory() {
        $category = Db::name('news_category')->where(['cid'=>['neq',6]])->select();
        return json($category, 1, '查询成功');
    }

    //新闻详情页
    public function newsdetail() {
        //浏览次数加1
        Db::name('news')->where(['id' => input('id')])->setInc('times');
        $info = Db::name('news')->where(['id' => input('id')])->find();
        $info['create_time'] = date('Y-m-d H:i:s', $info['create_time']);
        $this->assign('news_info',$info);
        return $this->fetch();
    }

    //快讯
    public function fastMessage() {

    }

    //首页公告
    public function newsNotice() {
        $info = Db::name('news')->field('id,title')->where(['is_del'=>1,'cid'=>6])->select();
        return json($info, 1, '查询成功');
    }

    //首页新闻列表
    public function newsIndex() {
        $info = Db::name('news')->where(['is_recommend' => 1,'is_del'=>1,'cid'=>['neq',6]])->order('create_time desc')->select();
        foreach ($info as $k => $v) {
            $info[$k]['create_time'] = date('Y-m-d', $v['create_time']);
            $arr=explode(',',substr($v['picurl'],0,-1));
            $info[$k]['picurl']=[];
            foreach($arr as $kk=>$vv){
                $info[$k]['picurl'][$kk]=$vv;
            }
            $info[$k]['content']=str_replace('"','\'',htmlspecialchars($v['content'],ENT_NOQUOTES));
        }
        return json($info, 1, '查询成功');
    }

    //创建策略
    public function strategy() {
        //工作日对应结果为 0, 休息日对应结果为 1, 节假日对应的结果为 2；
        $url = 'http://tool.bitefu.net/jiari/';
        $data['d'] = date('Ymd', time());
        $res = https_request($url, '', $data);
        if ($res == 0 && ((date('Hi', time()) >= 930 && date('Hi', time()) <= 1130) || (date('Hi', time()) >= 1300 && date('Hi', time()) <= 1450))) {
            echo '交易时间可以提交';
        } else {
            echo '非交易时间禁止提交';
        }



    }
    //查看银行卡
    public function bankindex() {
        $info = Request::instance()->param();

        $user_id=$info['account'];
        $list=bindingbank($user_id);
        if($list['ret_code']==8901){
            //没有银行卡被绑定
            return json('', 0, '银行卡暂无');
        }
        if($list['ret_code']==0000){
            //查询成功
            foreach($list['agreement_list'] as $k=>$v){

            }
        }


        $bankcart = Db::name('bankcart')->where(['admin_id' => $info['admin_id']])->select();
        if ($bankcart) {
            return json($bankcart, 1, '查询成功');
        } else {
            return json('', 0, '查询失败');
        }
    }

    //查看我的推广
    public function extendIndex() {
        $info = Request::instance()->param();
        $bankcart = Db::name('extend')
            ->alias('ex')
            ->field('ad.account,ad.balance,ad.tactics_balance,ad.card_id,ad.nick_name,ad.true_name,ad.is_del,ex.id,ex.create_time')
            ->join('link_admin ad', 'ad.id=ex.extend_id')
            ->where(['ex.admin_id' => $info['admin_id']])
            ->select();
        if ($bankcart) {
            return json($bankcart, 1, '查询成功');
        } else {
            return json('', 0, '查询失败');
        }
    }

    
    
 





    //转入--从钱包转入策略余额
    public function transferin(){
        $money = input('money');
        $account = $_COOKIE['username'];
        $trade_status = 1;//交易状态
        $create_time = time();//交易时间
        $trade_type = "转入策略余额";//交易类型
        $balance = Db::name('admin')->where('account',$account)->value('balance');
      	
      	if($balance<$money){
        	return json('', 1100, '钱包余额不足');      
        }
           
        $xbalance = $balance-$money;//每次转入后的剩余金钱
        //减少账户余额，增加策略余额
        Db::startTrans();
        $res = Db::name('admin')->where('account',$account)->setDec('balance',$money);
        $res2 = Db::name('admin')->where('account',$account)->setInc('tactics_balance',$money);
        //添加交易记录
        $data = [
            'account' => $account,
            'charge_num' => date('YmdHis').rand(1000,9999),
            'trade_status' => $trade_status,
            'create_time'=>$create_time,
            'trade_type'=>$trade_type,
            'xbalance'=>$xbalance,
            'trade_price'=>($money)
        ];
        $res3 = Db::name('trade')->insert($data);
        $admin = Db::name('admin')->where('account',$account)->find();
        if(!$res || !$res2 || !$res3){
            Db::rollback();
            return json('', 0, '转入失败');
        }
        Db::commit();
        return json($admin, 1, '转入成功');

    }

    //转出--从策略余额转出钱包
    public function transferout(){
        $money = input('money');
        $account = $_COOKIE['username'];
        $trade_status = 1;//交易状态 已交易
        $create_time = time();//交易时间
        $trade_type = "转入钱包";//交易类型
      	$tactics_balance = Db::name('admin')->where('account',$account)->value('tactics_balance');
      	if($tactics_balance<$money){
        	return json('', 1100, '策略余额不足'); 
        }            
        $balance = Db::name('admin')->where('account',$account)->value('balance');
        $xbalance = $balance+$money;//每次转出后的剩余金钱
        //增加账户余额，减少策略余额
        Db::startTrans();
        $res = Db::name('admin')->where('account',$account)->setInc('balance',$money);
        $res2 = Db::name('admin')->where('account',$account)->setDec('tactics_balance',$money);
        //添加交易记录
        $data = [
            'account' => $account,
            'charge_num' => date('YmdHis').rand(1000,9999),
            'trade_status' => $trade_status,
            'create_time'=>$create_time,
            'trade_type'=>$trade_type,
            'xbalance'=>$xbalance,
            'trade_price'=>$money
        ];
        $res3 = Db::name('trade')->insert($data);
        $admin = Db::name('admin')->where('account',$account)->find();
        if(!$res || !$res2 || !$res3){
            Db::rollback();
            return json('', 0, '转入失败');
        }
        Db::commit();
        return json($admin, 1, '转入成功');
    }
  
    

    public function qq(){
        ignore_user_abort();//关闭浏览器后，继续执行php代码
        set_time_limit(0);//程序执行时间无限制
        $sleep_time = 5;//多长时间执行一次
        $switch = config('switch');
        while($switch){
            $switch = config('switch');
            $msg=date("Y-m-d H:i:s").$switch;
            file_put_contents("log.log",$msg.'<br>',FILE_APPEND);//记录日志
            sleep($sleep_time);//等待时间，进行下一次操作。
        }
        exit();
    }


	//用户提现（商户付款）是否允许
    public function isExtractMoney() {
      	return json('', 1, '可以提现');
      	
        $data['account']=input('account');
        $data['trade_type']='提现';
        $data['create_time']=['egt',strtotime(date('Y-m-d'))];
        $res=Db::name("trade")->where($data)->find();
        if($res){
            return json('', 0, '今天已经提现过了');
        }
        return json('', 1, '可以提现');
    }
    //用户提现（商户付款）
    public function extractMoney() {

        $info = Request::instance()->param();
        $info['account']=$_COOKIE['username'];
        $info['service_charge']=Db::name("hide")->where('id',10)->value('value');
        $admin=Db::name('admin')->where(['account' => $info['account'],'is_del'=>1])->find();
        if($admin['trade_pwd']!=md5($info['trade_pwd'])){
            return json('', 0, '交易密码不正确');
        }
        if(($info['money']+$info['service_charge'])>$admin['balance']){
            return json('', 0, '提现金额超过余额');
        }

        $time=date('YmdHis', time());
        Db::startTrans();

        //创建提现手续费订单
        $para2=[
            'account'=>$info['account'],
            'charge_num'=>$time.rand(1000,9999),
            'trade_price'=>$info['service_charge'],
            'service_charge'=>0,
            'trade_type'=>'提现手续费',
            'trade_status'=>5,
            'sort'=>0,
            'is_del'=>1,
            'create_time'=>time(),
        ];
        $result3=Db::name('trade')->insert($para2);

        //创建提现订单
        $para=[
            'account'=>$info['account'],
            'charge_num'=>$time.rand(1000,9999),
            'trade_price'=>$info['money'],
            'service_charge'=>$info['service_charge'],
            'trade_type'=>'提现',
            'trade_status'=>5,
            'sort'=>0,
            'detailed'=>$para2['charge_num'],
            'is_del'=>1,
            'create_time'=>time(),
        ];
        $result1=Db::name('trade')->insert($para);

        //先进行扣钱，同意后在进行真实打钱，拒绝后钱再加回来
        $result2=Db::name('admin')->where(['account'=>$info['account'],'is_del'=>1])->update(['balance'=>$admin['balance']-$info['money']-$info['service_charge']]);

        if(!$result1 || !$result2 || !$result3){
            Db::rollback();
            return json('', 0, '提现发起失败');
        }
        Db::commit();
        file_put_contents("tixian.txt", $time.'      '.$info['account'].'提现了'.$info['money']."，余额由".$admin['balance']."变成了".
            ($admin['balance']-$info['money']-$info['service_charge']).'，其中手续费为'.
            $info['service_charge'].'，提现金额为'.$info['money']."\n", FILE_APPEND);
        return json('', 1, '提现发起成功,系统将在24小时内为您审核');
    }
    //用户提现（商家付款）之后的回调
    public function extractCallback(){
        import('webllpay.lib.llpay_notify', EXTEND_PATH,'.class.php');
        //计算得出通知验证结果
        $llpayNotify = new \LLpayNotify(config('llpay'));
        $llpayNotify->verifyNotify();
        if ($llpayNotify->result) { //验证成功
            //获取连连支付的通知返回参数，可参考技术文档中服务器异步通知参数列表
            $no_order = $llpayNotify->notifyResp['no_order'];//商户订单号
            $oid_paybill = $llpayNotify->notifyResp['oid_paybill'];//连连支付单号
            $result_pay = $llpayNotify->notifyResp['result_pay'];//支付结果，SUCCESS：为支付成功
            $money_order = $llpayNotify->notifyResp['money_order'];// 支付金额
            if($result_pay == "SUCCESS"){

                //修改提现订单交易状态
                Db::name('trade')->where(['charge_num'=>$no_order])->update(['trade_status'=>1]);
                
              	
                //请在这里加上商户的业务逻辑程序代(更新订单状态、入账业务)
                //——请根据您的业务逻辑来编写程序——
                //payAfter($llpayNotify->notifyResp);
            }
            file_put_contents("log.txt", "异步通知 验证成功\n", FILE_APPEND);
            die("{'ret_code':'0000','ret_msg':'交易成功'}"); //请不要修改或删除
            /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        } else {
            file_put_contents("log.txt", "异步通知 验证失败\n", FILE_APPEND);
            //验证失败
            die("{'ret_code':'9999','ret_msg':'验签失败'}");
            //调试用，写文本函数记录程序运行情况是否正常
            //logResult("这里写入想要调试的代码变量值，或其他运行的结果记录");
        }
    }
	
  
  	//分页公用函数 拆分数组    数组  一页数量  第几页
    public function fycommon($arr,$count,$page){
        $start = $count*$page-$count;
        return array_slice($arr,$start,$count);
    }
  
  
  
  
  	//最牛金顾
    public function mosthigh() {        
        
        $page=1;//默认为第一页

        //一页数量 先设为10条
        $count = 8;        
       // $month['sell_time']=['egt',strtotime(date('Y-m'))];   
      	$month['sell_time']=['between',[strtotime(date('Y-m'))-30*24*3600,strtotime(date('Y-m'))]];
        $monthdata = $this->ranking($month);
        //限制8条
        $monthdata = $this->fycommon($monthdata,$count,$page); 
      	if(!$monthdata){
           return json($monthdata, 0, '暂无数据');
        }          	
        return json($monthdata, 1, '查询成功');
        
    }

	 //排行榜列表
    public function rankingList() {
        $id=input('id');//1为月收益排行榜 2为周收益旁行榜 3为日收益排行榜 4为总收益排行榜 
        if($id==''){
            $id=1;//默认为月收益排行
        }

        $page = input('page');//第几页
        if($page==''){
            $page=1;//默认为第一页
        }

        //一页数量 先设为10条
        $count = 10;

      
      	$day['sell_time']=['between',[strtotime(date('Y-m-d'))-24*3600,strtotime(date('Y-m-d'))]];
		$week['sell_time']=['between',[strtotime(date('Y-m-d'))-(date('N')-1)*24*3600-7*24*3600,strtotime(date('Y-m-d'))-(date('N')-1)*24*3600]];
		$month['sell_time']=['between',[strtotime(date('Y-m'))-30*24*3600,strtotime(date('Y-m'))]];

        if($id==1){
            $monthdata = $this->ranking($month);
            foreach($monthdata as $k => $v){
                $monthdata[$k]['type'] = $id;
                //所有有多少条
                $monthdata[$k]['totalRows'] = count($monthdata);
                //当前页
                $monthdata[$k]['currentPage'] = $page;
                //一页数量
                $monthdata[$k]['numPerPage'] = $count;
                //总页数 向上取整
                $monthdata[$k]['totalPages'] = ceil(count($monthdata)/$count);            
            }

            $monthdata = $this->fycommon($monthdata,$count,$page);
            if(!$monthdata){
            	return json($monthdata, 0, '暂无数据');
            }          	
            return json($monthdata, 1, '查询成功');
        }


        if($id==2){
            $weekdata = $this->ranking($week);
            foreach($weekdata as $k => $v){
                $weekdata[$k]['type'] = $id;
                //所有有多少条
                $weekdata[$k]['totalRows'] = count($weekdata);
                //当前页
                $weekdata[$k]['currentPage'] = $page;
                //一页数量
                $weekdata[$k]['numPerPage'] = $count;
                //总页数 向上取整
                $weekdata[$k]['totalPages'] = ceil(count($weekdata)/$count);  
            }   
            $weekdata = $this->fycommon($weekdata,$count,$page);
          	if(!$weekdata){
            	return json($weekdata, 0, '暂无数据');
            }     
            return json($weekdata, 1, '查询成功');
        }
        if($id==3){
            $daydata = $this->ranking($day);
            foreach($daydata as $k => $v){
                $daydata[$k]['type'] = $id;
                //所有有多少条
                $daydata[$k]['totalRows'] = count($daydata);
                //当前页
                $daydata[$k]['currentPage'] = $page;
                //一页数量
                $daydata[$k]['numPerPage'] = $count;
                //总页数 向上取整
                $daydata[$k]['totalPages'] = ceil(count($daydata)/$count);  
            }            
            $daydata = $this->fycommon($daydata,$count,$page);
          	if(!$daydata){
            	return json($daydata, 0, '暂无数据');
            }     
            return json($daydata, 1, '查询成功');
        }
        if($id==4){
            $alldata = $this->ranking();
            foreach($alldata as $k => $v){
                $alldata[$k]['type'] = $id;
                //所有有多少条
                $alldata[$k]['totalRows'] = count($alldata);
                //当前页
                $alldata[$k]['currentPage'] = $page;
                //一页数量
                $alldata[$k]['numPerPage'] = $count;
                //总页数 向上取整
                $alldata[$k]['totalPages'] = ceil(count($alldata)/$count);   
            }  
            $alldata = $this->fycommon($alldata,$count,$page);
          	if(!$alldata){
            	return json($alldata, 0, '暂无数据');
            }     
            return json($alldata, 1, '查询成功');
        }
      
      
      	if($id=5){
        	$res = $this->winnersList();
          	 return json($res, 1, '查询成功');
        }
      
      	if($id=6){
        	$res = $this->winnersList();
          	 return json($res, 1, '查询成功');
        }
      
      	if($id=7){
        	$res = $this->winnersList();
          	 return json($res, 1, '查询成功');
        }
      
      
      
      
      
      
      
      
      
    }
  
  
	 //我的排名
     public function myranking() {
        $info = Request::instance()->param();
        $list=$this->ranking();

        foreach($list as $k=>$v){
            if(($v['subscribe_account']==$info['subscribe_account'])&&($v['trues']==$info['trues'])){
                return json($k+1,1,'排名查询成功');
            }
        }
       
     }
  
  
  
  
     //获取排行榜的人员，策略收益
    public function ranking($where=[]) {
        $where['status']=['in',[3,4]];
        //会员
        //判断用户实盘信息是否开放，真实的可以控制，炒股大赛的全部显示，不能控制
        $admin=Db::name('admin')->where(['is_del'=>1,'is_open_ranking'=>1])->select();
        
        //真实策略
        $strategy=Db::name('strategy')->where($where)->select();
        $list=[];
        $arr=[];
        //获取到一级为会员，二级为他的策略的多维数组
        foreach($admin as $k=>$v){
            foreach($strategy as $kk=>$vv){
                if($v['account']==$vv['account']){
                    $list[$k][$kk]=$vv;
                    //subscribe_account为获得详情的参数，为了与我的订阅中跳转参数一致
                    $list[$k][$kk]['subscribe_account']=$vv['account'];
                    //用户头像
                    $list[$k][$kk]['headurl']=$v['headurl'];
                    $list[$k][$kk]['jycount']=Db::name('strategy')->where($where)->where('account',$v['account'])->count();
                    $list[$k][$kk]['account']=$v['account'];
                    //人气
                    $list[$k][$kk]['renqi']=Db::name('subscribe')->where(['subscribe_account'=>$v['account'],'is_del'=>1])->count();
                  	//会员id
                  	$list[$k][$kk]['id']=$v['id'];
                    //最近交易时间
                    $time=Db::query('SELECT
                        max(link_strategy.buy_time) as a,
                        max(link_strategy.sell_time) as b
                        FROM
                        link_strategy
                        WHERE
                        link_strategy.account = ?',[$v['account']]);
                    $op[0]=$time[0]['a'];
                    $op[1]=$time[0]['b'];
                    $recently = max($op);//最近发布时间 时间戳格式
                    $chazhi = time()-$recently;
                    if($chazhi<(24*3600)){
                        $list[$k][$kk]['recently'] = date('H:i:s',$recently); 
                    }else{
                        $days = floor($chazhi/(24*3600));//天数向下取整
                        $list[$k][$kk]['recently'] = $days."天前";
                    }

                }
            }
        }
        //真实的策略
        if($list){
            //对数组的里层进行重新排序，改变key
            foreach($list as $k=>$v){
                $list[$k]=array_values($v);
            }
            //获取到收益率和胜率的新数组，真实策略为1，名字为手机号，炒股大赛为2，名字为昵称
            foreach($list as $k=>$v){
                $arr[$k]['trues']='1';
                //subscribe_account为获得详情的参数，为了与我的订阅中跳转参数一致
                $arr[$k]['subscribe_account']=$v[0]['subscribe_account'];
                //用户头像
                $arr[$k]['headurl']=$v[0]['headurl'];
                $arr[$k]['username']=$v[0]['account'];
                //手机号加*
                $arr[$k]['username'] = substr($arr[$k]['username'], 0,3);
                $arr[$k]['username'] = $arr[$k]['username']."********";
                //收益，平台分成后的钱（包括信用金）
                $shouyi=0;
                //信用金（交易本金）
                $credit=0;
                //盈利次数
                $win=0;
                foreach($v as $kk=>$vv){
                    $credit+=$vv['credit'];
                    $shouyi+=($vv['true_getmoney']-$vv['credit']);
                    //计算胜率
                    if($vv['true_getmoney']>$vv['credit']){$win+=1;}
                }
                //收益率
                $arr[$k]['profit']=(round($shouyi/$credit*100,2));
                //胜率
                $arr[$k]['win']=(round($win/count($v)*100,2)).'%';
                //真实股票交易次数
                $arr[$k]['jycount'] = $v[0]['jycount']."次";
                //查看持仓的账号 在前端页面隐藏
                $arr[$k]['account'] = $v[0]['account']; 
                //人气
                $arr[$k]['renqi'] = $v[0]['renqi'];
                //最近发布时间
                $arr[$k]['recently'] = $v[0]['recently'];  
              	//id
              	$arr[$k]['id'] = $v[0]['id'];  
            }
        }

        //炒股大赛策略
        $strategy_contest=Db::name('strategy_contest')->where($where)->select();
        $list_contest=[];
        $arr_contest=[];
        //获取到一级为会员，二级为他的策略的多维数组
        foreach($admin as $k=>$v){
            foreach($strategy_contest as $kk=>$vv){
                if($v['account']==$vv['account']){
                    $list_contest[$k][$kk]=$vv;
                    $list_contest[$k][$kk]['nick_name']=$v['nick_name'];
                    //subscribe_account为获得详情的参数，为了与我的订阅中跳转参数一致
                    $list_contest[$k][$kk]['subscribe_account']=$vv['account'];
                    //用户头像
                    $list_contest[$k][$kk]['headurl']=$v['headurl'];
                    $list_contest[$k][$kk]['jycount']=Db::name('strategy_contest')->where($where)->where('account',$v['account'])->count();
                    $list_contest[$k][$kk]['account']=$v['account'];
                    //人气
                    $list_contest[$k][$kk]['renqi']=Db::name('subscribe')->where(['subscribe_account'=>$v['account'],'is_del'=>1])->count();
                  	//会员id
                  	$list_contest[$k][$kk]['id']=$v['id'];
                    //最近交易时间
                    $time=Db::query('SELECT
                        max(link_strategy_contest.buy_time) as a,
                        max(link_strategy_contest.sell_time) as b
                        FROM
                        link_strategy_contest
                        WHERE
                        link_strategy_contest.account = ?',[$v['account']]);
                    $op[0]=$time[0]['a'];
                    $op[1]=$time[0]['b'];
                    $recently = max($op);//最近发布时间 时间戳格式
                    $chazhi = time()-$recently;
                    if($chazhi<(24*3600)){
                        $list_contest[$k][$kk]['recently'] = date('H:i:s',$recently); 
                    }else{
                        $days = floor($chazhi/(24*3600));//天数向下取整
                        $list_contest[$k][$kk]['recently'] = $days."天前";
                    }

                }
            }
        }

        //炒股大赛的策略
        if($list_contest){
            //对数组的里层进行重新排序，改变key
            foreach($list_contest as $k=>$v){
                $list_contest[$k]=array_values($v);
            }
            //获取到收益率和胜率的新数组，真实策略为1，名字为手机号，炒股大赛为2，名字为昵称(没有的话为手机号)
            foreach($list_contest as $k=>$v){
                $arr_contest[$k]['trues']='2';
                //subscribe_account为获得详情的参数，为了与我的订阅中跳转参数一致
                $arr_contest[$k]['subscribe_account']=$v[0]['subscribe_account'];
                //用户头像
                $arr_contest[$k]['headurl']=$v[0]['headurl'];
                if($v[0]['nick_name']!=$v[0]['account']){
                    $arr_contest[$k]['username']=$v[0]['nick_name'];
                }else{
                    $arr_contest[$k]['username']=$v[0]['account'];
                    //手机号加*
                    $arr_contest[$k]['username'] = substr($arr_contest[$k]['username'], 0,3);
                    $arr_contest[$k]['username'] = $arr_contest[$k]['username']."********";
                }
                //收益，(卖出价-买入价)*股数
                $shouyi=0;
                //信用金
                $credit=0;
                //盈利次数
                $win=0;
                foreach($v as $kk=>$vv){
                    $credit+=$vv['credit'];
                    $shouyi+=($vv['sell_price']-$vv['buy_price'])*$vv['stock_number'];
                    //计算胜率
                    if($vv['sell_price']>$vv['buy_price']){$win+=1;}
                }
                //收益率
                $arr_contest[$k]['profit']=(round($shouyi/$credit*100,2));
                //胜率
                $arr_contest[$k]['win']=(round($win/count($v)*100,2)).'%';
                //模拟股票交易次数
                $arr_contest[$k]['jycount'] = $v[0]['jycount']."次";
                //查看持仓的账号 在前端页面隐藏
                $arr_contest[$k]['account'] = $v[0]['account'];
                //人气
                $arr_contest[$k]['renqi'] = $v[0]['renqi'];
                //最近发布时间
                $arr_contest[$k]['recently'] = $v[0]['recently'];
              	//id
              	$arr_contest[$k]['id'] = $v[0]['id'];
            }
        }
      
      	//数组合并
        $para=array_merge($arr,$arr_contest);

        //排序
        array_multisort(array_column($para,'profit'),SORT_DESC,$para);

        //收益率加上%
      foreach ($para as $k => $v) {
        if($v['profit']<0){
          unset($para[$k]);
        }else{
          //$para[$k]['id']=$k+1;
          $para[$k]['profit']=$v['profit'].'%';
        }
      }

		return $para;
    }
  
   
  	//查看其余人的战绩
    public function subscribeInfo() {
        $day['sell_time']=['egt',strtotime(date('Y-m-d'))];
        $week['sell_time']=['egt',strtotime(date('Y-m-d'))-(date('N')-1)*24*3600];
        $month['sell_time']=['egt',strtotime(date('Y-m'))];

        $info = Request::instance()->param();
        //订阅的人的策略  true为1查询真实策略，2查询炒股大赛
        if($info['trues']==1){
            $list['day']=$this->shouyiInfo($info['subscribe_account'],$day);
            $list['week']=$this->shouyiInfo($info['subscribe_account'],$week);
            $list['month']=$this->shouyiInfo($info['subscribe_account'],$month);
            $list['all']=$this->shouyiInfo($info['subscribe_account']);
        }else{
            $list['day']=$this->shouyiInfoContest($info['subscribe_account'],$day);
            $list['week']=$this->shouyiInfoContest($info['subscribe_account'],$week);
            $list['month']=$this->shouyiInfoContest($info['subscribe_account'],$month);
            $list['all']=$this->shouyiInfoContest($info['subscribe_account']);
        }

        //总排名
        $allranking=$this->ranking();
        foreach($allranking as $k=>$v){
            if(($v['subscribe_account']==$info['subscribe_account'])&&($v['trues']==$info['trues'])){
                $list['allranking']=$k+1;
                //交易次数
                $list['jycount']=$v['jycount'];
            }
        }
                   
        //月排名
        $monthranking=$this->ranking($month);
        foreach($monthranking as $k=>$v){
            if(($v['subscribe_account']==$info['subscribe_account'])&&($v['trues']==$info['trues'])){
                $list['monthranking']=$k+1;
            }
        }

        //周排名
        $weekranking=$this->ranking($week);
        foreach($weekranking as $k=>$v){
            if(($v['subscribe_account']==$info['subscribe_account'])&&($v['trues']==$info['trues'])){
                $list['weekranking']=$k+1;
            }
        }

        //总资产
        if($info['trues']==1){            
            $balance=Db::name('admin')->where('account',$info['subscribe_account'])->value('balance');//账户余额
            $tactics_balance=Db::name('admin')->where('account',$info['subscribe_account'])->value('tactics_balance');//策略账户余额
            $list['allmoney']=$balance+$tactics_balance;
        }else{
            $list['allmoney']=Db::name('admin')->where('account',$info['subscribe_account'])->value('analog_money');
        }


        //最近发布时间
        if($info['trues']==1){            
            $time=Db::query('SELECT
                max(link_strategy.buy_time) as a,
                max(link_strategy.sell_time) as b
                FROM
                link_strategy
                WHERE
                link_strategy.account = ?',[$info['subscribe_account']]);
            $op[0]=$time[0]['a'];
            $op[1]=$time[0]['b'];
            $recently = max($op);//最近发布时间 时间戳格式
            $chazhi = time()-$recently;
            if($chazhi<(24*3600)){
                $list['recently'] = date('H:i:s',$recently); 
            }else{
                $days = floor($chazhi/(24*3600));//天数向下取整
                $list['recently'] = $days."天前";
            }
            
        }else{
            $time=Db::query('SELECT
                max(link_strategy_contest.buy_time) as a,
                max(link_strategy_contest.sell_time) as b
                FROM
                link_strategy_contest
                WHERE
                link_strategy_contest.account = ?',[$info['subscribe_account']]);
            $op[0]=$time[0]['a'];
            $op[1]=$time[0]['b'];
            $recently = max($op);//最近发布时间 时间戳格式
            $chazhi = time()-$recently;
            if($chazhi<(24*3600)){
                $list['recently'] = date('H:i:s',$recently); 
            }else{
                $days = floor($chazhi/(24*3600));//天数向下取整
                $list['recently'] = $days."天前";
            }
        }

        //判断我是否订阅此人
        $subscribe = Db::name('subscribe')
            ->where(['account'=>$info['account'],'subscribe_account' => $info['subscribe_account'],'trues'=>$info['trues']])->order('create_time desc')
            ->find();
        if($subscribe&&$subscribe['is_del']==1){
            $list['is_subscribe']=1;//订阅
        }else{
            $list['is_subscribe']=2;//不订阅
        }
        //被订阅数
        $list['subscribe_sum'] = Db::name('subscribe')->where(['subscribe_account' => $info['subscribe_account'],'is_del'=>1])->count();

        //策略创建的记录
        if($info['trues']==1){
            $store=Db::name('strategy')->where(['account' => $info['subscribe_account'],'status'=>['in',[3,4]]])->select();

            //查看的这个用户的总策略数（持仓中的不计算）
            $storecount=Db::name('strategy')->where(['account' => $info['subscribe_account'],'status'=>['in',[3,4]]])->count();
            //盈利的策略个数
           $sql=Db::query('SELECT
                    count(link_strategy.id) as a
                    FROM
                    link_strategy
                    WHERE
                    link_strategy.`status` BETWEEN 3 AND 4 AND
                    link_strategy.account = ? AND
                    link_strategy.true_getmoney > link_strategy.credit',[$info['subscribe_account']]);
            $storeprofit = $sql[0]['a'];
            //盈利策略
            $list['profitcl'] = ((round(($storeprofit/$storecount),2))*100)."%";
            //亏损策略
            $list['dropcl'] = ((1-(round(($storeprofit/$storecount),2)))*100)."%";

            if($store){
                //用户卖出股票时所能拿到的成数
                $chengshu=Db::name('hide')->where(['id' => 3])->value('value');
                foreach($store as $k=>$v){
                    if($v['true_getmoney']>$v['credit']){
                        //交易盈亏
                        $list['record'][$k]['getprice']=round(($v['true_getmoney']-$v['credit'])/$chengshu,2);
                        //盈利分配
                        $list['record'][$k]['truegetprice']=round($v['true_getmoney']-$v['credit'],2);

                        //亏损赔付
                        $list['record'][$k]['dropprice']='';
                    }else{
                        //交易盈亏
                        $list['record'][$k]['getprice']=round($v['true_getmoney']-$v['credit'],2);
                        //盈利分配
                        $list['record'][$k]['truegetprice']='';
                        //亏损赔付
                        $list['record'][$k]['dropprice']=round($v['true_getmoney']-$v['credit'],2);
                    }
                }                
                //单笔最大盈利=盈利分配/信用金（取最大值）
                //$preprofit = [];
                //foreach ($list['record'] as $k => $v) {
                    //$preprofit[$k] = ((round($list['record'][$k]['truegetprice']/$store[$k]['credit'],2))*100)."%";
                //}
                //$list['preprofit'] = max($preprofit);                
            }else{
                //持仓中的不计算
                $list['record']=[];
            }
        }else{
            $store=Db::name('strategy_contest')->where(['account' => $info['subscribe_account'],'status'=>['in',[3,4]]])->select();

             //查看的这个用户的总策略数（持仓中的不计算）
            $storecount=Db::name('strategy_contest')->where(['account' => $info['subscribe_account'],'status'=>['in',[3,4]]])->count();
            //盈利的策略个数
           $sql=Db::query('SELECT
                    count(link_strategy_contest.id) as a
                    FROM
                    link_strategy_contest
                    WHERE
                    link_strategy_contest.`status` BETWEEN 3 AND 4 AND
                    link_strategy_contest.account = ? AND
                    link_strategy_contest.credit <((sell_price-buy_price)*stock_number)',[$info['subscribe_account']]);
            $storeprofit = $sql[0]['a'];
            //盈利策略
            $list['profitcl'] = ((round(($storeprofit/$storecount),2))*100)."%";
            //亏损策略
            $list['dropcl'] = ((1-(round(($storeprofit/$storecount),2)))*100)."%";

            if($store){
                foreach($store as $k=>$v){
                    if($v['sell_price']>=$v['buy_price']){
                        //交易盈亏
                        $list['record'][$k]['getprice']=($v['sell_price']-$v['buy_price'])*$v['stock_number'];
                        //盈利分配
                        $list['record'][$k]['truegetprice']=($v['sell_price']-$v['buy_price'])*$v['stock_number'];
                        //亏损赔付
                        $list['record'][$k]['dropprice']='';
                    }else{
                        //交易盈亏
                        $list['record'][$k]['getprice']=($v['sell_price']-$v['buy_price'])*$v['stock_number'];
                        //盈利分配
                        $list['record'][$k]['truegetprice']='';
                        //亏损赔付
                        $list['record'][$k]['dropprice']=($v['sell_price']-$v['buy_price'])*$v['stock_number'];
                    }
                }
                //单笔最大盈利=盈利分配/信用金（取最大值）
                //$preprofit = [];
                //foreach ($list['record'] as $k => $v) {
                    //$preprofit[$k] = ((round($list['record'][$k]['truegetprice']/$store[$k]['credit'],2))*100)."%";
                //}
                //$list['preprofit'] = max($preprofit); 
            }else{
                $list['record']=[];
            }
        }
        return json($list, 1, '查询成功');
    }
  	
  
  	    //计算某个会员真实策略里的收益率
    public function shouyiInfo($account,$where=[]) {
        $where['account']=$account;
        $where['status']=['in',[3,4]];
        $store=Db::name('strategy')->where($where)->select();
        if($store){
            //收益，平台分成后的钱（包括信用金）
            $shouyi=0;
            //信用金（交易本金）
            $credit=0;
            foreach($store as $k=>$v){
                $credit+=$v['credit'];
                $shouyi+=($v['true_getmoney']-$v['credit']);
            }
            //收益率
            return (round($shouyi/$credit*100,2)).'%';
        }else{
            return '0%';
        }
    }
   //计算某个会员炒股大赛里的收益率
    public function shouyiInfoContest($account,$where=[]) {
        $where['account']=$account;
        $where['status']=['in',[3,4]];
        $store=Db::name('strategy_contest')->where($where)->select();
        if($store){
            //收益，(卖出价-买入价)*股数
            $shouyi=0;
            //信用金，市值，是一样的
            $credit=0;
            foreach($store as $k=>$v){
                $credit+=$v['credit'];
                $shouyi+=($v['sell_price']-$v['buy_price'])*$v['stock_number'];
            }
            //收益率
            return (round($shouyi/$credit*100,2)).'%';
        }else{
            return '0%';
        }
    }
  
  
  
  
  
  
  	//推送(内部调用)
    public function push($account='',$content='',$push_type=1,$true='') {
        if($true){
            //添加到推送记录表
            $data=[
                'push_type'=>$push_type,
                'account'=>$account,
                'regid'=>'',
                'content'=>$content,
                'status'=>1,
                'is_del'=>1,
                'create_time'=>time(),
            ];
            Db::name('push')->insert($data);
        }else{
            $client = new \JPush\Client(config('push')['app_key'],config('push')['master_secret']);
            $result=$client->push()
                ->setPlatform('all')
                ->addAlias(strval($account))
                ->setNotificationAlert($content)
                ->send();
            //添加到推送记录表
            $data=[
                'push_type'=>$push_type,
                'account'=>$account,
                'regid'=>'',
                'content'=>$content,
                'is_del'=>1,
                'create_time'=>time(),
            ];
            if($result['http_code']==200){
                $data['status']=1;
            }else{
                $data['status']=2;
            }
            Db::name('push')->insert($data);
        }
    }
  
  	//订阅和取消订阅
    public function createSubscribe() {
        $info = Request::instance()->param();
        //订阅
        if($info['is_subscribe']==1){
            $data=[
                'account'=>$info['account'],
                'subscribe_account'=>$info['subscribe_account'],
                'trues'=>$info['trues'],
                'subscribe_nick_name'=>$info['subscribe_nick_name'],
                'is_del'=>1,
                'create_time'=>time(),
            ];
            Db::name('subscribe')->insert($data);
          	//进行订阅推送
            $account=Db::name('settings')
                ->where(['account' => $info['subscribe_account'],'is_del'=>1])
                ->find();
            $content='有一位会员关注了您，请注意查看哦';
            if($account['subscribe_push']==1){
                $this->push($account['account'],$content,4);
            }else{
                $this->push($account['account'],$content,4,1);
            }
            return json('', 1, '订阅成功');
        //取消订阅
        }elseif($info['is_subscribe']==2){
            $where=[
                'account'=>$info['account'],
                'subscribe_account'=>$info['subscribe_account'],
                'trues'=>$info['trues'],
            ];
            Db::name('subscribe')->where($where)->update(['is_del'=>2]);
            return json('', 1, '取消订阅成功');
        }
    }
  
  
  	//查看获奖名单
    public function winnersList() {
        $day['sell_time']=['egt',strtotime(date('Y-m-d'))];
        $week['sell_time']=['egt',strtotime(date('Y-m-d'))-(date('N')-1)*24*3600];
        $month['sell_time']=['egt',strtotime(date('Y-m'))];      
        $list[0]['time']='day';
        $list[0]['list']=Db::name('winlist_day')
            ->alias('day')
            ->field('day.*,ad.headurl')
            ->join('link_admin ad','ad.account=day.subscribe_account')
            ->where(['ad.is_del'=>1])
            ->order('day.create_time desc')
            ->select();


        //计算收益率
        foreach ($list[0]['list'] as $k => $v) {
            //如果是真实策略
            if($list[0]['list'][$k]['trues']==1){
                $list[0]['list'][$k]['shouyi'] = $this->shouyiInfo($list[0]['list'][$k]['subscribe_account'],$day);
            }else{
                //如果是模拟策略
                $list[0]['list'][$k]['shouyi'] = $this->shouyiInfoContest($list[0]['list'][$k]['subscribe_account'],$day);
            }
        }  


        $list[1]['time']='week';
        $list[1]['list']=Db::name('winlist_week')
            ->alias('week')
            ->field('week.*,ad.headurl')
            ->join('link_admin ad','ad.account=week.subscribe_account')
            ->where(['ad.is_del'=>1])
            ->order('week.create_time desc')
            ->select();

        //计算收益率
        foreach ($list[1]['list'] as $k => $v) {
            //如果是真实策略
            if($list[1]['list'][$k]['trues']==1){
                $list[1]['list'][$k]['shouyi'] = $this->shouyiInfo($list[1]['list'][$k]['subscribe_account'],$week);
            }else{
                //如果是模拟策略
                $list[1]['list'][$k]['shouyi'] = $this->shouyiInfoContest($list[1]['list'][$k]['subscribe_account'],$week);
            }
        }       


        $list[2]['time']='month';
        $monthdate=Db::name('winlist_month')
            ->alias('month')
            ->field('month.*,ad.headurl')
            ->join('link_admin ad','ad.account=month.subscribe_account')
            ->where(['ad.is_del'=>1])
            ->order('month.create_time desc')
            ->select();
        if($monthdate){

            //把每个月的前三名放在一起
            foreach($monthdate as $k=>$v){
                $list[2]['list'][floor($k/3)]['list']['month']=$v['time'];
                $list[2]['list'][floor($k/3)]['list']['list'][]=$v;

				//计算收益率
                foreach ($list[2]['list'][floor($k/3)]['list']['list'] as $kk => $v) {
                    //如果是真实策略
                    if($list[2]['list'][floor($k/3)]['list']['list'][$kk]['trues']==1){
                        $list[2]['list'][floor($k/3)]['list']['list'][$kk]['shouyi'] = $this->shouyiInfo($list[2]['list'][floor($k/3)]['list']['list'][$kk]['subscribe_account'],$month);
                    }else{
                        //如果是模拟策略
                        $list[2]['list'][floor($k/3)]['list']['list'][$kk]['shouyi'] = $this->shouyiInfoContest($list[2]['list'][floor($k/3)]['list']['list'][$kk]['subscribe_account'],$month);
                    }
                }
               


            
            }
            //把每个月的前三名进行排序
            foreach($list[2]['list'] as $k=>$v){
                array_multisort(array_column($v['list']['list'],'ranking'),SORT_ASC,$v['list']['list']);
                $list[2]['list'][$k]=$v;
            }

          
        }else{
            $list[2]['list']=[];
        }
        return json($list, 1, '查询成功');
    }
    //添加获奖名单
    public function createWinners() {
       $day['sell_time']=['egt',strtotime(date('Y-m-d'))];
        $week['sell_time']=['egt',strtotime(date('Y-m-d'))-(date('N')-1)*24*3600];
        $month['sell_time']=['egt',strtotime(date('Y-m'))];
        //进行日冠军的添加
        $daywin=$this->ranking($day)[0];
        $daydata=[
            'subscribe_account'=>$daywin['subscribe_account'],
            'trues'=>$daywin['trues'],
            'subscribe_nick_name'=>$daywin['username'],
            'time'=>date('Y-m-d',time()-24*3600),
            'create_time'=>time(),
        ];
        Db::name('winlist_day')->insert($daydata);
        //进行周冠军的添加
        if(date('N')==1){
            $weekwin=$this->ranking($week)[0];
            $weekdata=[
                'subscribe_account'=>$weekwin['subscribe_account'],
                'trues'=>$weekwin['trues'],
                'subscribe_nick_name'=>$weekwin['username'],
                'time'=>date('Y').'-'.(date('W')-1),
                'create_time'=>time(),
            ];
            Db::name('winlist_week')->insert($weekdata);
        }
        //进行月冠军的添加
        if(date('Y-m-d',time())==date('Y-m-1')){
            $monthwin=[
                $this->ranking($month)[0],
                $this->ranking($month)[1],
                $this->ranking($month)[2],
            ];
            foreach($monthwin as $k=>$v){
                $monthdata=[
                    'subscribe_account'=>$v['subscribe_account'],
                    'trues'=>$v['trues'],
                    'ranking'=>$k+1,
                    'subscribe_nick_name'=>$v['username'],
                    'time'=>date('Y').'-'.(date('m')-1),
                    'create_time'=>time(),
                ];
                Db::name('winlist_month')->insert($monthdata);
            }
        }
    }

  
  	//申请递延
    public function changeDefer() {
        $info = Request::instance()->param();
        Db::name('strategy')->where(['id'=>$info['id']])->update(['defer'=>1]);
        return json('', 1, '申请递延成功');
    }
  
  
  	//修改止损
    public function updateLossValue() {
        $info = Request::instance()->param();
        if($info['trues']==1){
            Db::name('strategy')->where(['is_cancel_order'=>1])->where(['id' => $info['id']])->update(['loss_value'=>$info['loss_value']]);
        }else{
            Db::name('strategy_contest')->where(['id' => $info['id']])->update(['loss_value'=>$info['loss_value']]);
        }
        return json('', 1, '修改止损成功');
    }
  

  
  	//策略发起平仓，卖出股票
    public function sellStrategy() {
        //工作日对应结果为 0, 休息日对应结果为 1, 节假日对应的结果为 2；
        $url = 'http://tool.bitefu.net/jiari/';
        $data['d'] = date('Ymd', time());
        $res = https_request($url, '', $data);
        $data=[];
        if ($res == 0 && ((date('Hi', time()) >= 930 && date('Hi', time()) <= 1130) || (date('Hi', time()) >= 1300 && date('Hi', time()) <= 1450))) {     
      		
  
            //T+1买卖的手续费比例
            $poundage=Db::name('hide')->where(['id' => 4])->value('value');
            //T+D买卖的手续费比例
            $bigpoundage=Db::name('hide')->where(['id' => 8])->value('value');
            //用户卖出股票时所能拿到的成数
            $chengshu=Db::name('hide')->where(['id' => 3])->value('value');

            $info = Request::instance()->param();
            //策略信息
            $strategy=Db::name('strategy')->where(['id'=>$info['id']])->find();
          
          	//判断该策略是否卖出
          	if($strategy['status']==3||$strategy['status']==4){
            	 return json('', 1201, '该策略已卖出');            
            }
          
            //实际股票
            $json=$this->getOneStock3($strategy['stock_code']);
            //股票当前价格
            $nowprice=json_decode($json,true)['list']['result'][0]['data']['nowPri'];
          
          	//实盘委托
            if($strategy['is_real_disk']==1){
                $success=file_get_contents('http://127.0.0.1:5588/sell=SC,'.substr($strategy['stock_code'],2).','.$nowprice.','.$strategy['stock_number']);
                $success=explode('，',$success);
                if($success[0]=='委托成功'){
                    $data['sell_contract_num']=explode(':',$success[4])[1];
                }else{
                    return json('', 1300, '委托失败');
                }
            }
                              
            if($strategy['strategy_type']==1){
                //修改策略
                $data=[
                    'status'=>4,
                    'sell_price'=>$nowprice,
                    'sell_poundage'=>$strategy['double_value']*$poundage,
                    'all_poundage'=>$strategy['all_poundage']+$strategy['double_value']*$poundage,
                    'sell_time'=>time(),
                  	'sell_type'=>1,
                ];
            }else{
                //修改策略
                $data=[
                    'status'=>4,
                    'sell_price'=>$nowprice,
                    'sell_poundage'=>$strategy['double_value']*$bigpoundage,
                    'all_poundage'=>$strategy['all_poundage']+$strategy['double_value']*$bigpoundage,
                    'sell_time'=>time(),
                  	'sell_type'=>1,
                ];
            }

      		$get_money=$nowprice*$strategy['stock_number']-$strategy['market_value'];
            if($get_money>0){
                $data['true_getmoney']=$get_money*$chengshu+$strategy['credit']-$data['sell_poundage'];
            }else{
                $data['true_getmoney']=$get_money+$strategy['credit']-$data['sell_poundage'];
            }
      

            Db::startTrans();
            //修改策略记录
            $result1=Db::name('strategy')->where(['id'=>$info['id']])->update($data);
            //修改用户策略余额和冻结金额
            $result2=Db::name('admin')->where(['account' => $strategy['account'],'is_del'=>1])->setInc('tactics_balance',$data['true_getmoney']);
            $result3=Db::name('admin')->where(['account' => $strategy['account'],'is_del'=>1])->setDec('frozen_money',$strategy['credit']);
            //添加交易记录
            $trade=[
                'charge_num'=>$strategy['strategy_num'],
                'trade_price'=>$data['true_getmoney'],
                'account'=>$strategy['account'],
                'trade_type'=>'策略卖出',
                'trade_status'=>1,
                'is_del'=>1,
                'create_time'=>time(),
            ];
            $result4=Db::name('trade')->insert($trade);
            
            if(!$result1 || !$result2 || !$result3 || !$result4){
                Db::rollback();
                return json('', 0, '平仓失败');
            }
            Db::commit();
            return json($strategy['strategy_num'], 1, '平仓成功');
        } else {
            return json('', 1200, '非交易时间禁止平仓');
        }
    }
  
  	//炒股大赛的策略发起平仓，卖出股票
    public function sellStrategyContest() {
        //工作日对应结果为 0, 休息日对应结果为 1, 节假日对应的结果为 2；
        $url = 'http://tool.bitefu.net/jiari/';
        $data['d'] = date('Ymd', time());
        $res = https_request($url, '', $data);
     
		if ($res == 0 && ((date('Hi', time()) >= 930 && date('Hi', time()) <= 1130) || (date('Hi', time()) >= 1300 && date('Hi', time()) <= 1500))) {  
            $info = Request::instance()->param();
            //策略信息
            $strategy=Db::name('strategy_contest')->where(['id'=>$info['id']])->find();
            //实际股票
            $json=$this->getOneStock3($strategy['stock_code']);

            //股票当前价格
            $nowprice=json_decode($json,true)['list']['result'][0]['data']['nowPri'];

            //修改策略
            $data=[
                'status'=>4,
                'sell_price'=>$nowprice,
                'sell_time'=>time(),
              	'sell_type'=>1,
            ];

            Db::startTrans();
            //修改策略记录
            $result1=Db::name('strategy_contest')->where(['id'=>$info['id']])->update($data);
            //修改用户策略余额和冻结金额
            $result2=Db::name('admin')->where(['account' => $strategy['account'],'is_del'=>1])->setInc('analog_money',($nowprice-$strategy['buy_price'])*$strategy['stock_number']+$strategy['credit']);
            $result3=Db::name('admin')->where(['account' => $strategy['account'],'is_del'=>1])->setDec('frozen_analog_money',$strategy['credit']);

            if(!$result1 || !$result2 || !$result3){
                Db::rollback();
                return json('', 0, '平仓失败');
            }
            Db::commit();
            return json('', 1, '平仓成功');
        } else {
            return json('', 1200, '非交易时间禁止平仓');
        }
    }
  
  
  
    //用户反馈新的问题
    public function createFeedback(){
        $info = Request::instance()->param();
        $data=[
            'admin_id'=>$info['account'],
            'content'=>$info['content'],
            'status'=>1,
            'create_time'=>time(),
        ];
        Db::name('feedback')->insert($data);
        return json('', 1, '反馈成功');
    }
  
  
  
  
  
    //股票实时分时图(安卓)
    public function Tstock(){
        $list=[];
        $str=file_get_contents('http://money.finance.sina.com.cn/quotes_service/api/json_v2.php/CN_MarketData.getKLineData?symbol='.input('code').'&scale=5&ma=25&datalen=100');
        $arr=explode(',',substr($str,1,-1));

        if(date('N')==6){
            $date=date('Y-m-d',time()-24*3600);
        }elseif(date('N')==7){
            $date=date('Y-m-d',time()-2*24*3600);
        }else{
            $date=date('Y-m-d');
        }
        foreach($arr as $k=>$v){
            if(strpos($v,$date)){
                $list[]=$arr[$k];
                $list[]=$arr[$k+1];
                $list[]=$arr[$k+2];
                $list[]=$arr[$k+3];
                $list[]=$arr[$k+4];
                $list[]=$arr[$k+5];
            }
        }
        $store=[];
        foreach ($list as $k => $v) {
            if($k%6==0){
                $store[floor($k/6)]['minute']=strtotime(substr($v,6,-1));
            }elseif($k%6==1){
                $store[floor($k/6)]['open']=substr($v,6,-1);
            }elseif($k%6==2){
                $store[floor($k/6)]['max']=substr($v,6,-1);
            }elseif($k%6==3){
                $store[floor($k/6)]['min']=substr($v,5,-1);
            }elseif($k%6==4){
                $store[floor($k/6)]['close']=substr($v,7,-1);
            }elseif($k%6==5){
                $store[floor($k/6)]['volumn']=substr($v,8,-1);
            }
        }

        $json=$this->getOneStock2(input('code'));
        $nowPri=json_decode($json,true)['list']['result'][0]['data']['nowPri'];
        $yestodEndPri=json_decode($json,true)['list']['result'][0]['data']['yestodEndPri'];
        $last['dat']=$yestodEndPri;

        $ststr=[];
        foreach ($store as $k => $v) {
            $ststr[]=[
                strval($v['minute'].'000'),
                strval($v['close']),
                strval($nowPri),
                strval($v['volumn']),
                strval($v['open']),
            ];
        }
        $shuzu=[];
        foreach ($ststr as $k => $v) {
            $shuzu[$k*5]=$v;
            $shuzu[$k*5+1]=$v;
            $shuzu[$k*5+2]=$v;
            $shuzu[$k*5+3]=$v;
            $shuzu[$k*5+4]=$v;
        }

        $last['data']=$shuzu;

        if(date('N')==6){
            $last['date']=date('Ymd',time()-24*3600);
        }elseif(date('N')==7){
            $last['date']=date('Ymd',time()-2*24*3600);
        }else{
            $last['date']=date('Ymd');
        }

        return json($last, 1, '查询成功');
    }

    //添加银行卡
    public function addbankcard() {
        $info = Request::instance()->param();

        $str=file_get_contents('https://ccdcapi.alipay.com/validateAndCacheCardInfo.json?_input_charset=utf-8&cardNo='.$info['bank_num'].'&cardBinCheck=true');
        if(strpos($str,'errorCodes')){
            return json('', 0, '卡号输入有误，请重新输入');
        }

        $banktype=explode('"',explode(':',$str)[2])[1];
        foreach(config('terminal') as $k => $v) {
            if($v['banknum']==$banktype){
                $data['logo']=$v['logo'];
                $data['background']=$v['background'];
                $data['bank_name']=$v['name'];
                $data['bank_code']=$v['banknum'];
            }
        }

        $data['account']=$info['account'];
        $data['bank_num']=$info['bank_num'];
        $data['province']=$info['province'];
        $data['city']=$info['city'];
        $data['branchname']=$info['branchname'];
        $data['card_type']=2;
        $data['bank_require']=1;
        $data['is_del']=1;
        $data['create_time']=time();

        Db::name('bankcart')->insert($data);

        return json('', 1, '查询成功');
    }
  
  
  	//提交银行卡信息
    public function checkBankCard() {
        $info = Request::instance()->param();

        $str=file_get_contents('https://ccdcapi.alipay.com/validateAndCacheCardInfo.json?_input_charset=utf-8&cardNo='.$info['bank_num'].'&cardBinCheck=true');
        if(strpos($str,'errorCodes')){
            return json('', 0, '卡号输入有误，请重新输入');
        }
		$data=[];
        $banktype=explode('"',explode(':',$str)[2])[1];
        foreach(config('terminal') as $k => $v) {
            if($v['banknum']==$banktype){
                $data['logo']=$v['logo'];
                $data['background']=$v['background'];
                $data['bank_name']=$v['name'];
            }
        }
		if(!isset($data['background'])){
            $data['logo']=config('bank_default')['logo'];
            $data['background']=config('bank_default')['background'];
            $data['bank_name']='银行卡';
        }
      	
        $data['account']=$info['account'];
        $data['bank_num']=$info['bank_num'];
      	$data['bank_address']=$info['bank_address'];
        $data['card_type']=2;
        $data['bank_require']=1;
        $data['is_del']=1;
        $data['create_time']=time();

        Db::name('bankcart')->insert($data);
      
      	push($info['account'],'恭喜您，您的银行卡已经绑定成功！',1);
      
        return json('', 1, '提交成功');
    }



  
}
