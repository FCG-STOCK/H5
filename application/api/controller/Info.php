<?php
namespace app\API\controller;
use think\Db;
use think\Request;
use think\Controller;
use think\Validate;
use Stock;
class Info extends controller
{
  
  

  	//获取单个股票数据
    public function getOneStock($gid) {
        $arr=explode('~',iconv("gb2312", "utf-8//IGNORE",file_get_contents('http://qt.gtimg.cn/q='.$gid)));
		return $arr[3];
    }
  	
	

	

	//上传用户头像
    public function uppicture(){
        $info=Request::instance()->param(true);

        $headurl = request()->file('headurl');//头像

      	$h5url=Db::name('hide')->where(['id'=>26])->value('value');
      
        $account = $_COOKIE['username'];
        if($headurl){
            $data['headurl']='';//logo
            foreach($headurl as $file){
                $name1 = $file->getInfo()['name'];
                $num = strpos($name1,'.');
                $name1 = rand(10000000,99999999).substr($name1, $num);
                //中文文件名用此行代码转码
                // $name2=iconv('utf-8','gbk',$name1);
                // 移动到框架应用根目录/public/uploads/ 目录下
                $move = $file->validate(['ext'=>'jpg,jpeg,png'])->rule('date')->move(ROOT_PATH . 'public' . DS . 'uploads' . DS .date('Ymd',time()). DS . 'header',$name1);
                if($move){
                    $myheader = str_replace('\\','/',DS . 'public' . DS . 'uploads' . DS .date('Ymd',time()). DS . 'header' . DS . $name1);
                    //存库
                    $myheader = $h5url.$myheader;
                    $res = Db::name('admin')->where('account',$account)->update(['headurl'=>$myheader]);
                    return json($myheader, 1, '头像上传成功');
                }else{
                    // 上传失败获取错误信息
                    // echo $file->getError();
                    return json('', 1100, $file->getError());
                }
            }
        }else{
            return json('', 0, "未获取到文件");
        }
        

    }
  
  
  	
  	//上传身份证正面照
    public function uppicture2(){
        $info=Request::instance()->param(true);     
        $headurl = request()->file('front');//头像
      	$h5url=Db::name('hide')->where(['id'=>26])->value('value'); 
      
        if($headurl){
            $data['headurl']='';//logo
            foreach($headurl as $file){
                // $name1 = $file->getInfo()['name'];
                // $num = strpos($name1,'.');
                // $name1 = rand(10000000,99999999).substr($name1, $num);
                //中文文件名用此行代码转码
                // $name2=iconv('utf-8','gbk',$name1);
                // 移动到框架应用根目录/public/uploads/ 目录下
                $move = $file->validate(['ext'=>'jpg,jpeg,png'])->move(ROOT_PATH . 'public' . DS . 'uploads');

                $image = \think\Image::open(ROOT_PATH . 'public' . DS . 'uploads/'. $move->getSaveName());
 
                // 按照原图的比例生成一个最大为600*600的缩略图替换原图
                $image->thumb(1500, 1500)->save(ROOT_PATH . 'public' . DS . 'uploads/'.$move->getSaveName());
                if($move){
                    $myheader = str_replace('\\','/',DS . 'public' . DS . 'uploads' . DS .$move->getSaveName());
                    //存库
                    $myheader = $h5url.$myheader;
                    //$myheader = substr($myheader, 1);
                    return json($myheader, 1, '头像上传成功');
                }else{
                    // 上传失败获取错误信息
                    // echo $file->getError();
                    return json('', 1100, $file->getError());
                }
            }
        }else{
            return json('', 0, "未获取到文件");
        }

    }
  
  
    //上传身份证反面照
    public function uppicture3(){
        $info=Request::instance()->param(true);     
        $headurl = request()->file('back');//头像
       	$h5url=Db::name('hide')->where(['id'=>26])->value('value'); 
        if($headurl){
            $data['headurl']='';//logo
            foreach($headurl as $file){
                // $name1 = $file->getInfo()['name'];
                // $num = strpos($name1,'.');
                // $name1 = rand(10000000,99999999).substr($name1, $num);
                //中文文件名用此行代码转码
                // $name2=iconv('utf-8','gbk',$name1);
                // 移动到框架应用根目录/public/uploads/ 目录下
                $move = $file->validate(['ext'=>'jpg,jpeg,png'])->rule('date')->move(ROOT_PATH . 'public' . DS . 'uploads');



                $image = \think\Image::open(ROOT_PATH . 'public' . DS . 'uploads/'. $move->getSaveName());
 
                // 按照原图的比例生成一个最大为600*600的缩略图替换原图
                $image->thumb(1500, 1500)->save(ROOT_PATH . 'public' . DS . 'uploads/'.$move->getSaveName());

                if($move){
                    $myheader = str_replace('\\','/',DS . 'public' . DS . 'uploads' .  DS . $move->getSaveName());
                    //存库
                    $myheader = $h5url.$myheader;                   
                    return json($myheader, 1, '头像上传成功');
                }else{
                    // 上传失败获取错误信息
                    // echo $file->getError();
                    return json('', 1100, $file->getError());
                }
            }
        }else{
            return json('', 0, "未获取到文件");
        }

    }



    
  
  	//股票的倍数
    public function bs(){
        $res = Db::name('hide')->where('id',2)->value('value');
        return json($res,1,'倍数查询成功');
    }
  



  
	//判断股票是否停盘（创建策略）
    public function thischeckstockstop($gid) {
        $upstop=Db::name('hide')->where(['id' => 22])->value('value');
		$downstop=Db::name('hide')->where(['id'=>47])->value('value');
        $dattime=Db::name('hide')->where(['id' => 48])->value('value');
        $time=1;
        for($i=2;$i<12;$i++){
            if(isHoliday($i)!=0){
                $time+=1;
            }else{
                break;
            }
            continue;
        }
        $times=1;
        for($i=1;$i<12;$i++){
            if(isHoliday($i)!=0){
                $times+=1;
            }else{
                break;
            }
            continue;
        }
        $arr=explode('~',iconv("gb2312", "utf-8//IGNORE",file_get_contents('http://qt.gtimg.cn/q='.$gid)));
        if($arr[31]!='0.00'&&$arr[3]){
            if(strpos($arr[1],'ST')===0||strpos($arr[1],'ST')){
                return 201;
            }else{
                $str=file_get_contents('http://money.finance.sina.com.cn/quotes_service/api/json_v2.php/CN_MarketData.getKLineData?symbol='.$gid.'&scale=60&ma=25&datalen=9');
                $list=explode(',',substr($str,1,-1));
                foreach($list as $k=>$v){
                    if(strpos($v,date('Y-m-d',time()-($time+1)*3600*24).' 15:00:00')){
                        $qclose=substr($list[$k+4],7,5);
                    }
                    if(strpos($v,date('Y-m-d',time()-$times*3600*24).' 15:00:00')){
                        $yclose=substr($list[$k+4],7,5);
                    }
                }
                $now=$arr[3];
                $today=($now-$yclose)/$yclose;
                $yesterday=($yclose-$qclose)/$qclose;
                if($today>$upstop){
                    return 201;
                }
              	if($today<$downstop){
                    return 201;
                }
              	if($dattime!=1){
                	if($yesterday>$upstop){
                        return 201;
                    }
                  	if($yesterday<$downstop){
                        return 201;
                    }
                }
                return 200;
            }
        }else{
            return 201;
        }
    }
  	
  	public function createStrategy() {
        //工作日对应结果为 0, 休息日对应结果为 1, 节假日对应的结果为 2；
        $url = 'http://tool.bitefu.net/jiari/';
        $data['d'] = date('Ymd', time());
        $res = https_request($url, '', $data);
        $data=[];
          //if ($res == 0 && (date('w')<6&&date('w')>0) && ((date('Hi', time()) >= 930 && date('Hi', time()) <= 1130) || (date('Hi', time()) >= 1300  && date('Hi', time()) <= 1450))) {
            $info = Request::instance()->param(); 

            
           
           //股票已停牌
            if($this->thischeckstockstop($info['stock_code'])!=200){
                return json('', 201, '该股票已被风控，无法购买');
            }
           
           

            //用户的所有策略订单的市值最大额度
            $adminMaxMarket=Db::name('hide')->where(['id' => 7])->value('value');
            $list=Db::name('strategy')->where(['account'=>$info['account'],'status'=>['in',[1,2]]])->select();
            $alreadyAdminMaxMarket=0;
            foreach($list as $k=>$v){
                $alreadyAdminMaxMarket+=$v['market_value'];
            }
            //用户的所有策略订单的市值
            if($alreadyAdminMaxMarket>=$adminMaxMarket){
                return json('', 1101, '所有策略市值超过最大额度'.($adminMaxMarket/10000).'W');
            }
      
            $tactics_balance = Db::name('admin')->where(['account' => $info['account']])->value('tactics_balance');//策略余额
            if(($info['credit']+$info['buy_poundage'])>$tactics_balance){
                return json('', 1100, '策略余额不足,请先充值');

            }
            
            $admin = Db::name('admin')->where('account',$info['account'])->find();
                      
            //股票当前价格(加了5分)
            $json=$this->getOneStock2($info['stock_code']);
            $nowprice=json_decode($json,true)['list']['result'][0]['data']['nowPri']+0.00;


            //用户是否进入实盘
            if($admin['is_real_disk']==1){
                $shipanstock=new Stock();

                //实盘账户信息
                $shipaninfo=$this->getshipaninfo($admin['shipan_account'],substr($info['stock_code'],0,2));
                //实盘进行委托
                $success=$shipanstock->sendorder($shipaninfo['ip'],$shipaninfo['version'],$shipaninfo['yybID'],$shipaninfo['zhanghao'],
                    $shipaninfo['zhanghao'],$shipaninfo['mima'],$shipaninfo['port'],$shipaninfo['txmima'],0,4,$shipaninfo['gudongdaima'],
                    substr($info['stock_code'],2),0,$info['stock_number']);
                $success=json_decode($success,true);
                //有委托编号这个参数名和值，说明委托成功了
                if(array_key_exists('委托编号',$success['data1'])&&$success['data1']['委托编号']){
                    //买入合同编号
                    $data['buy_contract_num']=$success['data1']['委托编号'];
                    file_put_contents('shipan.txt','委托时间:'.date('Y-m-d H:i:s') ."    实盘中的策略，".'用户：'. $info['account'].
                        "，股票代码：".$info['stock_code'].'，股票数量：'.$info['stock_number']."通过委托成功才创建的\n", FILE_APPEND);
                }else{
                    file_put_contents('shipan.txt','委托时间:'.date('Y-m-d H:i:s') ."    委托失败了".
                        '原因：'. $success['data1']['保留信息']."\n", FILE_APPEND);
                    return json('', 1300, '委托失败');
                }
            }
           
            //创建策略
            $time=date('YmdHis', time());
            $data=[
                'strategy_num'=>$time.rand(1000,9999),
                'account'=>$info['account'],
                'strategy_type'=>$info['strategy_type'],
                'status'=>1,
                'stock_name'=>$info['stock_name'],
                'stock_code'=>$info['stock_code'],
                'stock_number'=>$info['stock_number'],
                'credit'=>$info['credit'],
                'market_value'=>$nowprice*$info['stock_number'],
                'double_value'=>$info['double_value'],
                'buy_price'=>$nowprice,
                'surplus_value'=>$info['surplus_value'],
                'loss_value'=>$info['loss_value'],
                'is_real_disk'=>$admin['is_real_disk'],
                //计算手续费时,T+1时第一天的递延费免费,T+5时4天的递延费免费
                'buy_poundage'=>$info['buy_poundage'],
                'all_poundage'=>'',
                'defer'=>$info['defer'],
                'buy_time'=>time(),
            ];

            //如果使用红包
            if(array_key_exists('packet_id',$info)&&$info['packet_id']){
                //使用红包的记录
                $data['is_packet']=1;
                //红包信息
                $packet=Db::name('packet')->where(['packet_id' => $info['packet_id']])->find();                
                $data['all_poundage']=$info['buy_poundage']-$packet['packet_money'];
            }else{               
                $data['all_poundage']=$info['buy_poundage'];
            }

            Db::startTrans();
            //修改红包使用记录
            if(array_key_exists('packet_id',$info)&&$info['packet_id']){
                $result1=Db::name('packet')->where(['packet_id' => $info['packet_id']])->update(['strategy_num'=>$data['strategy_num'],'is_use'=>1,'use_time'=>time()]);
                if(!$result1){
                    Db::rollback();
                    return json('', 0, '红包使用失败');
                }
            }

            //添加策略记录
            $result2=Db::name('strategy')->insert($data);
            //修改用户策略余额和冻结金额
            $result3=Db::name('admin')->where(['account' => $info['account'],'is_del'=>1])->setDec('tactics_balance',$data['credit']+$data['all_poundage']);
            $result4=Db::name('admin')->where(['account' => $info['account'],'is_del'=>1])->setInc('frozen_money',$data['credit']);
            //查询账户余额和策略账户余额
            $moneyarr = Db::name('admin')->where(['account' => $info['account'],'is_del'=>1])->field('balance,tactics_balance')->find();
            //添加交易记录
            $trade=[
                'charge_num'=>$data['strategy_num'],
                'trade_price'=>$data['credit']+$data['all_poundage'],
                'account'=>$info['account'],
                'trade_type'=>'策略创建',
                'trade_status'=>1,
                'is_del'=>1,
                'create_time'=>time(),
                'xbalance'=>$moneyarr['balance'],
                //'tactics_balance'=>$moneyarr['tactics_balance'],
            ];
            $result5=Db::name('trade')->insert($trade);
            //添加订单协议
            $arr=[
                'strategy_num'=>$data['strategy_num'],
                'investor'=>Db::name('hide')->where(['id' => 9])->value('value'),
                'account_truename'=>Db::name('admin')->where(['account' => $info['account'],'is_del'=>1])->value('true_name'),
                'stock'=>' '.$info['stock_name'].'[股票代码：'.substr($info['stock_code'],2).']',
                'stock_number'=>$info['stock_number'],
                'zhiyinglv'=>'20.0',
                'credit'=>$info['credit'],
                'sign_date'=>date('Y-m-d H:i:s'),
            ];
            if(date('N')==5){
              $t=date('Y-m-d');
              $t1=date('Y-m-d',time()+3*24*3600);
              $t5=date('Y-m-d',time()+7*24*3600);
              if($info['strategy_type']==1){
                  $arr['strategy_type']='T+1';
                  $arr['pingcang_time']=$t1.' 09:30:00-'.$t1.' 14:49:59';
              }else{
                  $arr['strategy_type']='T+5';
                  $arr['pingcang_time']=$t5.' 09:30:00-'.$t5.' 14:49:59';
              }
              $arr['t_day']=$t;
              $arr['t_one_day']=$t1;
              $arr['t_five_day']=$t5;
          }else{
              $t=date('Y-m-d');
              $t1=date('Y-m-d',time()+24*3600);
              $t5=date('Y-m-d',time()+4*24*3600);
              if($info['strategy_type']==1){
                  $arr['strategy_type']='T+1';
                  $arr['pingcang_time']=$t1.' 09:30:00-'.$t1.' 14:49:59';
              }else{
                  $arr['strategy_type']='T+5';
                  $arr['pingcang_time']=$t5.' 09:30:00-'.$t5.' 14:49:59';
              }
              $arr['t_day']=$t;
              $arr['t_one_day']=$t1;
              $arr['t_five_day']=$t5;
          }
            $result6=Db::name('agreement')->insert($arr);
            if(!$result2 || !$result3 || !$result4 || !$result5 || !$result6){
                Db::rollback();
                return json('', 0, '策略创建失败');
            }
            Db::commit();
            return json(['strategy_num'=>$data['strategy_num']], 1, '策略创建成功');
          //} else {
          //    return json('', 1200, '非交易时间禁止创建');
          //}
    }

	//获取实盘查询的必备参数
    public function getshipaninfo($id,$sh_sz=''){
        if(cache('shipan_account_arr')){
            $allshipan_account=cache('shipan_account_arr');
        }else{
            $allshipan_account=Db::name('shipan_account')->where(['is_del'=>1])->select();
            cache('shipan_account_arr', $allshipan_account, 3600);
        }
        $shipan=[];
        foreach($allshipan_account as $k=>$v){
            if($v['id']==$id){
                $shipan=$v;
            }
        }
        if($sh_sz){
            if($sh_sz=='sh'){
                $shipan['gudongdaima']=$shipan['shholder'];
                $shipan['ExchangeID']=1;
                $shipan['PriceType']=6;
            }else{
                $shipan['gudongdaima']=$shipan['szholder'];
                $shipan['ExchangeID']=0;
                $shipan['PriceType']=1;
            }
        }
        return $shipan;
    }


    //获取单个股票数据
    public function getOneStock2($gid) {
        $arr=explode('~',iconv("gb2312", "utf-8//IGNORE",file_get_contents('http://qt.gtimg.cn/q='.$gid)));

        $list['result'][0]['data']['gid']=$gid;
        $list['result'][0]['data']['increPer']=$arr[32];
        $list['result'][0]['data']['increase']=$arr[31];
        $list['result'][0]['data']['name']=$arr[1];
        $list['result'][0]['data']['todayStartPri']=$arr[5];
        $list['result'][0]['data']['yestodEndPri']=$arr[4];
        $list['result'][0]['data']['nowPri']=$arr[3];
        $list['result'][0]['data']['todayMax']=$arr[33];
        $list['result'][0]['data']['todayMin']=$arr[34];
        $list['result'][0]['data']['traNumber']=$arr[36];
        $list['result'][0]['data']['traAmount']=$arr[37];
        $list['result'][0]['data']['buyOne']=$arr[10];
        $list['result'][0]['data']['buyOnePri']=$arr[9];
        $list['result'][0]['data']['buyTwo']=$arr[12];
        $list['result'][0]['data']['buyTwoPri']=$arr[11];
        $list['result'][0]['data']['buyThree']=$arr[14];
        $list['result'][0]['data']['buyThreePri']=$arr[13];
        $list['result'][0]['data']['buyFour']=$arr[16];
        $list['result'][0]['data']['buyFourPri']=$arr[15];
        $list['result'][0]['data']['buyFive']=$arr[18];
        $list['result'][0]['data']['buyFivePri']=$arr[17];
        $list['result'][0]['data']['sellOne']=$arr[20];
        $list['result'][0]['data']['sellOnePri']=$arr[19];
        $list['result'][0]['data']['sellTwo']=$arr[22];
        $list['result'][0]['data']['sellTwoPri']=$arr[21];
        $list['result'][0]['data']['sellThree']=$arr[24];
        $list['result'][0]['data']['sellThreePri']=$arr[23];
        $list['result'][0]['data']['sellFour']=$arr[26];
        $list['result'][0]['data']['sellFourPri']=$arr[25];
        $list['result'][0]['data']['sellFive']=$arr[28];
        $list['result'][0]['data']['sellFivePri']=$arr[27];
        $list['result'][0]['data']['date']=substr($arr[30],0,4).'-'.substr($arr[30],4,2).'-'.substr($arr[30],6,2);
        $list['result'][0]['data']['time']=substr($arr[30],8,2).':'.substr($arr[30],10,2).':'.substr($arr[30],12,2);
        return json($list, 1, '查询成功');
    }
  
  	
  	//查看他人的模拟持仓 （持仓中的股票）
    public function simulatehold(){
        $info = Request::instance()->param();
        $trues = $info['trues'];//1真实 2模拟
        $subscribe_account = $info['subscribe_account'];//他人账号
        if($trues==1){//真实
            $strategy = Db::name('strategy')->where(['account'=>$subscribe_account,'status'=>['in',[1,2]]])->select();
            //持仓天数 当前时间-买入时间-休息时间
            foreach ($strategy as $k => $v) {
                 $strategy[$k]['hold_days'] = floor((time()-$v['buy_time']-$v['rest_day'])/(24*3600));
                 $strategy[$k]['buy_time'] = date('Y年m月d日',$strategy[$k]['buy_time']);
                 $strategy[$k]['sell_time'] = date('Y年m月d日',$strategy[$k]['sell_time']);
             }                      
        }else{//模拟
            $strategy = Db::name('strategy_contest')->where(['account'=>$subscribe_account,'status'=>['in',[1,2]]])->select(); 
            //持仓天数 当前时间-买入时间-休息时间
            foreach ($strategy as $k => $v) {
                 $strategy[$k]['hold_days'] = floor((time()-$v['buy_time'])/(24*3600));
                 $strategy[$k]['buy_time'] = date('Y年m月d日',$strategy[$k]['buy_time']);
                 $strategy[$k]['sell_time'] = date('Y年m月d日',$strategy[$k]['sell_time']);
             }
        }
        return json($strategy,1,'查询成功');
    }


    //查看其他的模拟记录（已平仓的股票）
    public function simulateclose(){
        $info = Request::instance()->param();
        $trues = $info['trues'];//1真实 2模拟
        $subscribe_account = $info['subscribe_account'];//他人账号
        if($trues==1){//真实
            $strategy = Db::name('strategy')->where(['account'=>$subscribe_account,'status'=>['in',[3,4]]])->order('sell_time desc')->select();
             //持仓天数 卖出时间-买入时间-休息时间
            foreach ($strategy as $k => $v) {
                 $strategy[$k]['hold_days'] = floor(($v['sell_time']-$v['buy_time']-$v['rest_day'])/(24*3600))."天";
                 $strategy[$k]['buy_time'] = date('Y年m月d日',$strategy[$k]['buy_time']);
                 $strategy[$k]['sell_time'] = date('Y年m月d日',$strategy[$k]['sell_time']);
             }                
        }else{//模拟
            $strategy = Db::name('strategy_contest')->where(['account'=>$subscribe_account,'status'=>['in',[3,4]]])->order('sell_time desc')->select();  
            //持仓天数 卖出时间-买入时间-休息时间
            foreach ($strategy as $k => $v) {
                 $strategy[$k]['hold_days'] = floor(($v['sell_time']-$v['buy_time'])/(24*3600))."天";
                 $strategy[$k]['buy_time'] = date('Y年m月d日',$strategy[$k]['buy_time']);
                 $strategy[$k]['sell_time'] = date('Y年m月d日',$strategy[$k]['sell_time']);
             }  
        }
        return json($strategy,1,'查询成功');
    }
   
  
    //我的订阅 查询账号和头像就行
    public function mysubscribe() {
        $info = Request::instance()->param();
        $account = $info['account'];//账号
        $res = Db::query('SELECT
                link_admin.account,
                link_admin.headurl,
                link_subscribe.trues,
                link_admin.id,
                link_admin.nick_name
                FROM
                link_subscribe
                INNER JOIN link_admin ON link_subscribe.subscribe_account = link_admin.account
                WHERE
                link_subscribe.account = ? and link_subscribe.is_del=1',[$account]);
      	
        foreach ($res as $k => $v) {
          	//如果是真实，显示手机号，模拟显示昵称
          	if($res[$k]['trues']==1){
            	$res[$k]['username'] = substr($v['account'], 0,3);
            	$res[$k]['username'] = $res[$k]['username']."********";
            
            }else{
            	if($res[$k]['account']==$res[$k]['nick_name']){
                	$res[$k]['username'] = substr($v['account'], 0,3);
            		$res[$k]['username'] = $res[$k]['username']."********";               	
                }else{
                	$res[$k]['username'] = $res[$k]['nick_name'];                 
                }
                       
            }
          
          
          
            
        }
      	if(!$res){
        	return json('',0,'暂无数据');
        }
        return json($res,1,'查询成功');
    }
  	
  
  
  	//创建炒股大赛策略
    public function createStrategyContest() {
        //工作日对应结果为 0, 休息日对应结果为 1, 节假日对应的结果为 2；
        $url = 'http://tool.bitefu.net/jiari/';
        $data['d'] = date('Ymd', time());
        $res = https_request($url, '', $data);
          if ($res == 0 && ((date('Hi', time()) >= 930 && date('Hi', time()) <= 1130) || (date('Hi', time()) >= 1300 && date('Hi', time()) <= 1500))) {

            $info = Request::instance()->param();

            

            //创建策略
            $time=date('YmdHis', time());
            $data=[
                'strategy_num'=>$time.rand(1000,9999),
                'account'=>$info['account'],
                'status'=>2,
                'stock_name'=>$info['stock_name'],
                'stock_code'=>$info['stock_code'],
                'stock_number'=>$info['stock_number'],
                'credit'=>$info['credit'],
                'buy_price'=>$info['buy_price'],
                'surplus_value'=>$info['surplus_value'],
                'loss_value'=>$info['loss_value'],
                'buy_time'=>time(),
            ];
            
            $analog_money = Db::name('admin')->where(['account' => $info['account']])->value('analog_money');//炒股大赛策略余额
            if($info['credit']>$analog_money){
                return json('', 1100, '策略余额不足,请先充值');

            }

            Db::startTrans();
            //添加策略记录
            $result2=Db::name('strategy_contest')->insert($data);
            //修改用户策略余额和冻结金额
            $result3=Db::name('admin')->where(['account' => $info['account'],'is_del'=>1])->setDec('analog_money',$data['credit']);
            $result4=Db::name('admin')->where(['account' => $info['account'],'is_del'=>1])->setInc('frozen_analog_money',$data['credit']);
            if(!$result2 || !$result3 || !$result4){
                Db::rollback();
                return json('', 0, '策略创建失败');
            }
            Db::commit();
            return json('', 1, '策略创建成功');
          } else {
              return json('', 1200, '非交易时间禁止创建');
          }
    }
  
  
  
  
  
  	 //获取排行榜的人员，策略收益
    public function myrank($where=[]) {
        $where['status']=['in',[3,4]];
        //会员
        //判断用户实盘信息是否开放，真实的可以控制，炒股大赛的全部显示，不能控制
        $admin=Db::name('admin')->where(['is_del'=>1,'is_open_ranking'=>1])->select();
        $admin_contest=Db::name('admin')->where(['is_del'=>1])->select();
        //真实策略
        $strategy=Db::name('strategy')->where($where)->select();
        $list=[];
        $arr=[];
        //获取到一级为会员，二级为他的策略的多维数组
        foreach($admin as $k=>$v){
            foreach($strategy as $kk=>$vv){
                if($v['account']==$vv['account']){
                    $list[$k][$kk]=$vv;
                    //subscribe_account为获得详情的参数，为了与我的订阅中跳转参数一致
                    $list[$k][$kk]['subscribe_account']=$vv['account'];
                    //用户头像
                    $list[$k][$kk]['headurl']=$v['headurl'];
                    $list[$k][$kk]['jycount']=Db::name('strategy')->where($where)->where('account',$v['account'])->count();
                    $list[$k][$kk]['account']=$v['account'];
                    //人气
                    $list[$k][$kk]['renqi']=Db::name('subscribe')->where(['subscribe_account'=>$v['account'],'is_del'=>1])->count();
                    //最近交易时间
                    $time=Db::query('SELECT
                        max(link_strategy.buy_time) as a,
                        max(link_strategy.sell_time) as b
                        FROM
                        link_strategy
                        WHERE
                        link_strategy.account = ?',[$v['account']]);
                    $op[0]=$time[0]['a'];
                    $op[1]=$time[0]['b'];
                    $recently = max($op);//最近发布时间 时间戳格式
                    $chazhi = time()-$recently;
                    if($chazhi<(24*3600)){
                        $list[$k][$kk]['recently'] = date('H:i:s',$recently); 
                    }else{
                        $days = floor($chazhi/(24*3600));//天数向下取整
                        $list[$k][$kk]['recently'] = $days."天前";
                    }

                }
            }
        }
        //真实的策略
        if($list){
            //对数组的里层进行重新排序，改变key
            foreach($list as $k=>$v){
                $list[$k]=array_values($v);
            }
            //获取到收益率和胜率的新数组，真实策略为1，名字为手机号，炒股大赛为2，名字为昵称
            foreach($list as $k=>$v){
                $arr[$k]['trues']='1';
                //subscribe_account为获得详情的参数，为了与我的订阅中跳转参数一致
                $arr[$k]['subscribe_account']=$v[0]['subscribe_account'];
                //用户头像
                $arr[$k]['headurl']=$v[0]['headurl'];
                $arr[$k]['username']=$v[0]['account'];
                //手机号加*
                $arr[$k]['username'] = substr($arr[$k]['username'], 0,3);
                $arr[$k]['username'] = $arr[$k]['username']."********";
                //收益，平台分成后的钱（包括信用金）
                $shouyi=0;
                //信用金（交易本金）
                $credit=0;
                //盈利次数
                $win=0;
                foreach($v as $kk=>$vv){
                    $credit+=$vv['credit'];
                    $shouyi+=($vv['true_getmoney']-$vv['credit']);
                    //计算胜率
                    if($vv['true_getmoney']>$vv['credit']){$win+=1;}
                }
                //收益率
                $arr[$k]['profit']=(round($shouyi/$credit*100,2)).'%';
                //胜率
                $arr[$k]['win']=(round($win/count($v)*100,2)).'%';
                //真实股票交易次数
                $arr[$k]['jycount'] = $v[0]['jycount']."次";
                //查看持仓的账号 在前端页面隐藏
                $arr[$k]['account'] = $v[0]['account']; 
                //人气
                $arr[$k]['renqi'] = $v[0]['renqi'];
                //最近发布时间
                $arr[$k]['recently'] = $v[0]['recently'];                    
            }
        }

        //炒股大赛策略
        $strategy_contest=Db::name('strategy_contest')->where($where)->select();
        $list_contest=[];
        $arr_contest=[];
        //获取到一级为会员，二级为他的策略的多维数组
        foreach($admin_contest as $k=>$v){
            foreach($strategy_contest as $kk=>$vv){
                if($v['account']==$vv['account']){
                    $list_contest[$k][$kk]=$vv;
                    $list_contest[$k][$kk]['nick_name']=$v['nick_name'];
                    //subscribe_account为获得详情的参数，为了与我的订阅中跳转参数一致
                    $list_contest[$k][$kk]['subscribe_account']=$vv['account'];
                    //用户头像
                    $list_contest[$k][$kk]['headurl']=$v['headurl'];
                    $list_contest[$k][$kk]['jycount']=Db::name('strategy_contest')->where($where)->where('account',$v['account'])->count();
                    $list_contest[$k][$kk]['account']=$v['account'];
                    //人气
                    $list_contest[$k][$kk]['renqi']=Db::name('subscribe')->where(['subscribe_account'=>$v['account'],'is_del'=>1])->count();
                    //最近交易时间
                    $time=Db::query('SELECT
                        max(link_strategy_contest.buy_time) as a,
                        max(link_strategy_contest.sell_time) as b
                        FROM
                        link_strategy_contest
                        WHERE
                        link_strategy_contest.account = ?',[$v['account']]);
                    $op[0]=$time[0]['a'];
                    $op[1]=$time[0]['b'];
                    $recently = max($op);//最近发布时间 时间戳格式
                    $chazhi = time()-$recently;
                    if($chazhi<(24*3600)){
                        $list_contest[$k][$kk]['recently'] = date('H:i:s',$recently); 
                    }else{
                        $days = floor($chazhi/(24*3600));//天数向下取整
                        $list_contest[$k][$kk]['recently'] = $days."天前";
                    }

                }
            }
        }

        //炒股大赛的策略
        if($list_contest){
            //对数组的里层进行重新排序，改变key
            foreach($list_contest as $k=>$v){
                $list_contest[$k]=array_values($v);
            }
            //获取到收益率和胜率的新数组，真实策略为1，名字为手机号，炒股大赛为2，名字为昵称(没有的话为手机号)
            foreach($list_contest as $k=>$v){
                $arr_contest[$k]['trues']='2';
                //subscribe_account为获得详情的参数，为了与我的订阅中跳转参数一致
                $arr_contest[$k]['subscribe_account']=$v[0]['subscribe_account'];
                //用户头像
                $arr_contest[$k]['headurl']=$v[0]['headurl'];
                if($v[0]['nick_name']){
                    $arr_contest[$k]['username']=$v[0]['nick_name'];
                }else{
                    $arr_contest[$k]['username']=$v[0]['account'];
                    //手机号加*
                    $arr_contest[$k]['username'] = substr($arr_contest[$k]['username'], 0,3);
                    $arr_contest[$k]['username'] = $arr_contest[$k]['username']."********";
                }
                //收益，(卖出价-买入价)*股数
                $shouyi=0;
                //信用金
                $credit=0;
                //盈利次数
                $win=0;
                foreach($v as $kk=>$vv){
                    $credit+=$vv['credit'];
                    $shouyi+=($vv['sell_price']-$vv['buy_price'])*$vv['stock_number'];
                    //计算胜率
                    if($vv['sell_price']>$vv['buy_price']){$win+=1;}
                }
                //收益率
                $arr_contest[$k]['profit']=(round($shouyi/$credit*100,2)).'%';
                //胜率
                $arr_contest[$k]['win']=(round($win/count($v)*100,2)).'%';
                //模拟股票交易次数
                $arr_contest[$k]['jycount'] = $v[0]['jycount']."次";
                //查看持仓的账号 在前端页面隐藏
                $arr_contest[$k]['account'] = $v[0]['account'];
                //人气
                $arr_contest[$k]['renqi'] = $v[0]['renqi'];
                //最近发布时间
                $arr_contest[$k]['recently'] = $v[0]['recently'];
            }
        }
        //数组合并
        $para=array_merge($arr,$arr_contest);
        array_multisort(array_column($para,'profit'),SORT_DESC,$para);
         echo "<pre>";

print_r($para);
echo "</pre>";die;
    }
  
  
  
  
  
  
  
  	//大盘股指实时行情_批量 
    public function grailindex(){
      	$info = Request::instance()->param();
      	$gid = $info['gid'];
       	$list=[];
        $str=file_get_contents('http://money.finance.sina.com.cn/quotes_service/api/json_v2.php/CN_MarketData.getKLineData?symbol='.$gid.'&scale=60&ma=25&datalen=120');
        $arr=explode(',',substr($str,1,-1));
        foreach($arr as $k=>$v){
            if(strpos($v,'15:00:00')){
                $list[]=$arr[$k];
                $list[]=$arr[$k+4];
            }
        }
        $string='[';
        foreach ($list as $k => $v) {
            if($k%2==0){               
              	$string.='['.strtotime(substr($v,6,-10)).'000,';             	
            }else{
                $string.=substr($v,7,-1).'],';
            }
        }
        $string=substr($string,0,-1);
        $string.=']';
      	
      
      	echo $string;
      
        
    }
  
  
  
  
  
  	//真实策略最近买入 买入时间最靠前的
    public function recentltbuy(){
        $res = Db::name('strategy')->order('buy_time desc')->group('stock_code')->limit(5)->select();      	
        return json($res, 1, '查询成功');
    }


    //真实策略别人都在买 买的数量最多的
    public function buymore(){
        $res = Db::query('SELECT
                link_strategy.stock_name,
                link_strategy.stock_code,
                count(link_strategy.stock_code) as buycount
                FROM
                link_strategy                
                GROUP BY
                link_strategy.stock_code
                ORDER BY buycount DESC
                LIMIT 5');
        return json($res, 1, '查询成功');
    }
  
  
  
  	
    
  
  
  	 //模拟策略最近买入
    public function recentltbuy_contest(){
        $res = Db::name('strategy_contest')->order('buy_time desc')->group('stock_code')->limit(5)->select();
        return json($res, 1, '查询成功');
    }


    //模拟策略别人都在买
    public function buymore_contest(){
        $res = Db::query('SELECT
                link_strategy_contest.stock_name,
                link_strategy_contest.stock_code,
                count(link_strategy_contest.stock_code) as buycount
                FROM
                link_strategy_contest
                GROUP BY
                link_strategy_contest.stock_code
                ORDER BY buycount DESC
                LIMIT 5');
        return json($res, 1, '查询成功');
    }
  
  
  
  
  	//查看已平仓的策略
    public function checkStrategyHistory() {
        $info = Request::instance()->param();
        $chengshu=Db::name('hide')->where(['id'=>3])->value('value');
        //已平仓的策略
        $list=Db::name('strategy')->where(['account'=>$info['account'],'status'=>['in',[3,4]]])->order('sell_time desc')->limit(8)->select();
        if($list){
            //总策略数
            $allStrategyNum=count($list);
            //盈利策略数
            $winStrategyNum=0;
            //总盈利
            $win=0;
            foreach($list as $k=>$v) {
                $list[$k]['buy_time']=date('Y-m-d',$v['buy_time']);
                $list[$k]['sell_time']=date('Y-m-d H:i:s',$v['sell_time']);
                if($v['true_getmoney']>$v['credit']){
                    //交易盈亏
                    $list[$k]['getprice']=round(($v['true_getmoney']-$v['credit'])/$chengshu,2);
                    //盈利分配
                    $list[$k]['truegetprice']=$v['true_getmoney']-$v['credit'];
                    //亏损赔付
                    $list[$k]['dropprice']=0;
                    $winStrategyNum+=1;
                }else{
                    //交易盈亏
                    $list[$k]['getprice']=$v['true_getmoney']-$v['credit'];
                    //盈利分配
                    $list[$k]['truegetprice']=0;
                    //亏损赔付
                    $list[$k]['dropprice']=$v['true_getmoney']-$v['credit'];
                }
                $win += ($v['true_getmoney'] - $v['credit']) ;
            }
            $arr=[
                'allStrategyNum'=>$allStrategyNum,
                'winStrategyNum'=>$winStrategyNum,
                'win'=>$win,
                'rate'=>round(($winStrategyNum/$allStrategyNum*100),2).'%',
            ];
        }else{
            $arr=[
                'allStrategyNum'=>0,
                'winStrategyNum'=>0,
                'win'=>0,
                'rate'=>'0%',
            ];
        }
        return json($list, 1, '查询成功',$arr);
    }
  
  
  	//查看持仓中的策略
    public function checkStrategy() {
        $info = Request::instance()->param();
        $list=Db::name('strategy')->where(['account'=>$info['account'],'status'=>['in',[1,2]]])->select();
      	foreach ($list as $k => $v) {
            $list[$k]['buy_time'] = date('m-d H:i:s',$v['buy_time']);
        }
        //用户信息
        $admin=Db::name('admin')->where(['account'=>$info['account'],'is_del'=>1])->find();
        $arr=[
            //总计余额
            'all_money'=>$admin['frozen_money']+$admin['tactics_balance']+$admin['balance'],
            //冻结金额
            'frozen_money'=>$admin['frozen_money'],
            //可用余额
            'can_money'=>$admin['tactics_balance']+$admin['balance'],
        ];
        return json($list, 1, '查询成功',$arr);
    }
  
  
  
  
  
  
  	//查看炒股大赛的已平仓的策略
    public function checkStrategyHistoryContest() {
        $info = Request::instance()->param();
        //已平仓的策略
        $list=Db::name('strategy_contest')->where(['account'=>$info['account'],'status'=>['in',[3,4]]])->order('sell_time desc')->limit(8)->select();
        //总策略数
        $allStrategyNum=count($list);
        //盈利策略数
        $winStrategyNum=0;
        //总盈利
        $win=0;
        foreach($list as $k=>$v) {
            if($v['sell_price']>=$v['buy_price']){
                //盈利分配
                $list[$k]['getprice']=round(($v['sell_price']-$v['buy_price'])*$v['stock_number'],2);
                //真正的盈利分配
                $list[$k]['truegetprice']=($v['sell_price']-$v['buy_price'])*$v['stock_number'];
                //亏损扣减
                $list[$k]['dropprice']=0;
                $winStrategyNum+=1;
                //时间格式转化
                $list[$k]['sell_time']=date('Y-m-d H:i:s',$v['sell_time']);

            }else{
                //盈利分配
                $list[$k]['getprice']=round(($v['sell_price']-$v['buy_price'])*$v['stock_number'],2);
                //真正的盈利分配
                $list[$k]['truegetprice']=0;
                //亏损扣减
                $list[$k]['dropprice']=round(($v['sell_price']-$v['buy_price'])*$v['stock_number'],2);
                //时间格式转化
                $list[$k]['sell_time']=date('Y-m-d H:i:s',$v['sell_time']);

            }
            $win += ($v['sell_price'] - $v['buy_price']) * $v['stock_number'];
        }
        $arr=[
            'allStrategyNum'=>$allStrategyNum,
            'winStrategyNum'=>$winStrategyNum,
            'win'=>$win,
            'rate'=>($winStrategyNum/$allStrategyNum*100).'%',
        ];
        return json($list, 1, '查询成功',$arr);
    }
  
  
  
  	//查看炒股大赛的持仓中的策略
    public function checkStrategyContest() {
        $info = Request::instance()->param();
        $list=Db::name('strategy_contest')->where(['account'=>$info['account'],'status'=>['in',[1,2]]])->order('buy_time desc')->limit(4)->select();
        foreach ($list as $k => $v) {
            $list[$k]['buy_time'] = date('m-d H:i:s',$v['buy_time']);
        }
        //用户信息
        $admin=Db::name('admin')->where(['account'=>$info['account'],'is_del'=>1])->find();
        $arr=[
            //总计余额
            'all_money'=>$admin['analog_money']+$admin['frozen_analog_money'],
            //冻结金额
            'frozen_money'=>$admin['frozen_analog_money'],
            //可用余额
            'can_money'=>$admin['analog_money'],
        ];
        return json($list, 1, '查询成功',$arr);
    }
  
  
  
  
  	//查看已平仓的策略的具体 传入策略单号
    public function checkStrategyHistory2() {
        $info = Request::instance()->param();
        $chengshu=Db::name('hide')->where(['id'=>3])->value('value');
        //已平仓的策略
        $list=Db::name('strategy')->where(['strategy_num'=>$info['strategy_num'],'status'=>['in',[3,4]]])->select();
        if($list){
            //总策略数
            $allStrategyNum=count($list);
            //盈利策略数
            $winStrategyNum=0;
            //总盈利
            $win=0;
            foreach($list as $k=>$v) {
                $list[$k]['buy_time']=date('Y-m-d',$v['buy_time']);
                $list[$k]['sell_time']=date('Y-m-d',$v['sell_time']);
              	//卖出类型
              	if($list[$k]['sell_type']==1){
                	$list[$k]['sell_type'] = "主动卖出";
                }
              	if($list[$k]['sell_type']==2){
                	$list[$k]['sell_type'] = "达到日期";
                }
              	if($list[$k]['sell_type']==3){
                	$list[$k]['sell_type'] = "触发止盈";
                }
              	if($list[$k]['sell_type']==4){
                	$list[$k]['sell_type'] = "触发止损";
                }
                if($v['true_getmoney']>$v['credit']){
                    //交易盈亏
                    $list[$k]['getprice']=round(($v['true_getmoney']-$v['credit'])/$chengshu,2);
                  	
                    //盈利分配
                    $list[$k]['truegetprice']=$v['true_getmoney']-$v['credit'];
                    //亏损赔付
                    $list[$k]['dropprice']=0;
                    $winStrategyNum+=1;
                    //持仓天数
                     $list[$k]['hold_days'] = floor(($v['sell_time']-$v['buy_time']-$v['rest_day'])/(24*3600))."天";
                }else{
                    //交易盈亏
                    $list[$k]['getprice']=$v['true_getmoney']-$v['credit'];
                    //盈利分配
                    $list[$k]['truegetprice']=0;
                    //亏损赔付
                    $list[$k]['dropprice']=$v['true_getmoney']-$v['credit'];
                    //持仓天数
                     $list[$k]['hold_days'] = floor(($v['sell_time']-$v['buy_time']-$v['rest_day'])/(24*3600))."天";
                }
                $win += ($v['true_getmoney'] - $v['credit']) ;
            }
            $arr=[
                'allStrategyNum'=>$allStrategyNum,
                'winStrategyNum'=>$winStrategyNum,
                'win'=>$win,
                'rate'=>round(($winStrategyNum/$allStrategyNum*100),2).'%',
            ];
        }else{
            $arr=[
                'allStrategyNum'=>0,
                'winStrategyNum'=>0,
                'win'=>0,
                'rate'=>'0%',
            ];
        }
        return json($list, 1, '查询成功',$arr);
    }
  
  
  
  
  
  
  	//查看炒股大赛的已平仓的策略 传入策略单号
    public function checkStrategyHistoryContest2() {
        $info = Request::instance()->param();
        //已平仓的策略
        $list=Db::name('strategy_contest')->where(['strategy_num'=>$info['strategy_num'],'status'=>['in',[3,4]]])->select();
        //总策略数
        $allStrategyNum=count($list);
        //盈利策略数
        $winStrategyNum=0;
        //总盈利
        $win=0;
        foreach($list as $k=>$v) {
          	//卖出类型
              	if($list[$k]['sell_type']==1){
                	$list[$k]['sell_type'] = "主动卖出";
                }
              	if($list[$k]['sell_type']==2){
                	$list[$k]['sell_type'] = "达到日期";
                }
              	if($list[$k]['sell_type']==3){
                	$list[$k]['sell_type'] = "触发止盈";
                }
              	if($list[$k]['sell_type']==4){
                	$list[$k]['sell_type'] = "触发止损";
                }
            if($v['sell_price']>=$v['buy_price']){
                //交易盈亏
                $list[$k]['getprice']=round(($v['sell_price']-$v['buy_price'])*$v['stock_number'],2);
                //盈利分配
                $list[$k]['truegetprice']=round(($v['sell_price']-$v['buy_price'])*$v['stock_number'],2);
                //亏损扣减
                $list[$k]['dropprice']=0;
                $winStrategyNum+=1;
                //时间格式转化
                $list[$k]['sell_time']=date('Y-m-d H:i:s',$v['sell_time']);
              	$list[$k]['buy_time']=date('Y-m-d H:i:s',$v['buy_time']);
              	//持仓天数
                $list[$k]['hold_days'] = 1;
            }else{
                //盈利分配
                $list[$k]['getprice']=round(($v['sell_price']-$v['buy_price'])*$v['stock_number'],2);
                //真正的盈利分配
                $list[$k]['truegetprice']=0;
                //亏损扣减
                $list[$k]['dropprice']=round(($v['sell_price']-$v['buy_price'])*$v['stock_number'],2);
                //时间格式转化
                $list[$k]['sell_time']=date('Y-m-d H:i:s',$v['sell_time']);
              	$list[$k]['buy_time']=date('Y-m-d H:i:s',$v['buy_time']);
              	//持仓天数
                $list[$k]['hold_days'] = 1;
            }
            $win += ($v['sell_price'] - $v['buy_price']) * $v['stock_number'];
        }
        $arr=[
            'allStrategyNum'=>$allStrategyNum,
            'winStrategyNum'=>$winStrategyNum,
            'win'=>$win,
            'rate'=>($winStrategyNum/$allStrategyNum*100).'%',
        ];
        return json($list, 1, '查询成功');
    }
  
  
    
  
  
  	//点击开放实盘信息
    public function ifopen(){
        $account = input('account');//账号
        $ifopen = input('ifopen');//是否开放 1开放 2不开放
        $res = Db::name('admin')->where('account',$account)->update(['is_open_ranking'=>$ifopen]);
        if(!$res){
            return json('',0,'实盘信息修改失败');
        }
        return json('',1,'实盘信息修改成功');

    }



    //查看个人股票信息是否开放
    public function lookopen(){
        $account = $_COOKIE['username'];
        $res = Db::name('admin')->where('account',$account)->value('is_open_ranking');
        return json($res,1,'是否开放实盘信息查看成功');
        
    }


    //会员中心实名认证
    public function authentication(){
        $info = Request::instance()->param();   
        $data['true_name'] = $info['true_name'];//名字
        $data['card_id'] = $info['card_id'];//身份证号
        $data['frontcard'] = $info['frontcard'];//身份证正面照
        $data['backcard'] = $info['backcard'];//身份证反面照
        $data['is_true'] = '3';//实名认证待审核
        
        Db::name('admin')->where(['account'=>$info['account'],'is_del'=>1])->update($data);
        return json('', 1, '提交成功');
    }
  
  
  
  	//判断股票是否停盘
    public function checkstockstop() {
        $upstop=Db::name('hide')->where(['id' => 22])->value('value');
        $downstop=Db::name('hide')->where(['id'=>47])->value('value');
        $dattime=Db::name('hide')->where(['id' => 48])->value('value');
      	$time=1;
        for($i=1;$i<12;$i++){
            //工作日对应结果为 0, 休息日对应结果为 1, 节假日对应的结果为 2；
            $url='http://api.goseek.cn/Tools/holiday?';
            $data['date'] = date('Ymd', time()-$i*24*3600);
            $jj = https_request($url, '', $data);
            $res=json_decode($jj,true)['data'];
            if($res){
                $time+=1;
            }else{
                break;
            }
            continue;
        }
        $times=1;
        for($i=2;$i<13;$i++){
            //工作日对应结果为 0, 休息日对应结果为 1, 节假日对应的结果为 2；
            $url='http://api.goseek.cn/Tools/holiday?';
            $data['date'] = date('Ymd', time()-$i*24*3600);
            $jj = https_request($url, '', $data);
            $res=json_decode($jj,true)['data'];
            if($res){
                $times+=1;
            }else{
                break;
            }
            continue;
        }
      
        $arr=explode('~',iconv("gb2312", "utf-8//IGNORE",file_get_contents('http://qt.gtimg.cn/q='.input('gid'))));
        if($arr[31]!='0.00'&&$arr[3]){
            if(strpos($arr[1],'ST')===0||strpos($arr[1],'ST')){
                return json('', 201, '已停盘');
            }else{
                $str=file_get_contents('http://money.finance.sina.com.cn/quotes_service/api/json_v2.php/CN_MarketData.getKLineData?symbol='.input('gid').'&scale=60&ma=25&datalen=9');
                $list=explode(',',substr($str,1,-1));

                foreach($list as $k=>$v){
                 if(strpos($v,date('Y-m-d',time()-($times+1)*3600*24).' 15:00:00')){
                      $qclose=substr($list[$k+4],7,5);
                  }
                  if(strpos($v,date('Y-m-d',time()-$time*3600*24).' 15:00:00')){
                      $yclose=substr($list[$k+4],7,5);
                  }
                }
                $now=$arr[3];
                $today=($now-$yclose)/$yclose;
                $yesterday=($yclose-$qclose)/$qclose;
                if($today>$upstop){
                    return json('', 201, '涨停');
                }
              	if($today<$downstop){
                    return json('', 201, '跌停');
                }
              	if($dattime!=1){
                	if($yesterday>$upstop){
                        return json('', 201, '涨停');
                    }
                  	if($yesterday<$downstop){
                        return json('', 201, '跌停');
                    }
                }
                return json('', 200, '查询成功');
            }
        }else{
            return json('', 201, '已停盘');
        }
    }
  
  
 
  

}
