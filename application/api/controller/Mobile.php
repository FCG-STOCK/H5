<?php
namespace app\API\controller;
use think\Db;
use think\Request;
use think\Controller;
use think\Validate;
use Stock;
header('Content-Type:text/html;charset=utf8');
class Mobile extends controller
{

	
	public function json($list, $status, $msg,$para='') {
        $arr['status'] = $status;
        $arr['msg'] = $msg;
        $arr['list'] = $list;
        if($para){
            $arr['para'] = $para;
        }
        return json_encode($arr,JSON_UNESCAPED_UNICODE);
    }





	//公告列表页
    public function noticeList() {
        $list = Db::name('news')->where(['cid' => 6,'is_del'=>1])->order('sort desc,create_time desc')->select();
        foreach($list as $k => $v){
            $list[$k]['create_time'] = date('Y-m-d', $v['create_time']);
            $arr=explode(',',substr($v['picurl'],0,-1));
            $list[$k]['picurl']=[];
            foreach($arr as $kk=>$vv){
                $list[$k]['picurl'][$kk]=$vv;
            }
            $list[$k]['content']=str_replace('"','\'',htmlspecialchars($v['content'],ENT_NOQUOTES));
            $list[$k]['is_new']='';
            if($k<=4){
                $list[$k]['is_new']=1;
            }else{
                $list[$k]['is_new']=2;
            }
        }
        return $this->json($list, 1, '查询成功');
    }

	
  
  	//查看后台添加的模拟策略
    public function strategyAnalog() {
        $zixuan = Db::name("strategy_analog")->where(["is_del"=>1])->order('create_time desc')->select();
        if($zixuan){
            foreach($zixuan as $k=>$v){
                $zixuan[$k]['create_time']=date('m-d H:i',$v['create_time']);
                $zixuan[$k]['account']=substr($v['account'],0,3).'****'.substr($v['account'],7);
            }
        }
        return $this->json($zixuan, 1, '查询成功');
    }
  	//查看后台添加的模拟策略
    public function strategyAnalog111() {
        $zixuan = Db::name("strategy_analog")->where(["is_del"=>1])->order('create_time desc')->select();
        if($zixuan){
            foreach($zixuan as $k=>$v){
                $zixuan[$k]['create_time']=$this->gettimeago($v['create_time']);
                $zixuan[$k]['account']=substr($v['account'],0,3).'****'.substr($v['account'],7);
            }
        }
        return $this->json($zixuan, 1, '查询成功');
    }
    //获取距离现在时间的长度
    public function gettimeago($time){
        $range=time()-$time;
        if($range<60){
            $content=$range.'秒前';
        }elseif($range<3600){
            $minute=floor($range/60);
            $content=$minute.'分钟前';
        }elseif($range<3600*24){
            $hour=floor($range/3600);
            $content=$hour.'小时前';
        }elseif($range<3600*24*30){
            $day=floor($range/(3600*24));
            $content=$day.'天前';
        }elseif($range<3600*24*30*12){
            $month=floor($range/(3600*24*30));
            $content=$month.'月前';
        }else{
            $year=floor($range/(3600*24*30*12));
            $content=$year.'年前';
        }
        return $content;
    }
  
  	//查看我的自选
    public function myChooseStock() {
        //if(token()==100){return json('', 100, '您的账号已在别的设备登陆');}

        $info = Request::instance()->param();

        $zixuan = Db::name("stock_zixuan")->where(['account'=>$info['account'],"is_del"=>1])->order('id desc')->select();
        if($zixuan){
            $hot_code='';
            foreach($zixuan as $k=>$v){
                $hot_code.=$v['stock_code'].',';
            }
            $shipanstock=new Stock();
            if(date('Hi')>930){
                $result=$shipanstock->Hq_list(substr($hot_code,0,-1));
                $arr=explode(';',$result);
                unset($arr[count($arr)-1]);
                foreach($arr as $k=>$v){
                    $zixuan[$k]['nowprice']=explode(',',$v)[3];
                    $zixuan[$k]['rate_price']=round(explode(',',$v)[3]-explode(',',$v)[2],2);
                    $zixuan[$k]['rate']=(round((explode(',',$v)[3]-explode(',',$v)[2])/explode(',',$v)[2],4)*100).'%';
                }
            }else{
                $result=$shipanstock->Hq_real_list(substr($hot_code,0,-1));
                $arr=json_decode($result,true);
                foreach($arr as $k=>$v){
                    $zixuan[$k]['nowprice']=$v['nowPri'];
                    $zixuan[$k]['rate_price']=round($v['nowPri']-$v['closePrice'],2);
                    $zixuan[$k]['rate']=(round(($v['nowPri']-$v['closePrice'])/$v['closePrice'],4)*100).'%';
                }
            }
          	
          	
          	//禁止买卖股票列表
            $forbidden = Db::name('forbidden_list')->where('is_del',1)->select();
            foreach($forbidden as $k=>$v){
                   foreach($zixuan as $kk=>$vv){

                        if($v['stock_code']==$vv['stock_code']){
                            unset($zixuan[$kk]);
                        }
                   } 
            }
            $zixuan = array_values($zixuan);
        }
        return $this->json($zixuan, 1, '查询成功');
    }
  
  
  	//添加至我的自选
    public function addChooseStock() {
        //if(token()==100){return json('', 100, '您的账号已在别的设备登陆');}
		
      
        $info = Request::instance()->param();

        if(!Db::name('stock_zixuan')->where(['account'=>$info['account'],'stock_code'=>$info['stock_code'],'is_del'=>1])->find()){
            $data=[
                'account'=>$info['account'],
                'stock_name'=>$info['stock_name'],
                'stock_code'=>$info['stock_code'],
            ];
            Db::name('stock_zixuan')->insert($data);
        }
        return $this->json('', 1, '添加成功');
    }
    //删除某个我的自选
    public function deleteChooseStock() {
        //if(token()==100){return json('', 100, '您的账号已在别的设备登陆');}

        $info = Request::instance()->param();

        Db::name('stock_zixuan')->where(['account'=>$info['account'],'stock_code'=>$info['stock_code']])->update(['is_del'=>2]);
        return json('', 1, '删除成功');
    }
    //清空我的自选
    public function deleteAllChooseStock() {
        //if(token()==100){return json('', 100, '您的账号已在别的设备登陆');}

        $info = Request::instance()->param();

        Db::name('stock_zixuan')->where(['account'=>$info['account']])->update(['is_del'=>2]);
        return $this->json('', 1, '清空成功');
    }
  
  
  
  	//判断一支股票是否 是我的自选
    public function ifChooseStock() {
        //if(token()==100){return json('', 100, '您的账号已在别的设备登陆');}

        $info = Request::instance()->param();
		$res = Db::name('stock_zixuan')->where(['account'=>$info['account'],'stock_code'=>$info['stock_code'],'is_del'=>1])->find();

        if(!$res){
            return $this->json('', 0, '不是我的自选');
        }
        return $this->json('', 1, '是我的自选');
    }
  
  
  
  	//股票模糊搜索页面
    public function search_stock_list(){
      	$shipanstock=new Stock();
        $info = Request::instance()->param();
        //热门股票 显示3条
        $hot = Db::name("stock_recommend")->where(["is_recommend"=>1])->order('id desc')->limit(5)->select();
        if($hot){
            $hot_code='';
            foreach($hot as $k=>$v){
                $hot_code.=$v['stock_code'].',';
            }
            if(date('Hi')>930){
                $hot_result=$shipanstock->Hq_list(substr($hot_code,0,-1));
                $hot_arr=explode(';',$hot_result);
                unset($hot_arr[count($hot_arr)-1]);
                foreach($hot_arr as $k=>$v){
                    $hot[$k]['nowprice']=round(explode(',',$v)[3],2);
                    $hot[$k]['rate_price']=round(explode(',',$v)[3]-explode(',',$v)[2],2);
                    $hot[$k]['rate']=(round((explode(',',$v)[3]-explode(',',$v)[2])/explode(',',$v)[2],4)*100).'%';
                }
            }else{
                $hot_result=$shipanstock->Hq_real_list(substr($hot_code,0,-1));
                $hot_arr=json_decode($hot_result,true);
                foreach($hot_arr as $k=>$v){
                    $hot[$k]['nowprice']=round($v['nowPri'],2);
                    $hot[$k]['rate_price']=round($v['nowPri']-$v['closePrice'],2);
                    $hot[$k]['rate']=(round(($v['nowPri']-$v['closePrice'])/$v['closePrice'],4)*100).'%';
                }
            }
        }
      	//禁止买卖股票列表
      	$forbidden = Db::name('forbidden_list')->where('is_del',1)->select();
      	
      	foreach($hot as $k=>$v){
               foreach($forbidden as $kk=>$vv){

					if($v['stock_code']==$vv['stock_code']){
                    	unset($hot[$k]);
                    }
               } 
        }
      	$hot = array_values($hot);
      
        //历史搜索 显示3条
        $search = Db::name("stock_search")->where(['account'=>$info['account'],'is_del'=>1])->order('id desc')->limit(10)->select();
        if($search){
            $search_code='';
            foreach($search as $k=>$v){
                $search_code.=$v['stock_code'].',';
            }
            if(date('Hi')>930){
                $search_result=$shipanstock->Hq_list(substr($search_code,0,-1));
                $search_arr=explode(';',$search_result);
                unset($search_arr[count($search_arr)-1]);
                foreach($search_arr as $k=>$v){
                    $search[$k]['nowprice']=round(explode(',',$v)[3],2);
                    $search[$k]['rate_price']=round(explode(',',$v)[3]-explode(',',$v)[2],2);
                    $search[$k]['rate']=(round((explode(',',$v)[3]-explode(',',$v)[2])/explode(',',$v)[2],4)*100).'%';
                }
            }else{
                $search_result=$shipanstock->Hq_real_list(substr($search_code,0,-1));
                $search_arr=json_decode($search_result,true);
                foreach($search_arr as $k=>$v){
                    $search[$k]['nowprice']=round($v['nowPri'],2);
                    $search[$k]['rate_price']=round($v['nowPri']-$v['closePrice'],2);
                    $search[$k]['rate']=(round(($v['nowPri']-$v['closePrice'])/$v['closePrice'],4)*100).'%';
                }
            }
        }
      	//禁止买卖股票列表
      	$forbidden = Db::name('forbidden_list')->where('is_del',1)->select();
      	foreach($search as $k=>$v){
               foreach($forbidden as $kk=>$vv){
					if($v['stock_code']==$vv['stock_code']){
                    	unset($search[$k]);
                    }
               } 
        }
      	$search = array_values($search);
        return $this->json($hot, 1, '查询成功', $search);
    }
  
  
  
  	public function Kmonth(){
		
      	//获取
        $stock = new Stock;
        
        $stock_data = $stock->Kmonth(input('code'));
      	return $stock_data;

    }
    public function Kweek(){
		//获取
        $stock = new Stock;
        
        $stock_data = $stock->Kweek(input('code'));
      	return $stock_data;


    }
    
  
  
  	public function kday(){
      
      
      	//获取
        $stock = new Stock;
        
        $stock_data = $stock->Kday(input('code'));
      	return $stock_data;


        return json($last_new,1,'查询成功');

    }
  

    public function minhour(){
        
      	//获取
        $stock = new Stock;
        
        $stock_data = $stock->Minhour(input('code'));

      	return $stock_data;
    }

  
  	//选中一只股获取详细信息(买入时调用)
    public function stockDetail_tobuy(){        
        $info = Request::instance()->param();
        
      	//获取股票行情
        $stock = new Stock;
        
        $stock_data = $stock->Hq($info['stock_code']);

        $arr=explode('~',iconv("gb2312", "utf-8//IGNORE",$stock_data));
      
        $data=[
            'name'=>$arr[1],
            'code'=>$arr[2],
            'nowPri'=>$arr[3],
            'todayopen'=>$arr[5],
            'yestodayshou'=>$arr[4],
            'chengjiaoliang'=>$arr[36],  //成交量（手）
            'zongshizhi'=>$arr[45],      //总市值
            'increase'=>$arr[31],        //涨跌
            'increPer'=>$arr[32],        //涨跌%
            'upstopprice'=>$arr[47],     //涨停价
            'downstopprice'=>$arr[48],   //跌停价
            'zuigao'=>$arr[41],          //最高
            'zuidi'=>$arr[42],           //最低
            'shiyinglv'=>$arr[39],       //市盈率
            'shijinglv'=>$arr[46],       //市净率
            'buyOnePri'=>$arr[9],
            'buyOne'=>$arr[10],
            'buyTwoPri'=>$arr[11],
            'buyTwo'=>$arr[12],
            'buyThreePri'=>$arr[13],
            'buyThree'=>$arr[14],
            'buyFourPri'=>$arr[15],
            'buyFour'=>$arr[16],
            'buyFivePri'=>$arr[17],
            'buyFive'=>$arr[18],
            'sellOnePri'=>$arr[19],
            'sellOne'=>$arr[20],
            'sellTwoPri'=>$arr[21],
            'sellTwo'=>$arr[22],
            'sellThreePri'=>$arr[23],
            'sellThree'=>$arr[24],
            'sellFourPri'=>$arr[25],
            'sellFour'=>$arr[26],
            'sellFivePri'=>$arr[27],
            'sellFive'=>$arr[28],
        ];

        return $this->json($data, 200, '查询成功');
    }
  
  	//判断这支股票是否停盘
    public function thischeckstockstop($gid) {
        $upstop=Db::name('hide')->where(['id' => 22])->value('value');
		$downstop=Db::name('hide')->where(['id'=>47])->value('value');
        $dattime=Db::name('hide')->where(['id' => 48])->value('value');
        
        $time=1;
        for($i=2;$i<12;$i++){
            if(isHoliday($i)!=0){
                $time+=1;
            }else{
                break;
            }
            continue;
        }
        $times=1;
        for($i=1;$i<12;$i++){
            if(isHoliday($i)!=0){
                $times+=1;
            }else{
                break;
            }
            continue;
        }

      	//获取股票行情
      	$stock = new Stock;        
        $stock_data = $stock->Hq($gid);
        $arr=explode('~',iconv("gb2312", "utf-8//IGNORE",$stock_data));
        if($arr[31]!='0.00'&&$arr[3]){
            if(strpos($arr[1],'ST')===0||strpos($arr[1],'ST')){
                return 201;
            }else{
                
              	$str = $stock->Stop($gid);
                $list=explode(',',substr($str,1,-1));
                foreach($list as $k=>$v){
                    if(strpos($v,date('Y-m-d',time()-($time+1)*3600*24).' 15:00:00')){
                        $qclose=substr($list[$k+4],7,5);
                    }
                    if(strpos($v,date('Y-m-d',time()-$times*3600*24).' 15:00:00')){
                        $yclose=substr($list[$k+4],7,5);
                    }
                }
                $now=$arr[3];
                $today=($now-$yclose)/$yclose;
                $yesterday=($yclose-$qclose)/$qclose;
                if($today>$upstop){
                    return 201;
                }
              	if($today<$downstop){
                    return 201;
                }
              	if($dattime!=1){
                	if($yesterday>$upstop){
                        return 201;
                    }
                  	if($yesterday<$downstop){
                        return 201;
                    }
                }
              	if($now<0.1){
                    return 201;
                }
                return 200;
            }
        }else{
            return 201;
        }
    }

  	
  
  
  	//获取实盘查询的必备参数
    public function getshipaninfo($id,$sh_sz=''){
        if(cache('shipan_account_arr')){
            $allshipan_account=cache('shipan_account_arr');
        }else{
            $allshipan_account=Db::name('shipan_account')->where(['is_del'=>1])->select();
            cache('shipan_account_arr', $allshipan_account, 3600);
        }
        $shipan=[];
        foreach($allshipan_account as $k=>$v){
            if($v['id']==$id){
                $shipan=$v;
            }
        }
        if($sh_sz){
            if($sh_sz=='sh'){
                $shipan['gudongdaima']=$shipan['shholder'];
                $shipan['ExchangeID']=1;
                $shipan['PriceType']=6;
            }else{
                $shipan['gudongdaima']=$shipan['szholder'];
                $shipan['ExchangeID']=0;
                $shipan['PriceType']=1;
            }
        }
        return $shipan;
    }
  
  
  
  
	//创建策略
    public function createStrategy() {
        //是否限制股票交易时间
        $hide=Db::name('hide')->where(['id'=>29])->value('value');
        if($hide==1){
            if (isHoliday()!=0 || (date('Hi', time()) < 930 || ((date('Hi', time()) > 1130) && (date('Hi', time()) < 1300)) || date('Hi', time()) > 1455)) {
                return $this->json('', 0, '非交易时间禁止创建');
            }
        }else{
            if(date('Hi', time())>830 && date('Hi', time())<930){
                return $this->json('', 0, '非交易时间禁止创建');
            }
        }

        $info = Request::instance()->param();

        //用户的所有策略订单的市值最大额度
        $adminMaxMarket=Db::name('hide')->where(['id' => 7])->value('value');
        $list=Db::name('strategy')->where(['is_cancel_order'=>1])->where(['account'=>$info['account'],'status'=>['in',[1,2]]])->select();
        $alreadyAdminMaxMarket=0;
        foreach($list as $k=>$v){
            $alreadyAdminMaxMarket+=$v['market_value'];
        }
        //用户的所有策略订单的市值
        if($alreadyAdminMaxMarket>=$adminMaxMarket){
            return $this->json('', 0, '所有策略市值超过最大额度'.($adminMaxMarket/10000).'W');
        }

        //用户信息
        $admin=Db::name('admin')->where(['account' => $info['account'],'is_del'=>1])->find();
        if($admin['tactics_balance']<($info['credit']+$info['buy_poundage'])){
            return $this->json('', 0, '策略余额不足');
        }
      
      	if($admin['is_true']!=1){
            return $this->json('', 0, '用户未实名认证');
        }

      	if($info['strategy_type']==1){
        	$credit_min = Db::name('hide')->where('id',16)->value('value');//最低信用金
        	$credit_max = Db::name('hide')->where('id',17)->value('value');//最高信用金
          	if($info['credit']<$credit_min){return $this->json('', 0, '最低信用金为'.$credit_min);}
        	if($info['credit']>$credit_max){return $this->json('', 0, '最高信用金为'.$credit_max);}
        }
      
        //股数不能为空
        if(!$info['stock_number']){
            return $this->json('', 0, '股数不能为空');
        }

        //股票已停牌
        if($this->thischeckstockstop($info['stock_code'])!=200){
            return $this->json('', 0, '股票已被风控，无法买卖');
        }

        //股票当前价格
        $json=$this->getOneStock2($info['stock_code']);

        $nowprice=json_decode($json,true)['list']['result'][0]['data']['nowPri'];
        if($nowprice<0.1){
            return $this->json('', 0, '股票已停牌，无法买卖');
        }

      	if($info['surplus_value']<$nowprice){return $this->json('', 0, '止盈价格不能小于现价');}

      	$beishu=Db::name('hide')->where(['id'=>2])->value('value');
        $beishu=explode(',',$beishu);
        if (!in_array($info['double_value']/$info['credit'],$beishu)){return $this->json('', 0, '倍数不符合规则');}
      
        //用户是否进入实盘
        if($admin['is_real_disk']==1){
            $shipanstock=new Stock();

            //实盘账户信息
            //$shipaninfo=$this->getshipaninfo(substr($info['stock_code'],0,2));
          	//实盘账户信息
            $shipaninfo=$this->getshipaninfo($admin['shipan_account'],substr($info['stock_code'],0,2));

            //实盘进行委托
            $success=$shipanstock->sendorder($shipaninfo['ip'],$shipaninfo['version'],$shipaninfo['yybID'],$shipaninfo['zhanghao'],
                $shipaninfo['zhanghao'],$shipaninfo['mima'],$shipaninfo['port'],$shipaninfo['txmima'],0,$shipaninfo['PriceType'],$shipaninfo['gudongdaima'],
                substr($info['stock_code'],2),0,$info['stock_number']);
            $success=json_decode($success,true);
            //有委托编号这个参数名和值，说明委托成功了
            if(array_key_exists('委托编号',$success['data1'])&&$success['data1']['委托编号']){
                //买入合同编号
                $data['buy_contract_num']=$success['data1']['委托编号'];
                $log=[
                    'type'=>'4',
                    'content'=>"实盘中的策略，".'用户：'. $info['account']."，股票代码：".$info['stock_code'].'，股票数量：'.$info['stock_number']."通过委托成功才创建的",
                    'create_time'=>date('Y-m-d H:i:s'),
                ];
                Db::name('log')->insert($log);
            }else{
                $log=[
                    'type'=>'4',
                    'content'=>"实盘中的策略，委托失败了".'原因：'. $success['data1']['保留信息'],
                    'create_time'=>date('Y-m-d H:i:s'),
                ];
                Db::name('log')->insert($log);
                return $this->json('', 0, '委托失败');
            }
        }

        //创建策略
        $time=date('YmdHis', time());
        $data['strategy_num']=$time.rand(1000,9999);
        $data['account']=$info['account'];
        $data['strategy_type']=$info['strategy_type'];
        $data['status']=1;
        $data['stock_name']=$info['stock_name'];
        $data['stock_code']=$info['stock_code'];
        $data['stock_number']=$info['stock_number'];
        $data['credit']=$info['credit'];
        $data['market_value']=$nowprice*$info['stock_number'];
        $data['double_value']=$info['double_value'];
        $data['buy_price']=$nowprice;
        $data['surplus_value']=$info['surplus_value'];
        $data['loss_value']=$info['loss_value'];
        //是否进入实盘
        $data['is_real_disk']=$admin['is_real_disk'];
        //计算手续费时,T+1时第一天的递延费免费,T+5时会先收4天的递延费
        $data['buy_poundage']=$info['buy_poundage'];
        $data['all_poundage']='';
        $data['defer']=$info['defer'];
        $data['buy_time']=time();

        //如果使用红包
        if(array_key_exists('packet_id',$info)&&$info['packet_id']){
            //使用红包的记录
            $data['is_packet']=1;
            //红包信息
            $packet=Db::name('packet')->where(['packet_id' => $info['packet_id']])->find();
            $data['all_poundage']=$info['buy_poundage']-$packet['packet_money'];
        }else{
            $data['all_poundage']=$info['buy_poundage'];
        }

        Db::startTrans();
        //修改红包使用记录
        if(array_key_exists('packet_id',$info)&&$info['packet_id']){
            $result1=Db::name('packet')->where(['packet_id' => $info['packet_id']])->update(['strategy_num'=>$data['strategy_num'],'is_use'=>1,'use_time'=>time()]);
            if(!$result1){
                Db::rollback();
                return $this->json('', 0, '红包使用失败');
            }
        }

        //添加策略记录
        $result2=Db::name('strategy')->insert($data);
        //修改用户策略余额和冻结金额
        $result3=Db::name('admin')->where(['account' => $info['account'],'is_del'=>1])->update(['tactics_balance'=>$admin['tactics_balance']-$data['credit']-$data['all_poundage']]);
        $result4=Db::name('admin')->where(['account' => $info['account'],'is_del'=>1])->update(['frozen_money'=>$admin['frozen_money']+$data['credit']]);
        //添加交易记录
        $trade=[
            'charge_num'=>$data['strategy_num'],
            'trade_price'=>$data['credit']+$data['all_poundage'],
            'account'=>$info['account'],
            'trade_type'=>'策略创建',
            'trade_status'=>1,
            'is_del'=>1,
            'create_time'=>time(),
        ];
        $result5=Db::name('trade')->insert($trade);

        //添加订单协议
        $arr=[
            'strategy_num'=>$data['strategy_num'],
            'investor'=>Db::name('hide')->where(['id' => 9])->value('value'),
            'account_truename'=>$admin['true_name'],
            'stock'=>' '.$info['stock_name'].'[股票代码：'.substr($info['stock_code'],2).']',
            'stock_number'=>$info['stock_number'],
            'zhiyinglv'=>'20.0',
            'credit'=>$info['credit'],
            'sign_date'=>date('Y-m-d H:i:s'),
        ];

        if(date('N')==5){
            $t=date('Y-m-d');
            $t1=date('Y-m-d',time()+3*24*3600);
            $t5=date('Y-m-d',time()+7*24*3600);
            if($info['strategy_type']==1){
                $arr['strategy_type']='T+1';
                $arr['pingcang_time']=$t1.' 09:30:00-'.$t1.' 14:49:59';
            }else{
                $arr['strategy_type']='T+5';
                $arr['pingcang_time']=$t5.' 09:30:00-'.$t5.' 14:49:59';
            }
            $arr['t_day']=$t;
            $arr['t_one_day']=$t1;
            $arr['t_five_day']=$t5;
        }else{
            $t=date('Y-m-d');
            $t1=date('Y-m-d',time()+24*3600);
            $t5=date('Y-m-d',time()+4*24*3600);
            if($info['strategy_type']==1){
                $arr['strategy_type']='T+1';
                $arr['pingcang_time']=$t1.' 09:30:00-'.$t1.' 14:49:59';
            }else{
                $arr['strategy_type']='T+5';
                $arr['pingcang_time']=$t5.' 09:30:00-'.$t5.' 14:49:59';
            }
            $arr['t_day']=$t;
            $arr['t_one_day']=$t1;
            $arr['t_five_day']=$t5;
        }
        $result6=Db::name('agreement')->insert($arr);
        if(!$result2 || !$result3 || !$result4 || !$result5 || !$result6){
            Db::rollback();
            return $this->json('', 0, '策略创建失败');
        }
        Db::commit();
        return $this->json(['strategy_num'=>$data['strategy_num']], 1, '策略创建成功');
    }
  
  	//倍数
  	public function bs(){
    
    	//策略可进行翻倍的倍数
        $list['times']=Db::name('hide')->where(['id' => 2])->value('value');
      	$list['times']=explode(',',$list['times']);
      	$list['poundage']=Db::name('hide')->where(['id' => 4])->value('value');
      	$list['bigpoundage']=Db::name('hide')->where(['id' => 8])->value('value');
      	$list['defer']=Db::name('hide')->where(['id' => 5])->value('value');
      
      	$list['credit_bs']=Db::name('hide')->where(['id' => 15])->value('value');//信用金的倍数(T+1)
      	$list['credit_min']=Db::name('hide')->where(['id' => 16])->value('value');//最低信用金(T+1)
      	$list['credit_max']=Db::name('hide')->where(['id' => 17])->value('value');//最高信用金(T+1)
      	$list['if_allow_up']=Db::name('hide')->where(['id' => 49])->value('value');//是否允许客户上调止盈 1允许 2不允许
      	$list['surplus_value']=Db::name('hide')->where(['id' => 50])->value('value');//止盈点  
      	return $this->json($list, 1, '倍数查询成功');
    
    
    }
  
  
  
  
  
  
  	//查看持仓中的策略
    public function checkStrategy() {
        //if($this->token()==100){return $this->json('', 100, '您的账号已在别的设备登陆');}

        $info = Request::instance()->param();
        $list=Db::name('strategy')
            ->where(['is_cancel_order'=>['in',[1,2]]])
            ->where(['account'=>$info['account'],'status'=>['in',[1,2,3]]])
            ->order('buy_time desc')
            ->select();

        //app按钮是否能点
        if(isHoliday()!=0){
            foreach($list as $k=>$v){
                $list[$k]['cansell']=2;
            }
        }else{
            foreach($list as $k=>$v){
                if(date('Ymd')>date('Ymd',$v['buy_time'])){
                    $list[$k]['cansell']=1;
                }else{
                    $list[$k]['cansell']=2;
                }
            }
        }

        //查询现价
        if($list){
          	$shipanstock = new Stock;
            $str='';
            foreach($list as $k=>$v){
                $str.=$v['stock_code'].',';
            }
            $str=substr($str,0,-1);
            $result=$shipanstock->Hq_real_list($str);
            $para=json_decode($result,true);
            foreach($para as $k=>$v){
                $list[$k]['nowprice']=$v['nowPri'];
            }
        }

        //显示的平仓时间
        foreach ($list as $k => $v) {
            if($v['strategy_type']==2){
                $time=0;
                for($i=1;$i<12;$i++){
                    if(isHoliday($i)==0){$time+=1;}
                    if($time==4){
                        $list[$k]['day']=date('m-d 14:50', time()+$i*24*3600);
                        break;
                    }
                    continue;
                }
            }else{
                $list[$k]['day']='下交易日14:50发起平仓请求';
            }
            $list[$k]['buy_time']=date('m-d H:i',$v['buy_time']);
          	//追加之后的信用金总和
          	$list[$k]['credit']= $v['credit']+$v['credit_add'];
          
        }

        //用户信息
        $admin=Db::name('admin')->where(['account'=>$info['account'],'is_del'=>1])->find();
        $arr=[
            //总计余额
            'all_money'=>round($admin['frozen_money']+$admin['tactics_balance'],2),
            //冻结金额
            'frozen_money'=>$admin['frozen_money'],
            //可用余额
            'can_money'=>$admin['tactics_balance'],
        ];
        return $this->json($list, 1, '查询成功',$arr);
    }
  
  
  
  
  	//查看已平仓的策略
    public function checkStrategyHistory() {
        //if($this->token()==100){return $this->json('', 100, '您的账号已在别的设备登陆');}

        $info = Request::instance()->param();
        $chengshu=Db::name('hide')->where(['id'=>3])->value('value');

        //已平仓的策略
        $list=Db::name('strategy')
            ->where(['is_cancel_order'=>1])
            ->where(['account'=>$info['account'],'status'=>4])
            ->order('sell_time desc')
            ->select();
        if($list){
            //总策略数
            $allStrategyNum=count($list);
            //盈利策略数
            $winStrategyNum=0;
            //总盈利
            $win=0;
            foreach($list as $k=>$v) {
                $list[$k]['buy_time']=date('m-d H:i',$v['buy_time']);
                $list[$k]['sell_time']=date('m-d H:i',$v['sell_time']);
                if($v['sell_price']>$v['buy_price']){
                    //交易盈亏
                    $list[$k]['getprice']=round(($v['true_getmoney']+$v['sell_poundage']-$v['credit']-$v['credit_add'])/$chengshu,2);
                    //盈利分配
                    $list[$k]['truegetprice']=$v['true_getmoney']+$v['sell_poundage']-$v['credit']-$v['credit_add'];
                    //亏损赔付
                    $list[$k]['dropprice']=0;
                    $winStrategyNum+=1;
                }else{
                    //交易盈亏
                    $list[$k]['getprice']=$v['true_getmoney']+$v['sell_poundage']-$v['credit']-$v['credit_add'];
                    //盈利分配
                    $list[$k]['truegetprice']=0;
                    //亏损赔付
                    $list[$k]['dropprice']=$v['true_getmoney']+$v['sell_poundage']-$v['credit']-$v['credit_add'];
                }
                $win += ($v['true_getmoney']+$v['sell_poundage'] - $v['credit']-$v['credit_add']) ;
            }
            $arr=[
                'allStrategyNum'=>$allStrategyNum,
                'winStrategyNum'=>$winStrategyNum,
                'win'=>$win,
                'rate'=>round(($winStrategyNum/$allStrategyNum*100),2).'%',
            ];
        }else{
            $arr=[
                'allStrategyNum'=>0,
                'winStrategyNum'=>0,
                'win'=>0,
                'rate'=>'0%',
            ];
        }
        return $this->json($list, 1, '查询成功',$arr);
    }
  
  
  
  	public function checkStrategyHistorydetails() {
       // if($this->token()==100){return $this->json('', 100, '您的账号已在别的设备登陆');}

        $info = Request::instance()->param();
        $strategy=Db::name('strategy')->where(['is_cancel_order'=>1])->where(['id'=>$info['id']])->find();
        if($strategy['sell_price']>$strategy['buy_price']){
            $chengshu=Db::name('hide')->where(['id'=>3])->value('value');
            //交易盈亏
            $strategy['getprice']=round(($strategy['true_getmoney']+$strategy['sell_poundage']-$strategy['credit']-$strategy['credit_add'])/$chengshu,2);
            //盈利分配
            $strategy['truegetprice']=$strategy['true_getmoney']+$strategy['sell_poundage']-$strategy['credit']-$strategy['credit_add'];
            //亏损赔付
            $strategy['dropprice']=0;
        }else{
            //交易盈亏
            $strategy['getprice']=$strategy['true_getmoney']+$strategy['sell_poundage']-$strategy['credit']-$strategy['credit_add'];
            //盈利分配
            $strategy['truegetprice']=0;
            //亏损赔付
            $strategy['dropprice']=$strategy['true_getmoney']+$strategy['sell_poundage']-$strategy['credit']-$strategy['credit_add'];
        }
        $strategy['day']=(strtotime(date('Ymd',$strategy['sell_time']))-strtotime(date('Ymd',$strategy['buy_time'])))/24/3600;
        $strategy['buy_time']=date('m-d H:i',$strategy['buy_time']);
        $strategy['sell_time']=date('m-d H:i',$strategy['sell_time']);
        return $this->json($strategy, 1, '查询成功');
    }
  
  
  	//申请递延
    public function changeDefer() {
        //if($this->token()==100){return $this->json('', 100, '您的账号已在别的设备登陆');}

        $info = Request::instance()->param();

        $strategy=Db::name('strategy')->where(['is_cancel_order'=>1])->where(['id'=>$info['id']])->find();

        $day=0;//工作日天数
        $times=1;//休息天数
        //t+1的时间为1天，t+5的时间为4天
        if($strategy['strategy_type']==1){
            $free=1;
        }else{
            $free=4;
        }
        //当前时间到买入的时间的天数
        $all=(strtotime(date('Ymd'))-strtotime(date('Ymd',$strategy['buy_time'])))/24/3600;
        for($i=1;$i<=$all;$i++){
            if(isHoliday($i)!=0){
                $times+=1;
            }else{
                $day+=1;
            }
            if($day>=$free){
                break;
            }else{
                continue;
            }
        }
        if(isHoliday()!=0){
            $data['rest_day']=$times-1+1;
        }else{
            $data['rest_day']=$times-1;
        }

        $data['defer']=1;
        Db::name('strategy')->where(['is_cancel_order'=>1])->where(['id'=>$info['id']])->update($data);

        //file_put_contents('shenqing.txt','买入时间：'.date('Y-m-d H:i:s',$strategy['buy_time']).
        //    '，申请时间:'.date('Y-m-d H:i:s')."，休息时间为".$data['rest_day']."天\n", FILE_APPEND);
        return $this->json('', 1, '申请递延成功');
    }
  
  
  	//策略发起平仓，卖出股票（改变状态）
    public function sellStrategy() {
        //是否限制股票交易时间
        $hide=Db::name('hide')->where(['id'=>29])->value('value');
        if($hide==1){
            if (isHoliday()!=0 || (date('Hi', time()) < 930 || ((date('Hi', time()) > 1130) && (date('Hi', time()) < 1300)) || date('Hi', time()) > 1455)) {
                return $this->json('', 0, '非交易时间禁止创建');
            }
        }else{
            if(date('Hi', time())>830 && date('Hi', time())<930){
                return $this->json('', 0, '非交易时间禁止创建');
            }
        }

        $info = Request::instance()->param();
        //策略信息
        $strategy=Db::name('strategy')->where(['is_cancel_order'=>1])->where(['id'=>$info['id']])->find();

        if($strategy['status']>2){
            return $this->json('', 0, '此策略正在卖出');
        }

        if(date('Ymd',time())<=date('Ymd',$strategy['buy_time'])){
            return $this->json('', 0, '当天禁止卖出策略');
        }

        if($this->thischeckstockstop($strategy['stock_code'])!=200){
            return $this->json('', 0, '股票已被风控，无法买卖');
        }

        //实盘委托
        if($strategy['is_real_disk']==1){
            $shipanstock=new Stock();
            //实盘账户信息
            //$shipaninfo=$this->getshipaninfo(substr($strategy['stock_code'],0,2));
          	//实盘账户信息
            $shipaninfo=$this->getshipaninfo($strategy['shipan_account'],substr($strategy['stock_code'],0,2));
            //实盘进行委托
            $success=$shipanstock->sendorder($shipaninfo['ip'],$shipaninfo['version'],$shipaninfo['yybID'],$shipaninfo['zhanghao'],
                $shipaninfo['zhanghao'],$shipaninfo['mima'],$shipaninfo['port'],$shipaninfo['txmima'],1,$shipaninfo['PriceType'],$shipaninfo['gudongdaima'],
                substr($strategy['stock_code'],2),0,$strategy['stock_number']);
            $success=json_decode($success,true);
            //有委托编号这个参数名和值，说明委托成功了
            if(array_key_exists('委托编号',$success['data1'])&&$success['data1']['委托编号']){
                //卖出合同编号
                $data['sell_contract_num']=$success['data1']['委托编号'];
            }else{
                //file_put_contents('shipan.txt','委托时间:'.date('Y-m-d H:i:s').
                //    "    实盘中的策咯".$strategy['strategy_num']."在实盘想卖出，但是委托失败了\n", FILE_APPEND);
                $log=[
                    'type'=>'5',
                    'content'=>"实盘中的策咯".$strategy['strategy_num']."在实盘想卖出，但是委托失败了",
                    'create_time'=>date('Y-m-d H:i:s'),
                ];
                Db::name('log')->insert($log);
                return $this->json('', 0, '委托失败');
            }
        }

        $data['status']=3;
        $data['sell_type']=1;
        $data['sell_time']=time();

        //修改策略记录
        $res=Db::name('strategy')->where(['is_cancel_order'=>1])->where(['id'=>$info['id']])->update($data);
        if($res){
            return $this->json('', 1, '平仓申请成功');
        }else{
            return $this->json('', 0, '平仓申请失败');
        }
    }
  
  
  	//创建炒股大赛策略
    public function createStrategyContest() {
        //是否限制股票交易时间
        $hide=Db::name('hide')->where(['id'=>29])->value('value');
        if($hide==1){
            if (isHoliday()!=0 || (date('Hi', time()) < 930 || ((date('Hi', time()) > 1130) && (date('Hi', time()) < 1300)) || date('Hi', time()) > 1455)) {
                return $this->json('', 0, '非交易时间禁止创建');
            }
        }else{
            if(date('Hi', time())>830 && date('Hi', time())<930){
                return $this->json('', 0, '非交易时间禁止创建');
            }
        }

        $info = Request::instance()->param();

        //用户信息
        $admin=Db::name('admin')->where(['account' => $info['account'],'is_del'=>1])->find();
        if($admin['analog_money']<$info['stock_number']*$info['buy_price']){
            return $this->json('', 0, '炒股大赛余额不足');
        }
        //股数不能为空
        if(!$info['stock_number']){
            return $this->json('', 0, '股数不能为空');
        }

        //股票已停牌
        if($this->thischeckstockstop($info['stock_code'])!=200){
            return $this->json('', 0, '股票已被风控，无法买卖');
        }

        //股票当前价格
        $json=$this->getOneStock2($info['stock_code']);
        $nowprice=json_decode($json,true)['list']['result'][0]['data']['nowPri'];

        //创建策略
        $time=date('YmdHis', time());
        $data=[
            'strategy_num'=>$time.rand(1000,9999),
            'account'=>$info['account'],
            'status'=>2,
            'stock_name'=>$info['stock_name'],
            'stock_code'=>$info['stock_code'],
            'stock_number'=>$info['stock_number'],
            'credit'=>$info['credit'],
            'buy_price'=>$nowprice,
            'surplus_value'=>$info['surplus_value'],
            'loss_value'=>$info['loss_value'],
            'buy_time'=>time(),
        ];

        Db::startTrans();
        //添加策略记录
        $result2=Db::name('strategy_contest')->insert($data);
        //修改用户策略余额和冻结金额
        $result3=Db::name('admin')->where(['account' => $info['account'],'is_del'=>1])->setDec('analog_money',$data['credit']);
        $result4=Db::name('admin')->where(['account' => $info['account'],'is_del'=>1])->setInc('frozen_analog_money',$data['credit']);
        if(!$result2 || !$result3 || !$result4){
            Db::rollback();
            return $this->json('', 0, '策略创建失败');
        }
        Db::commit();
        return $this->json('', 1, '策略创建成功');
    }
  
  
  	//搜索界面查询数据
    public function getOneStock2($gid) {
        
      	$stock = new Stock;
        $list = $stock->Hq_real($gid);
        //$arr=explode('~',iconv("gb2312", "utf-8//IGNORE",$arr));
        //返回的是json字符串
      	$list = json_decode($list,true);


        return json($list, 1, '查询成功');
    }
  	
  
  
  	//修改止损
    public function updateLossValue() {
        //if($this->token()==100){return $this->json('', 100, '您的账号已在别的设备登陆');}

        $info = Request::instance()->param();
        if($info['trues']==1){
            $loss_value=Db::name('strategy')->where(['is_cancel_order'=>1])->where(['id' => $info['id']])->value('loss_value');
            Db::name('strategy')->where(['is_cancel_order'=>1])->where(['id' => $info['id']])->update(['loss_value'=>$info['loss_value']]);
        }else{
            $loss_value=Db::name('strategy_contest')->where(['id' => $info['id']])->value('loss_value');
            Db::name('strategy_contest')->where(['id' => $info['id']])->update(['loss_value'=>$info['loss_value']]);
        }
        //file_put_contents('chedan_zhisun.txt','时间:'.date('Y-m-d H:i:s') ."    止损价格由".$loss_value.'变成'.$info['loss_value']."\n", FILE_APPEND);
        return $this->json('', 1, '修改止损成功');
    }
  
  
  
  	//股票进行模糊搜索
    public function search_stock_api(){
        $info = Request::instance()->param();

        $stock_new = new Stock;

        //可买股票类型
        $canbuy=Db::name('hide')->where(['id'=>23])->value('value');//sz00,sz30,sh60
        $canbuyarr=explode(',',$canbuy);
        //不可买股票类型
        $cannotbuy=Db::name('hide')->where(['id'=>24])->value('value');//st,pt
        $cannotbuyarr=explode(',',$cannotbuy);
        //不可买的具体股票
        $forbidden_list = Db::name('forbidden_list')->where('is_del',1)->select();

        $stock=[];
        $str = $stock_new->Tosearch($info['content']);
      	$str=iconv("gb2312", "utf-8//IGNORE",$str);
        $list=explode(';',explode("\"",$str)[1]);
        foreach($list as $k=>$v){
            $stockcode=explode(',',$v)[3];
            $stockname=explode(',',$v)[4];
            //排除A股之外的基金期货等
            if(strlen($stockcode)==8){
                if($canbuy){
                    //包含可买股票则可以购买
                    $result1=0;
                    foreach($canbuyarr as $kk=>$vv){
                        if(strpos($stockcode,$vv)||strpos($stockcode,$vv)===0){
                            $result1=1;
                        }
                    }
                }else{
                    //可买股票类型为空，全都能买
                    $result1=1;
                }
                if($cannotbuy){
                    //包含不可买股票则不可以购买
                    $result2=1;
                    foreach($cannotbuyarr as $kk=>$vv){
                        if(strpos($stockcode,$vv)||strpos($stockcode,$vv)===0){
                            $result2=0;
                        }
                    }
                }else{
                    //不可买股票类型为空，全都能买
                    $result2=1;
                }
                if($forbidden_list){
                    $result3=1;
                    foreach($forbidden_list as $kk=>$vv){
                        if($stockcode==$vv['stock_code']){
                            $result3=0;
                        }
                    }
                }else{
                    $result3=1;
                }
                //两种规则都满足则可以购买
                if($result1==1&&$result2==1&&$result3==1){
                    $stock[]=[
                        'stock_code'=>$stockcode,
                        'stock_name'=>$stockname,
                    ];
                }
            }
        }

        if($stock){
            $code='';
            foreach($stock as $k=>$v){
                $code.=$v['stock_code'].',';
            }
           
          	if(date('Hi')>930){
            	$result=$stock_new->hq_list(substr($code,0,-1));
                $pp=explode(';',$result);
                unset($pp[count($pp)-1]);
                foreach($pp as $k=>$v){
                    $stock[$k]['nowprice']=round(explode(',',$v)[3],2);
                    $stock[$k]['rate_price']=round(explode(',',$v)[3]-explode(',',$v)[2],2);
                    $stock[$k]['rate']=(round((explode(',',$v)[3]-explode(',',$v)[2])/explode(',',$v)[2],4)*100).'%';
                    if(!$stock[$k]['rate_price']){
                        unset($stock[$k]);
                    }
                }
            }else{
            	$result=$stock_new->Hq_real_list(substr($code,0,-1));
                $arr=json_decode($result,true);
                foreach($arr as $k=>$v){
                    $stock[$k]['nowprice']=round($v['nowPri'],2);
                    $stock[$k]['rate_price']=round($v['nowPri']-$v['closePrice'],2);
                    $stock[$k]['rate']=(round(($v['nowPri']-$v['closePrice'])/$v['closePrice'],4)*100).'%';
                    if(!$stock[$k]['rate_price']){
                        unset($stock[$k]);
                    }
                }
            }
            $stock=array_values($stock);
        }

        return $this->json($stock, 1, '查询成功');
    }
  
  
  
  
  	//炒股大赛的策略发起平仓，卖出股票
    public function sellStrategyContest() {
        //是否限制股票交易时间
        $hide=Db::name('hide')->where(['id'=>29])->value('value');
        if($hide==1){
            if (isHoliday()!=0 || (date('Hi', time()) < 930 || ((date('Hi', time()) > 1130) && (date('Hi', time()) < 1300)) || date('Hi', time()) > 1455)) {
                return $this->json('', 0, '非交易时间禁止创建');
            }
        }else{
            if(date('Hi', time())>830 && date('Hi', time())<930){
                return $this->json('', 0, '非交易时间禁止创建');
            }
        }

        $info = Request::instance()->param();
        //策略信息
        $strategy=Db::name('strategy_contest')->where(['id'=>$info['id']])->find();

        if($strategy['status']!=2){
            return $this->json('', 0, '此策略正在卖出');
        }

        if(date('Ymd',time())<=date('Ymd',$strategy['buy_time'])){
            return $this->json('', 0, '当天禁止卖出策略');
        }

        //实际股票
        $json=$this->getOneStock2($strategy['stock_code']);

        //股票当前价格
        $nowprice=json_decode($json,true)['list']['result'][0]['data']['nowPri'];

        //修改策略
        $data=[
            'status'=>4,
            'sell_price'=>$nowprice,
            'sell_time'=>time(),
            'sell_type'=>1,
        ];

        Db::startTrans();
        //修改策略记录
        $result1=Db::name('strategy_contest')->where(['id'=>$info['id']])->update($data);
        //修改用户策略余额和冻结金额
        $result2=Db::name('admin')->where(['account' => $strategy['account'],'is_del'=>1])->setInc('analog_money',($nowprice-$strategy['buy_price'])*$strategy['stock_number']+$strategy['credit']);
        $result3=Db::name('admin')->where(['account' => $strategy['account'],'is_del'=>1])->setDec('frozen_analog_money',$strategy['credit']);

        if(!$result1 || !$result2 || !$result3){
            Db::rollback();
            return $this->json('', 0, '平仓失败');
        }
        Db::commit();
        return $this->json('', 1, '平仓成功');
    }
  
  
  
  
  
  
  
  	//查看其余人的战绩
    public function subscribeInfo() {
       // if($this->token()==100){return $this->json('', 100, '您的账号已在别的设备登陆');}

        $times=1;
        for($i=1;$i<12;$i++){
            if(isHoliday($i)!=0){
                $times+=1;
            }else{
                break;
            }
            continue;
        }

        $day['sell_time']=['between',[strtotime(date('Y-m-d'))-$times*24*3600,strtotime(date('Y-m-d'))]];
        $week['sell_time']=['between',[strtotime(date('Y-m-d'))-(date('N')-1)*24*3600-7*24*3600,strtotime(date('Y-m-d'))-(date('N')-1)*24*3600]];
        $month['sell_time']=['between',[strtotime(date('Y-m'))-30*24*3600,strtotime(date('Y-m'))]];

//        $day['sell_time']=['egt',strtotime(date('Y-m-d'))];
//        $week['sell_time']=['egt',strtotime(date('Y-m-d'))-(date('N')-1)*24*3600];
//        $month['sell_time']=['egt',strtotime(date('Y-m'))];

        $info = Request::instance()->param();
      	$info['subscribe_account']=Db::name('admin')->where('id',$info['aid'])->value('account');
        //订阅的人的策略  true为1查询真实策略，2查询炒股大赛
        if($info['trues']==1){
            $list['day']=$this->shouyiInfo($info['subscribe_account'],$day);
            $list['week']=$this->shouyiInfo($info['subscribe_account'],$week);
            $list['month']=$this->shouyiInfo($info['subscribe_account'],$month);
            $list['all']=$this->shouyiInfo($info['subscribe_account']);
        }else{
            $list['day']=$this->shouyiInfoContest($info['subscribe_account'],$day);
            $list['week']=$this->shouyiInfoContest($info['subscribe_account'],$week);
            $list['month']=$this->shouyiInfoContest($info['subscribe_account'],$month);
            $list['all']=$this->shouyiInfoContest($info['subscribe_account']);
        }

        //此会员的会员信息
        $admin=Db::name('admin')->where(['account' => $info['subscribe_account'],'is_del'=>1])->find();
        if($admin['nick_name']){
            $list['nick_name']=$admin['nick_name'];
        }else{
            $list['nick_name']=$admin['account'];
        }
        $list['headurl']=$admin['headurl'];

        //总排名
        $ranking=$this->ranking();
        foreach($ranking as $k=>$v){
            if(($v['subscribe_account']==$info['subscribe_account'])&&($v['trues']==$info['trues'])){
                $list['ranking']=$k+1;
            }
        }

        //判断我是否订阅此人
        $subscribe = Db::name('subscribe')
            ->where(['account'=>$info['account'],'subscribe_account' => $info['subscribe_account'],'trues'=>$info['trues'],'is_del'=>1])
            ->find();
        if($subscribe){
            $list['is_subscribe']=1;
        }else{
            $list['is_subscribe']=2;
        }
        //被订阅数
        $list['subscribe_sum'] = Db::name('subscribe')->where(['subscribe_account' => $info['subscribe_account'],'is_del'=>1])->count();

        //战绩图
        $war[date('Ymd')-6]=[];
        $war[date('Ymd')-5]=[];
        $war[date('Ymd')-4]=[];
        $war[date('Ymd')-3]=[];
        $war[date('Ymd')-2]=[];
        $war[date('Ymd')-1]=[];
        $war[date('Ymd')]=[];

        //策略创建的历史记录
        if($info['trues']==1){
            $store=Db::name('strategy')
                ->where(['is_cancel_order'=>1])
                ->where(['account' => $info['subscribe_account'],'status'=>['in',[3,4]]])
                ->order('sell_time desc')
                ->select();
            if($store){
                //用户卖出股票时所能拿到的成数
                $chengshu=Db::name('hide')->where(['id' => 3])->value('value');
                foreach($store as $k=>$v){
                    //策略创建的记录
                    $list['record'][$k]=$v;
                    if($v['true_getmoney']>$v['credit']){
                        //交易盈亏
                        $list['record'][$k]['getprice']=round(($v['true_getmoney']-$v['credit'])/$chengshu,2);
                        //盈利分配
                        $list['record'][$k]['truegetprice']=$v['true_getmoney']-$v['credit'];
                        //亏损赔付
                        $list['record'][$k]['dropprice']=0;
                    }else{
                        //交易盈亏
                        $list['record'][$k]['getprice']=$v['true_getmoney']-$v['credit'];
                        //盈利分配
                        $list['record'][$k]['truegetprice']=0;
                        //亏损赔付
                        $list['record'][$k]['dropprice']=$v['true_getmoney']-$v['credit'];
                    }
                    //战绩图
                    if($v['sell_time']<strtotime(date('Ymd')-5)){
                        $war[date('Ymd')-6][]=$v;
                    }
                    if($v['sell_time']<strtotime(date('Ymd')-4)){
                        $war[date('Ymd')-5][]=$v;
                    }
                    if($v['sell_time']<strtotime(date('Ymd')-3)){
                        $war[date('Ymd')-4][]=$v;
                    }
                    if($v['sell_time']<strtotime(date('Ymd')-2)){
                        $war[date('Ymd')-3][]=$v;
                    }
                    if($v['sell_time']<strtotime(date('Ymd')-1)){
                        $war[date('Ymd')-2][]=$v;
                    }
                    if($v['sell_time']<strtotime(date('Ymd'))){
                        $war[date('Ymd')-1][]=$v;
                    }
                    if($v['sell_time']<time()){
                        $war[date('Ymd')][]=$v;
                    }
                }
                //战绩图
                //收益，平台分成后的钱（包括信用金）
                $shouyi=0;
                //信用金（交易本金）
                $credit=0;
                foreach($war as $k=>$v){
                    $list['war'][$k]['time']=$k;
                    if($v){
                        foreach($v as $kk=>$vv){
                            $credit+=$vv['credit'];
                            $shouyi+=($vv['true_getmoney']-$vv['credit']);
                        }
                        //收益率
                        $list['war'][$k]['income']=(round($shouyi/$credit*100,2)).'%';
                    }else{
                        $list['war'][$k]['income']='0%';
                    }
                }
                $list['war']=array_values($list['war']);

            }else{
                //没有策略创建的历史记录
                $list['record']=[];
            }
        }else{
            $store=Db::name('strategy_contest')
                ->where(['account' => $info['subscribe_account'],'status'=>['in',[3,4]]])
                ->order('sell_time desc')
                ->select();
            if($store){
                foreach($store as $k=>$v){
                    $list['record'][$k]=$v;
                    if($v['sell_price']>=$v['buy_price']){
                        //交易盈亏
                        $list['record'][$k]['getprice']=($v['sell_price']-$v['buy_price'])*$v['stock_number'];
                        //盈利分配
                        $list['record'][$k]['truegetprice']=($v['sell_price']-$v['buy_price'])*$v['stock_number'];
                        //亏损赔付
                        $list['record'][$k]['dropprice']=0;
                    }else{
                        //交易盈亏
                        $list['record'][$k]['getprice']=($v['sell_price']-$v['buy_price'])*$v['stock_number'];
                        //盈利分配
                        $list['record'][$k]['truegetprice']=0;
                        //亏损赔付
                        $list['record'][$k]['dropprice']=($v['sell_price']-$v['buy_price'])*$v['stock_number'];
                    }
                    //战绩图
                    if($v['sell_time']<strtotime(date('Ymd')-5)){
                        $war[date('Ymd')-6][]=$v;
                    }
                    if($v['sell_time']<strtotime(date('Ymd')-4)){
                        $war[date('Ymd')-5][]=$v;
                    }
                    if($v['sell_time']<strtotime(date('Ymd')-3)){
                        $war[date('Ymd')-4][]=$v;
                    }
                    if($v['sell_time']<strtotime(date('Ymd')-2)){
                        $war[date('Ymd')-3][]=$v;
                    }
                    if($v['sell_time']<strtotime(date('Ymd')-1)){
                        $war[date('Ymd')-2][]=$v;
                    }
                    if($v['sell_time']<strtotime(date('Ymd'))){
                        $war[date('Ymd')-1][]=$v;
                    }
                    if($v['sell_time']<time()){
                        $war[date('Ymd')][]=$v;
                    }
                }
                //战绩图
                //收益，平台分成后的钱（包括信用金）
                $shouyi=0;
                //信用金（交易本金）
                $credit=0;
                foreach($war as $k=>$v){
                    $list['war'][$k]['time']=$k;
                    if($v){
                        foreach($v as $kk=>$vv){
                            $credit+=$vv['credit'];
                            $shouyi+=($vv['sell_price']-$vv['buy_price'])*$vv['stock_number'];
                        }
                        //收益率
                        $list['war'][$k]['income']=(round($shouyi/$credit*100,2)).'%';
                    }else{
                        $list['war'][$k]['income']='0%';
                    }
                }
                $list['war']=array_values($list['war']);
            }else{
                //没有炒股大赛创建的历史记录
                $list['record']=[];
            }
        }
        if($list['record']){
            foreach ($list['record'] as $k => $v) {
                $list['record'][$k]['buy_time']=date('Y-m-d',$v['buy_time']);
                $list['record'][$k]['sell_time']=date('Y-m-d',$v['sell_time']);
            }
        }

        //策略创建的持股记录
        if($info['trues']==1){
            $buy_store=Db::name('strategy')
                ->where(['is_cancel_order'=>1])
                ->where(['account' => $info['subscribe_account'],'status'=>['in',[1,2]]])
                ->order('buy_time desc')
                ->select();
            if($buy_store){
                //查询现价
                $str='';
                foreach($buy_store as $k=>$v){
                    $str.=$v['stock_code'].',';
                }
                $str=substr($str,0,-1);

                $result=file_get_contents("http://hq.sinajs.cn/list=".$str);
                $para=explode(';',$result);
                unset($para[count($para)-1]);
                foreach($para as $k=>$v){
                    $buy_store[$k]['nowprice']=explode(',',$v)[3];
                }

                foreach($buy_store as $k=>$v){
                    $list['buy_record'][$k]=$v;
                    $list['buy_record'][$k]['buy_time']=date('m-d',$v['buy_time']);
                    //策略盈亏(收益)
                    $list['buy_record'][$k]['income']=($v['nowprice']-$v['buy_price'])*$v['stock_number'];
                }

            }else{
                //没有策略创建的持股记录
                $list['buy_record']=[];
            }
        }else{
            $buy_store=Db::name('strategy_contest')->where(['account' => $info['subscribe_account'],'status'=>['in',[1,2]]])->order('buy_time desc')->select();
            if($buy_store){
                //查询现价
                $str='';
                foreach($buy_store as $k=>$v){
                    $str.=$v['stock_code'].',';
                }
                $str=substr($str,0,-1);

                $result=file_get_contents("http://hq.sinajs.cn/list=".$str);
                $para=explode(';',$result);
                unset($para[count($para)-1]);
                foreach($para as $k=>$v){
                    $buy_store[$k]['nowprice']=explode(',',$v)[3];
                }

                foreach($buy_store as $k=>$v){
                    $list['buy_record'][$k]=$v;
                    $list['buy_record'][$k]['buy_time']=date('m-d',$v['buy_time']);
                    //策略盈亏(收益)
                    $list['buy_record'][$k]['income']=($v['nowprice']-$v['buy_price'])*$v['stock_number'];
                }
            }else{
                //没有炒股大赛创建的持股记录
                $list['buy_record']=[];
            }
        }
      	$list['nick_name']=substr($list['nick_name'],0,3).'****'.substr($list['nick_name'],7,4);


        return $this->json($list, 1, '查询成功');
    }
  
  
  
  	//获取排行榜的人员，策略收益
    public function ranking($where=[]) {
        $where['status']=['in',[3,4]];
        //信息是否开放(排行榜是否看见)的会员
        $admin=Db::name('admin')->where(['is_open_ranking'=>1,'is_del'=>1])->select();

        //真实策略
        $strategy=Db::name('strategy')->where(['is_cancel_order'=>1])->where($where)->select();
        $list=[];
        $arr=[];
        //获取到一级为会员，二级为他的策略的多维数组
        foreach($admin as $k=>$v){
            foreach($strategy as $kk=>$vv){
                if($v['account']==$vv['account']){
                    $list[$k][$kk]=$vv;
                    //subscribe_account为获得详情的参数，为了与我的订阅中跳转参数一致
                    $list[$k][$kk]['subscribe_account']=$vv['account'];
                    //用户头像
                    $list[$k][$kk]['headurl']=$v['headurl'];
                }
            }
        }
        //真实的策略
        if($list){
            //对数组的里层进行重新排序，改变key
            foreach($list as $k=>$v){
                $list[$k]=array_values($v);
            }
            //获取到收益率和胜率的新数组，真实策略为1，名字为手机号，炒股大赛为2，名字为昵称
            foreach($list as $k=>$v){
                $arr[$k]['trues']='1';
                //subscribe_account为获得详情的参数，为了与我的订阅中跳转参数一致
                $arr[$k]['subscribe_account']=$v[0]['subscribe_account'];
                //用户头像
                $arr[$k]['headurl']=$v[0]['headurl'];
                $arr[$k]['username']=$v[0]['account'];
                //收益，平台分成后的钱（包括信用金）
                $shouyi=0;
                //信用金（交易本金）
                $credit=0;
                //盈利次数
                $win=0;
                foreach($v as $kk=>$vv){
                    $credit+=$vv['credit'];
                    $shouyi+=($vv['true_getmoney']-$vv['credit']);
                    //计算胜率
                    if($vv['true_getmoney']>$vv['credit']){$win+=1;}
                }
                //收益率
                $arr[$k]['profit']=(round($shouyi/$credit*100,2));
                //胜率
                $arr[$k]['win']=(round($win/count($v)*100,2)).'%';
            }
        }

        //炒股大赛策略
        $strategy_contest=Db::name('strategy_contest')->where($where)->select();
        $list_contest=[];
        $arr_contest=[];
        //获取到一级为会员，二级为他的策略的多维数组
        foreach($admin as $k=>$v){
            foreach($strategy_contest as $kk=>$vv){
                if($v['account']==$vv['account']){
                    $list_contest[$k][$kk]=$vv;
                    $list_contest[$k][$kk]['nick_name']=$v['nick_name'];
                    //subscribe_account为获得详情的参数，为了与我的订阅中跳转参数一致
                    $list_contest[$k][$kk]['subscribe_account']=$vv['account'];
                    //用户头像
                    $list_contest[$k][$kk]['headurl']=$v['headurl'];
                }
            }
        }
        //炒股大赛的策略
        if($list_contest){
            //对数组的里层进行重新排序，改变key
            foreach($list_contest as $k=>$v){
                $list_contest[$k]=array_values($v);
            }
            //获取到收益率和胜率的新数组，真实策略为1，名字为手机号，炒股大赛为2，名字为昵称(没有的话为手机号)
            foreach($list_contest as $k=>$v){
                $arr_contest[$k]['trues']='2';
                //subscribe_account为获得详情的参数，为了与我的订阅中跳转参数一致
                $arr_contest[$k]['subscribe_account']=$v[0]['subscribe_account'];
                //用户头像
                $arr_contest[$k]['headurl']=$v[0]['headurl'];
                if($v[0]['nick_name']){
                    $arr_contest[$k]['username']=$v[0]['nick_name'];
                }else{
                    $arr_contest[$k]['username']=$v[0]['account'];
                }
                //收益，(卖出价-买入价)*股数
                $shouyi=0;
                //信用金
                $credit=0;
                //盈利次数
                $win=0;
                foreach($v as $kk=>$vv){
                    $credit+=$vv['credit'];
                    $shouyi+=($vv['sell_price']-$vv['buy_price'])*$vv['stock_number'];
                    //计算胜率
                    if($vv['sell_price']>$vv['buy_price']){$win+=1;}
                }
                //收益率
                $arr_contest[$k]['profit']=(round($shouyi/$credit*100,2));
                //胜率
                $arr_contest[$k]['win']=(round($win/count($v)*100,2)).'%';
            }
        }
        //数组合并
        $para=array_merge($arr,$arr_contest);

        //排序
        array_multisort(array_column($para,'profit'),SORT_DESC,$para);

        //收益率加上%
        foreach ($para as $k => $v) {
            if($v['profit']<0){
                unset($para[$k]);
            }else{
                $para[$k]['id']=$k+1;
                $para[$k]['profit']=$v['profit'].'%';
            }
        }

        if(count($para)>10){
            foreach ($para as $k => $v) {
                if($k>=10){unset($para[$k]);}
            }
        }

        return $para;
    }
  
  
  
  	//计算某个会员真实策略里的收益率
    public function shouyiInfo($account,$where=[]) {
        $where['account']=$account;
        $where['status']=['in',[3,4]];
        $store=Db::name('strategy')->where(['is_cancel_order'=>1])->where($where)->select();
        if($store){
            //收益，平台分成后的钱（包括信用金）
            $shouyi=0;
            //信用金（交易本金）
            $credit=0;
            foreach($store as $k=>$v){
                $credit+=$v['credit'];
                $shouyi+=($v['true_getmoney']-$v['credit']);
            }
            //收益率
            return (round($shouyi/$credit*100,2)).'%';
        }else{
            return '0%';
        }
    }
  
  
  	//计算某个会员炒股大赛里的收益率
    public function shouyiInfoContest($account,$where=[]) {
        $where['account']=$account;
        $where['status']=['in',[3,4]];
        $store=Db::name('strategy_contest')->where($where)->select();
        if($store){
            //收益，(卖出价-买入价)*股数
            $shouyi=0;
            //信用金，市值，是一样的
            $credit=0;
            foreach($store as $k=>$v){
                $credit+=$v['credit'];
                $shouyi+=($v['sell_price']-$v['buy_price'])*$v['stock_number'];
            }
            //收益率
            return (round($shouyi/$credit*100,2)).'%';
        }else{
            return '0%';
        }
    }
  
  
  
  	//订阅和取消订阅
    public function createSubscribe() {
        //if($this->token()==100){return $this->json('', 100, '您的账号已在别的设备登陆');}

        $info = Request::instance()->param();
      
      
      	//换成aid
      	$info['subscribe_account'] = Db::name('admin')->where('id',$info['aid'])->value('account');

        //订阅
        if($info['is_subscribe']==1){
            $data=[
                'account'=>$info['account'],
                'subscribe_account'=>$info['subscribe_account'],
                'trues'=>$info['trues'],
                'subscribe_nick_name'=>$info['subscribe_nick_name'],
                'is_del'=>1,
                'create_time'=>time(),
            ];
            Db::name('subscribe')->insert($data);

            //进行订阅推送
            $account=Db::name('settings')
                ->where(['account' => $info['subscribe_account'],'is_del'=>1])
                ->find();
            $content='有一位会员关注了您，请注意查看哦';
           // if($account['subscribe_push']==1){
               // $this->push($account['account'],$content,4);
          //  }else{
           //     $this->push($account['account'],$content,4,1);
           // }

            return $this->json('', 1, '关注成功');
            //取消订阅
        }elseif($info['is_subscribe']==2){
            $where=[
                'account'=>$info['account'],
                'subscribe_account'=>$info['subscribe_account'],
                'trues'=>$info['trues'],
            ];
            Db::name('subscribe')->where($where)->update(['is_del'=>2]);
            return $this->json('', 1, '取消关注成功');
        }
    }
  
  
  
  	//我的订阅
    public function mysubscribe() {
       // if($this->token()==100){return $this->json('', 100, '您的账号已在别的设备登陆');}

        $info = Request::instance()->param();
        $list = Db::name('subscribe')->where(['account' => $info['account'],'is_del'=>1])->select();
        foreach($list as $k=>$v){
            //true为1查询真实策略，2查询炒股大赛
            if($v['trues']==1){
                $store=Db::name('strategy')
                    ->where(['is_cancel_order'=>1])
                    ->where(['account' => $v['subscribe_account'],'status'=>['in',[3,4]]])
                    ->select();
                if($store){
                    //收益，平台分成后的钱（包括信用金）
                    $shouyi=0;
                    //信用金（交易本金）
                    $credit=0;
                    foreach($store as $kk=>$vv){
                        $credit+=$vv['credit'];
                        $shouyi+=($vv['true_getmoney']-$vv['credit']);
                    }
                    //收益率
                    $list[$k]['profit']=(round($shouyi/$credit*100,2)).'%';
                }else{
                    $list[$k]['profit']='0%';
                }
                $list[$k]['headurl']=Db::name('admin')->where(['account' => $v['subscribe_account'],'is_del'=>1])->value('headurl');
            }else{
                $store=Db::name('strategy_contest')->where(['account' => $v['subscribe_account'],'status'=>['in',[3,4]]])->select();
                if($store){
                    //收益，(卖出价-买入价)*股数
                    $shouyi=0;
                    //信用金
                    $credit=0;
                    foreach($store as $kk=>$vv){
                        $credit+=$vv['credit'];
                        $shouyi+=($vv['sell_price']-$vv['buy_price'])*$vv['stock_number'];
                    }
                    //收益率
                    $list[$k]['profit']=(round($shouyi/$credit*100,2)).'%';
                }else{
                    $list[$k]['profit']='0%';
                }
                $list[$k]['headurl']=Db::name('admin')->where(['account' => $v['subscribe_account'],'is_del'=>1])->value('headurl');
            }
        }
      	foreach($list as $k=>$v){
          	if($list[$k]['subscribe_nick_name']==$list[$k]['subscribe_account']){
            	$list[$k]['subscribe_nick_name']=substr($v['subscribe_nick_name'],0,3).'****'.substr($v['subscribe_nick_name'],7,4);
            }
			
          	$list[$k]['aid'] = Db::name('admin')->where('account',$v['subscribe_account'])->value('id');
		}
      	
        return $this->json($list, 1, '查询成功');
    }

  	public function dayrankingList() {
        $times=1;
        for($i=1;$i<12;$i++){
            if(isHoliday($i)!=0){
                $times+=1;
            }else{
                break;
            }
            continue;
        }

        $day['sell_time']=['between',[strtotime(date('Y-m-d'))-$times*24*3600,strtotime(date('Y-m-d'))]];
        $list=$this->ranking($day);
      	foreach($list as $k=>$v){
          	preg_match("/^1\d{10}$/", $v['username']) ? $list[$k]['username']=substr($v['username'],0,3).'****'.substr($v['username'],7,4) : $list[$k]['username']=substr($v['username'],0,3).'****'.substr($v['username'],6);
			preg_match("/^1\d{10}$/", $v['subscribe_account']) ? $list[$k]['subscribe_account']=substr($v['subscribe_account'],0,3).'****'.substr($v['subscribe_account'],7,4) : $list[$k]['subscribe_account']=substr($v['subscribe_account'],0,3).'****'.substr($v['subscribe_account'],6);
          	$nick_name=Db::name('admin')->where('account',$v['subscribe_account'])->value('nick_name');
          	preg_match("/^1\d{10}$/", $nick_name) ? $list[$k]['nick_name']=substr($nick_name,0,3).'****'.substr($nick_name,7,4) : $list[$k]['nick_name']=substr($nick_name,0,3).'****'.substr($nick_name,6);
		}
        return $this->json($list, 1, '查询成功');
    }
    public function weekrankingList() {
        $week['sell_time']=['between',[strtotime(date('Y-m-d'))-(date('N')-1)*24*3600-7*24*3600,strtotime(date('Y-m-d'))-(date('N')-1)*24*3600]];
        $list=$this->ranking($week);
      	foreach($list as $k=>$v){
			preg_match("/^1\d{10}$/", $v['subscribe_account']) ? $list[$k]['subscribe_account']=substr($v['subscribe_account'],0,3).'****'.substr($v['subscribe_account'],7,4) : $list[$k]['subscribe_account']=substr($v['subscribe_account'],0,3).'****'.substr($v['subscribe_account'],6);
          	$nick_name=Db::name('admin')->where('account',$v['subscribe_account'])->value('nick_name');
          	preg_match("/^1\d{10}$/", $nick_name) ? $list[$k]['nick_name']=substr($nick_name,0,3).'****'.substr($nick_name,7,4) : $list[$k]['nick_name']=substr($nick_name,0,3).'****'.substr($nick_name,6);
		}
        return $this->json($list, 1, '查询成功');
    }
    public function monthrankingList() {
        $month['sell_time']=['between',[strtotime(date('Y-m'))-30*24*3600,strtotime(date('Y-m'))]];
        $list=$this->ranking($month);
      	foreach($list as $k=>$v){
			preg_match("/^1\d{10}$/", $v['subscribe_account']) ? $list[$k]['subscribe_account']=substr($v['subscribe_account'],0,3).'****'.substr($v['subscribe_account'],7,4) : $list[$k]['subscribe_account']=substr($v['subscribe_account'],0,3).'****'.substr($v['subscribe_account'],6);
          	$nick_name=Db::name('admin')->where('account',$v['subscribe_account'])->value('nick_name');
          	preg_match("/^1\d{10}$/", $nick_name) ? $list[$k]['nick_name']=substr($nick_name,0,3).'****'.substr($nick_name,7,4) : $list[$k]['nick_name']=substr($nick_name,0,3).'****'.substr($nick_name,6);
		}
        return $this->json($list, 1, '查询成功');
    }
    public function allrankingList() {
        $list=$this->ranking();
      	foreach($list as $k=>$v){
          	preg_match("/^1\d{10}$/", $v['username']) ? $list[$k]['username']=substr($v['username'],0,3).'****'.substr($v['username'],7,4) : $list[$k]['username']=substr($v['username'],0,3).'****'.substr($v['username'],6);
			preg_match("/^1\d{10}$/", $v['subscribe_account']) ? $list[$k]['subscribe_account']=substr($v['subscribe_account'],0,3).'****'.substr($v['subscribe_account'],7,4) : $list[$k]['subscribe_account']=substr($v['subscribe_account'],0,3).'****'.substr($v['subscribe_account'],6);
          	$nick_name=Db::name('admin')->where('account',$v['subscribe_account'])->value('nick_name');
          	preg_match("/^1\d{10}$/", $nick_name) ? $list[$k]['nick_name']=substr($nick_name,0,3).'****'.substr($nick_name,7,4) : $list[$k]['nick_name']=substr($nick_name,0,3).'****'.substr($nick_name,6);
		}
        return $this->json($list, 1, '查询成功');
    }
  
  
  
  	//排行榜列表
    public function rankingList() {
        $times=1;
        for($i=1;$i<12;$i++){
            if(isHoliday($i)!=0){
                $times+=1;
            }else{
                break;
            }
            continue;
        }

        $day['sell_time']=['between',[strtotime(date('Y-m-d'))-$times*24*3600,strtotime(date('Y-m-d'))]];
        $week['sell_time']=['between',[strtotime(date('Y-m-d'))-(date('N')-1)*24*3600-7*24*3600,strtotime(date('Y-m-d'))-(date('N')-1)*24*3600]];
        $month['sell_time']=['between',[strtotime(date('Y-m'))-30*24*3600,strtotime(date('Y-m'))]];
      
        $list=[
            'day'=>$this->ranking($day),
            'week'=>$this->ranking($week),
            'month'=>$this->ranking($month),
            'all'=>$this->ranking(),
        ];
      
      	foreach ($list['month'] as $k => $v) {
            $list['month'][$k]['aid'] = Db::name('admin')->where('account',$list['month'][$k]['subscribe_account'])->value('id');
          	preg_match("/^1\d{10}$/", $v['subscribe_account']) ? $list['month'][$k]['subscribe_account'] = substr($v['subscribe_account'],0,3).'****'.substr($v['subscribe_account'],7,4) : $list['month'][$k]['subscribe_account'] = substr($v['subscribe_account'],0,3).'****'.substr($v['subscribe_account'],6);
        }
        return $this->json($list, 1, '查询成功');
    }
  
  
  
  
  	//添加至我的历史搜索
    public function addHistoryStock() {
        $info = Request::instance()->param();
        if(!Db::name('stock_search')->where(['account'=>$info['account'],'stock_code'=>$info['stock_code'],'is_del'=>1])->find()){
            $data=[
                'account'=>$info['account'],
                'stock_name'=>$info['stock_name'],
                'stock_code'=>$info['stock_code'],
            ];
            Db::name('stock_search')->insert($data);
        }
        return $this->json('', 1, '添加成功');
    }
    //删除某个我的历史搜索
    public function deleteHistoryStock() {
        $info = Request::instance()->param();
        Db::name('stock_search')->where(['account'=>$info['account'],'stock_code'=>$info['stock_code']])->update(['is_del'=>2]);
        return $this->json('', 1, '删除成功');
    }
    //清空我的历史搜索
    public function deleteAllHistoryStock() {
        $info = Request::instance()->param();
        Db::name('stock_search')->where(['account'=>$info['account']])->update(['is_del'=>2]);
        return $this->json('', 1, '清空成功');
    }
  
  

  
  
  	//用户红包列表
    public function redPacketList() {
        //if($this->token()==100){return $this->json('', 100, '您的账号已在别的设备登陆');}

        $info = Request::instance()->param();
        //Db::name('push')->where(['account' => $info['account'],'push_type'=>2,'status'=>1,'is_del'=>1])->update(['is_del'=>2]);
        $packet=Db::name('packet')->where(['account' => $info['account'],'is_use'=>2,'daoqi_time'=>['>',time()]])->select();
        foreach ($packet as $k => $v) {
            $packet[$k]['create_time']=date('Y-m-d',$v['create_time']);
            $packet[$k]['daoqi_time']=date('Y-m-d',$v['daoqi_time']);
        }
        return $this->json($packet, 1, '查询成功');
    }
  
  
  
  
  
  
  
  	//新闻列表页，传分类id
    public function newsCateList() {
        $info = Request::instance()->param();
        $list = Db::name('news')->where(['cid' => $info['cid'],'is_del'=>1])->order('create_time desc')->select();
        foreach($list as $k => $v){
            $list[$k]['create_time'] = date('Y-m-d', $v['create_time']);
            $arr=explode(',',substr($v['picurl'],0,-1));
            $list[$k]['picurl']=[];
            foreach($arr as $kk=>$vv){
                $list[$k]['picurl'][$kk]=$vv;
            }
            $list[$k]['content']=str_replace('"','\'',htmlspecialchars($v['content'],ENT_NOQUOTES));
            $list[$k]['is_new']='';
            if($k<=4){
                $list[$k]['is_new']=1;
            }else{
                $list[$k]['is_new']=2;
            }
        }
        return $this->json($list, 1, '查询成功');
    }
  
  
  
  
  	//余额接口 策略账户余额，模拟余额
    public function account_money() {
        $info = Request::instance()->param();
        $list = Db::name('admin')->field('tactics_balance,analog_money')->where(['account' => $info['account'],'is_del'=>1])->find();
        
        return $this->json($list, 1, '余额查询成功');
    }
  
  
  
  
  
  	//股票进行模糊搜索
    public function search_stock_api_rank(){
        $info = Request::instance()->param();

        $stock_new = new Stock;
		
        //可买股票类型
        $canbuy=Db::name('hide')->where(['id'=>23])->value('value');//sz00,sz30,sh60
        $canbuyarr=explode(',',$canbuy);
        //不可买股票类型
        $cannotbuy=Db::name('hide')->where(['id'=>24])->value('value');//st,pt
        $cannotbuyarr=explode(',',$cannotbuy);
        //不可买的具体股票
        $forbidden_list = Db::name('forbidden_list')->where('is_del',1)->select();

        $stock=[];
      	$info['content'] = 's';
        $str = $stock_new->Tosearch($info['content']);
      	$str=iconv("gb2312", "utf-8//IGNORE",$str);
        $list=explode(';',explode("\"",$str)[1]);
        foreach($list as $k=>$v){
            $stockcode=explode(',',$v)[3];
            $stockname=explode(',',$v)[4];
            //排除A股之外的基金期货等
            if(strlen($stockcode)==8){
                if($canbuy){
                    //包含可买股票则可以购买
                    $result1=0;
                    foreach($canbuyarr as $kk=>$vv){
                        if(strpos($stockcode,$vv)||strpos($stockcode,$vv)===0){
                            $result1=1;
                        }
                    }
                }else{
                    //可买股票类型为空，全都能买
                    $result1=1;
                }
                if($cannotbuy){
                    //包含不可买股票则不可以购买
                    $result2=1;
                    foreach($cannotbuyarr as $kk=>$vv){
                        if(strpos($stockcode,$vv)||strpos($stockcode,$vv)===0){
                            $result2=0;
                        }
                    }
                }else{
                    //不可买股票类型为空，全都能买
                    $result2=1;
                }
                if($forbidden_list){
                    $result3=1;
                    foreach($forbidden_list as $kk=>$vv){
                        if($stockcode==$vv['stock_code']){
                            $result3=0;
                        }
                    }
                }else{
                    $result3=1;
                }
                //两种规则都满足则可以购买
                if($result1==1&&$result2==1&&$result3==1){
                    $stock[]=[
                        'stock_code'=>$stockcode,
                        'stock_name'=>$stockname,
                    ];
                }
            }
        }

        if($stock){
            $code='';
            foreach($stock as $k=>$v){
                $code.=$v['stock_code'].',';
            }
           
          	if(date('Hi')>930){
            	$result=$stock_new->hq_list(substr($code,0,-1));
                $pp=explode(';',$result);
                unset($pp[count($pp)-1]);
                foreach($pp as $k=>$v){
                    $stock[$k]['nowprice']=round(explode(',',$v)[3],2);
                    $stock[$k]['rate_price']=round(explode(',',$v)[3]-explode(',',$v)[2],2);
                    $stock[$k]['rate']=(round((explode(',',$v)[3]-explode(',',$v)[2])/explode(',',$v)[2],4)*100).'%';
                    if(!$stock[$k]['rate_price']){
                        unset($stock[$k]);
                    }
                }
            }else{
            	$result=$stock_new->Hq_real_list(substr($code,0,-1));
                $arr=json_decode($result,true);
                foreach($arr as $k=>$v){
                    $stock[$k]['nowprice']=round($v['nowPri'],2);
                    $stock[$k]['rate_price']=round($v['nowPri']-$v['closePrice'],2);
                    $stock[$k]['rate']=(round(($v['nowPri']-$v['closePrice'])/$v['closePrice'],4)*100).'%';
                    if(!$stock[$k]['rate_price']){
                        unset($stock[$k]);
                    }
                }
            }
            $stock=array_values($stock);
        }

        return $this->json($stock, 1, '查询成功');
    }
  
  
  
  
  	/**
     * 追加信用金
     * @param  int    account                     用户
     * @param  int    str_id                      策略id
     * @param  int    credit_add                  追加的信用金
     */
    public function credit_add(){

        $info = Request::instance()->param();

        if(!preg_match("/^[1-9][0-9]*$/",$info['credit_add'])){
            return $this->json('', 0, '追加的信用金为正整数');
        }

        $strategy = Db::name('strategy')->where('id',$info['str_id'])->find();
        if(!$strategy){
            return $this->json('', 0, '该策略不存在');
        }
      
      	$tactics_balance=Db::name('admin')->where(['account'=>$info['account'],'is_del'=>1])->value('tactics_balance');
        if($tactics_balance<=$info['credit_add']){
            return $this->json('', 0, "策略余额不足");
        }

        //追加的信用金的上限（倍数）
        $credit_max = Db::name('hide')->where('id',19)->value('value');
        //允许追加信用金次数
        $credit_allow_num = Db::name('hide')->where('id',20)->value('value');
        //追加信用金记录表
        $credit_add_num = Db::name('credit_add_record')->where('str_id',$info['str_id'])->order('create_time asc')->count();

        //追加的信用金的上限（具体的值）
        $credit_max = $credit_max*$strategy['credit'];
        
        if(($strategy['credit_add']+$info['credit_add'])>$credit_max){
            return $this->json('', 0, '超过可允许追加的最大信用金'.$credit_max);
        }

        if(!$credit_allow_num){
            return $this->json('', 0, '禁止追加信用金');
        }

        if($credit_add_num>=$credit_allow_num){
            return $this->json('', 0, '您已超过信用金可追加次数');
        }


        //止损股价 = （市值-0.75*追加后的信用金）/股数
        $after_credit = $strategy['credit']+$strategy['credit_add']+$info['credit_add'];//追加后的信用金
        $bfb = Db::name('hide')->where('id',18)->value('value');
        $loss_value = round(($strategy['market_value']-$bfb*$after_credit)/$strategy['stock_number'],2);
        //改变改策略的止损价
        $res = Db::name('strategy')->where('id',$info['str_id'])->update(['loss_value'=>$loss_value]);

        //减少用户的策略余额 增加用户的冻结金额
        $admin=Db::name('admin')->where(['account'=>$info['account'],'is_del'=>1])->find();
        $bool = Db::name('admin')
            ->where(['account'=>$info['account'],'is_del'=>1])
            ->update([
                'tactics_balance'=>$admin['tactics_balance']-$info['credit_add'],
                'frozen_money'=>$admin['frozen_money']+$info['credit_add'],
            ]);
        $bool2 = Db::name('strategy')->where('id',$info['str_id'])->setInc('credit_add',$info['credit_add']); //增加该策略的信用金

        //存记录表
        $data = [
            'account'=>$info['account'],
            'str_id'=>$info['str_id'],
            'credit_before'=>$strategy['credit'],
            'credit_add'=>$info['credit_add'],
            'create_time'=>time(),

        ];
        $bool3 = Db::name('credit_add_record')->insert($data);

        $trade=[
            'account'=>$info['account'],
            'charge_num'=>$strategy['strategy_num'],
            'trade_price'=>$info['credit_add'],
            'service_charge'=>0,
            'xbalance'=>0,
            'trade_type'=>'追加信用金',
            'trade_status'=>1,
            'detailed'=>'',
            'is_del'=>1,
            'create_time'=>time(),
        ];
        $bool4 = Db::name('trade')->insert($trade);

        if(!$res || !$bool || !$bool2 || !$bool3 || !$bool4){
            return $this->json('', 1, '追加信用金失败');
        }
        return $this->json('', 1, '追加信用金成功');

    }
  
  
  
  
  
  
  	//查看炒股大赛的持仓中的策略
    public function checkStrategyContest() {
        //if($this->token()==100){return $this->json('', 100, '您的账号已在别的设备登陆');}

        $info = Request::instance()->param();
        $list=Db::name('strategy_contest')->where(['account'=>$info['account'],'status'=>['in',[1,2]]])->order('buy_time desc')->select();

        //查询现价
        if($list){
            if(date('Hi')>931){
                $str='';
                foreach($list as $k=>$v){
                    $str.=$v['stock_code'].',';
                }
                $str=substr($str,0,-1);

                $result=file_get_contents("http://hq.sinajs.cn/list=".$str);
                $para=explode(';',$result);
                unset($para[count($para)-1]);
                foreach($para as $k=>$v){
                    $list[$k]['nowprice']=explode(',',$v)[3];
                }
            }else{
                $times=1;
                for($i=1;$i<12;$i++){
                    if(isHoliday($i)!=0){
                        $times+=1;
                    }else{
                        break;
                    }
                    continue;
                }
                foreach($list as $k=>$v){
                    $str=file_get_contents('http://money.finance.sina.com.cn/quotes_service/api/json_v2.php/CN_MarketData.getKLineData?symbol='.$v['stock_code'].'&scale=60&ma=5&datalen=9');
                    $arr=explode(',',substr($str,1,-1));
                    foreach($arr as $kk=>$vv){
                        if(strpos($vv,date('Y-m-d',time()-$times*3600*24).' 15:00:00')){
                            $list[$k]['nowprice']=substr($arr[$kk+4],7,5);
                        }
                    }
                }
            }
        }

        //买入时间
        foreach($list as $k=>$v){
            $list[$k]['buy_time']=date('m-d H:i',$v['buy_time']);
            $list[$k]['day']='';
        }

        //用户信息
        $admin=Db::name('admin')->where(['account'=>$info['account'],'is_del'=>1])->find();
        $arr=[
            //总计余额
            'all_money'=>$admin['analog_money']+$admin['frozen_analog_money'],
            //冻结金额
            'frozen_money'=>$admin['frozen_analog_money'],
            //可用余额
            'can_money'=>$admin['analog_money'],
        ];
        return $this->json($list, 1, '查询成功',$arr);
    }
  
  
  
  	//查看炒股大赛的已平仓的策略
    public function checkStrategyHistoryContest() {
       // if($this->token()==100){return $this->json('', 100, '您的账号已在别的设备登陆');}

        $info = Request::instance()->param();
        //已平仓的策略
        $list=Db::name('strategy_contest')->where(['account'=>$info['account'],'status'=>['in',[3,4]]])->order('sell_time desc')->select();
        if($list){
            //总策略数
            $allStrategyNum=count($list);
            //盈利策略数
            $winStrategyNum=0;
            //总盈利
            $win=0;
            foreach($list as $k=>$v) {
                $list[$k]['buy_time']=date('m-d H:i',$v['buy_time']);
                $list[$k]['sell_time']=date('m-d H:i',$v['sell_time']);
                if($v['sell_price']>=$v['buy_price']){
                    //交易盈亏
                    $list[$k]['getprice']=($v['sell_price']-$v['buy_price'])*$v['stock_number'];
                    //盈利分配
                    $list[$k]['truegetprice']=($v['sell_price']-$v['buy_price'])*$v['stock_number'];
                    //亏损赔付
                    $list[$k]['dropprice']=0;
                    $winStrategyNum+=1;
                }else{
                    //交易盈亏
                    $list[$k]['getprice']=($v['sell_price']-$v['buy_price'])*$v['stock_number'];
                    //盈利分配
                    $list[$k]['truegetprice']=0;
                    //亏损赔付
                    $list[$k]['dropprice']=($v['sell_price']-$v['buy_price'])*$v['stock_number'];
                }
                $win += ($v['sell_price'] - $v['buy_price']) * $v['stock_number'];
            }
            $arr=[
                'allStrategyNum'=>$allStrategyNum,
                'winStrategyNum'=>$winStrategyNum,
                'win'=>$win,
                'rate'=>round(($winStrategyNum/$allStrategyNum*100),2).'%',
            ];
        }else{
            $arr=[
                'allStrategyNum'=>0,
                'winStrategyNum'=>0,
                'win'=>0,
                'rate'=>'0%',
            ];
        }
        return $this->json($list, 1, '查询成功',$arr);
    }

    //查看已平仓的炒股大赛的详情
    public function checkStrategyHistoryContestdetails() {
       // if($this->token()==100){return $this->json('', 100, '您的账号已在别的设备登陆');}

        $info = Request::instance()->param();
        $strategy=Db::name('strategy_contest')->where(['id'=>$info['id']])->find();
        if($strategy['sell_price']>$strategy['buy_price']){
            //交易盈亏
            $strategy['getprice']=($strategy['sell_price']-$strategy['buy_price'])*$strategy['stock_number'];
            //盈利分配
            $strategy['truegetprice']=($strategy['sell_price']-$strategy['buy_price'])*$strategy['stock_number'];
            //亏损赔付
            $strategy['dropprice']=0;
        }else{
            //交易盈亏
            $strategy['getprice']=($strategy['sell_price']-$strategy['buy_price'])*$strategy['stock_number'];
            //盈利分配
            $strategy['truegetprice']=0;
            //亏损赔付
            $strategy['dropprice']=($strategy['sell_price']-$strategy['buy_price'])*$strategy['stock_number'];
        }
        $strategy['day']=date('Ymd',$strategy['sell_time'])-date('Ymd',$strategy['buy_time']);
        $strategy['buy_time']=date('Y-m-d H:i',$strategy['buy_time']);
        $strategy['sell_time']=date('Y-m-d H:i',$strategy['sell_time']);
        $strategy['fanhuan']=$strategy['credit']+($strategy['sell_price']-$strategy['buy_price'])*$strategy['stock_number'];
        return $this->json($strategy, 1, '查询成功');
    }
  
  
  
  	
  	public function hide(){
    
    	$list['allow_kuisun'] = Db::name('hide')->where('id',18)->value('value');
      	return $this->json($list, 1, '查询成功');
    
    }
  
  
  	//修改止盈
    public function update_surplus_value() {
        $info = Request::instance()->param();
        if($info['trues']==1){
            Db::name('strategy')->where(['is_cancel_order'=>1])->where(['id' => $info['id']])->update(['surplus_value'=>$info['surplus_value']]);
        }else{
            Db::name('strategy_contest')->where(['id' => $info['id']])->update(['surplus_value'=>$info['surplus_value']]);
        }
        return $this->json('', 1, '修改止盈成功');
    }
  
  
  
  
  
  	//涨幅榜
  	public function up_stock(){
    	if(cache('up_stock')){
            $str=cache('up_stock');
        }else{
            $str = file_get_contents("http://vip.stock.finance.sina.com.cn/quotes_service/api/json_v2.php/Market_Center.getHQNodeData?page=1&num=40&sort=changepercent&asc=0&node=hs_a&symbol=&_s_r_a=init");
            cache('up_stock',$str,60);
        }
        $str=iconv("gb2312", "utf-8//IGNORE",$str);
        $arr = explode('},', substr($str, 1,-1));

        $new_arr = [];
        foreach ($arr as $k => $v) {
            $shuzu=explode(",", substr($v, 1));
            $new_arr[$k]['name'] = substr(explode(":", $shuzu[2])[1],1,-1);
            $new_arr[$k]['trade'] = substr(explode(":", $shuzu[3])[1],1,-1);
            $new_arr[$k]['changepercent'] = substr(explode(":", $shuzu[5])[1],1,-1);
            $new_arr[$k]['code'] = substr(explode(":", $shuzu[0])[1],1,-1);
        }
        foreach ($new_arr as $k => $v) {
            if($k>12){
                unset($new_arr[$k]);
            }
        }
    	return $this->json($new_arr, 1, '成功');

    }
  
  
  
  	//跌幅榜
  	public function down_stock(){
    	//if(cache('up_stock')){
          //  $str=cache('up_stock');
       // }else{
            $str = file_get_contents("http://vip.stock.finance.sina.com.cn/quotes_service/api/json_v2.php/Market_Center.getHQNodeData?page=1&num=40&sort=changepercent&asc=1&node=hs_a&symbol=&_s_r_a=init");
          //  cache('up_stock',$str,60);
       // }
        $str=iconv("gb2312", "utf-8//IGNORE",$str);
        $arr = explode('},', substr($str, 1,-1));

        $new_arr = [];
        foreach ($arr as $k => $v) {
            $shuzu=explode(",", substr($v, 1));
            $new_arr[$k]['name'] = substr(explode(":", $shuzu[2])[1],1,-1);
            $new_arr[$k]['trade'] = substr(explode(":", $shuzu[3])[1],1,-1);
            $new_arr[$k]['changepercent'] = substr(explode(":", $shuzu[5])[1],1,-1);
            $new_arr[$k]['code'] = substr(explode(":", $shuzu[0])[1],1,-1);
        }
        foreach ($new_arr as $k => $v) {
            if($k>12){
                unset($new_arr[$k]);
            }
        }
    	return $this->json($new_arr, 1, '成功');
    }
  
  
  
    //用户反馈新的问题
    public function createFeedback(){
        $info = Request::instance()->param();
        $data=[
            'admin_id'=>$info['account'],
            'content'=>$info['content'],
            'status'=>1,
            'create_time'=>time(),
        ];
        Db::name('feedback')->insert($data);
        return $this->json('', 1, '反馈成功');
    }
    //用户反馈的问题的列表
    public function feedbackList(){
        if(token()==100){return json('', 100, '您的账号已在别的设备登陆');}

        $info = Request::instance()->param();
        $list=Db::name('feedback')->where(['admin_id'=>$info['account']])->select();
        $arr=[];
        if($list){
            foreach ($list as $k => $v) {
                $arr[$k]['question']=$v;
                $arr[$k]['question']['create_time']=date('Y-m-d H:i:s',$v['create_time']);
                $arr[$k]['list']=Db::name('feedback_talk')->where(['fid'=>$v['id']])->order('create_time asc')->select();
                if($arr[$k]['list']){
                    foreach ($arr[$k]['list'] as $kk => $vv) {
                        $arr[$k]['list'][$kk]['create_time']=date('Y-m-d H:i:s',$vv['create_time']);
                    }
                }
            }
        }
        return $this->json($arr, 1, '查询成功');
    }
    //用户对于反馈的问题进行回复
    public function createFeedbackTalk(){
        if(token()==100){return json('', 100, '您的账号已在别的设备登陆');}

        $info = Request::instance()->param();
        $data=[
            'fid'=>$info['fid'],
            'account'=>$info['account'],
            'content'=>$info['content'],
            'status'=>1,
            'create_time'=>time(),
        ];
        Db::name('feedback_talk')->insert($data);
        return $this->json('', 1, '回复成功');
    }
    //用户对于反馈的问题的提问的历史记录
    public function feedbackTalkList(){
        if(token()==100){return json('', 100, '您的账号已在别的设备登陆');}

        $info = Request::instance()->param();
        //查询反馈问题
        $feedback=Db::name('feedback')->where(['id'=>$info['id']])->find();
        //查询问题的所有回复
        $list=Db::name('feedback_talk')->where(['fid'=>$info['id']])->order('create_time asc')->select();
        $arr[0]['id']=0;
        $arr[0]['fid']=$feedback['id'];
        $arr[0]['account']=$feedback['admin_id'];
        $arr[0]['content']=$feedback['content'];
        $arr[0]['status']=1;
        $arr[0]['create_time']=date('m-d H:i:s',$feedback['create_time']);
        foreach ($list as $k => $v) {
            $arr[$k+1]=$v;
            $arr[$k+1]['create_time']=date('m-d H:i:s',$v['create_time']);
        }
        return $this->json($arr, 1, '查询成功');
    }
  
  
  
  

}