<?php
namespace app\index\controller;
use think\Db;
use think\Request;
use think\Controller;
use think\Validate;
class Ranking extends controller
{
  
  	public function yanzheng() {
       
        if(!cookie('username')){
            $this->redirect('index/login');
        }
      
      	$account = $_COOKIE['username'];
      	$lastlogin_time = $_COOKIE['lastlogin_time'];
      	$db_lastlogin_time = Db::name('admin')->where('account',$account)->value('lastlogin_time');
      	if($lastlogin_time != $db_lastlogin_time){
        	$this->redirect('index/login');
        }
      
      
    }

	//排行榜列表
	public function rankingList(){
		$times=1;
		for($i=1;$i<12;$i++){
			if(isHoliday($i)!=0){
				$times+=1;
			}else{
				break;
			}
			continue;
		}

		$day['sell_time']=['between',[strtotime(date('Y-m-d'))-$times*24*3600,strtotime(date('Y-m-d'))]];
		$week['sell_time']=['between',[strtotime(date('Y-m-d'))-(date('N')-1)*24*3600-7*24*3600,strtotime(date('Y-m-d'))-(date('N')-1)*24*3600]];
		$month['sell_time']=['between',[strtotime(date('Y-m'))-30*24*3600,strtotime(date('Y-m'))]];
		$list=[
			'day'=>$this->ranking($day),
			'week'=>$this->ranking($week),
			'month'=>$this->ranking($month),
			'all'=>$this->ranking(),
		];
		foreach($list as $k=>$v){
			foreach($v as $kk=>$vv){
			    $list[$k][$kk]['username']=substr($vv['username'],0,3).'****'.substr($vv['username'],7,4);
			}
		}


		$bannerurl = Db::name('banner')->where(['is_del'=>1,'ban_id'=>3])->value('bannerurl');
		$this->assign('bannerurl',$bannerurl);

		$this->assign('list',$list);

		return $this->fetch();
	}

	//获取排行榜的人员，策略收益
	public function ranking($where=[],$all=1) {
		$where['status']=['in',[3,4]];
		//信息是否开放(排行榜是否看见)的会员
		$admin=Db::name('admin')->where(['is_open_ranking'=>1,'is_del'=>1])->select();

		//真实策略
		$strategy=Db::name('strategy')->where(['is_cancel_order'=>1])->where($where)->select();
		$list=[];
		$arr=[];
		//获取到一级为会员，二级为他的策略的多维数组
		foreach($admin as $k=>$v){
			foreach($strategy as $kk=>$vv){
				if($v['account']==$vv['account']){
					$list[$k][$kk]=$vv;
					//subscribe_account为获得详情的参数，为了与我的订阅中跳转参数一致
					$list[$k][$kk]['subscribe_account']=$vv['account'];
					//用户头像
					$list[$k][$kk]['headurl']=$v['headurl'];
					//会员id
                    $list[$k][$kk]['id']=$v['id'];

				}
			}
		}
		//真实的策略
		if($list){
			//对数组的里层进行重新排序，改变key
			foreach($list as $k=>$v){
				$list[$k]=array_values($v);
			}
			//获取到收益率和胜率的新数组，真实策略为1，名字为手机号，炒股大赛为2，名字为昵称
			foreach($list as $k=>$v){
				$arr[$k]['trues']='1';
				//subscribe_account为获得详情的参数，为了与我的订阅中跳转参数一致
				$arr[$k]['subscribe_account']=$v[0]['subscribe_account'];
				//用户头像
				$arr[$k]['headurl']=$v[0]['headurl'];
				$arr[$k]['username']=$v[0]['account'];
				//收益，平台分成后的钱（包括信用金）
				$shouyi=0;
				//信用金（交易本金）
				$credit=0;
				//盈利次数
				$win=0;
				foreach($v as $kk=>$vv){
					$credit+=$vv['credit'];
					$shouyi+=($vv['true_getmoney']-$vv['credit']);
					//计算胜率
					if($vv['true_getmoney']>$vv['credit']){$win+=1;}
				}
				//收益率
				$arr[$k]['profit']=(round($shouyi/$credit*100,2));
				//胜率
				$arr[$k]['win']=(round($win/count($v)*100,2)).'%';
				//id
                $arr[$k]['id'] = $v[0]['id'];
			}
		}

		//炒股大赛策略
		$strategy_contest=Db::name('strategy_contest')->where($where)->select();
		$list_contest=[];
		$arr_contest=[];
		//获取到一级为会员，二级为他的策略的多维数组
		foreach($admin as $k=>$v){
			foreach($strategy_contest as $kk=>$vv){
				if($v['account']==$vv['account']){
					$list_contest[$k][$kk]=$vv;
					$list_contest[$k][$kk]['nick_name']=$v['nick_name'];
					//subscribe_account为获得详情的参数，为了与我的订阅中跳转参数一致
					$list_contest[$k][$kk]['subscribe_account']=$vv['account'];
					//用户头像
					$list_contest[$k][$kk]['headurl']=$v['headurl'];
					//会员id
                    $list_contest[$k][$kk]['id']=$v['id'];
				}
			}
		}
		//炒股大赛的策略
		if($list_contest){
			//对数组的里层进行重新排序，改变key
			foreach($list_contest as $k=>$v){
				$list_contest[$k]=array_values($v);
			}
			//获取到收益率和胜率的新数组，真实策略为1，名字为手机号，炒股大赛为2，名字为昵称(没有的话为手机号)
			foreach($list_contest as $k=>$v){
				$arr_contest[$k]['trues']='2';
				//subscribe_account为获得详情的参数，为了与我的订阅中跳转参数一致
				$arr_contest[$k]['subscribe_account']=$v[0]['subscribe_account'];
				//用户头像
				$arr_contest[$k]['headurl']=$v[0]['headurl'];
				if($v[0]['nick_name']){
					$arr_contest[$k]['username']=$v[0]['nick_name'];
				}else{
					$arr_contest[$k]['username']=$v[0]['account'];
				}
				//收益，(卖出价-买入价)*股数
				$shouyi=0;
				//信用金
				$credit=0;
				//盈利次数
				$win=0;
				foreach($v as $kk=>$vv){
					$credit+=$vv['credit'];
					$shouyi+=($vv['sell_price']-$vv['buy_price'])*$vv['stock_number'];
					//计算胜率
					if($vv['sell_price']>$vv['buy_price']){$win+=1;}
				}
				//收益率
				$arr_contest[$k]['profit']=(round($shouyi/$credit*100,2));
				//胜率
				$arr_contest[$k]['win']=(round($win/count($v)*100,2)).'%';
				//id
                $arr_contest[$k]['id'] = $v[0]['id'];
			}
		}
		//数组合并
		$para=array_merge($arr,$arr_contest);

		//排序
		array_multisort(array_column($para,'profit'),SORT_DESC,$para);

		if($all==1){
			//收益率加上%，只要正的，只要前十个
			foreach ($para as $k => $v) {
				if($v['profit']<=0){
					unset($para[$k]);
				}else{
					
					$para[$k]['profit']=$v['profit'].'%';
				}
			}
			if(count($para)>10){
				foreach ($para as $k => $v) {
					if($k>=10){unset($para[$k]);}
				}
			}
		}

		return $para;
	}

	//订阅和取消订阅
	public function createSubscribe() {
		$info = Request::instance()->param();
		$info['account']=cookie('username');
		//订阅
		if($info['is_subscribe']==1){
			$data=[
				'account'=>$info['account'],
				'subscribe_account'=>$info['subscribe_account'],
				'trues'=>$info['trues'],
				'subscribe_nick_name'=>$info['subscribe_nick_name'],
				'is_del'=>1,
				'create_time'=>time(),
			];
			Db::name('subscribe')->insert($data);
			return json('', 1, '关注成功');
			//取消订阅
		}elseif($info['is_subscribe']==2){
			$where=[
				'account'=>$info['account'],
				'subscribe_account'=>$info['subscribe_account'],
				'trues'=>$info['trues'],
			];
			Db::name('subscribe')->where($where)->update(['is_del'=>2]);
			return json('', 1, '取消关注成功');
		}
	}
	//我的订阅
	public function ranking_subscribe() {
      	$this->yanzheng();
		$info['account']=cookie('username');
		$list = Db::name('subscribe')->where(['account' => $info['account'],'is_del'=>1])->select();

		foreach($list as $k=>$v){
		   $is_open_ranking=Db::name('admin')->where(['account' => $v['subscribe_account'],'is_del'=>1])->value('is_open_ranking');
		   if($is_open_ranking!=1){
		      unset($list[$k]);
		   }
		}



		foreach($list as $k=>$v){


			$list[$k]['bs'] = Db::name('admin')->where('account',$v['subscribe_account'])->value('id');


			//true为1查询真实策略，2查询炒股大赛
			if($v['trues']==1){
				$store=Db::name('strategy')
					->where(['is_cancel_order'=>1])
					->where(['account' => $v['subscribe_account'],'status'=>['in',[3,4]]])
					->select();
				if($store){
					//收益，平台分成后的钱（包括信用金）
					$shouyi=0;
					//信用金（交易本金）
					$credit=0;
					foreach($store as $kk=>$vv){
						$credit+=$vv['credit'];
						$shouyi+=($vv['true_getmoney']-$vv['credit']);
					}
					//收益率
					$list[$k]['profit']=(round($shouyi/$credit*100,2)).'%';
				}else{
					$list[$k]['profit']='0%';
				}
				$list[$k]['headurl']=Db::name('admin')->where(['account' => $v['subscribe_account'],'is_del'=>1])->value('headurl');
			}else{
				$store=Db::name('strategy_contest')->where(['account' => $v['subscribe_account'],'status'=>['in',[3,4]]])->select();
				if($store){
					//收益，(卖出价-买入价)*股数
					$shouyi=0;
					//信用金
					$credit=0;
					foreach($store as $kk=>$vv){
						$credit+=$vv['credit'];
						$shouyi+=($vv['sell_price']-$vv['buy_price'])*$vv['stock_number'];
					}
					//收益率
					$list[$k]['profit']=(round($shouyi/$credit*100,2)).'%';
				}else{
					$list[$k]['profit']='0%';
				}
				$list[$k]['headurl']=Db::name('admin')->where(['account' => $v['subscribe_account'],'is_del'=>1])->value('headurl');
			}
		}
		foreach($list as $k=>$v){
			$list[$k]['subscribe_nick_name']=substr($v['subscribe_nick_name'],0,3).'****'.substr($v['subscribe_nick_name'],7,4);
		}
		$this->assign('list',$list);




		$this->assign('count',count($list));

		return $this->fetch();
	}
	//查看其余人的战绩
	public function ranking_record() {

		$this->yanzheng();

		$times=1;
		for($i=1;$i<12;$i++){
			if(isHoliday($i)!=0){
				$times+=1;
			}else{
				break;
			}
			continue;
		}

		$day['sell_time']=['between',[strtotime(date('Y-m-d'))-$times*24*3600,strtotime(date('Y-m-d'))]];
		$week['sell_time']=['between',[strtotime(date('Y-m-d'))-(date('N')-1)*24*3600-7*24*3600,strtotime(date('Y-m-d'))-(date('N')-1)*24*3600]];
		$month['sell_time']=['between',[strtotime(date('Y-m'))-30*24*3600,strtotime(date('Y-m'))]];

		$info = Request::instance()->param();

		$info['subscribe_account'] = Db::name('admin')->where('id',$info['id'])->value('account');



		$info['account']=cookie('username');
		//订阅的人的策略  true为1查询真实策略，2查询炒股大赛
		if($info['trues']==1){
			$list['day']=$this->shouyiInfo($info['subscribe_account'],$day);
			$list['week']=$this->shouyiInfo($info['subscribe_account'],$week);
			$list['month']=$this->shouyiInfo($info['subscribe_account'],$month);
			$list['all']=$this->shouyiInfo($info['subscribe_account']);
		}else{
			$list['day']=$this->shouyiInfoContest($info['subscribe_account'],$day);
			$list['week']=$this->shouyiInfoContest($info['subscribe_account'],$week);
			$list['month']=$this->shouyiInfoContest($info['subscribe_account'],$month);
			$list['all']=$this->shouyiInfoContest($info['subscribe_account']);
		}

		//此会员的会员信息
		$admin=Db::name('admin')->where(['account' => $info['subscribe_account'],'is_del'=>1])->find();
		if($admin['nick_name']){
			$list['nick_name']=$admin['nick_name'];
		}else{
			$list['nick_name']=$admin['account'];
		}
		$list['subscribe_account']=$admin['account'];
		$list['subscribe_nick_name']=$list['nick_name'];
		$list['trues']=$info['trues'];
		$list['headurl']=$admin['headurl'];

		//总排名
		$ranking=$this->ranking([],2);

		foreach($ranking as $k=>$v){
			if(($v['subscribe_account']==$info['subscribe_account'])&&($v['trues']==$info['trues'])){
				$list['ranking']=$k+1;
			}
		}

		//判断我是否订阅此人
		$subscribe = Db::name('subscribe')
			->where(['account'=>$info['account'],'subscribe_account' => $info['subscribe_account'],'trues'=>$info['trues'],'is_del'=>1])
			->find();
		if($subscribe){
			$list['is_subscribe']=1;
		}else{
			$list['is_subscribe']=2;
		}
		//被订阅数
		$list['subscribe_sum'] = Db::name('subscribe')->where(['subscribe_account' => $info['subscribe_account'],'is_del'=>1])->count();

		//战绩图
		$war[date('Ymd')-6]=[];
		$war[date('Ymd')-5]=[];
		$war[date('Ymd')-4]=[];
		$war[date('Ymd')-3]=[];
		$war[date('Ymd')-2]=[];
		$war[date('Ymd')-1]=[];
		$war[date('Ymd')]=[];

		//策略创建的历史记录
		if($info['trues']==1){
			$store=Db::name('strategy')
				->where(['is_cancel_order'=>1])
				->where(['account' => $info['subscribe_account'],'status'=>['in',[3,4]]])
				->order('sell_time desc')
				->select();
			if($store){
				//用户卖出股票时所能拿到的成数
				$chengshu=Db::name('hide')->where(['id' => 3])->value('value');
				foreach($store as $k=>$v){
					//策略创建的记录
					$list['record'][$k]=$v;
					if($v['true_getmoney']>$v['credit']){
						//交易盈亏
						$list['record'][$k]['getprice']=round(($v['true_getmoney']-$v['credit'])/$chengshu,2);
						//盈利分配
						$list['record'][$k]['truegetprice']=$v['true_getmoney']-$v['credit'];
						//亏损赔付
						$list['record'][$k]['dropprice']=0;
					}else{
						//交易盈亏
						$list['record'][$k]['getprice']=$v['true_getmoney']-$v['credit'];
						//盈利分配
						$list['record'][$k]['truegetprice']=0;
						//亏损赔付
						$list['record'][$k]['dropprice']=$v['true_getmoney']-$v['credit'];
					}
					//战绩图
					if($v['sell_time']<strtotime(date('Ymd')-5)){
						$war[date('Ymd')-6][]=$v;
					}
					if($v['sell_time']<strtotime(date('Ymd')-4)){
						$war[date('Ymd')-5][]=$v;
					}
					if($v['sell_time']<strtotime(date('Ymd')-3)){
						$war[date('Ymd')-4][]=$v;
					}
					if($v['sell_time']<strtotime(date('Ymd')-2)){
						$war[date('Ymd')-3][]=$v;
					}
					if($v['sell_time']<strtotime(date('Ymd')-1)){
						$war[date('Ymd')-2][]=$v;
					}
					if($v['sell_time']<strtotime(date('Ymd'))){
						$war[date('Ymd')-1][]=$v;
					}
					if($v['sell_time']<time()){
						$war[date('Ymd')][]=$v;
					}
				}
				//战绩图
				//收益，平台分成后的钱（包括信用金）
				$shouyi=0;
				//信用金（交易本金）
				$credit=0;
				foreach($war as $k=>$v){
					$list['war'][$k]['time']=$k;
					if($v){
						foreach($v as $kk=>$vv){
							$credit+=$vv['credit'];
							$shouyi+=($vv['true_getmoney']-$vv['credit']);
						}
						//收益率
						$list['war'][$k]['income']=(round($shouyi/$credit*100,2)).'%';
					}else{
						$list['war'][$k]['income']='0%';
					}
				}
				$list['war']=array_values($list['war']);

			}else{
				//没有策略创建的历史记录
				$list['record']=[];
			}
		}else{
			$store=Db::name('strategy_contest')
				->where(['account' => $info['subscribe_account'],'status'=>['in',[3,4]]])
				->order('sell_time desc')
				->select();
			if($store){
				foreach($store as $k=>$v){
					$list['record'][$k]=$v;
					if($v['sell_price']>=$v['buy_price']){
						//交易盈亏
						$list['record'][$k]['getprice']=($v['sell_price']-$v['buy_price'])*$v['stock_number'];
						//盈利分配
						$list['record'][$k]['truegetprice']=($v['sell_price']-$v['buy_price'])*$v['stock_number'];
						//亏损赔付
						$list['record'][$k]['dropprice']=0;
					}else{
						//交易盈亏
						$list['record'][$k]['getprice']=($v['sell_price']-$v['buy_price'])*$v['stock_number'];
						//盈利分配
						$list['record'][$k]['truegetprice']=0;
						//亏损赔付
						$list['record'][$k]['dropprice']=($v['sell_price']-$v['buy_price'])*$v['stock_number'];
					}
					//战绩图
					if($v['sell_time']<strtotime(date('Ymd')-5)){
						$war[date('Ymd')-6][]=$v;
					}
					if($v['sell_time']<strtotime(date('Ymd')-4)){
						$war[date('Ymd')-5][]=$v;
					}
					if($v['sell_time']<strtotime(date('Ymd')-3)){
						$war[date('Ymd')-4][]=$v;
					}
					if($v['sell_time']<strtotime(date('Ymd')-2)){
						$war[date('Ymd')-3][]=$v;
					}
					if($v['sell_time']<strtotime(date('Ymd')-1)){
						$war[date('Ymd')-2][]=$v;
					}
					if($v['sell_time']<strtotime(date('Ymd'))){
						$war[date('Ymd')-1][]=$v;
					}
					if($v['sell_time']<time()){
						$war[date('Ymd')][]=$v;
					}
				}
				//战绩图
				//收益，平台分成后的钱（包括信用金）
				$shouyi=0;
				//信用金（交易本金）
				$credit=0;
				foreach($war as $k=>$v){
					$list['war'][$k]['time']=$k;
					if($v){
						foreach($v as $kk=>$vv){
							$credit+=$vv['credit'];
							$shouyi+=($vv['sell_price']-$vv['buy_price'])*$vv['stock_number'];
						}
						//收益率
						$list['war'][$k]['income']=(round($shouyi/$credit*100,2)).'%';
					}else{
						$list['war'][$k]['income']='0%';
					}
				}
				$list['war']=array_values($list['war']);
			}else{
				//没有炒股大赛创建的历史记录
				$list['record']=[];
			}
		}
		if($list['record']){
			foreach ($list['record'] as $k => $v) {
				$list['record'][$k]['buy_time']=date('Y-m-d',$v['buy_time']);
				$list['record'][$k]['sell_time']=date('Y-m-d',$v['sell_time']);
			}
		}

		//策略创建的持股记录
		if($info['trues']==1){
			$buy_store=Db::name('strategy')
				->where(['is_cancel_order'=>1])
				->where(['account' => $info['subscribe_account'],'status'=>['in',[1,2]]])
				->order('buy_time desc')
				->select();
			if($buy_store){
				//查询现价
				$str='';
				foreach($buy_store as $k=>$v){
					$str.=$v['stock_code'].',';
				}
				$str=substr($str,0,-1);

				$result=file_get_contents("http://hq.sinajs.cn/list=".$str);
				$para=explode(';',$result);
				unset($para[count($para)-1]);
				foreach($para as $k=>$v){
					$buy_store[$k]['nowprice']=explode(',',$v)[3];
				}

				foreach($buy_store as $k=>$v){
					$list['buy_record'][$k]=$v;
					$list['buy_record'][$k]['buy_time']=date('m-d',$v['buy_time']);
					//策略盈亏(收益)
					$list['buy_record'][$k]['income']=($v['nowprice']-$v['buy_price'])*$v['stock_number'];
				}

			}else{
				//没有策略创建的持股记录
				$list['buy_record']=[];
			}
		}else{
			$buy_store=Db::name('strategy_contest')->where(['account' => $info['subscribe_account'],'status'=>['in',[1,2]]])->order('buy_time desc')->select();
			if($buy_store){
				//查询现价
				$str='';
				foreach($buy_store as $k=>$v){
					$str.=$v['stock_code'].',';
				}
				$str=substr($str,0,-1);

				$result=file_get_contents("http://hq.sinajs.cn/list=".$str);
				$para=explode(';',$result);
				unset($para[count($para)-1]);
				foreach($para as $k=>$v){
					$buy_store[$k]['nowprice']=explode(',',$v)[3];
				}

				foreach($buy_store as $k=>$v){
					$list['buy_record'][$k]=$v;
					$list['buy_record'][$k]['buy_time']=date('m-d',$v['buy_time']);
					//策略盈亏(收益)
					$list['buy_record'][$k]['income']=($v['nowprice']-$v['buy_price'])*$v['stock_number'];
				}
			}else{
				//没有炒股大赛创建的持股记录
				$list['buy_record']=[];
			}
		}
		$list['nick_name']=substr($list['nick_name'],0,3).'****'.substr($list['nick_name'],7,4);
		$this->assign('list',$list);

		return $this->fetch();
	}
	//计算某个会员真实策略里的收益率
	public function shouyiInfo($account,$where=[]) {
		$where['account']=$account;
		$where['status']=['in',[3,4]];
		$store=Db::name('strategy')->where(['is_cancel_order'=>1])->where($where)->select();
		if($store){
			//收益，平台分成后的钱（包括信用金）
			$shouyi=0;
			//信用金（交易本金）
			$credit=0;
			foreach($store as $k=>$v){
				$credit+=$v['credit'];
				$shouyi+=($v['true_getmoney']-$v['credit']);
			}
			//收益率
			return (round($shouyi/$credit*100,2)).'%';
		}else{
			return '0%';
		}
	}
	//计算某个会员炒股大赛里的收益率
	public function shouyiInfoContest($account,$where=[]) {
		$where['account']=$account;
		$where['status']=['in',[3,4]];
		$store=Db::name('strategy_contest')->where($where)->select();
		if($store){
			//收益，(卖出价-买入价)*股数
			$shouyi=0;
			//信用金，市值，是一样的
			$credit=0;
			foreach($store as $k=>$v){
				$credit+=$v['credit'];
				$shouyi+=($v['sell_price']-$v['buy_price'])*$v['stock_number'];
			}
			//收益率
			return (round($shouyi/$credit*100,2)).'%';
		}else{
			return '0%';
		}
	}





	



}