<?php
namespace app\index\controller;
use think\Db;
use think\Request;
use think\Controller;
use think\Validate;
class Guide extends controller
{
    
  
  
  
  
  	//新手引导 列表
    public function index(){
        $res = Db::name('guide')->field(array('id','problem'))->where('is_del',1)->select();
        $this->assign('res',$res);
        return $this->fetch();

    }
    //新手引导详情
    public function info(){
        $info = Request::instance()->param();
        $res = Db::name('guide')->where('id',$info['id'])->find();
        $this->assign('res',$res);
        return $this->fetch();
    }
  
  
}