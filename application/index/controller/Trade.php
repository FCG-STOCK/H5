<?php
namespace app\index\controller;
use think\Db;
use think\Request;
use think\Controller;
use think\Validate;
class Trade extends controller
{
  
  	public function __construct(Request $request) {
         parent::__construct($request);
         if(!cookie('username')){
             $this->redirect('index/login');
         }
      	 if(!cookie('lastlogin_time')){
             $this->redirect('index/login');
         }
      
       	$account = $_COOKIE['username'];
       	$lastlogin_time = $_COOKIE['lastlogin_time'];
       	$db_lastlogin_time = Db::name('admin')->where('account',$account)->value('lastlogin_time');
       	if($lastlogin_time != $db_lastlogin_time){
         	$this->redirect('index/login');
         }
      
      
     }
  
    public function index(){
        return $this->fetch();
    }
    public function simulation(){
        return $this->fetch();
    }
    public function ploy(){
        return $this->fetch();
    }
    public function rank(){
        return $this->fetch();
    }
    public function experts(){
        return $this->fetch();
    }
    public function info(){
        return $this->fetch();
    }
    public function subdy(){
        return $this->fetch();
    }
    public function trade_hold_info(){
        return $this->fetch();
    }
    public function trade_hold_info_two(){
        return $this->fetch();
    }
    
}