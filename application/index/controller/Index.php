<?php
namespace app\index\controller;
use think\Db;
use think\Request;
use think\Controller;
use think\Validate;
class Index extends controller
{

    // public function __construct(Request $request) {
    //     parent::__construct($request);
    //     if(!cookie('username')){
    //         $this->redirect('index/login');
    //     }
    // }

    public function login(){
        
        return $this->fetch();
    }
  
  	public function imformation(){
        
        return $this->fetch();
    }

    public function header(){
        return $this->fetch();

    }
    public function footer(){
        return $this->fetch();

    }
    public function optional_search(){
        return $this->fetch();

    }
    public function optional_search_trade(){
        return $this->fetch();
    }


    

   public function index(){

        $shouye_news = Db::name('news')
            ->where(['cid'=>['neq',6],'is_recommend'=>1,'is_del'=>1])
            ->order('create_time desc')
            ->limit(15)
            ->select();

        foreach ($shouye_news as $k => $v) {
            $shouye_news[$k]['create_time'] = date('Y-m-d',$v['create_time']);
            $arr = explode(',', $v['picurl']);
            $shouye_news[$k]['picurl'] = $arr[0];
        }

        // echo "<pre>";
        // print_r($shouye_news);
        // echo "</pre>";die;

        $this->assign('shouye_news',$shouye_news);


        //首页公告新闻轮播
        $gnews = Db::name('news')->where('cid',6)->where('is_del',1)->select();
        foreach ($gnews as $k => $v) {
            $gnews[$k]['create_time'] = date('Y-m-d',$gnews[$k]['create_time']);
        }

        $this->assign('gnews',$gnews);


        $banner = Db::name('banner')->where(['is_del'=>1,'ban_id'=>1])->select();

        $this->assign('banner',$banner);
        
        return $this->fetch();

    }

    public function news_info(){

        //浏览次数加1
        Db::name('news')->where(['id' => input('id')])->setInc('times');
        
        $id = input('id');

        $res = Db::name('news')->where('id',$id)->find();

        $res['create_time'] = date('Y-m-d',$res['create_time']);

        $this->assign('res',$res);
        
        return $this->fetch();

    }


    public function register(){
        return $this->fetch();

    }


    public function forget(){
        return $this->fetch();
    }



     /*
    cid 1头条  2股票  3理财  4基金  5私募  7研报
    */
    //头条
    public function news(){       
        $info = Request::instance()->param();
        
        if(array_key_exists('cid',$info)){
            $cid = $info['cid'];
        }else{
            $cid=1;//默认头条
        }

        //查询种类
        $news_cat = Db::name('news_category')->where(['cid'=>['neq',6]])->select();
        $this->assign('news_cat',$news_cat);
        $this->assign('cid',$cid);
        
        $news = Db::name('news')->where(['cid'=>$cid,'is_del'=>1])->order('create_time desc')->select();


        foreach ($news as $k => $v) {
            $news[$k]['create_time'] = date('Y-m-d',$news[$k]['create_time']);
            $news[$k]['picurl'] = explode(',', $v['picurl']);
            if(count($news[$k]['picurl'])>3 || count($news[$k]['picurl'])==3){
                $news[$k]['bs'] = 2;//图片的数量大于3，给一个标识2
            }else{
                $news[$k]['bs'] = 1;
            }
        }
        $this->assign('news',$news);
        return $this->fetch();
    }   

    

    //公告
    public function notice(){
        $news = Db::name('news')->where('cid',6)->where('is_del',1)->order('create_time desc')->select();
        foreach ($news as $k => $v) {
            $news[$k]['create_time'] = date('Y-m-d',$news[$k]['create_time']);
        }
        $this->assign('news',$news);
        return $this->fetch();
    }



    

    //获取单个股票数据
    public function getOneStock($gid) {
        $url = 'http://web.juhe.cn:8080/finance/stock/hs';
        $data['key'] = '7fdd610575a30a240bba1b9cd7529896';
        $data['gid'] = $gid;
        $res = https_request($url, '', $data);
        $stock = json_decode($res, true);
        return json($stock, 1, '查询成功');
    }





  
  
  
  	//搜索界面查询数据
    public function getOneStock2($gid) {
        $arr=explode('~',iconv("gb2312", "utf-8//IGNORE",file_get_contents('http://qt.gtimg.cn/q='.$gid)));
        return $arr[3];
    }
  
  
  	

  	//操作指南
    public function introducation(){      
        return $this->fetch();
    }
  
  	//下载
    public function down(){
        return $this->fetch();
    }
  
  	//h5注册页
    public function registerh5(){
      	$register = input('register');
      	$this->assign('register',$register);
        return $this->fetch();
    }
  	
  
  	//新消息
    public function information(){
        $info = Request::instance()->param();
        $info['account']=cookie('username');
        $list=Db::name('push')->where(['account' => $info['account'],'status'=>1,'is_del'=>1])->select();
        $arr['all']=count($list);
        $arr['xitong']=0;
        $arr['hongbao']=0;
        $arr['celue']=0;
        $arr['dingyue']=0;
        foreach ($list as $k => $v) {
            if($v['push_type']==1){
                $arr['xitong']+=1;
            }elseif($v['push_type']==2){
                $arr['hongbao']+=1;
            }elseif($v['push_type']==3){
                $arr['celue']+=1;
            }elseif($v['push_type']==4){
                $arr['dingyue']+=1;
            }
        }
        $this->assign('list',$arr);
        return $this->fetch();
    }
    //消息中心系统消息
    public function systemPush() {
        $info = Request::instance()->param();
        $info['account']=cookie('username');
        Db::name('push')->where(['account' => $info['account'],'push_type'=>1,'status'=>1,'is_del'=>1])->update(['is_del'=>2]);
        $list=Db::name('push')->where(['account' => $info['account'],'push_type'=>1,'status'=>1,'is_del'=>2])->order('create_time desc')->select();
        foreach ($list as $k => $v) {$list[$k]['create_time']=date('Y-m-d H:i:s',$v['create_time']);}
        $this->assign('list',$list);
        return $this->fetch();
    }
    //消息中心策略消息
    public function strategyPush() {
        $info = Request::instance()->param();
        $info['account']=cookie('username');
        Db::name('push')->where(['account' => $info['account'],'push_type'=>3,'status'=>1,'is_del'=>1])->update(['is_del'=>2]);
        $list=Db::name('push')->where(['account' => $info['account'],'push_type'=>3,'status'=>1,'is_del'=>2])->order('create_time desc')->select();
        foreach ($list as $k => $v) {$list[$k]['create_time']=date('Y-m-d',$v['create_time']);}
        $this->assign('list',$list);
        return $this->fetch();
    }
    //消息中心订阅消息
    public function subscribePush() {
        $info = Request::instance()->param();
        $info['account']=cookie('username');
        Db::name('push')->where(['account' => $info['account'],'push_type'=>4,'status'=>1,'is_del'=>1])->update(['is_del'=>2]);
        $list=Db::name('push')->where(['account' => $info['account'],'push_type'=>4,'status'=>1,'is_del'=>2])->order('create_time desc')->select();
        foreach ($list as $k => $v) {$list[$k]['create_time']=date('Y-m-d',$v['create_time']);}
        $this->assign('list',$list);
        return $this->fetch();
    }
  
  

}