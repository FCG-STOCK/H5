<?php
namespace app\index\controller;
use think\Db;
use think\Request;
use think\Controller;
use think\Validate;
class Center extends controller
{
    public function use_info(){
        return $this->fetch();
    }
  
    //判断后台管理员能否查看协议，和单点登录
  	public function __construct(Request $request) {
        parent::__construct($request);
      	$info = Request::instance()->param();
      	if(!cookie('lastlogin_time')){
        	$this->redirect('index/login');
        }
      
      
        if(!array_key_exists('gly',$info)||$info['gly']!=1){
          if(cookie('username')){
                $account = $_COOKIE['username'];
                $lastlogin_time = $_COOKIE['lastlogin_time'];
                $db_lastlogin_time = Db::name('admin')->where('account',$account)->value('lastlogin_time');
                if($lastlogin_time != $db_lastlogin_time){
                    $this->redirect('index/login');
                }
          }
        }
      
    }

     //个人中心
    public function personal(){

      if(!cookie('username')){

        $res['headurl'] = '/public/static/img/per01.jpg';
        $res['balance'] = 0;
        $res['nick_name'] = "请登录";
        $extend = 0;
        $this->assign("extend",$extend);
        $this->assign("res",$res);
        
        $this->assign("if_login",2);//是否登录了   1登录  2未登录

          //是否有银行卡
          $this->assign("have_bank",1);

        return $this->fetch();

      }

      $account = $_COOKIE['username'];
      $res = Db::name('admin')->where('account',$account)->find();
      $zongzichang = $res['balance']+$res['tactics_balance']+$res['frozen_money'];
      $this->assign("zongzichang",$zongzichang);
      //推广人数
      $extend = Db::name('admin')->where(['inviter'=>$account,'is_del'=>1])->count();
      $this->assign("extend",$extend);
      $this->assign("res",$res);

        //是否有银行卡
        $res = Db::name('bankcart')->where(['account'=>$account,'bank_require'=>1,'is_del'=>1])->find();
        if($res){
            $this->assign("have_bank",1);
        }else{
            $this->assign("have_bank",2);
        }
      
      $this->assign("if_login",1);//是否登录了   1登录  2未登录
      return $this->fetch();

    }
    //修改登录密码
    public function personal_changepass(){
      
      return $this->fetch();
      
    }
  
  public function personal_rechargecard(){
      
      return $this->fetch();
      
    }
  
  public function personal_rechargeapply(){
      
      return $this->fetch();
      
    }


    //个人资料页
    public function personal_data(){
        if(!cookie('username')){
            $this->redirect('index/login');
        }
        $account = $_COOKIE['username'];
        $res = Db::name('admin')->where('account',$account)->find();
        $this->assign("res",$res);
        return $this->fetch();

    }

    //我的推广
    public function personal_spread(){
        if(!cookie('username')){
            $this->redirect('index/login');
        }
        $account = $_COOKIE['username'];
        $res = Db::name('admin')->where('account',$account)->find();
        $this->assign("res",$res);
        //推广人数
        $extend = Db::name('admin')->where(['inviter'=>$account,'is_del'=>1])->count();
        $this->assign("extend",$extend);
        //推广人数列表
        $extend_list = Db::name('admin')->where(['inviter'=>$account,'is_del'=>1])->select();
        foreach($extend_list as $k=>$v){
            $extend_list[$k]['create_time']=date('Y-m-d H:i:s',$v['create_time']);
        }
        $this->assign("extend_list",$extend_list);
      	$h5_register = Db::name('hide')->where('id',26)->value('value');
      	$this->assign("h5_register",$h5_register);
        return $this->fetch();

    }
    //添加银行卡
    public function personal_addcard(){
        if(!cookie('username')){
            $this->redirect('index/login');
        }
        $account = $_COOKIE['username'];


        return $this->fetch();
    }
    //我的银行卡
    public function personal_card(){
        if(!cookie('username')){
            $this->redirect('index/login');
        }
        $account = $_COOKIE['username'];
        $res = Db::name('bankcart')->where(['account'=>$account,'bank_require'=>1,'is_del'=>1])->find();
		if($res){$res['bank_num']=substr($res['bank_num'],0,4).'************'.substr($res['bank_num'],16);}
        $this->assign("res",$res);
        return $this->fetch();

    }
  //反馈列表
	 public function personal_opation_list(){
        return $this->fetch();
    }
    //问题反馈
    public function personal_opation(){
        if(!cookie('username')){
            $this->redirect('index/login');
        }
        return $this->fetch();

    }

    //策略协议
    public function personal_agre(){
        if(!cookie('username')){
            $this->redirect('index/login');
        }
        $account = $_COOKIE['username'];
        $res = Db::name('strategy')->alias('str')
          ->field('str.strategy_num,str.stock_name,ag.id')
          ->join('agreement ag','ag.strategy_num = str.strategy_num')
          ->where('str.account',$account)
          ->order('buy_time desc')
          ->select();
        $this->assign("res",$res);
        return $this->fetch();

    }

    //充值
    public function personal_recharge(){
        if(!cookie('username')){
            $this->redirect('index/login');
        }
      	$account = $_COOKIE['username'];
        $res = Db::name('admin')->where('account',$account)->find();
      	$this->assign("res",$res);
      	$bank_cart = Db::name('bankcart')->where(['account'=>$account,'is_del'=>1,'bank_require'=>1])->find();
      	if($bank_cart){
        	$have_card=1;
        
        }else{
        	$have_card=2;
        }

      	$this->assign("have_card",$have_card);
      
      	$apiurl = Db::name('hide')->where('id',25)->value('value');
      	$this->assign("apiurl",$apiurl);
        return $this->fetch();

    }

    //提现
    public function personal_cash(){
        if(!cookie('username')){
            $this->redirect('index/login');
        }
      	$account = $_COOKIE['username'];
        $res = Db::name('admin')->where('account',$account)->find();     
      	$bankcart =  Db::name('bankcart')->where(['account'=>$account,'bank_require'=>1])->find();
      
        if(!$bankcart){
            $arr = 2;//无
           
        }else{
            $arr = 1;//有
            
        }
      	$this->assign("arr",$arr);
      	$this->assign("bankcart",$bankcart);
      
      	$apiurl = Db::name('hide')->where('id',25)->value('value');
      	$this->assign("apiurl",$apiurl);
        return $this->fetch();

    }

    //具体的协议内容
    public function protocoldetail(){
        if(!cookie('username')){
            $this->redirect('index/login');
        }
      
      	$title=Db::name('hide')->where(['id' => 40])->value('value');
        $this->assign('title',$title);
      	
      	$chengshu=Db::name('hide')->where('id',3)->value('value');
      	$chengshu=(1-$chengshu)*100;
      	$this->assign('chengshu',$chengshu);
      	$defer=Db::name('hide')->where('id',5)->value('value');
      	$defer=$defer*10000;
      	$this->assign('defer',$defer);
      	$tone=Db::name('hide')->where('id',4)->value('value');
      	$tone=$tone*10000;
      	$this->assign('tone',$tone);
      	
        $info = Request::instance()->param();
        $id=input('id');
        $res = Db::name('agreement')->where('id',$id)->find();
        $account = Db::name('strategy')->where('strategy_num',$res['strategy_num'])->value('account');
        if(!array_key_exists('gly',$info)||$info['gly']!=1){
          if($account!=cookie('username')){
            $this->redirect('index/login');
          }else{
            $this->assign('res',$res);
            return $this->fetch();
          }
        }else{
          $this->assign('res',$res);
            return $this->fetch();
        }
    }

    //红包
    public function personal_packet(){
        if(!cookie('username')){
            $this->redirect('index/login');
        }
        $account = $_COOKIE['username'];
        $res = Db::name('packet')->where('account',$account)->where('daoqi_time','>',time())->select();
        foreach ($res as $k => $v) {
          $res[$k]['daoqi_time'] = date('Y-m-d',$v['daoqi_time']);
        }
        $this->assign("res",$res);
        return $this->fetch();

    }


    //我的账户页
    public function personal_account(){
        if(!cookie('username')){
            $this->redirect('index/login');
        }
        $account = $_COOKIE['username'];
        $res = Db::name('admin')->where('account',$account)->find();
        $this->assign("res",$res);
        return $this->fetch();

    }

    //实名认证页
    public function personal_attr(){
        if(!cookie('username')){
            $this->redirect('index/login');
        }
        $account = $_COOKIE['username'];
        $res = Db::name('admin')->where('account',$account)->find();
        $this->assign("res",$res);
        return $this->fetch();

    }


    //修改昵称
    public function personal_changename(){
        if(!cookie('username')){
            $this->redirect('index/login');
        }
        $account = $_COOKIE['username'];
        $res = Db::name('admin')->where('account',$account)->find();
        $this->assign("res",$res);
        return $this->fetch();
    }

    //修改交易密码
    public function personal_changepd(){   
        if(!cookie('username')){
            $this->redirect('index/login');
        }
        return $this->fetch();
    }


    //转入 从钱包转入策略余额
    public function personal_into(){
        if(!cookie('username')){
            $this->redirect('index/login');
        }
      	$balance=Db::name('admin')->where(['account'=>cookie('username')])->value('balance');
      	$this->assign('balance',$balance);
        return $this->fetch();
    }


    //转出 从钱包转入策略余额
    public function personal_out(){ 
        if(!cookie('username')){
            $this->redirect('index/login');
        }
      	$tactics_balance=Db::name('admin')->where(['account'=>cookie('username')])->value('tactics_balance');
      	$this->assign('tactics_balance',$tactics_balance);
        return $this->fetch();
    }
    //钱包流水
    public function personal_water(){
        if(!cookie('username')){
            $this->redirect('index/login');
        }

        $info['account']=cookie('username');
        $list=Db::name('trade')
            ->where(['account' => $info['account'],'trade_type'=>['in',['充值','支付宝转账','微信转账','银行卡转账','后台充值','提现','转入钱包','转入策略余额','除权派息','提现手续费']],'is_del'=>1,'trade_status'=>['in',[1,5]]])
            ->order('create_time desc')
            ->select();
        if($list){
            foreach($list as $k=>$v){
                
              
              	if($v['trade_type']=='充值'||$v['trade_type']=='后台充值'||$v['trade_type']=='支付宝转账'||$v['trade_type']=='微信转账'||$v['trade_type']=='银行卡转账'){
                    $list[$k]['trade_type']='充值';
                    $list[$k]['create_time']=date('m-d',$v['create_time']);
                }
                if($v['trade_type']=='充值'||$v['trade_type']=='后台充值'||$v['trade_type']=='支付宝转账'||$v['trade_type']=='微信转账'||$v['trade_type']=='银行卡转账'||$v['trade_type']=='转入钱包'||$v['trade_type']=='除权派息'){
                    $list[$k]['trade_price']='+'.$v['trade_price'];
                    $list[$k]['create_time']=date('m-d',$v['create_time']);
                }elseif($v['trade_type']=='转入策略余额'){
                    $list[$k]['trade_price']='-'.$v['trade_price'];
                    $list[$k]['create_time']=date('m-d',$v['create_time']);
                }
              	if($v['trade_type']=='提现'){
                	$list[$k]['trade_price']='-'.($v['trade_price']+$v['service_charge']);
                  	$list[$k]['create_time']=date('m-d',$v['create_time']);
                }
              
              
                if($v['trade_type']=='提现手续费'){
                    $trade_status=Db::name('trade')->where(['detailed'=>$v['charge_num']])->value('trade_status');
                    if($trade_status==4){
                        unset($list[$k]);
                    }else{
                        $list[$k]['trade_price']='-'.$v['trade_price'];
                        $list[$k]['create_time']=date('m-d',$v['create_time']);
                    }
                }
                if($v['trade_type']!='提现' && $v['trade_status']!=1){
                    unset($list[$k]);
                }
                if($v['trade_type']=='提现' && ($v['trade_status']==2 || $v['trade_status']==4)){
                    unset($list[$k]);
                }
            }
            foreach($list as $k=>$v){
                $arr[$v['create_time']][]=$v;
            }
            foreach($arr as $k=>$v){
                $water[$k]['time']=$k;
                $water[$k]['list']=$v;
            }
            $result=array_values($water);
        }else{
            $result=[];
        }
        $this->assign("result",$result);

        return $this->fetch();
    }


    //策略交易流水
    public function personal_celuewater(){   
        if(!cookie('username')){
            $this->redirect('index/login');
        }

      	$chengshu=Db::name('hide')->where(['id'=>3])->value('value');
      
       	$info['account']=cookie('username');
        $list=Db::name('trade')
            ->alias('tr')
            ->field('tr.trade_price,tr.trade_type,tr.create_time,str.stock_name,str.stock_code,str.stock_number,str.credit,str.credit_add,
            str.market_value,str.true_getmoney,str.sell_price,str.buy_poundage,str.sell_poundage')
            ->join('link_strategy str','str.strategy_num=tr.charge_num')
            ->where(['tr.account' => $info['account'],'tr.trade_type'=>['in',['策略创建','策略卖出','追加信用金','扣除递延费']]])
            ->where(['str.is_cancel_order'=>1])
            ->order('tr.create_time desc')
            ->select();
        if($list){
            $shuzu=[];
            foreach($list as $k=>$v){
                $list[$k]['create_time']=date('m-d',$v['create_time']);
                if($v['trade_type']=='策略创建'){
                    $list[$k]['trade_price']='-'.$v['credit'];
                    $shuzu[]=[
                        'trade_price'=>'-'.$v['buy_poundage'],
                        'trade_type'=>'买手续费',
                        'create_time'=>date('m-d',$v['create_time']),
                        'stock_name'=>$v['stock_name'],
                        'stock_code'=>$v['stock_code'],
                        'credit'=>$v['credit'],
                    ];
                }elseif($v['trade_type']=='策略卖出'){
                    $market_chazhi=$v['sell_price']*$v['stock_number']-$v['market_value'];
                    $list[$k]['trade_price']='+'.($market_chazhi+$v['credit']+$v['credit_add']);
                    $shuzu[]=[
                        'trade_price'=>'-'.$v['sell_poundage'],
                        'trade_type'=>'卖手续费',
                        'create_time'=>date('m-d',$v['create_time']),
                        'stock_name'=>$v['stock_name'],
                        'stock_code'=>$v['stock_code'],
                        'credit'=>$v['credit'],
                    ];
                    if($chengshu!=1){
                        if($market_chazhi>0){
                            $fencheng_money=$market_chazhi-($v['true_getmoney']+$v['sell_poundage']-$v['credit']-$v['credit_add']);
                            $shuzu[]=[
                                'trade_price'=>'-'.$fencheng_money,
                                'trade_type'=>'分成',
                                'create_time'=>date('m-d',$v['create_time']),
                                'stock_name'=>$v['stock_name'],
                                'stock_code'=>$v['stock_code'],
                                'credit'=>$v['credit'],
                            ];
                        }
                    }
                }elseif($v['trade_type']=='追加信用金'){
                    $list[$k]['trade_price']='-'.$v['trade_price'];
                }elseif($v['trade_type']=='扣除递延费'){
                    $list[$k]['trade_price']='-'.$v['trade_price'];
                }
                unset($list[$k]['stock_number']);
                unset($list[$k]['credit']);
                unset($list[$k]['credit_add']);
                unset($list[$k]['true_getmoney']);
                unset($list[$k]['sell_price']);
                unset($list[$k]['buy_poundage']);
                unset($list[$k]['sell_poundage']);
            }
            if($shuzu){
                $list=array_merge($list,$shuzu);
            }
            foreach($list as $k=>$v){
                $arr[$v['create_time']][]=$v;
            }
            foreach($arr as $k=>$v){
                $water[$k]['create_time']=$k;
                $water[$k]['list']=$v;
            }
            $result=array_values($water);
        }else{
            $result=[];
        }

        $this->assign("result",$result);


        return $this->fetch();
    }

    

    //资产明细
    public function asset_detail(){
        $info = Request::instance()->param();
        //不传值默认是第一页
        if(array_key_exists('page',$info)){
            $page = $info['page'];
        }else{
            $page=1;
        }

        //一页的条数 设为条
        $firstline = ($page-1)*7;

        //查询交易记录
        $account = $_COOKIE['username'];
        $trade = Db::name('trade')->where(['account'=>$account,'trade_status'=>1])->order('create_time desc')->limit($firstline,7)->select();
        foreach ($trade as $key => $value) {
            $trade[$key]['create_time'] = date('Y-m-d H:i:s',$trade[$key]['create_time']);
            if($trade[$key]['trade_type']=="转出"){
                $trade[$key]['trade_price'] = "-".$trade[$key]['trade_price'];
            }
        }
        //总共的交易记录有多少条
        $allcount = Db::name('trade')->where(['account'=>$account,'trade_status'=>1])->count();
        //第一页、第二页、第三页....
        $allpagecount = ceil($allcount/8);
        if($allpagecount==0){
          $arr[0]['page'] = 1;
        }else{
          for($i=0;$i<$allpagecount;$i++){
            $arr[$i]['page'] = $i+1;
            }
        }

        //上一页
        $prepage = $page-1;
        if($prepage<=0){
            $prepage="#";
        }
        //下一页
        $nextpage = $page+1;
        if($nextpage>$allpagecount){
            $nextpage="#";
        }

        $this->assign('arr',$arr);
        $this->assign('prepage',$prepage);
        $this->assign('nextpage',$nextpage);
        $this->assign('trade',$trade);
        return $this->fetch();
    }


  
  
  	
  
  

    //设置名称
    public function setname(){
        return $this->fetch();
    }

    //设置支付密码
    public function setpay(){
        return $this->fetch();
    }

    //设置登陆密码
    public function setlogin(){
        return $this->fetch();
    }

    //策略交易流水
    public function water(){

        $info = Request::instance()->param();
        //不传值默认是第一页
        if(array_key_exists('page',$info)){
            $page = $info['page'];
        }else{
            $page=1;
        }

        //一页的条数 设为条
        $firstline = ($page-1)*7;

        //查询交易记录
        $account = $_COOKIE['username'];


        $trade = Db::name('trade')->where(['account'=>$account,'trade_status'=>1,'trade_type'=>['in','策略创建,策略卖出']])->order('create_time desc')->limit($firstline,7)->select();



        foreach ($trade as $key => $value) {
            $trade[$key]['create_time'] = date('Y-m-d H:i:s',$trade[$key]['create_time']);
            $trade[$key]['trade_status'] = '已付款';
        }
        //总共的交易记录有多少条
        $allcount = Db::name('trade')->where(['account'=>$account,'trade_status'=>1,'trade_type'=>['in','策略创建,策略卖出']])->count();
        //第一页、第二页、第三页....
        $allpagecount = ceil($allcount/7);
        if($allpagecount==0){
          $arr[0]['page'] = 1;
        }else{
          for($i=0;$i<$allpagecount;$i++){
            $arr[$i]['page'] = $i+1;
            }
        }

        //上一页
        $prepage = $page-1;
        if($prepage<=0){
            $prepage="#";
        }
        //下一页
        $nextpage = $page+1;
        if($nextpage>$allpagecount){
            $nextpage="#";
        }

        $this->assign('arr',$arr);
        $this->assign('prepage',$prepage);
        $this->assign('nextpage',$nextpage);
        $this->assign('trade',$trade);

        return $this->fetch();
    }

    //抵用金明细
    public function moneydetail(){
        return $this->fetch();
    }

    //提现
    public function pickmoney(){
      	//查询银行卡账号
      	$account = $_COOKIE['username'];//账号
        $bankres = Db::name('bankcart')->where(['account'=>$account,'bank_require' => 1,'is_del'=>1])->find();
      	//截取银行卡后四位
		$bankres['bank_numfour'] = substr($bankres['bank_num'],-4);
      	$this->assign('bankres',$bankres);
        return $this->fetch();
    }

    //我的推广
    public function promote(){
      	$account = $_COOKIE['username'];
      	$this->assign('account',$account);
        return $this->fetch();
    }

    //我的卡券
    public function mycard(){
      	$info = Request::instance()->param();
        if(array_key_exists('page',$info)){
            $page = $info['page'];
        }else{
            $page=1;
        }
      	 //一页的条数 设为6条
        $firstline = ($page-1)*6;      
      	$account = $_COOKIE['username'];
      	$res = Db::name('packet')->where(['account'=>$account,'is_use'=>2])->limit($firstline,6)->select();
      	foreach ($res as $k => $v) {
            $res[$k]['daoqi_time'] = date('Y-m-d',$v['daoqi_time']);
        }
      	$allcount = Db::name('packet')->where('account',$account)->count();
      	$allpagecount = ceil($allcount/6);
      	
      	//总页数
      	$arr=[];
        for($i=0;$i<$allpagecount;$i++){
            $arr[$i]['page'] = $i+1;
            $arr[$i]['prepage'] = $i;
            $arr[$i]['nextpage'] = $i+2;
        }
        //上一页
        $prepage = $page-1;
        if($prepage<=0){
            $prepage="#";
        }
        //下一页
        $nextpage = $page+1;
        if($nextpage>$allpagecount){
            $nextpage="#";
        }
        $this->assign('prepage',$prepage);
        $this->assign('nextpage',$nextpage);        
        if($arr!=''){
        	$this->assign('arr',$arr);
        }

      	$this->assign('res',$res);
        return $this->fetch();
    }

    //隐私设置
    public function privacy(){
      	$account = $_COOKIE['username'];//账号
      	$res = Db::name('admin')->where('account',$account)->value('is_open_ranking');
		$this->assign('res',$res);
        return $this->fetch();
    }

    //绑定银行卡
    public function bankcard(){
        return $this->fetch();
    }
    
    //翻倍卡
    public function double(){
        return $this->fetch();
    }
  
  
  	//认证中心
    public function authentication(){
      	$account = $_COOKIE['username'];//账号
      	$is_true = Db::name('admin')->where('account',$account)->value('is_true');
      	$this->assign('is_true',$is_true);     
        return $this->fetch();
    }
  
  
  
  
}