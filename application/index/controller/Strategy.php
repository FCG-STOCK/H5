<?php
namespace app\index\controller;
use think\Db;
use think\Request;
use think\Controller;
use think\Validate;
use Stock;
class Strategy extends controller
{
  
  	 public function __construct(Request $request) {
         parent::__construct($request);
         if(!cookie('username')){
             $this->redirect('index/login');
         }
      
       	$account = $_COOKIE['username'];
       	$lastlogin_time = $_COOKIE['lastlogin_time'];
       	$db_lastlogin_time = Db::name('admin')->where('account',$account)->value('lastlogin_time');
       	if($lastlogin_time != $db_lastlogin_time){
         	$this->redirect('index/login');
         }
      
      
     }
  	public function ploy(){
      	$zhiying=Db::name('hide')->where('id',21)->value('value');
      	$this->assign('zhiying',$zhiying);
      	$zhiyinglv=$zhiying*100-100;
      	$this->assign('zhiyinglv',$zhiyinglv);
      	$is_zhiying=Db::name('hide')->where('id',49)->value('value');
      	$this->assign('is_zhiying',$is_zhiying);
        return $this->fetch();
    }
    public function ployD(){
      	$zhiying=Db::name('hide')->where('id',21)->value('value');
      	$this->assign('zhiying',$zhiying);
      	$zhiyinglv=$zhiying*100-100;
      	$this->assign('zhiyinglv',$zhiyinglv);
      	$is_zhiying=Db::name('hide')->where('id',49)->value('value');
      	$this->assign('is_zhiying',$is_zhiying);
        return $this->fetch();
    }
  	public function info_t1(){
        return $this->fetch();
    }
  	public function info_t5(){
        return $this->fetch();
    }
    //股票模糊搜索页面
    public function search_stock(){
        if(!cookie('username')){
            $this->redirect('index/login');
        }
        //热门股票 显示6条

        $arr = [];
        $arr2 = [];
        $hot = Db::name("stock_recommend")->where("is_recommend",1)->limit(6)->select();

        foreach ($hot as $key => $value) {
            $arr[$key] =  $this->getOneStock2($hot[$key]['stock_code']);
        }
        foreach ($arr as $key => $value) {
            $arr2[$key] = json_decode($arr[$key],true);
        }
        foreach ($arr2 as $key => $value) {
            $res[$key] = $arr2[$key]['list']['result'][0]['data']['nowPri'];
        }
        foreach ($res as $key => $value) {
            $hot[$key]['nowPri'] = $res[$key];
        }
        foreach ($hot as $key => $value) {
            $hot[$key]['cookie'] = $hot[$key]['stock_code']."(".$hot[$key]['stock_name'].")";
            $hot[$key]['id'] = $hot[$key]['stock_code']."bj";
        }

        //其他用户都在买 显示6条
        $otherarr = [];
        $otherarr2 = [];
        $other = Db::query('SELECT
                link_strategy.stock_name,
                link_strategy.stock_code,
                count(link_strategy.stock_code) as buycount
                FROM
                link_strategy
                GROUP BY
                link_strategy.stock_code
                ORDER BY buycount DESC
                LIMIT 6');

        //如果没有用户在买
        if(!$other){
            $other=[];
            $this->assign('hot',$hot);
            $this->assign('other',$other);
            return $this->fetch();
        }else{
            foreach ($other as $key => $value) {
                $otherarr[$key] =  $this->getOneStock2($other[$key]['stock_code']);
            }
            foreach ($otherarr as $key => $value) {
                $otherarr2[$key] = json_decode($otherarr[$key],true);
            }
            foreach ($otherarr2 as $key => $value) {
                $nowPri[$key] = $otherarr2[$key]['list']['result'][0]['data']['nowPri'];
            }
            foreach ($nowPri as $key => $value) {
                $other[$key]['nowPri'] = $nowPri[$key];
            }
            foreach ($other as $key => $value) {
                $other[$key]['cookie'] = $other[$key]['stock_code']."(".$other[$key]['stock_name'].")";
                $other[$key]['id'] = $other[$key]['stock_code']."otbj";
            }

            $this->assign('hot',$hot);
            $this->assign('other',$other);
            return $this->fetch();

        }
    }

    //股票模糊搜索页面
    public function search_stock_simulation(){
        if(!cookie('username')){
            $this->redirect('index/login');
        }
        //热门股票 显示6条

        $arr = [];
        $arr2 = [];
        $hot = Db::name("stock_recommend")->where("is_recommend",1)->limit(6)->select();

        foreach ($hot as $key => $value) {
            $arr[$key] =  $this->getOneStock2($hot[$key]['stock_code']);
        }
        foreach ($arr as $key => $value) {
            $arr2[$key] = json_decode($arr[$key],true);
        }
        foreach ($arr2 as $key => $value) {
            $res[$key] = $arr2[$key]['list']['result'][0]['data']['nowPri'];
        }
        foreach ($res as $key => $value) {
            $hot[$key]['nowPri'] = $res[$key];
        }
        foreach ($hot as $key => $value) {
            $hot[$key]['cookie'] = $hot[$key]['stock_code']."(".$hot[$key]['stock_name'].")";
            $hot[$key]['id'] = $hot[$key]['stock_code']."bj";
        }

        //其他用户都在买 显示6条
        $otherarr = [];
        $otherarr2 = [];
        $other = Db::query('SELECT
                link_strategy.stock_name,
                link_strategy.stock_code,
                count(link_strategy.stock_code) as buycount
                FROM
                link_strategy
                GROUP BY
                link_strategy.stock_code
                ORDER BY buycount DESC
                LIMIT 6');

        //如果没有用户在买
        if(!$other){
            $other=[];
            $this->assign('hot',$hot);
            $this->assign('other',$other);
            return $this->fetch();
        }else{
            foreach ($other as $key => $value) {
                $otherarr[$key] =  $this->getOneStock2($other[$key]['stock_code']);
            }
            foreach ($otherarr as $key => $value) {
                $otherarr2[$key] = json_decode($otherarr[$key],true);
            }
            foreach ($otherarr2 as $key => $value) {
                $nowPri[$key] = $otherarr2[$key]['list']['result'][0]['data']['nowPri'];
            }
            foreach ($nowPri as $key => $value) {
                $other[$key]['nowPri'] = $nowPri[$key];
            }
            foreach ($other as $key => $value) {
                $other[$key]['cookie'] = $other[$key]['stock_code']."(".$other[$key]['stock_name'].")";
                $other[$key]['id'] = $other[$key]['stock_code']."otbj";
            }

            $this->assign('hot',$hot);
            $this->assign('other',$other);
            return $this->fetch();

        }
    }



    //搜索界面查询数据
    public function getOneStock2($gid) {
        $arr=explode('~',iconv("gb2312", "utf-8//IGNORE",file_get_contents('http://qt.gtimg.cn/q='.$gid)));

        $list['result'][0]['data']['gid']=$gid;
        $list['result'][0]['data']['increPer']=$arr[32];
        $list['result'][0]['data']['increase']=$arr[31];
        $list['result'][0]['data']['name']=$arr[1];
        $list['result'][0]['data']['todayStartPri']=$arr[5];
        $list['result'][0]['data']['yestodEndPri']=$arr[4];
        $list['result'][0]['data']['nowPri']=$arr[3];
        $list['result'][0]['data']['todayMax']=$arr[33];
        $list['result'][0]['data']['todayMin']=$arr[34];
        $list['result'][0]['data']['traNumber']=$arr[36];
        $list['result'][0]['data']['traAmount']=$arr[37];
        $list['result'][0]['data']['buyOne']=$arr[10];
        $list['result'][0]['data']['buyOnePri']=$arr[9];
        $list['result'][0]['data']['buyTwo']=$arr[12];
        $list['result'][0]['data']['buyTwoPri']=$arr[11];
        $list['result'][0]['data']['buyThree']=$arr[14];
        $list['result'][0]['data']['buyThreePri']=$arr[13];
        $list['result'][0]['data']['buyFour']=$arr[16];
        $list['result'][0]['data']['buyFourPri']=$arr[15];
        $list['result'][0]['data']['buyFive']=$arr[18];
        $list['result'][0]['data']['buyFivePri']=$arr[17];
        $list['result'][0]['data']['sellOne']=$arr[20];
        $list['result'][0]['data']['sellOnePri']=$arr[19];
        $list['result'][0]['data']['sellTwo']=$arr[22];
        $list['result'][0]['data']['sellTwoPri']=$arr[21];
        $list['result'][0]['data']['sellThree']=$arr[24];
        $list['result'][0]['data']['sellThreePri']=$arr[23];
        $list['result'][0]['data']['sellFour']=$arr[26];
        $list['result'][0]['data']['sellFourPri']=$arr[25];
        $list['result'][0]['data']['sellFive']=$arr[28];
        $list['result'][0]['data']['sellFivePri']=$arr[27];
        $list['result'][0]['data']['date']=substr($arr[30],0,4).'-'.substr($arr[30],4,2).'-'.substr($arr[30],6,2);
        $list['result'][0]['data']['time']=substr($arr[30],8,2).':'.substr($arr[30],10,2).':'.substr($arr[30],12,2);

        return json($list, 1, '查询成功');
    }


   

    //创建策略页面（投入信用金）
    public function optional_info(){
      if(!cookie('username')){
            $this->redirect('index/login');
        }
      $gid = input('gid');
      $stock_data = $this->getOneStock2($gid);
      $stock_data = json_decode($stock_data,true);
      $res = $stock_data['list']['result'][0]['data'];
      if($res['increase']<0){
        $res['bs'] = 2;//小于0颜色变绿
      }else{
        $res['bs'] = 1;//大于0颜色变红
      }
      $this->assign('res',$res);

      //策略可进行翻倍的倍数
      $celue_bs = Db::name('hide')->where('id',2)->value('value');
      $this->assign('celue_bs',$celue_bs);

      //红包(到期红包不显示)
      $account = $_COOKIE['username'];
      $packet = Db::name('packet')->where('account',$account)->where('daoqi_time','>',time())->select();
      foreach ($packet as $k => $v) {
        $packet[$k]['daoqi_time'] = date('Y-m-d',$v['daoqi_time']);
      }
      $this->assign("packet",$packet);

      //策略余额
      $tactics_balance = Db::name('admin')->where('account',$account)->value('tactics_balance');
      $this->assign('tactics_balance',$tactics_balance);

      return $this->fetch();
    }



    //创建策略页面（投入信用金）
    public function optional_info_two(){
      if(!cookie('username')){
            $this->redirect('index/login');
        }
      $gid = input('gid');
      $stock_data = $this->getOneStock2($gid);
      $stock_data = json_decode($stock_data,true);
      $res = $stock_data['list']['result'][0]['data'];
      if($res['increase']<0){
        $res['bs'] = 2;//小于0颜色变绿
      }else{
        $res['bs'] = 1;//大于0颜色变红
      }
      $this->assign('res',$res);

      //策略可进行翻倍的倍数
      $celue_bs = Db::name('hide')->where('id',2)->value('value');
      $this->assign('celue_bs',$celue_bs);

      //红包(到期红包不显示)
      $account = $_COOKIE['username'];
      $packet = Db::name('packet')->where('account',$account)->where('daoqi_time','>',time())->select();
      foreach ($packet as $k => $v) {
        $packet[$k]['daoqi_time'] = date('Y-m-d',$v['daoqi_time']);
      }
      $this->assign("packet",$packet);

      //策略余额
      $tactics_balance = Db::name('admin')->where('account',$account)->value('tactics_balance');
      $this->assign('tactics_balance',$tactics_balance);

      return $this->fetch();
    }


    //创建策略页面（投入信用金）
    public function optional_contest_info(){
      if(!cookie('username')){
            $this->redirect('index/login');
        }
      $gid = input('gid');
      $stock_data = $this->getOneStock2($gid);
      $stock_data = json_decode($stock_data,true);
      $res = $stock_data['list']['result'][0]['data'];
      if($res['increase']<0){
        $res['bs'] = 2;//小于0颜色变绿
      }else{
        $res['bs'] = 1;//大于0颜色变红
      }
      $this->assign('res',$res);

      //策略可进行翻倍的倍数
      $celue_bs = Db::name('hide')->where('id',2)->value('value');
      $this->assign('celue_bs',$celue_bs);

      //红包(到期红包不显示)
      $account = $_COOKIE['username'];
      $packet = Db::name('packet')->where('account',$account)->where('daoqi_time','>',time())->select();
      foreach ($packet as $k => $v) {
        $packet[$k]['daoqi_time'] = date('Y-m-d',$v['daoqi_time']);
      }
      $this->assign("packet",$packet);

      //策略余额
      $analog_money = Db::name('admin')->where('account',$account)->value('analog_money');
      $this->assign('analog_money',$analog_money);

      return $this->fetch();
    }






    //选择红包页面
    public function information_packet(){
        if(!cookie('username')){
            $this->redirect('index/login');
        }
        $account = $_COOKIE['username'];
        $packet = Db::name('packet')->where('account',$account)->select();
        foreach ($packet as $k => $v) {
          $packet[$k]['daoqi_time'] = date('Y-m-d',$v['daoqi_time']);
        }
        $this->assign("packet",$packet);
        return $this->fetch();
    }

    

	//创建策略T+1
	public function buyt1(){
      	      	               	
		return $this->fetch();
	}


	//创建策略T+D
	public function buytd(){
		return $this->fetch();
	}


	//创建策略T+1详情页
	public function buyt1detail(){
      	$account = $_COOKIE['username'];
      	$res = Db::name('packet')->where('account',$account)->select();
      	$this->assign('res',$res);
		$stockid = input("stockid");
		$this->assign("stockid",$stockid);
      
      	//红包
		$red = Db::name('packet')->where(['account'=>$account,'is_use'=>2])->order('daoqi_time ASC')->limit(3)->select();   
      	foreach ($red as $k => $v) {
            $red[$k]['daoqi_time'] = date('Y-m-d',$v['daoqi_time']);
        }

      	$this->assign("red",$red);
      
		return $this->fetch();
	}


	//创建策略T+d详情页
	public function buytddetail(){
      	$stockid = input("stockid");
		$this->assign("stockid",$stockid);
      
      	$account = $_COOKIE['username'];
      	$res = Db::name('packet')->where('account',$account)->select();
      	$this->assign('res',$res);
      	//红包
		$red = Db::name('packet')->where(['account'=>$account,'is_use'=>2])->order('daoqi_time ASC')->limit(3)->select();   
      	foreach ($red as $k => $v) {
            $red[$k]['daoqi_time'] = date('Y-m-d',$v['daoqi_time']);
        }
      	$this->assign("red",$red);
      
		return $this->fetch();
	}


	//创建策略T+1切换股票
	public function t1switch(){
		return $this->fetch();
	}


	//创建策略T+d切换股票
	public function tdswitch(){
		return $this->fetch();
	}

	
  
  	//获取单个股票数据
    public function getOneStock($gid) {
        $arr=explode('~',iconv("gb2312", "utf-8//IGNORE",file_get_contents('http://qt.gtimg.cn/q='.$gid)));
		return $arr[3];
    }
  
  



    //查看持仓中的策略
  public function strategy() {
    $info = Request::instance()->param();
    $info['account']=cookie('username');
    $list=Db::name('strategy')
      ->where(['is_cancel_order'=>['in',[1,2]]])
      ->where(['account'=>$info['account'],'status'=>['in',[1,2,3]]])
      ->order('buy_time desc')
      ->select();

    //查询现价
    if($list){
      if(date('Hi')>931){
        $str='';
        foreach($list as $k=>$v){
          $str.=$v['stock_code'].',';
        }
        $str=substr($str,0,-1);
        $result=file_get_contents("http://hq.sinajs.cn/list=".$str);
        $para=explode(';',$result);
        unset($para[count($para)-1]);
        foreach($para as $k=>$v){
          $list[$k]['nowprice']=explode(',',$v)[3];
        }
      }else{
        $times=1;
        for($i=1;$i<12;$i++){
          if(isHoliday($i)!=0){
            $times+=1;
          }else{
            break;
          }
          continue;
        }
        foreach($list as $k=>$v){
          $str=file_get_contents('http://money.finance.sina.com.cn/quotes_service/api/json_v2.php/CN_MarketData.getKLineData?symbol='.$v['stock_code'].'&scale=60&ma=5&datalen=9');
          $arr=explode(',',substr($str,1,-1));
          foreach($arr as $kk=>$vv){
            if(strpos($vv,date('Y-m-d',time()-$times*3600*24).' 15:00:00')){
              $list[$k]['nowprice']=substr($arr[$kk+4],7,5);
            }
          }
        }
      }
    }


    //浮动盈亏
    $fudong=0;
    //显示的平仓时间
    foreach ($list as $k => $v) {
      //卖出按钮是否能点
      if(date('Y-m-d',$v['buy_time'])==date('Y-m-d')){
        $cansell=2;
      }else{
        $cansell=1;
      }
      if(isHoliday()!=0){
        $cansell=2;
      }
      $list[$k]['cansell']=$cansell;

      //卖出按钮上的字是什么
      if(date('Y-m-d',$v['buy_time'])==date('Y-m-d')){
        $list[$k]['font']='卖出';//原新购
      }else{
        $list[$k]['font']='卖出';
      }

      //下方的提示信息
      $msg='';
      if($v['defer']==1){
        $msg='已申请自动递延   ';
      }
      if($v['strategy_type']==2){
        $time=0;
        for($i=1;$i<12;$i++){
          if(isHoliday($i)==0){$time+=1;}
          if($time==4){
            $msg.=date('m-d 14:50', time()+$i*24*3600);
            break;
          }
          continue;
        }
      }else{
        //新购
        if(date('Y-m-d',$v['buy_time'])==date('Y-m-d')){
          $msg.='下交易日14:50发起平仓请求';
        }
      }
      $list[$k]['msg']=$msg;


      $list[$k]['buy_time']=date('Y-m-d',$v['buy_time']);
      if($v['nowprice']>=$v['buy_price']){
        $list[$k]['chajia']=$v['surplus_value']-$v['nowprice'];
      }else{
        $list[$k]['chajia']=$v['nowprice']-$v['loss_value'];
      }
      $list[$k]['fudong']=round($v['nowprice']*$v['stock_number']-$v['buy_price']*$v['stock_number'],2);
      $fudong+=$list[$k]['fudong'];
      
      //加上小数点
      if(count(explode('.',$list[$k]['fudong']))!=2){
         $list[$k]['fudong']=$list[$k]['fudong'].'.00';
      }
      
    }
    if(count(explode('.',$fudong))!=2){
         $fudong=$fudong.'.00';
      }


    //用户信息
    $admin=Db::name('admin')->where(['account'=>$info['account'],'is_del'=>1])->find();
    $arr=[
      //总计余额
      'all_money'=>round($admin['frozen_money']+$admin['tactics_balance'],2),
      //冻结金额
      'frozen_money'=>$admin['frozen_money'],
      //可用余额
      'can_money'=>$admin['tactics_balance'],
      //浮动盈亏
      'fudong'=>$fudong,
    ];

    if(is_null($arr['frozen_money']) || is_null($arr['can_money'])){
      $arr['frozen_money'] = 0;
      $arr['can_money'] = 0;
    }
    
    
    


    //炒股大赛
    $listcontest=Db::name('strategy_contest')->where(['account'=>$info['account'],'status'=>['in',[1,2]]])->order('buy_time desc')->select();

    //查询现价
    if($listcontest){
      if(date('Hi')>931){
        $str='';
        foreach($listcontest as $k=>$v){
          $str.=$v['stock_code'].',';
        }
        $str=substr($str,0,-1);

        $result=file_get_contents("http://hq.sinajs.cn/list=".$str);
        $para=explode(';',$result);
        unset($para[count($para)-1]);
        foreach($para as $k=>$v){
          $listcontest[$k]['nowprice']=explode(',',$v)[3];
        }
      }else{
        $times=1;
        for($i=1;$i<12;$i++){
          if(isHoliday($i)!=0){
            $times+=1;
          }else{
            break;
          }
          continue;
        }
        foreach($listcontest as $k=>$v){
          $str=file_get_contents('http://money.finance.sina.com.cn/quotes_service/api/json_v2.php/CN_MarketData.getKLineData?symbol='.$v['stock_code'].'&scale=60&ma=5&datalen=9');
          $arrnow=explode(',',substr($str,1,-1));
          foreach($arrnow as $kk=>$vv){
            if(strpos($vv,date('Y-m-d',time()-$times*3600*24).' 15:00:00')){
              $listcontest[$k]['nowprice']=substr($arrnow[$kk+4],7,5);
            }
          }
        }
      }
    }

    //浮动盈亏
    $fudong=0;
    //买入时间
    foreach($listcontest as $k=>$v){
      //卖出按钮是否能点
      if(date('Y-m-d',$v['buy_time'])==date('Y-m-d')){
        $cansell=2;
      }else{
        $cansell=1;
      }
      if(isHoliday()!=0){
        $cansell=2;
      }
      $listcontest[$k]['cansell']=$cansell;

      //卖出按钮上的字是什么
      if(date('Y-m-d',$v['buy_time'])==date('Y-m-d')){
        $listcontest[$k]['font']='卖出';//原新购
      }else{
        $listcontest[$k]['font']='卖出';
      }

      $listcontest[$k]['buy_time']=date('Y-m-d',$v['buy_time']);
      $listcontest[$k]['day']='';
      if($v['nowprice']>=$v['buy_price']){
        $listcontest[$k]['chajia']=$v['surplus_value']-$v['nowprice'];
      }else{
        $listcontest[$k]['chajia']=$v['nowprice']-$v['loss_value'];
      }
      $listcontest[$k]['fudong']=$v['nowprice']*$v['stock_number']-$v['buy_price']*$v['stock_number'];
      $fudong+=$listcontest[$k]['fudong'];
      //加上小数点
      if(count(explode('.',$listcontest[$k]['fudong']))!=2){
         $listcontest[$k]['fudong']=$listcontest[$k]['fudong'].'.00';
      }
      
      
    }
   if(count(explode('.',$fudong))!=2){
         $fudong=$fudong.'.00';
      }


    //用户信息
    $admin=Db::name('admin')->where(['account'=>$info['account'],'is_del'=>1])->find();
    $arrcontest=[
      //总计余额
      'all_money'=>$admin['analog_money']+$admin['frozen_analog_money'],
      //冻结金额
      'frozen_money'=>$admin['frozen_analog_money'],
      //可用余额
      'can_money'=>$admin['analog_money'],
      //浮动盈亏
      'fudong'=>$fudong,
    ];

    if(is_null($arrcontest['frozen_money']) || is_null($arrcontest['can_money'])){
      $arrcontest['frozen_money'] = 0;
      $arrcontest['can_money'] = 0;
    }


    $shuzu=[
      'celue'=>[
        'list'=>$list,
        'arr'=>$arr,
      ],
      'dasai'=>[
        'list'=>$listcontest,
        'arr'=>$arrcontest,
      ]
    ];
    $this->assign('shuzu',$shuzu);

      $bannerurl = Db::name('banner')->where(['is_del'=>1,'ban_id'=>2])->value('bannerurl');

      $this->assign('bannerurl',$bannerurl);



    return $this->fetch();
  }



  //策略列表页
  public function strategy_ajax(){
    $info = Request::instance()->param();
    $info['account']=cookie('username');
    $list=Db::name('strategy')
      ->where(['is_cancel_order'=>['in',[1,2]]])
      ->where(['account'=>$info['account'],'status'=>['in',[1,2,3]]])
      ->order('buy_time desc')
      ->select();

    //查询现价
    if($list){
      if(date('Hi')>931){
        $str='';
        foreach($list as $k=>$v){
          $str.=$v['stock_code'].',';
        }
        $str=substr($str,0,-1);
        $result=file_get_contents("http://hq.sinajs.cn/list=".$str);
        $para=explode(';',$result);
        unset($para[count($para)-1]);
        foreach($para as $k=>$v){
          $list[$k]['nowprice']=explode(',',$v)[3];
        }
      }else{
        $times=1;
        for($i=1;$i<12;$i++){
          if(isHoliday($i)!=0){
            $times+=1;
          }else{
            break;
          }
          continue;
        }
        foreach($list as $k=>$v){
          $str=file_get_contents('http://money.finance.sina.com.cn/quotes_service/api/json_v2.php/CN_MarketData.getKLineData?symbol='.$v['stock_code'].'&scale=60&ma=5&datalen=9');
          $arr=explode(',',substr($str,1,-1));
          foreach($arr as $kk=>$vv){
            if(strpos($vv,date('Y-m-d',time()-$times*3600*24).' 15:00:00')){
              $list[$k]['nowprice']=substr($arr[$kk+4],7,5);
            }
          }
        }
      }
    }


    //浮动盈亏
    $fudong=0;
    //显示的平仓时间
    foreach ($list as $k => $v) {
      //卖出按钮是否能点
      if(date('Y-m-d',$v['buy_time'])==date('Y-m-d')){
        $cansell=2;
      }else{
        $cansell=1;
      }
      if(isHoliday()!=0){
        $cansell=2;
      }
      $list[$k]['cansell']=$cansell;

      //卖出按钮上的字是什么
      if(date('Y-m-d',$v['buy_time'])==date('Y-m-d')){
        $list[$k]['font']='新购';
      }else{
        $list[$k]['font']='卖出';
      }

      //下方的提示信息
      $msg='';
      if($v['defer']==1){
        $msg='已申请自动递延   ';
      }
      if($v['strategy_type']==2){
        $time=0;
        for($i=1;$i<12;$i++){
          if(isHoliday($i)==0){$time+=1;}
          if($time==4){
            $msg.=date('m-d 14:50', time()+$i*24*3600);
            break;
          }
          continue;
        }
      }else{
        //新购
        if(date('Y-m-d',$v['buy_time'])==date('Y-m-d')){
          $msg.='下交易日14:50发起平仓请求';
        }
      }
      $list[$k]['msg']=$msg;


      $list[$k]['buy_time']=date('Y-m-d',$v['buy_time']);
      if($v['nowprice']>=$v['buy_price']){
        $list[$k]['chajia']=$v['surplus_value']-$v['nowprice'];
      }else{
        $list[$k]['chajia']=$v['nowprice']-$v['loss_value'];
      }
      $list[$k]['fudong']=round($v['nowprice']*$v['stock_number']-$v['buy_price']*$v['stock_number'],2);
      $fudong+=$list[$k]['fudong'];
		
      //加上小数点
      if(count(explode('.',$list[$k]['fudong']))!=2){
         $list[$k]['fudong']=$list[$k]['fudong'].'.00';
      }
      if(count(explode('.',$fudong))!=2){
         $fudong=$fudong.'.00';
      }

    }

    //用户信息
    $admin=Db::name('admin')->where(['account'=>$info['account'],'is_del'=>1])->find();
    $arr=[
      //总计余额
      'all_money'=>round($admin['frozen_money']+$admin['tactics_balance'],2),
      //冻结金额
      'frozen_money'=>$admin['frozen_money'],
      //可用余额
      'can_money'=>$admin['tactics_balance'],
      //浮动盈亏
      'fudong'=>round($fudong,2),
    ];



    $shuzu=[
      'celue'=>[
        'list'=>$list,
        'arr'=>$arr,
      ]
    ];
    
    return json($shuzu,'1','持仓列表查询成功');

    // $this->assign('shuzu',$shuzu);
    // return $this->fetch();

  }




  //炒股持仓大赛列表
  public function strategy_contest_ajax(){
    $info = Request::instance()->param();
    $info['account']=cookie('username');
    //炒股大赛
    $listcontest=Db::name('strategy_contest')->where(['account'=>$info['account'],'status'=>['in',[1,2]]])->order('buy_time desc')->select();

    //查询现价
    if($listcontest){
      if(date('Hi')>931){
        $str='';
        foreach($listcontest as $k=>$v){
          $str.=$v['stock_code'].',';
        }
        $str=substr($str,0,-1);

        $result=file_get_contents("http://hq.sinajs.cn/list=".$str);
        $para=explode(';',$result);
        unset($para[count($para)-1]);
        foreach($para as $k=>$v){
          $listcontest[$k]['nowprice']=explode(',',$v)[3];
        }
      }else{
        $times=1;
        for($i=1;$i<12;$i++){
          if(isHoliday($i)!=0){
            $times+=1;
          }else{
            break;
          }
          continue;
        }
        foreach($listcontest as $k=>$v){
          $str=file_get_contents('http://money.finance.sina.com.cn/quotes_service/api/json_v2.php/CN_MarketData.getKLineData?symbol='.$v['stock_code'].'&scale=60&ma=5&datalen=9');
          $arr=explode(',',substr($str,1,-1));
          foreach($arr as $kk=>$vv){
            if(strpos($vv,date('Y-m-d',time()-$times*3600*24).' 15:00:00')){
              $listcontest[$k]['nowprice']=substr($arr[$kk+4],7,5);
            }
          }
        }
      }
    }

    //浮动盈亏
    $fudong=0;
    //买入时间
    foreach($listcontest as $k=>$v){
      //卖出按钮是否能点
      if(date('Y-m-d',$v['buy_time'])==date('Y-m-d')){
        $cansell=2;
      }else{
        $cansell=1;
      }
      if(isHoliday()!=0){
        $cansell=2;
      }
      $listcontest[$k]['cansell']=$cansell;

      //卖出按钮上的字是什么
      if(date('Y-m-d',$v['buy_time'])==date('Y-m-d')){
        $listcontest[$k]['font']='新购';
      }else{
        $listcontest[$k]['font']='卖出';
      }

      $listcontest[$k]['buy_time']=date('Y-m-d',$v['buy_time']);
      $listcontest[$k]['day']='';
      if($v['nowprice']>=$v['buy_price']){
        $listcontest[$k]['chajia']=$v['surplus_value']-$v['nowprice'];
      }else{
        $listcontest[$k]['chajia']=$v['nowprice']-$v['loss_value'];
      }
      $listcontest[$k]['fudong']=round($v['nowprice']*$v['stock_number']-$v['buy_price']*$v['stock_number'],2);
      $fudong+=$listcontest[$k]['fudong'];
      //加上小数点
      if(count(explode('.',$listcontest[$k]['fudong']))!=2){
         $listcontest[$k]['fudong']=$listcontest[$k]['fudong'].'.00';
      }
      
    }
    
    if(count(explode('.',$fudong))!=2){
         $fudong=$fudong.'.00';
      }

    //用户信息
    $admin=Db::name('admin')->where(['account'=>$info['account'],'is_del'=>1])->find();
    $arrcontest=[
      //总计余额
      'all_money'=>$admin['analog_money']+$admin['frozen_analog_money'],
      //冻结金额
      'frozen_money'=>$admin['frozen_analog_money'],
      //可用余额
      'can_money'=>$admin['analog_money'],
      //浮动盈亏
      'fudong'=>round($fudong,2),
    ];


    

    $shuzu=[
      'dasai'=>[
        'list'=>$listcontest,
        'arr'=>$arrcontest,
      ]
    ];

    return json($shuzu,'1','模拟持仓列表查询成功');



  }














  //策略发起平仓，卖出股票（改变状态）
  public function sellStrategy() {
    $data=[];
    if (isHoliday()==0 && ((date('Hi', time()) >= 930 && date('Hi', time()) <= 1130) || (date('Hi', time()) >= 1300 && date('Hi', time()) <= 1455))) {

      $info = Request::instance()->param();
      //策略信息
      $strategy=Db::name('strategy')->where(['is_cancel_order'=>1])->where(['id'=>$info['id']])->find();

      if($strategy['status']>2){
        return json('', 0, '此策略正在卖出');
      }

      if(date('Ymd',time())<=date('Ymd',$strategy['buy_time'])){
        return json('', 0, '当天禁止卖出策略');
      }

      //实盘委托
      if($strategy['is_real_disk']==1){
        $shipanstock=new Stock();
        //实盘账户信息
        $shipaninfo=$this->getshipaninfo($strategy['shipan_account'],substr($strategy['stock_code'],0,2));
        //实盘进行委托
        $success=$shipanstock->sendorder($shipaninfo['ip'],$shipaninfo['version'],$shipaninfo['yybID'],$shipaninfo['zhanghao'],
          $shipaninfo['zhanghao'],$shipaninfo['mima'],$shipaninfo['port'],$shipaninfo['txmima'],1,4,$shipaninfo['gudongdaima'],
          substr($strategy['stock_code'],2),0,$strategy['stock_number']);
        $success=json_decode($success,true);
        //有委托编号这个参数名和值，说明委托成功了
        if(array_key_exists('委托编号',$success['data1'])&&$success['data1']['委托编号']){
          //卖出合同编号
          $data['sell_contract_num']=$success['data1']['委托编号'];
        }else{
          file_put_contents('shipan.txt','委托时间:'.date('Y-m-d H:i:s').
            "    实盘中的策咯".$strategy['strategy_num']."在实盘想卖出，但是委托失败了\n", FILE_APPEND);
          return json('', 0, '委托失败');
        }
      }

      $data['status']=3;
      $data['sell_type']=1;
      $data['sell_time']=time();
      //修改策略记录
      $res=Db::name('strategy')->where(['is_cancel_order'=>1])->where(['id'=>$info['id']])->update($data);
      if($res){
        return json('', 1, '平仓申请成功');
      }else{
        return json('', 0, '平仓申请失败');
      }
    } else {
      return json('', 0, '非交易时间禁止平仓');
    }
  }

  	//获取实盘查询的必备参数
    public function getshipaninfo($id,$sh_sz=''){
        if(cache('shipan_account_arr')){
            $allshipan_account=cache('shipan_account_arr');
        }else{
            $allshipan_account=Db::name('shipan_account')->where(['is_del'=>1])->select();
            cache('shipan_account_arr', $allshipan_account, 3600);
        }
        $shipan=[];
        foreach($allshipan_account as $k=>$v){
            if($v['id']==$id){
                $shipan=$v;
            }
        }
        if($sh_sz){
            if($sh_sz=='sh'){
                $shipan['gudongdaima']=$shipan['shholder'];
                $shipan['ExchangeID']=1;
                $shipan['PriceType']=6;
            }else{
                $shipan['gudongdaima']=$shipan['szholder'];
                $shipan['ExchangeID']=0;
                $shipan['PriceType']=1;
            }
        }
        return $shipan;
    }
    
  //炒股大赛的策略发起平仓，卖出股票
  public function sellStrategyContest() {

     if (isHoliday()==0 && ((date('Hi', time()) >= 930 && date('Hi', time()) <= 1130) || (date('Hi', time()) >= 1300 && date('Hi', time()) <= 1455))) {
      $info = Request::instance()->param();
      //策略信息
      $strategy=Db::name('strategy_contest')->where(['id'=>$info['id']])->find();

      if($strategy['status']!=2){
        return json('', 0, '此策略正在卖出');
      }

       if(date('Ymd',time())<=date('Ymd',$strategy['buy_time'])){
         return json('', 0, '当天禁止卖出策略');
       }

      //实际股票
      $json=$this->getOneStock2($strategy['stock_code']);

      //股票当前价格
      $nowprice=json_decode($json,true)['list']['result'][0]['data']['nowPri'];

      //修改策略
      $data=[
        'status'=>4,
        'sell_price'=>$nowprice,
        'sell_time'=>time(),
        'sell_type'=>1,
      ];

      Db::startTrans();
      //修改策略记录
      $result1=Db::name('strategy_contest')->where(['id'=>$info['id']])->update($data);
      //修改用户策略余额和冻结金额
      $result2=Db::name('admin')->where(['account' => $strategy['account'],'is_del'=>1])->setInc('analog_money',($nowprice-$strategy['buy_price'])*$strategy['stock_number']+$strategy['credit']);
      $result3=Db::name('admin')->where(['account' => $strategy['account'],'is_del'=>1])->setDec('frozen_analog_money',$strategy['credit']);

      if(!$result1 || !$result2 || !$result3){
        Db::rollback();
        return json('', 0, '平仓失败');
      }
      Db::commit();
      return json('', 1, '平仓成功');
     } else {
       return json('', 0, '非交易时间禁止平仓');
     }
  }
  
  //申请递延
  public function changeDefer() {
    $info = Request::instance()->param();

    $strategy=Db::name('strategy')->where(['is_cancel_order'=>1])->where(['id'=>$info['id']])->find();

    $day=0;//工作日天数
    $times=1;//休息天数
    //t+1的时间为1天，t+5的时间为4天
    if($strategy['strategy_type']==1){
      $free=1;
    }else{
      $free=4;
    }
    //当前时间到买入的时间的天数
    $all=(strtotime(date('Ymd'))-strtotime(date('Ymd',$strategy['buy_time'])))/24/3600;
    for($i=1;$i<=$all;$i++){
      if(isHoliday($i)!=0){
        $times+=1;
      }else{
        $day+=1;
      }
      if($day>=$free){
        break;
      }else{
        continue;
      }
    }
    if(isHoliday()!=0){
      $data['rest_day']=$times-1+1;
    }else{
      $data['rest_day']=$times-1;
    }

    $data['defer']=1;
    Db::name('strategy')->where(['is_cancel_order'=>1])->where(['id'=>$info['id']])->update($data);

    file_put_contents('shenqing.txt','买入时间：'.date('Y-m-d H:i:s',$strategy['buy_time']).
      '，申请时间:'.date('Y-m-d H:i:s')."，休息时间为".$data['rest_day']."天\n", FILE_APPEND);
    return json('', 1, '申请递延成功');
  }
  //修改止损
  public function updateLossValue() {
    $info = Request::instance()->param();
    if($info['trues']==1){
      Db::name('strategy')->where(['is_cancel_order'=>1])->where(['id' => $info['id']])->update(['loss_value'=>$info['loss_value']]);
    }else{
      Db::name('strategy_contest')->where(['id' => $info['id']])->update(['loss_value'=>$info['loss_value']]);
    }
    return json('', 1, '修改止损成功');
  }



  //查看已平仓的策略
  public function strategy_simulation() {
    $info = Request::instance()->param();
    $info['account']=cookie('username');
    $chengshu=Db::name('hide')->where(['id'=>3])->value('value');

    //已平仓的策略
    $list=Db::name('strategy')
      ->where(['is_cancel_order'=>1])
      ->where(['account'=>$info['account'],'status'=>4])
      ->order('sell_time desc')
      ->select();
    if($list){
      //总策略数
      $allStrategyNum=count($list);
      //盈利策略数
      $winStrategyNum=0;
      //总盈利
      $win=0;
      foreach($list as $k=>$v) {
        $list[$k]['buy_time']=date('Y-m-d',$v['buy_time']);
        $list[$k]['sell_time']=date('Y-m-d H:i:s',$v['sell_time']);
        if($v['true_getmoney']>$v['credit']){
          //交易盈亏
          $list[$k]['getprice']=round(($v['true_getmoney']+$v['sell_poundage']-$v['credit'])/$chengshu,2);
          //盈利分配
          $list[$k]['truegetprice']=round($v['true_getmoney']+$v['sell_poundage']-$v['credit'],2);
          //亏损赔付
          $list[$k]['dropprice']=0;
          $winStrategyNum+=1;
        }else{
          //交易盈亏
          $list[$k]['getprice']=round($v['true_getmoney']+$v['sell_poundage']-$v['credit'],2);
          //盈利分配
          $list[$k]['truegetprice']=0;
          //亏损赔付
          $list[$k]['dropprice']=round($v['true_getmoney']+$v['sell_poundage']-$v['credit'],2);
        }
        $win += round(($v['true_getmoney']+$v['sell_poundage'] - $v['credit']),2);
      }
      $arr=[
        'allStrategyNum'=>$allStrategyNum,
        'winStrategyNum'=>$winStrategyNum,
        'win'=>$win,
        'rate'=>round(($winStrategyNum/$allStrategyNum*100),2).'%',
      ];
    }else{
      $arr=[
        'allStrategyNum'=>0,
        'winStrategyNum'=>0,
        'win'=>0,
        'rate'=>'0%',
      ];
    }
    $celue=[
      'list'=>$list,
      'arr'=>$arr
    ];
    $this->assign('celue',$celue);

    return $this->fetch();
  }

  //查看已平仓的策略的详情
  public function strategy_simulation_info() {
    $info = Request::instance()->param();
    $strategy=Db::name('strategy')->where(['is_cancel_order'=>1])->where(['id'=>$info['id']])->find();
    if($strategy['true_getmoney']>$strategy['credit']){
      $chengshu=Db::name('hide')->where(['id'=>3])->value('value');
      //交易盈亏
      $strategy['getprice']=round(($strategy['true_getmoney']+$strategy['sell_poundage']-$strategy['credit'])/$chengshu,2);
      //盈利分配
      $strategy['truegetprice']=round($strategy['true_getmoney']+$strategy['sell_poundage']-$strategy['credit'],2);
      //亏损赔付
      $strategy['dropprice']=0;
    }else{
      //交易盈亏
      $strategy['getprice']=round($strategy['true_getmoney']+$strategy['sell_poundage']-$strategy['credit'],2);
      //盈利分配
      $strategy['truegetprice']=0;
      //亏损赔付
      $strategy['dropprice']=round($strategy['true_getmoney']+$strategy['sell_poundage']-$strategy['credit'],2);
    }
    $strategy['day']=(strtotime(date('Ymd',$strategy['sell_time']))-strtotime(date('Ymd',$strategy['buy_time'])))/24/3600;
    $strategy['buy_time']=date('m-d H:i',$strategy['buy_time']);
    $strategy['sell_time']=date('m-d H:i',$strategy['sell_time']);
    if($strategy['sell_type']==1){
      $strategy['sell_type']='主动卖出';
    }elseif($strategy['sell_type']==2){
      $strategy['sell_type']='达到日期';
    }elseif($strategy['sell_type']==3){
      $strategy['sell_type']='触发止盈';
    }elseif($strategy['sell_type']==4){
      $strategy['sell_type']='触发止损';
    }

    $this->assign('strategy',$strategy);

    return $this->fetch();
  }
  //查看炒股大赛的已平仓的策略
  public function strategy_simulation_contest() {
    $info = Request::instance()->param();
    $info['account']=cookie('username');
    //已平仓的策略
    $list=Db::name('strategy_contest')->where(['account'=>$info['account'],'status'=>['in',[3,4]]])->order('sell_time desc')->select();
    if($list){
      //总策略数
      $allStrategyNum=count($list);
      //盈利策略数
      $winStrategyNum=0;
      //总盈利
      $win=0;
      foreach($list as $k=>$v) {
        $list[$k]['buy_time']=date('Y-m-d',$v['buy_time']);
        $list[$k]['sell_time']=date('Y-m-d',$v['sell_time']);
        if($v['sell_price']>=$v['buy_price']){
          //交易盈亏
          $list[$k]['getprice']=round(($v['sell_price']-$v['buy_price'])*$v['stock_number'],2);
          //盈利分配
          $list[$k]['truegetprice']=round(($v['sell_price']-$v['buy_price'])*$v['stock_number'],2);
          //亏损赔付
          $list[$k]['dropprice']=0;
          $winStrategyNum+=1;
        }else{
          //交易盈亏
          $list[$k]['getprice']=round(($v['sell_price']-$v['buy_price'])*$v['stock_number'],2);
          //盈利分配
          $list[$k]['truegetprice']=0;
          //亏损赔付
          $list[$k]['dropprice']=round(($v['sell_price']-$v['buy_price'])*$v['stock_number'],2);
        }
        $win += round(($v['sell_price'] - $v['buy_price']) * $v['stock_number'],2);
      }
      $arr=[
        'allStrategyNum'=>$allStrategyNum,
        'winStrategyNum'=>$winStrategyNum,
        'win'=>$win,
        'rate'=>round(($winStrategyNum/$allStrategyNum*100),2).'%',
      ];
    }else{
      $arr=[
        'allStrategyNum'=>0,
        'winStrategyNum'=>0,
        'win'=>0,
        'rate'=>'0%',
      ];
    }
    $celue=[
      'list'=>$list,
      'arr'=>$arr
    ];
    $this->assign('celue',$celue);

    return $this->fetch();
  }

  //查看已平仓的炒股大赛的详情
  public function strategy_simulation_contest_info() {
    $info = Request::instance()->param();
    $strategy=Db::name('strategy_contest')->where(['id'=>$info['id']])->find();
    if($strategy['sell_price']>$strategy['buy_price']){
      //交易盈亏
      $strategy['getprice']=round(($strategy['sell_price']-$strategy['buy_price'])*$strategy['stock_number'],2);
      //盈利分配
      $strategy['truegetprice']=round(($strategy['sell_price']-$strategy['buy_price'])*$strategy['stock_number'],2);
      //亏损赔付
      $strategy['dropprice']=0;
    }else{
      //交易盈亏
      $strategy['getprice']=round(($strategy['sell_price']-$strategy['buy_price'])*$strategy['stock_number'],2);
      //盈利分配
      $strategy['truegetprice']=0;
      //亏损赔付
      $strategy['dropprice']=round(($strategy['sell_price']-$strategy['buy_price'])*$strategy['stock_number'],2);
    }
    $strategy['day']=date('Ymd',$strategy['sell_time'])-date('Ymd',$strategy['buy_time']);
    $strategy['buy_time']=date('m-d H:i',$strategy['buy_time']);
    $strategy['sell_time']=date('m-d H:i',$strategy['sell_time']);
    $strategy['fanhuan']=round($strategy['credit']+($strategy['sell_price']-$strategy['buy_price'])*$strategy['stock_number'],2);
    if($strategy['sell_type']==1){
      $strategy['sell_type']='主动卖出';
    }elseif($strategy['sell_type']==2){
      $strategy['sell_type']='达到日期';
    }elseif($strategy['sell_type']==3){
      $strategy['sell_type']='触发止盈';
    }elseif($strategy['sell_type']==4){
      $strategy['sell_type']='触发止损';
    }
    $this->assign('strategy',$strategy);

    return $this->fetch();
  }
  
  
  
  	//股票实时分时图(安卓)
    public function Tstock(){
        $list=[];
        $str=file_get_contents('http://money.finance.sina.com.cn/quotes_service/api/json_v2.php/CN_MarketData.getKLineData?symbol='.input('code').'&scale=5&ma=25&datalen=100');
        $arr=explode(',',substr($str,1,-1));

        if(date('N')==6){
            $date=date('Y-m-d',time()-24*3600);
        }elseif(date('N')==7){
            $date=date('Y-m-d',time()-2*24*3600);
        }else{
            $date=date('Y-m-d');
        }
        foreach($arr as $k=>$v){
            if(strpos($v,$date)){
                $list[]=$arr[$k];
                $list[]=$arr[$k+1];
                $list[]=$arr[$k+2];
                $list[]=$arr[$k+3];
                $list[]=$arr[$k+4];
                $list[]=$arr[$k+5];
            }
        }
        $store=[];
        foreach ($list as $k => $v) {
            if($k%6==0){
                $store[floor($k/6)]['minute']=strtotime(substr($v,6,-1));
            }elseif($k%6==1){
                $store[floor($k/6)]['open']=substr($v,6,-1);
            }elseif($k%6==2){
                $store[floor($k/6)]['max']=substr($v,6,-1);
            }elseif($k%6==3){
                $store[floor($k/6)]['min']=substr($v,5,-1);
            }elseif($k%6==4){
                $store[floor($k/6)]['close']=substr($v,7,-1);
            }elseif($k%6==5){
                $store[floor($k/6)]['volumn']=substr($v,8,-1);
            }
        }

        $json=$this->getOneStock2(input('code'));
        $nowPri=json_decode($json,true)['list']['result'][0]['data']['nowPri'];
        $yestodEndPri=json_decode($json,true)['list']['result'][0]['data']['yestodEndPri'];
        $last['dat']=$yestodEndPri;

        $ststr=[
            [
                0=>'0930',
                1=>strval($store[0]['close']),
                2=>strval($nowPri),
                3=>strval($store[0]['volumn']),
                4=>strval($store[0]['open']),
            ]
        ];
        foreach ($store as $k => $v) {
            $ststr[]=[
                strval(date('Hi',$v['minute'])),
                strval($v['close']),
                strval($nowPri),
                strval($v['volumn']),
                strval($v['open']),
            ];
        }
        $shuzu=[
            [
                0=>'0930',
                1=>$ststr[0][1],
                2=>$ststr[0][2],
                3=>$ststr[0][3],
                4=>$ststr[0][4],
            ]
        ];
        foreach ($ststr as $k => $v) {
            if(strlen(substr($v[0],2)+1)==1){
                $a1='0'.(substr($v[0],2)+1);
                $a2='0'.(substr($v[0],2)+2);
                $a3='0'.(substr($v[0],2)+3);
                $a4='0'.(substr($v[0],2)+4);
                if((substr($v[0],2)+5)==10){
                    $a5=(substr($v[0],2)+5);
                }else{
                    $a5='0'.(substr($v[0],2)+5);
                }
            }else{
                $a1=(substr($v[0],2)+1);
                $a2=(substr($v[0],2)+2);
                $a3=(substr($v[0],2)+3);
                $a4=(substr($v[0],2)+4);
                $a5=(substr($v[0],2)+5);
            }
            $shuzu[$k*5+1]=[
                0=>substr($v[0],0,2).$a1,
                1=>$v[1],
                2=>$v[2],
                3=>$v[3],
                4=>$v[4],
            ];
            $shuzu[$k*5+1+1]=[
                0=>substr($v[0],0,2).$a2,
                1=>$v[1],
                2=>$v[2],
                3=>$v[3],
                4=>$v[4],
            ];
            $shuzu[$k*5+2+1]=[
                0=>substr($v[0],0,2).$a3,
                1=>$v[1],
                2=>$v[2],
                3=>$v[3],
                4=>$v[4],
            ];
            $shuzu[$k*5+3+1]=[
                0=>substr($v[0],0,2).$a4,
                1=>$v[1],
                2=>$v[2],
                3=>$v[3],
                4=>$v[4],
            ];
            if(substr($v[0],2)+5==60){
                if(substr($v[0],1)=='955'){
                    $shuzu[$k*5+4+1]=[
                        0=>1000,
                        1=>$v[1],
                        2=>$v[2],
                        3=>$v[3],
                        4=>$v[4],
                    ];
                }else{
                    $shuzu[$k*5+4+1]=[
                        0=>substr($v[0],0,1).(substr($v[0],2)+45),
                        1=>$v[1],
                        2=>$v[2],
                        3=>$v[3],
                        4=>$v[4],
                    ];
                }
            }else{
                $shuzu[$k*5+4+1]=[
                    0=>substr($v[0],0,2).$a5,
                    1=>$v[1],
                    2=>$v[2],
                    3=>$v[3],
                    4=>$v[4],
                ];
            }
        }

        $last['data']=$shuzu;

        if(date('N')==6){
            $last['date']=date('Ymd',time()-24*3600);
        }elseif(date('N')==7){
            $last['date']=date('Ymd',time()-2*24*3600);
        }else{
            $last['date']=date('Ymd');
        }
        return json($last, 1, '查询成功');
    }




    //股票进行模糊搜索
    public function search_stock_api(){
        $info = Request::instance()->param();

        $stock=[];
        $str=iconv("gb2312", "utf-8//IGNORE",file_get_contents('http://suggest3.sinajs.cn/suggest/key='.$info['content']));
        $list=explode(';',explode("\"",$str)[1]);
        foreach($list as $k=>$v){
            $stockcode=explode(',',$v)[3];
            $stockname=explode(',',$v)[4];
            //排除A股之外的基金期货等
            if(strlen($stockcode)==8&&(strpos($stockcode,'sz00')||strpos($stockcode,'sz00')===0||strpos($stockcode,'sz30')||strpos($stockcode,'sz30')===0||strpos($stockcode,'sh60')||strpos($stockcode,'sh60')===0)){
                if(strpos($stockname,'st')||strpos($stockname,'st')===0||strpos($stockname,'ST')||strpos($stockname,'ST')===0||strpos($stockname,'pt')||strpos($stockname,'pt')===0||strpos($stockname,'PT')||strpos($stockname,'PT')===0){

                }else{
                    $stock[]=[
                        'stock_code'=>explode(',',$v)[3],
                        'stock_name'=>explode(',',$v)[4],
                    ];
                }
            }
        }

        if($stock){
            $code='';
            foreach($stock as $k=>$v){
                $code.=$v['stock_code'].',';
            }
            $result=file_get_contents("http://hq.sinajs.cn/list=".substr($code,0,-1));
            $pp=explode(';',$result);
            unset($pp[count($pp)-1]);
            foreach($pp as $k=>$v){

                $stock[$k]['nowprice']=round(explode(',',$v)[3],2);
                $stock[$k]['rate_price']=round(explode(',',$v)[3]-explode(',',$v)[2],2);
                $stock[$k]['rate']=(round((explode(',',$v)[3]-explode(',',$v)[2])/explode(',',$v)[2],4)*100).'%';
                if(!$stock[$k]['rate_price']){
                    unset($stock[$k]);
                }
            }
            $stock=array_values($stock);
        }

        return json($stock, 1, '查询成功');
    }

}