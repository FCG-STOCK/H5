$(function(){

    //公共页面引入
    $(".includeDom").each(function(){
        var html = $(this).attr("include");
        $(this).load(html);
    })

    //图片懒加载
    $(".loaderimg").lazyload({effect: "fadeIn",threshold :-50,failurelimit :3,});

    //rem 插件引入
    window['adaptive'].desinWidth = 750;
    window['adaptive'].baseFont = 20;
    window['adaptive'].init();

    winsize()

    $(window).resize(function(event) {
            winsize()
    });

    function winsize () {
        var wh = $(window).height();
        // $('.content-box').css("min-height",wh);
    }

    jQuery(document).ready(function() { var userAgent = navigator.userAgent.toLowerCase();
    //获取UA信息判断ua中是否含有和app端约定好的标识ezhouxing 
    if(userAgent.indexOf("diandian") != -1){ $("header,.guide-top,.guide-tapp,.personal-title,.vouchers-title,.optional-info-list").addClass("app"); }else{ $("header,.guide-top,.guide-tapp,.personal-title,.vouchers-title,.optional-info-list").removeClass("app"); } })
    
});


//公用提醒样式
function submitsuccess(msg){
    $('.submit-success').html(msg);
    $('.submit-success').fadeIn();
    setTimeout(function(){
        $('.submit-success').fadeOut();
    },1000);
}
$(document).ready(function () {
    setTimeout(function(){
        $('.loading').addClass("active");
    },500)
});
var u = navigator.userAgent;
var isAndroid = u.indexOf('Android') > -1 || u.indexOf('Adr') > -1; //android终端
var isiOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/); //ios终端
if(isAndroid){
    $(".ploy-money div input").css("left","0")
}else{
}

//公用验证码方法
function yzm(){
    var phone = $("#username").val();
    if(phone=='' || !(/^1[34578]\d{9}$/.test(phone))){        
        submitsuccess("请输入正确的手机号");
        return;
    }
    $(function () {               
        $.ajax({
            url: "/API/index/sendMessageforgetpw",
            data: { mobile: $("#username").val()},
            dataType: "json",
            method:"POST",
            success: function (data) {
                // alert("发送成功");
                var count = 60;
                var time = setInterval(function(){
                    count--;
                    $("#seconds").val("("+ count +")");
                    $("#seconds").removeAttr("onclick");
                    if(count==0){
                        clearInterval(time);
                        $("#seconds").val("重发");
                        $("#seconds").attr("onclick","yzm()");
                    }
                },1000)
            },
            error:function(){
                alert("手机号不正确");
            }
        })                                         
    })        
}


// 获取参数单一
function getSearchString(key) {
    let Url = window.location.search;
    var str = Url;
    str = str.substring(1, str.length);
    var arr = str.split("&");
    var obj = new Object();
    for (var i = 0; i < arr.length; i++) {
        var tmp_arr = arr[i].split("=");
        obj[decodeURIComponent(tmp_arr[0])] = decodeURIComponent(tmp_arr[1]);
    }
    return obj[key];
}
// 获取全部参数 obj
function search(){
    let Url = window.location.search;
    var str = Url;
    str = str.substring(1, str.length); 
    var arr = str.split("&");
    var obj = new Object();
    for (var i = 0; i < arr.length; i++) {
        var tmp_arr = arr[i].split("=");
        obj[decodeURIComponent(tmp_arr[0])] = decodeURIComponent(tmp_arr[1]);
    }
    return obj;
}


window.alert = function (name) {
  const iframe = document.createElement('IFRAME');
  iframe.style.display = 'none';
  iframe.setAttribute('src', 'data:text/plain,');
  document.documentElement.appendChild(iframe);
  window.frames[0].window.alert(name);
  iframe.parentNode.removeChild(iframe);
};

















