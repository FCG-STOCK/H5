function login(){
   var phone = $(".user").val();
    if(phone=='' || !(/^1[3465789]\d{9}$/.test(phone))){
     	 submitsuccess('用户名格式不正确');
         return;
    }   
    $(function () {        
        $.ajax({
            url: "/API/index/login",
            data: { account: $(".user").val(),                              
                    pwd: $(".pwr").val()
                      },
            dataType: "json",
            method:"post", 
            success: function (data) {
                // alert('ajax调用成功');
                var dataObj = JSON.parse(data);
                if(dataObj.status==1){
                    //用户名存cookie
                    var nick_name = dataObj.list['nick_name'];
                   
                    window.location.href = '/index/center/personal.html';
                }else{
				
                      deleteCookie();
                        submitsuccess(dataObj.msg);
                      
                    
                    
                }                            
            }
        })                                        
    })             
}
// 清除所有的cookie
function deleteCookie() {
    var cookies = document.cookie.split(";");
    for (var i = 0; i < cookies.length; i++) {
        var cookie = cookies[i];
        var eqPos = cookie.indexOf("=");
        var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
        document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT; path=/";
    }
    if(cookies.length > 0)
    {
        for (var i = 0; i < cookies.length; i++) {
            var cookie = cookies[i];
            var eqPos = cookie.indexOf("=");
            var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
            var domain = location.host.substr(location.host.indexOf('.'));
            document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT; path=/; domain=" + domain;
        }
    }
}


function register(){
        var phone = $("#username").val();
        if(phone=='' || !(/^1[3465789]\d{9}$/.test(phone))){
            submitsuccess("请输入正确的手机号码!");
            return;
        }
  
        if($("#passworde").val().length<6){
            submitsuccess("密码长度不能小于6位!");
            return;
        }

       // if($("#inviter_tel").val()==''){
         //   submitsuccess("请输入邀请码!");
           // return;
        //}
  
        if($("#verify").val()==''){
            submitsuccess("请输入验证码!");
            return;
        }
  
        $(function () {               
            $.ajax({
                url: "/API/index/register",
                data: { Account: $("#username").val(),
                        yzm: $("#verify").val(),
                        pwd: $("#passworde").val(),
                        inviter:$("#inviter_tel").val()
                        },
                dataType: "json",
                method:"POST",
                success: function (data) {
                    var dataObj = JSON.parse(data);
                    if(dataObj.status==1){
                        submitsuccess("注册成功!");
                        setTimeout(function () {window.location.href = 'login.html'; }, 1000);//延迟1秒后跳转
                        
                    }else{
                        submitsuccess(dataObj.msg);
                    }                            

                },
                error:function(){
                    alert("注册失败");
                }
            })
                                         
        })        
}  