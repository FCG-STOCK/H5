 //公用提醒样式
 function submitsuccess(msg){
    $('.submit-success').html(msg);
    $('.submit-success').fadeIn();
    setTimeout(function(){
        $('.submit-success').fadeOut();
    },1000);
}
$(function(){
    $(".price-search").click(function(){
        if($(this).hasClass("on")){
            $(this).removeClass("on");
            $(".search-absolute").fadeOut(10)
        }else{
            $(this).addClass("on");
            $(".search-absolute").fadeIn(10)
        }
    })
    $(".aui-navBar-item").click(function(){
        $(".price-search").removeClass("on");
        $(".search-absolute").fadeOut(10);
        $("#test").html('').fadeOut(10);
        $(".search-absolute input").val('')
    })

    //键入触发事件
    $('#searchKey').keyup(function(){
        //如果输入的值为空就刷新当前页面
        var content = ($('#searchKey').val()).toUpperCase();
        if(!content){
            location.reload();
        }
        $.ajax({
            url: "/index/strategy/search_stock_api",
            data: { content: $('#searchKey').val()
                      },
            dataType: "json",
            method:"post", 
            success: function (data) {
                // alert('ajax调用成功');
                var dataObj = JSON.parse(data);
                if(dataObj.status==1){
                    var len = dataObj.list.length;
                    var res = [];
                    for(var i=0;i<len;i++){
                        res[i] = dataObj.list[i].stock_code+"("+dataObj.list[i].stock_name+")";
                    }
                    renderTab(res);
                    $('.search-over').css("display","block");
                }                      
            }
        })                     
    });
    
})
//渲染表格
function renderTab(list){
    if(list.length==0){
        $('#test').html('');
        return;
    }
    var colStr = '';
    for(var i=0,len=list.length;i<len;i++){
        var a = list[i].substr(0,8).toLowerCase();
        var b = list[i].substr(9);
        //去除字符串最后一个字符
        b = b.substr(0,b.length-1);
        list[i] = a+"("+b+")";
        colStr+= "<li id=\""+a.toLowerCase()+"\" onclick='code(\""+a.toLowerCase()+"\",1)';>" +list[i]+"</li>";

    }
    $('#test').html(colStr);
}
//跳转h5创建策略页
function code(str,status){
    // var searchKey = $("#"+str).val();

    // if(!searchKey){
    // 	return;
    // }
                        
    $.ajax({
        url: "/api/index/checkstockstop",
        data: {gid:str},
        dataType: "json",
        method:"post",
        async:true,
        success: function (data) {
            // alert('ajax调用成功');
            var dataObj = JSON.parse(data);
            if(dataObj.status==200){
                addHistory(searchKey);//添加历史记录
                window.location.href="/index/strategy/optional_info?gid="+str;
            }else{
                submitsuccess("此股票不能购买");
                return;
            }
        }
    })
}