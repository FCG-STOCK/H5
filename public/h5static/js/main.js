$(function(){

    //公共页面引入
    $(".includeDom").each(function(){
        var html = $(this).attr("include");
        $(this).load(html);
    })

    //图片懒加载
    $(".loaderimg").lazyload({effect: "fadeIn",threshold :-50,failurelimit :3,});

    //rem 插件引入
    window['adaptive'].desinWidth = 750;
    window['adaptive'].baseFont = 20;
    window['adaptive'].init();

    winsize()

    $(window).resize(function(event) {
            winsize()
    });

    function winsize () {
        var wh = $(window).height();
        $('.content-box').css("min-height",wh);
    }

    $(".login-from .clickable").click(function () {
        if ($(this).hasClass('hover')) {
            $(this).removeClass('hover');
            $(this).siblings('input').attr("type","password")
        }else{
            $(this).addClass('hover');
            $(this).siblings('input').attr("type","text")
        }
    })
});

//加载动画
window.load = {
  ind: null,
  show: function(){
    load.ind = layer.open({
      type: 2,
      // content: '加载中',
      shadeClose: false,
      shade: 'background-color: rgba(0,0,0,0.8)' //自定义遮罩的透明度
    });
  },
  hide: function () {
    layer.close(load.ind);
  }
};

load.show()
$(window).load(function() {
  load.hide()
});

//公用提醒样式
function submitsuccess(msg){
    $('.submit-success').html(msg);
    $('.submit-success').fadeIn();
    setTimeout(function(){
        $('.submit-success').fadeOut();
    },1000);
}