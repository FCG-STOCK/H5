<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:70:"D:\xampp\htdocs\peiqi/application/index\view\ranking\ranking_list.html";i:1551775939;s:66:"D:\xampp\htdocs\peiqi\application\index\view\index\inc\footer.html";i:1551775939;}*/ ?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta http-equiv="content-language" content="zh-CN" />
        <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no" />
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black" />
        <meta name="format-detection" content="telephone=no" />
        <meta name="keywords" content="" />
        <meta name="description" content="" />
        <meta name="author" content="令克网络-高端网站建设-https://www.link-web.cn/" />
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
        <title>赢在策略</title>
        <link href="/public/static/img/favicon.ico" rel="shortcut icon">
        <link rel="stylesheet" href="/public/static/css/reset.css">
        <link rel="stylesheet" href="/public/static/css/style.css">
        <script src="/public/static/js/jquery-1.11.3.js"></script>
        <script src="/public/static/js/html5.js"></script>
        <script src="/public/static/js/adaptive-version2.js"></script>
        <script src="/public/static/js/jquery.easing.1.3.js"></script>
        <script src="/public/static/js/jquery.transit.js"></script>
        <script src="/public/static/js/jquery.lazyload.js"></script>
    </head>
    <body>
        <!--<div class="includeDom" include="inc/header.html" data-intro="scroll"></div>-->
        <div class="content-box">
            <div class="panking-top">
                <div class="banner">
                    <img src="<?php echo $bannerurl; ?>" alt="">
                </div>
                <div class="panking-top-b">
                    <div class="title">高手推荐<a class="fr" href="<?php echo url('ranking_subscribe'); ?>">我的订阅</a></div>
                    <div class="panking-top-list swiper-container">
                        <ul class="f-cb swiper-wrapper">
                            <?php if(is_array($list['month']) || $list['month'] instanceof \think\Collection || $list['month'] instanceof \think\Paginator): if( count($list['month'])==0 ) : echo "" ;else: foreach($list['month'] as $key=>$vo): ?>
                            <li class="swiper-slide">
                                <a href="<?php echo url('ranking_record',['id'=>$vo['id'],'trues'=>$vo['trues']]); ?>">
                                    <div class="tt f-cb">
                                        <img src="<?php echo $vo['headurl']; ?>" alt="" class="pic">
                                        <p><span>月收益率</span> <?php echo $vo['profit']; ?></p>
                                        <p><span>月胜率 </span> <?php echo $vo['win']; ?></p>
                                    </div>
                                    <h3><?php echo $vo['username']; ?></h3>
                                </a>
                            </li>
                            <?php endforeach; endif; else: echo "" ;endif; ?>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="panking-bot">
                <div class="panking-bot-tit">
                    <h2>高手排行</h2>
                    <ul class="list f-cb">
                        <li class="hover">总收益</li>
                        <li>日收益</li>
                        <li>周收益</li>
                        <li>月收益</li>
                    </ul>
                </div>
                <div class="panking-bot-con">
                    <div class="lists">
                        <ul class="f-cb">
                            <?php if(is_array($list['all']) || $list['all'] instanceof \think\Collection || $list['all'] instanceof \think\Paginator): if( count($list['all'])==0 ) : echo "" ;else: foreach($list['all'] as $key=>$vo): ?>
                            <li>
                                <a href="<?php echo url('ranking_record',['id'=>$vo['id'],'trues'=>$vo['trues']]); ?>">
                                    <span class="num"><?php echo $key+1; ?></span>
                                    <span class="pic" style="background: url(<?php echo $vo['headurl']; ?>) no-repeat center;border-radius: 50%;"></span>
                                    <span class="name"><?php echo $vo['username']; ?></span>
                                    <span class="tb">总收益率</span>
                                    <span class="tb-x"><?php echo $vo['profit']; ?></span>
                                </a>
                            </li>
                            <?php endforeach; endif; else: echo "" ;endif; ?>
                        </ul>
                    </div>
                    <div class="lists">
                        <ul class="f-cb">
                            <?php if(is_array($list['day']) || $list['day'] instanceof \think\Collection || $list['day'] instanceof \think\Paginator): if( count($list['day'])==0 ) : echo "" ;else: foreach($list['day'] as $key=>$vo): ?>
                            <li>
                                <a href="<?php echo url('ranking_record',['id'=>$vo['id'],'trues'=>$vo['trues']]); ?>">
                                    <span class="num"><?php echo $key+1; ?></span>
                                    <span class="pic" style="background: url(<?php echo $vo['headurl']; ?>) no-repeat center;border-radius: 50%;"></span>
                                    <span class="name"><?php echo $vo['username']; ?></span>
                                    <span class="tb">总收益率</span>
                                    <span class="tb-x"><?php echo $vo['profit']; ?></span>
                                </a>
                            </li>
                            <?php endforeach; endif; else: echo "" ;endif; ?>
                        </ul>
                    </div>
                    <div class="lists">
                        <ul class="f-cb">
                            <?php if(is_array($list['week']) || $list['week'] instanceof \think\Collection || $list['week'] instanceof \think\Paginator): if( count($list['week'])==0 ) : echo "" ;else: foreach($list['week'] as $key=>$vo): ?>
                            <li>
                                <a href="<?php echo url('ranking_record',['id'=>$vo['id'],'trues'=>$vo['trues']]); ?>">
                                    <span class="num"><?php echo $key+1; ?></span>
                                    <span class="pic" style="background: url(<?php echo $vo['headurl']; ?>) no-repeat center;border-radius: 50%;"></span>
                                    <span class="name"><?php echo $vo['username']; ?></span>
                                    <span class="tb">总收益率</span>
                                    <span class="tb-x"><?php echo $vo['profit']; ?></span>
                                </a>
                            </li>
                            <?php endforeach; endif; else: echo "" ;endif; ?>
                        </ul>
                    </div>
                    <div class="lists">
                        <ul class="f-cb">
                            <?php if(is_array($list['month']) || $list['month'] instanceof \think\Collection || $list['month'] instanceof \think\Paginator): if( count($list['month'])==0 ) : echo "" ;else: foreach($list['month'] as $key=>$vo): ?>
                            <li>
                                <a href="<?php echo url('ranking_record',['id'=>$vo['id'],'trues'=>$vo['trues']]); ?>">
                                    <span class="num"><?php echo $key+1; ?></span>
                                    <span class="pic" style="background: url(<?php echo $vo['headurl']; ?>) no-repeat center;border-radius: 50%;"></span>
                                    <span class="name"><?php echo $vo['username']; ?></span>
                                    <span class="tb">总收益率</span>
                                    <span class="tb-x"><?php echo $vo['profit']; ?></span>
                                </a>
                            </li>
                            <?php endforeach; endif; else: echo "" ;endif; ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!--<div class="includeDom" include="inc/footer.html" data-id="2"></div>-->
        <div class="includeDom" data-intro="scroll" data-id="2">
            <footer>
    <ul class="f-cb">
        <li>
            <a href="/index/index/index.html">
                <div class="pic-box">
                    <img src="/public/static/img/footer01.png" alt="" class="bg">
                    <img src="/public/static/img/footer01_h.png" alt="" class="pic">
                </div>
                <h2>首页</h2>
            </a>
        </li>
        <li>
            <a href="/index/strategy/strategy.html">
                <div class="pic-box">
                    <img src="/public/static/img/footer02.png" alt="" class="bg">
                    <img src="/public/static/img/footer02_h.png" alt="" class="pic">
                </div>
                <h2>策略</h2>
            </a>
        </li>
        <li>
            <a href="/index/ranking/rankingList.html">
                <div class="pic-box">
                    <img src="/public/static/img/footer03.png" alt="" class="bg">
                    <img src="/public/static/img/footer03_h.png" alt="" class="pic">
                </div>
                <h2>排行</h2>
            </a>
        </li>
        <li>
            <a href="/index/center/personal.html">
                <div class="pic-box">
                    <img src="/public/static/img/footer04.png" alt="" class="bg">
                    <img src="/public/static/img/footer04_h.png" alt="" class="pic">
                </div>
                <h2>我的</h2>
            </a>
        </li>
    </ul>
</footer>
<script>
    var id = $('footer').parent(".includeDom").data('id');
    $('footer li').eq(id).addClass('hover');
</script>
        </div>
        <script src="/public/static/js/main.js"></script>
        <script src="/public/static/swiper/swiper.js"></script>
        <link rel="stylesheet" type="text/css" href="/public/static/swiper/swiper.css">
        <script>
            $(function () {
                var swiper = new Swiper('.panking-top-list', {
                    slidesPerView:2,
                    spaceBetween:10,
                });

                $('.panking-bot-con .lists').eq(0).fadeIn();

                $('.panking-bot-tit .list li').click(function(){
                    $(this).addClass('hover').siblings('li').removeClass('hover')
                    $('.panking-bot-con .lists').eq($(this).index()).fadeIn().siblings('.lists').hide();
                })
            })
        </script>
    </body>
</html>