<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:77:"/home/wwwroot/m/application/index/view/strategy/strategy_simulation_info.html";i:1548925629;s:60:"/home/wwwroot/m/application/index/view/index/inc/footer.html";i:1548925626;}*/ ?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta http-equiv="content-language" content="zh-CN" />
        <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no" />
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black" />
        <meta name="format-detection" content="telephone=no" />
        <meta name="keywords" content="" />
        <meta name="description" content="" />
        <meta name="author" content="令克网络-高端网站建设-https://www.link-web.cn/" />
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
        <title>赢在策略</title>
        <link href="/public/static/img/favicon.ico" rel="shortcut icon">
        <link rel="stylesheet" href="/public/static/css/reset.css">
        <link rel="stylesheet" href="/public/static/css/style.css">
        <script src="/public/static/js/jquery-1.11.3.js"></script>
        <script src="/public/static/js/html5.js"></script>
        <script src="/public/static/js/adaptive-version2.js"></script>
        <script src="/public/static/js/jquery.easing.1.3.js"></script>
        <script src="/public/static/js/jquery.transit.js"></script>
        <script src="/public/static/js/jquery.lazyload.js"></script>
    </head>
    <body>
        <!--<div class="includeDom" include="inc/header.html" data-intro="index"></div>-->
        <div class="content-box data">
            <div class="personal-title">详情</div>
            <ul class="strategy_simulation_info">
                <li>
                    <dl class="title f-cb">
                        <dd class="tt"><?php echo $strategy['stock_name']; ?>（<?php echo $strategy['stock_code']; ?>）</dd>
                        <dd>状态：已结算</dd>
                    </dl>
                </li>
                <li>
                    <dl class="title f-cb">
                        <dd class="tt smallwidth">交易细明</dd>
                        <dd class="bigwidth">单号:<?php echo $strategy['strategy_num']; ?></dd>
                    </dl>
                    <dl class="con f-cb">
                        <dd class="smallwidth">交易本金 <?php echo $strategy['credit']; ?></dd>
                        <dd class="bigwidth">交易股数 <?php echo $strategy['stock_number']; ?></dd>
                        <dd class="smallwidth">买入价 <?php echo $strategy['buy_price']; ?></dd>
                        <dd class="bigwidth">卖出价 <?php echo $strategy['sell_price']; ?></dd>
                        <dd class="smallwidth">买入时间 <?php echo $strategy['buy_time']; ?></dd>
                        <dd class="bigwidth">卖出时间 <?php echo $strategy['sell_time']; ?></dd>
                        <dd class="smallwidth">买入类型 即时成交</dd>
                        <dd class="bigwidth">卖出类型 <?php echo $strategy['sell_type']; ?></dd>
                    </dl>
                </li>
                <li>
                    <dl class="title">
                        <dd class="smallwidth">策略盈亏 <?php echo $strategy['getprice']; ?></dd>
                        <dd class="bigwidth">盈利分配 <?php echo $strategy['truegetprice']; ?></dd>
                        <dd class="smallwidth">亏损扣减 <?php echo $strategy['dropprice']; ?></dd>
                        <dd class="bigwidth">总手续费 <?php echo $strategy['all_poundage']; ?></dd>
                        <dd class="smallwidth">买手续费 <?php echo $strategy['buy_poundage']; ?></dd>
                        <dd class="bigwidth">卖手续费 <?php echo $strategy['sell_poundage']; ?></dd>
                    </dl>
                </li>
                <li>
                    <dl class="title f-cb">
                        <dd class="tt">履约信用金结算</dd>
                    </dl>
                    <dl class="con f-cb">
                        <dd class="smallwidth">冻结 <?php echo $strategy['credit']; ?></dd>
                        <dd class="bigwidth">返还 <?php echo $strategy['true_getmoney']; ?></dd>
                    </dl>
                </li>
                <li>
                    <dl class="title f-cb">
                        <dd>合约期限 <?php echo $strategy['day']; ?>天</dd>
                    </dl>
                </li>
            </ul>
        </div>
        <!--<div class="includeDom" include="inc/footer.html" data-id="1"></div>-->
        <div class="includeDom" data-intro="scroll" data-id="1">
            <footer>
    <ul class="f-cb">
        <li>
            <a href="/index/index/index.html">
                <div class="pic-box">
                    <img src="/public/static/img/footer01.png" alt="" class="bg">
                    <img src="/public/static/img/footer01_h.png" alt="" class="pic">
                </div>
                <h2>首页</h2>
            </a>
        </li>
        <li>
            <a href="/index/strategy/strategy.html">
                <div class="pic-box">
                    <img src="/public/static/img/footer02.png" alt="" class="bg">
                    <img src="/public/static/img/footer02_h.png" alt="" class="pic">
                </div>
                <h2>策略</h2>
            </a>
        </li>
        <li>
            <a href="/index/ranking/rankingList.html">
                <div class="pic-box">
                    <img src="/public/static/img/footer03.png" alt="" class="bg">
                    <img src="/public/static/img/footer03_h.png" alt="" class="pic">
                </div>
                <h2>排行</h2>
            </a>
        </li>
        <li>
            <a href="/index/center/personal.html">
                <div class="pic-box">
                    <img src="/public/static/img/footer04.png" alt="" class="bg">
                    <img src="/public/static/img/footer04_h.png" alt="" class="pic">
                </div>
                <h2>我的</h2>
            </a>
        </li>
    </ul>
</footer>
<script>
    var id = $('footer').parent(".includeDom").data('id');
    $('footer li').eq(id).addClass('hover');
</script>
        </div>
        <script src="/public/static/js/main.js"></script>
    </body>
</html>