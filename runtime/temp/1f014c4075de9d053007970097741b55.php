<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:82:"/www/wwwroot/celue2.0.h5.solingke.cn/application/index/view/index/information.html";i:1557999094;s:81:"/www/wwwroot/celue2.0.h5.solingke.cn/application/index/view/index/inc/footer.html";i:1554779792;}*/ ?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta http-equiv="content-language" content="zh-CN" />
        <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no" />
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black" />
        <meta name="format-detection" content="telephone=no" />
        <meta name="keywords" content="" />
        <meta name="description" content="" />
         
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
        <title>消息</title>
        <link href="/public/static/img/favicon.ico" rel="shortcut icon">
        <link rel="stylesheet" href="/public/static/css/reset.css">
        <link rel="stylesheet" href="/public/static/css/style.css">
        <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
        <script src="/public/static/js/jquery-1.11.3.js"></script>
        <script src="/public/static/js/html5.js"></script>
        <script src="/public/static/js/adaptive-version2.js"></script>
        <script src="/public/static/js/jquery.easing.1.3.js"></script>
        <script src="/public/static/js/jquery.transit.js"></script>
        <script src="/public/static/js/jquery.lazyload.js"></script>
    </head>
    <body>
        <div id="infoheader" class="guide-top">
                <a href="javascript:history.back(-1);" class="back"></a>
                消息
        </div>
        <!--<div class="includeDom" include="inc/header.html" data-intro="index"></div>-->
        <div class="content-box">
            <div class="information">
                <div class="w94 f-cb">
                    <a href="<?php echo url('subscribePush'); ?>" class="ico1">订阅动态
                        <?php if(!(empty($list['dingyue']) || (($list['dingyue'] instanceof \think\Collection || $list['dingyue'] instanceof \think\Paginator ) && $list['dingyue']->isEmpty()))): ?>
                        <span class="num"><?php echo $list['dingyue']; ?></span>
                        <?php endif; ?>
                    </a>
                    <a href="<?php echo url('strategyPush'); ?>" class="ico2">策略通知
                        <?php if(!(empty($list['celue']) || (($list['celue'] instanceof \think\Collection || $list['celue'] instanceof \think\Paginator ) && $list['celue']->isEmpty()))): ?>
                        <span class="num"><?php echo $list['celue']; ?></span>
                        <?php endif; ?>
                    </a>
                    <a href="<?php echo url('center/personal_packet'); ?>" class="ico3">新红包
                        <!--<span class="fonts">您有新的红包存入账户</span>-->
                    </a>
                    <a href="<?php echo url('systemPush'); ?>" class="ico4">系统消息
                        <?php if(!(empty($list['xitong']) || (($list['xitong'] instanceof \think\Collection || $list['xitong'] instanceof \think\Paginator ) && $list['xitong']->isEmpty()))): ?>
                        <span class="num"><?php echo $list['xitong']; ?></span>
                        <?php endif; ?>
                    </a>
                </div>
            </div>
        </div>
        <!--<div class="includeDom" include="inc/footer.html"></div>-->
        <div class="includeDom" data-id="4">
            <footer>
    <ul class="f-cb">
        <!-- <li>
            <a href="/index/index/index.html">
                <div class="pic-box">
                    <img src="/public/static/img/footer01.png" alt="" class="bg">
                    <img src="/public/static/img/footer01_h.png" alt="" class="pic">
                </div>
                <h2>首页</h2>
            </a>
        </li>
        <li>
            <a href="/index/strategy/strategy.html">
                <div class="pic-box">
                    <img src="/public/static/img/footer02.png" alt="" class="bg">
                    <img src="/public/static/img/footer02_h.png" alt="" class="pic">
                </div>
                <h2>策略</h2>
            </a>
        </li>
        <li>
            <a href="/index/ranking/rankingList.html">
                <div class="pic-box">
                    <img src="/public/static/img/footer03.png" alt="" class="bg">
                    <img src="/public/static/img/footer03_h.png" alt="" class="pic">
                </div>
                <h2>排行</h2>
            </a>
        </li>
        <li>
            <a href="/index/center/personal.html">
                <div class="pic-box">
                    <img src="/public/static/img/footer04.png" alt="" class="bg">
                    <img src="/public/static/img/footer04_h.png" alt="" class="pic">
                </div>
                <h2>我的</h2>
            </a>
        </li> -->
        <li>
            <a href="/index/index/index.html">
                <div class="pic-box">
                    <img src="/public/static/img/img_7.png" alt="" class="bg">
                    <img src="/public/static/img/img_8.png" alt="" class="pic">
                </div>
                <h2>首页</h2>
            </a>
        </li>
        <li>
            <a href="/index/price/quotes.html">
                <div class="pic-box">
                    <img src="/public/static/img/img_9.png" alt="" class="bg">
                    <img src="/public/static/img/img_10.png" alt="" class="pic">
                </div>
                <h2>行情</h2>
            </a>
        </li>
        <li>
            <a href="/index/trade/index.html">
                <div class="pic-box">
                    <img src="/public/static/img/img_11.png" alt="" class="bg">
                    <img src="/public/static/img/img_12.png" alt="" class="pic">
                </div>
                <h2>交易</h2>
            </a>
        </li>
        <li>
            <a href="/index/news/index.html">
                <div class="pic-box">
                    <img src="/public/static/img/img_13.png" alt="" class="bg">
                    <img src="/public/static/img/img_14.png" alt="" class="pic">
                </div>
                <h2>资讯</h2>
            </a>
        </li>
        <li>
            <a href="/index/center/personal.html">
                <div class="pic-box">
                    <img src="/public/static/img/img_15.png" alt="" class="bg">
                    <img src="/public/static/img/img_16.png" alt="" class="pic">
                </div>
                <h2>我的</h2>
            </a>
        </li>
    </ul>
    <div class="loading">
            <div class="loadings">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div> 
        <!-- <img src="/public/static/img/logo.png" alt="" class="logo"> -->
    </div>
</footer>
<script>
    var id = $('footer').parent(".includeDom").data('id');
    $('footer li').eq(id).addClass('hover');
</script>
        </div>
        <script src="/public/static/js/main.js"></script>
    </body>
</html>