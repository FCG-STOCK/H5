<?php if (!defined('THINK_PATH')) exit(); /*a:1:{s:73:"/wwwroot/h5.hdcelue.com/application/index/view/strategy/search_stock.html";i:1547022942;}*/ ?>
 <!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>搜索页面</title>
  	<link href="/public/static/img/favicon.ico" rel="shortcut icon">
	<meta content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" name="viewport"/>
	<meta content="yes" name="apple-mobile-web-app-capable"/>
	<meta content="black" name="apple-mobile-web-app-status-bar-style"/>
	<meta content="telephone=no" name="format-detection"/>
	<link href="/public/static/css/reset.css" rel="stylesheet" type="text/css"/>
	<link href="/public/static/css/style.css" rel="stylesheet" type="text/css"/>
</head>
<body>
	<header class="aui-navBar aui-navBar-fixed active">
			<form class="aui-center-search" action="###">
				<input type="text" name="" id='searchKey' placeholder="搜索股票代码/拼音首字母">
			</form>
			<a href="javascript:;" class="aui-navBar-item" onclick='window.location.href="/index/index/index"'>
				<span class="aui-badge">取消</span>
			</a>
			<ul class="search-over" id="test">
		        <li><a href="javascript:;">sz000001</a></li>
		        <li><a href="javascript:;">sz000002</a></li>
		    </ul>
	</header>



	<!--股票搜索内容-->
	<!-- <div class="xial" id="show" style="display: none">
		<ul id="test">
		</ul>
	</div> -->

	<div class="content-box optional">
		<div class="list">
			<h2>历史纪录</h2>
			<ul id="history" class="f-cb">
				
			</ul>
		</div>
		<div class="list">
			<h2>热门股票</h2>
			<ul id="hotstock" class="f-cb">
				<?php if(is_array($hot) || $hot instanceof \think\Collection || $hot instanceof \think\Paginator): if( count($hot)==0 ) : echo "" ;else: foreach($hot as $key=>$vo): ?>
				<li>
					<a  href="javascript:;" class="aui-palace-grid"  onclick='hot("<?php echo $vo['stock_code']; ?>",1)';>
						<input type="hidden" value="<?php echo $vo['cookie']; ?>" id="<?php echo $vo['stock_code']; ?>">
						<h3><?php echo $vo['stock_name']; ?></h3>
                        <h4 class="hotid"><?php echo $vo['stock_code']; ?></h4>
                        <p id="<?php echo $vo['id']; ?>"><?php echo $vo['nowPri']; ?></p>
					</a>
				</li>
				<?php endforeach; endif; else: echo "" ;endif; ?>
			</ul>
		</div>
		<div class="list">
			<h2>其他用户都在买</h2>
			<ul id="hotstock" class="f-cb">
				<?php if(!(empty($other) || (($other instanceof \think\Collection || $other instanceof \think\Paginator ) && $other->isEmpty()))): if(is_array($other) || $other instanceof \think\Collection || $other instanceof \think\Paginator): if( count($other)==0 ) : echo "" ;else: foreach($other as $key=>$vo): ?>
				<li>
					<a  href="javascript:;" class="aui-palace-grid"  onclick='hot("<?php echo $vo['stock_code']; ?>",1)';>
						<h3><?php echo $vo['stock_name']; ?></h3>
                        <h4 class="hotid"><?php echo $vo['stock_code']; ?></h4>
                        <p id="<?php echo $vo['id']; ?>"><?php echo $vo['nowPri']; ?></p>
						<input type="hidden" value="<?php echo $vo['cookie']; ?>" id="<?php echo $vo['stock_code']; ?>">
					</a>
				</li>
				<?php endforeach; endif; else: echo "" ;endif; endif; ?>
			</ul>
		</div>
	</div>

	<section class="aui-scrollView soc">

		<!--历史记录-->
		<!-- <div class="divHeight"></div>
		<div class="aui-head-access">
			<div class="aui-head-access-hd">
				<h3 style="font-size: 0.9em;">历史记录</h3>
			</div>
		</div>
		<div class="aui-palace clearfix">
		</div>
 -->

		<!--热门股票-->

		<!-- <div class="divHeight"></div>
		<div class="aui-head-access">
			<div class="aui-head-access-hd">
				<h3 style="font-size: 0.9em;">热门股票</h3>
			</div>
		</div>
		<div class="aui-palace clearfix">
			<?php if(is_array($hot) || $hot instanceof \think\Collection || $hot instanceof \think\Paginator): if( count($hot)==0 ) : echo "" ;else: foreach($hot as $key=>$vo): ?>

			<a  href="javascript:;" class="aui-palace-grid"  onclick='hot("<?php echo $vo['stock_code']; ?>",1)';>
				<p><?php echo $vo['stock_name']; ?></p>
				<p class="hotid"><?php echo $vo['stock_code']; ?></p>
				<p class="lve" id="<?php echo $vo['id']; ?>"><?php echo $vo['nowPri']; ?></p>
				<input type="hidden" value="<?php echo $vo['cookie']; ?>" id="<?php echo $vo['stock_code']; ?>">
			</a>
			<?php endforeach; endif; else: echo "" ;endif; ?>



		</div> -->
      
      
      
		<!--其他用户都在买-->

		<!-- <div class="divHeight"></div>
		<div class="aui-head-access">
			<div class="aui-head-access-hd">
				<h3 style="font-size: 0.9em;">其他用户都在买</h3>
			</div>
		</div>
		<div class="aui-palace clearfix" id="hotstock">
          	


		</div>    -->   


	</section>
	
	<div class="submit-success" style="display: none;"></div>

	<script type="text/javascript">


		//跳转h5创建策略页
		function hot(str,status){

			var searchKey = $("#"+str).val();

			if(!searchKey){
				return;
			}
								
			$.ajax({
				url: "/api/index/checkstockstop",
				data: {gid:str},
				dataType: "json",
				method:"post",
				async:true,
				success: function (data) {
					// alert('ajax调用成功');
					var dataObj = JSON.parse(data);
					if(dataObj.status==200){
						addHistory(searchKey);//添加历史记录
						window.location.href="/index/strategy/optional_info?gid="+str;
					}else{
						
						submitsuccess("此股票不能购买");
						return;
					}
				}
			})

		}

	</script>


	<script type="text/javascript" src="/public/static/js/jquery-3.3.1.min.js"></script>

	<!--全部股票的js数组-->
	
	<!--jq缓存-->
	<script type="text/javascript" src="/public/static/js/jquery.cookie.js"></script>
	


	<script type="text/javascript">
		//公用提醒样式
		function submitsuccess(msg){
		    $('.submit-success').html(msg);
		    $('.submit-success').fadeIn();
		    setTimeout(function(){
		        $('.submit-success').fadeOut();
		    },1000);
		}
      
      		$(document).ready(function(){
				$('body').append("<script src='/public/static/js/stock.js'><\/script>");
          	
               	var test=new Array();
              	var length = stock.length;
                for(var i=0;i<length;i++){
                    test[i] = stock[i]['c'].toUpperCase()+"("+stock[i]['n']+")"+stock[i]['p'];
                }

                var colStr = '';
                for(var i=0,len=test.length;i<len;i++){
                    // colStr+=test[i];
                    var a = test[i].substr(0,8);
                    var b = test[i].substr(2,6);
                    colStr+= "<li id=\""+a+"\" onclick='code(\""+a+"\",1)';>" +test[i]+"</li>";

                }
                $('#test').html(colStr);
                //键入触发事件
                $('#searchKey').keyup(function(){
                    //如果输入的值为空就刷新当前页面
                    var keyWord = ($('#searchKey').val()).toUpperCase();
                    if(!keyWord){
                        location.reload();
                    }
                    var searchResult = fuzzySearch();
                    if(searchResult != undefined){
                        renderTab(searchResult);
                        $('.search-over').css("display","block");
                    }else{
                        $('.search-over').css("display","none");
                    }
                });

                function fuzzySearch(){
                    //获取值并转换成大写
                    var keyWord = ($('#searchKey').val()).toUpperCase();
                    if(!keyWord){
                        return undefined;
                    }else{
                        var len = test.length;
                        var arr = [];
                        var res = [];
                        var reg = new RegExp(keyWord);
                        for(var i=0;i<len;i++){
                            //如果字符串中不包含目标字符会返回-1
                            if(test[i].match(reg)){
                                arr.push(test[i]);
                            }
                        }
                        //取前10条数据
                        if(arr.length>10){
                            for(var i=0;i<10;i++){
                                //截取字符串
                                var num = arr[i].indexOf(')');
                                arr[i] = arr[i].substr(0,num+1);

                                res.push(arr[i]);
                            }
                        }else{
                            for(var i=0;i<arr.length;i++){
                                //截取字符串
                                var num = arr[i].indexOf(')');
                                arr[i] = arr[i].substr(0,num+1);
                                res.push(arr[i]);
                            }

                        }
                        return res;
                    }
                }

                //渲染表格
                function renderTab(list){
                    if(list.length==0){
                        $('#test').html('');
                        return;
                    }
                    var colStr = '';
                    for(var i=0,len=list.length;i<len;i++){
                        var a = list[i].substr(0,8).toLowerCase();
                        var b = list[i].substr(9);
                        //去除字符串最后一个字符
                        b = b.substr(0,b.length-1);
                        list[i] = a+"("+b+")";
                        colStr+= "<li id=\""+a.toLowerCase()+"\" onclick='code(\""+a.toLowerCase()+"\",1)';>" +list[i]+"</li>";

                    }
                    $('#test').html(colStr);
                }
        	})
      
	</script>




	<!--模糊搜索js-->
	<script type="text/javascript">
		

		

		//跳转h5创建策略页
		function code(str,status){


			// var searchKey = $("#"+str).val();

			// if(!searchKey){
			// 	return;
			// }
								
			$.ajax({
				url: "/api/index/checkstockstop",
				data: {gid:str},
				dataType: "json",
				method:"post",
				async:true,
				success: function (data) {
					// alert('ajax调用成功');
					var dataObj = JSON.parse(data);
					if(dataObj.status==200){
						addHistory(searchKey);//添加历史记录
						window.location.href="/index/strategy/optional_info?gid="+str;
					}else{
						
						submitsuccess("此股票不能购买");
						return;
					}
				}
			})

		}

	</script>

	<!--历史记录js-->
	<script type="text/javascript">
		var save_max_len=50;//最多保存50条搜索记录，超过50条，新的记录覆盖最旧的
		var display_max_len=6;//下拉框最多显示3条记录
		$(document).ready(function() {
			showHistory();

		});
		//显示历史记录
		function showHistory() {
			var data = new Array();//cookie的值
			var cookie=$.cookie("search_history");//获取cookie
			if(cookie!=null){
				data = JSON.parse(cookie); //从cookie中取出数组
			}

			$("#history").empty();//清除原来的显示内容，以免重复显示
			if (data != null) {
				var len = data.length>display_max_len?display_max_len:data.length;//显示时只显示特定的条数
				var limit = data.length-len-1;
				for (var i = data.length-1; i >limit ; i--) {
					//截取字符串
					var code = new Array();//股票代码
					var name = new Array();//股票名称
					var num = data[i].indexOf('(');
					code[i] = data[i].substr(0,num);
					name[i] = data[i].substr(num+1);
					//去除字符串最后一个字符
					name[i] = name[i].substr(0,name[i].length-1);
					var bs = code[i]+"bs";
					var bq = code[i]+"bq";
					var liHtml= "<li><a  class='aui-palace-grid'  onclick='hot(\""+code[i]+"\",1)'; >"+"<h3>"+name[i]+"</h3>"+"<h4>"+code[i]+"</h4>"+"<p class='lve' id=\""+bs+"\">6.064"+"</p>"+"<input type='hidden' value=\""+data[i]+"\" id=\""+code[i]+"\">"+"</a></li>";
					$("#history").append(liHtml);
					// $.ajax({
					//                 url: "/stockht/API/index/getOneStock?gid="+code[i],
					//                 dataType: "json",
					//                 method:"get",
					//                 //for循环是同步的
					//                 async:true,
					//                 success: function relash(lala) {
					//                     // alert('ajax调用成功');
					//                     var dataObj = JSON.parse(lala);
					//                     if(dataObj.status==1){
					//                     	var res = dataObj.list.result;
					//                     	var nowpri = res[0].data.nowPri;
					//                     	var code = res[0].data.gid;
					//                     	var name = res[0].data.name;
					//                     	var bs = code+"bs";
					// 			$("#"+bs).html(nowpri);
					//                     }else{
					//                         alert("获取数据失败");
					//                     }
					//                 }
					//             })
				}
				reflash();
			}
			/* 等待五秒后再次请求数据 */
			// window.setTimeout("showHistory()", 5000);
		}

		//执行ajax
		function reflash(){
			var data = new Array();//cookie的值
			var code = new Array();//股票代码
			var cookie=$.cookie("search_history");//获取cookie
			if(cookie!=null){
				data = JSON.parse(cookie); //从cookie中取出数组
			}
			if (data != null) {
				var len = data.length>display_max_len?display_max_len:data.length;//显示时只显示特定的条数
				var limit = data.length-len-1;
				for (var i = data.length-1; i >limit ; i--) {
					var num = data[i].indexOf('(');
					code[i] = data[i].substr(0,num);
					$.ajax({
						url: "/index/strategy/getOneStock2?gid="+code[i],
						dataType: "json",
						method:"get",
						//for循环是同步的
						async:true,
						success: function relash(lala) {
							// alert('ajax调用成功');
							var dataObj = JSON.parse(lala);
							if(dataObj.status==1){
								var res = dataObj.list.result;
								var nowpri = res[0].data.nowPri;
								var code = res[0].data.gid;
								var name = res[0].data.name;
								var bs = code+"bs";
								$("#"+bs).html(nowpri);
							}else{
								alert("获取数据失败");
							}
						}
					})
				}
			}
			/* 等待五秒后再次请求数据 */
			window.setTimeout("reflash()", 5000);
		}
		//添加历史记录
		function addHistory(str) {
			var data = new Array();
			var cookie=$.cookie("search_history");
			if(cookie!=null){
				data = JSON.parse(cookie);
			}
			//如果历史记录中有，就先删除，然后再添加（保持最近搜索的记录在最新），否则，直接添加
			var index=-1;
			if(data){
				index=data.indexOf(str);
			}
			if(index>-1){
				data.splice(index,1);//删除原来的
			}

			//最多保留save_max_len条记录，超过最大条数，就把第一条删除
			if(data && data.length==save_max_len){
				data.splice(0,1);
			}
			data.push(str);
			$.cookie('search_history', JSON.stringify(data), {expires : 365});//设置一年有效期
		}

	</script>

	<!--热门股票js-->
	<script type="text/javascript">
		// function change(){
		// var count = $('#hotstock').children('a').length;
		// var content = $('.hotid').html();
		// console.log(content);

		// var $elements = $('.hotid');
		// var len = $elements.length;
		// $elements.each(function() {
		// 	var $this = $(this);
		// 	alert($this.html());
		// });
		reflash2();
		//执行ajax
		function reflash2(){
			var data = new Array();//cookie的值
			var code = new Array();//股票代码
			if (data != null) {
				var $elements = $('.hotid');
				$elements.each(function() {
					var $this = $(this);
					var code = $this.html();
					$.ajax({
						url: "/index/strategy/getOneStock2?gid="+code,
						dataType: "json",
						method:"get",
						//for循环是同步的
						async:true,
						success: function relash(lala) {
							// alert('ajax调用成功');
							var dataObj = JSON.parse(lala);
							if(dataObj.status==1){
								var res = dataObj.list.result;
								var nowpri = res[0].data.nowPri;
								var code = res[0].data.gid;
								var name = res[0].data.name;
								var bj = code+"bj";
                              	var otbj = code+"otbj";
                              	$("#"+otbj).html(nowpri);
								$("#"+bj).html(nowpri);
							}else{
								alert("获取数据失败");
							}
						}
					})

				})
			}
			/* 等待五秒后再次请求数据 */
			window.setTimeout("reflash2()", 5000);
		}
	</script>


</body>
</html>