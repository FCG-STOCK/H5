<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:69:"/www/wwwroot/peiqi.solingke.cn/application/index/view/trade/ploy.html";i:1554273878;s:75:"/www/wwwroot/peiqi.solingke.cn/application/index/view/index/inc/footer.html";i:1554779792;}*/ ?>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta http-equiv="content-language" content="zh-CN" />
        <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no" />
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black" />
        <meta name="format-detection" content="telephone=no" />
        <meta name="keywords" content="" />
        <meta name="description" content="" />
        <meta name="author" content="令克网络-互联网系统解决方案服务商-https://www.link-web.cn/" />
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
        <title>股票行情</title>
        <link href="/public/static/img/favicon.ico" rel="shortcut icon">
        <link rel="stylesheet" href="/public/static/css/reset.css">
        <link rel="stylesheet" href="/public/static/css/style.css">
        <link rel="stylesheet" href="/public/static/css/price.css">
        <link rel="stylesheet" href="/public/static/awesome/css/font-awesome.css">
        <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
        <script src="/public/static/js/vue.js"></script>
        <script src="/public/static/js/jquery-1.11.3.js"></script>
        <script src="/public/static/js/html5.js"></script>
        <script src="/public/static/js/adaptive-version2.js"></script>
        <script src="/public/static/js/jquery.easing.1.3.js"></script>
        <script src="/public/static/js/jquery.transit.js"></script>
        <script src="/public/static/js/jquery.lazyload.js"></script>
        <style>.price-search{top:0.09rem}</style>
        <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    </head>
    <body>
        <div id="app">
            <div class='por ploy-header'>
                    <div class="guide-top">
                        <a href="/index/trade/simulation.html" class="back"><i class="fa fa-angle-left"></i></a>
                        {{title}}
                        <div class="price-search" onclick="window.location.href='/index/index/optional_search_trade.html'">切换</div>
                    </div>
                </div>
            <div class="m20">
                <div class="play-bei">
                    <h2>投入信用金</h2>
                    <ul class='f-cb'>
                        <li v-for='item in bei' @click="changes(item,$event)"><span>{{item}}</span></li>
                    </ul>
                </div>
                <div class="play-num on">
                        买入数量
                        <p>{{list.stock_number}}股(市值:{{(list.stock_number * nowPri/10000).toFixed(2)}}万,资金利用率: {{(list.stock_number * nowPri / tuijian *100).toFixed(2)}} %)</p>
                </div>
                <div class="play-loss">
                    <ul>
                        <li class="red">
                                止盈
                                <p>涨至 <span class="green">20.00%</span> 发起平仓</p>
                                <div class="f-cb">
                                    <span @click="profitLess">-</span>
                                    <input type="text" disabled v-model="list.surplus_value">
                                    <span @click="profitAdd">+</span>
                                </div>
                            </li>
                            <li>
                                止损
                                <p>下跌 <span class="green">10.00%</span> 发起平仓</p>
                                <div class="f-cb">
                                    <span @click="lossLess">-</span>
                                    <input type="text" disabled v-model="list.loss_value">
                                    <span @click="lossAdd">+</span>
                                </div>
                            </li>
                    </ul>
                </div>
            </div>
            <div class="m20">
                    <div class="play-num">
                            预计可赚
                        <p>{{((list.surplus_value - nowPri)*list.stock_number).toFixed(2)}}元</p>
                    </div>
                    <div class="play-num">
                            合计
                        <p>{{parseFloat(moneyone)}}元</p>
                    </div>
                    <div class="play-num">
                            策略余额
                        <p>{{analog_money}}元</p>
                    </div>
                </div>
            <div class="ploy-bottom">
                <h2>我已同意阅读并同意<a href="">《相关协议》</a></h2>
                <a href="javascript:;" @click="submit">创建策略</a>
            </div>
        </div>
        <div style="height:1.2rem"></div>
        <div class="includeDom"  data-id="2">
            <footer>
    <ul class="f-cb">
        <!-- <li>
            <a href="/index/index/index.html">
                <div class="pic-box">
                    <img src="/public/static/img/footer01.png" alt="" class="bg">
                    <img src="/public/static/img/footer01_h.png" alt="" class="pic">
                </div>
                <h2>首页</h2>
            </a>
        </li>
        <li>
            <a href="/index/strategy/strategy.html">
                <div class="pic-box">
                    <img src="/public/static/img/footer02.png" alt="" class="bg">
                    <img src="/public/static/img/footer02_h.png" alt="" class="pic">
                </div>
                <h2>策略</h2>
            </a>
        </li>
        <li>
            <a href="/index/ranking/rankingList.html">
                <div class="pic-box">
                    <img src="/public/static/img/footer03.png" alt="" class="bg">
                    <img src="/public/static/img/footer03_h.png" alt="" class="pic">
                </div>
                <h2>排行</h2>
            </a>
        </li>
        <li>
            <a href="/index/center/personal.html">
                <div class="pic-box">
                    <img src="/public/static/img/footer04.png" alt="" class="bg">
                    <img src="/public/static/img/footer04_h.png" alt="" class="pic">
                </div>
                <h2>我的</h2>
            </a>
        </li> -->
        <li>
            <a href="/index/index/index.html">
                <div class="pic-box">
                    <img src="/public/static/img/img_7.png" alt="" class="bg">
                    <img src="/public/static/img/img_8.png" alt="" class="pic">
                </div>
                <h2>首页</h2>
            </a>
        </li>
        <li>
            <a href="/index/price/quotes.html">
                <div class="pic-box">
                    <img src="/public/static/img/img_9.png" alt="" class="bg">
                    <img src="/public/static/img/img_10.png" alt="" class="pic">
                </div>
                <h2>行情</h2>
            </a>
        </li>
        <li>
            <a href="/index/trade/index.html">
                <div class="pic-box">
                    <img src="/public/static/img/img_11.png" alt="" class="bg">
                    <img src="/public/static/img/img_12.png" alt="" class="pic">
                </div>
                <h2>交易</h2>
            </a>
        </li>
        <li>
            <a href="/index/news/index.html">
                <div class="pic-box">
                    <img src="/public/static/img/img_13.png" alt="" class="bg">
                    <img src="/public/static/img/img_14.png" alt="" class="pic">
                </div>
                <h2>资讯</h2>
            </a>
        </li>
        <li>
            <a href="/index/center/personal.html">
                <div class="pic-box">
                    <img src="/public/static/img/img_15.png" alt="" class="bg">
                    <img src="/public/static/img/img_16.png" alt="" class="pic">
                </div>
                <h2>我的</h2>
            </a>
        </li>
    </ul>
    <div class="loading">
            <div class="loadings">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div> 
        <!-- <img src="/public/static/img/logo.png" alt="" class="logo"> -->
    </div>
</footer>
<script>
    var id = $('footer').parent(".includeDom").data('id');
    $('footer li').eq(id).addClass('hover');
</script>
        </div>
        <div class="loading">
            <img src="/public/static/img/logo.png" alt="" class="logo">
        </div>
        <script>
            jQuery(document).ready(function() {
                $('.loading').addClass("active");
            })
        </script>
        <script src="/public/static/js/main.js"></script>
        <script src="/public/static/js/jquery.cookie.js"></script>
        <script>
            var id = $('.ploy-header').parent(".includeDom").data('id');
            $('.ploy-list li').eq(id).addClass('hover');
            $(function(){
                $(".ploy-bottom h2").click(function () {
                    if (!$(this).hasClass("on")) {
                        $(this).addClass("on")
                    }else{
                        $(this).removeClass("on")
                    }
                })
            })
            let msd=search()
            var vm=new Vue({
            el:'#app',
            props: {
            },
            data() {
                return {
                    title:"美的集团SZ000333",
                    money:"1000",
                    moneyone:"",
                    bei:[1000,2000,3000,5000,8000,10000,20000,30000],
                    loss:'',
                    profit:'14.3',
                    name:"",
                    nowPri:"",
                    tuijian:"",
                    poundage:"",
                    list:{
                        // market_value:"",
                        // double_value:"",
                        // strategy_type:"1",
                        stock_name:"",
                        stock_code:"",
                        stock_number:"",
                        credit:"",
                        buy_price:"",
                        // buy_poundage:"0",
                        surplus_value:"",
                        loss_value:"",
                        // defer:2,
                        // packet_id:""
                    },
                    analog_money:''
                };
            },
            computed: {
            },
            created() {
            },
            mounted() {
                if (sessionStorage.getItem('packet')) {
                    let packet = JSON.parse(sessionStorage.getItem('packet'))
                    this.name=packet.money
                    // this.list.packet_id = packet.id
                }
                this.stockDetail_tobuy()
                this.account_money()
                this.moneyone = this.bei[this.bei.length-1]
                this.list.credit = this.moneyone
            },
            methods: {
                // 选择金额
                inputs(val,e){
                    console.log($(e.target)[0].value)
                    if($(e.target)[0].value==''){
                        this.list.surplus_value =  (this.nowPri *1.2).toFixed(2)
                        this.list.stock_number =0
                        this.loss = "00.00"
                        this.list.loss_value ="0.00"
                        return false;
                    }
                    this.moneyone = $(e.target)[0].value*1000;
                    this.list.credit = this.moneyone
                    let nums = this.bei[$(".play-bei li.on").index()]
                    this.tuijian= (this.moneyone*nums);
                    this.list.double_value = this.tuijian
                    this.algorithm()
                },
                // 选择倍数
                changes(item,e){
                    e.preventDefault();
                    if($(e.currentTarget).hasClass("on")){
                    }else{
                        $(e.currentTarget).addClass("on").siblings().removeClass("on")
                        this.tuijian= $(e.currentTarget).find("span").text();
                        // this.list.double_value = this.tuijian
                        this.moneyone = this.tuijian
                        this.algorithm()
                    }
                },
                // 盈利减
                profitLess(){
                    if(this.list.surplus_value>this.nowPri){
                        this.list.surplus_value= (this.list.surplus_value-0.01).toFixed(2)
                    }else{
                        return false;
                    }
                },
                // 盈利加
                profitAdd(){
                    let num = (this.nowPri *1.2).toFixed(2)
                    if(this.list.surplus_value<num){
                        this.list.surplus_value= (parseFloat(this.list.surplus_value) + 0.01).toFixed(2)
                    }else{
                        return false;
                    }
                },
                // 亏损减
                lossLess(){
                    let less = ((90)* this.nowPri/100).toFixed(2);
                    console.log(less)
                    if(this.list.loss_value>less){
                        this.list.loss_value= (this.list.loss_value-0.01).toFixed(2)
                    }else{
                        return false;
                    }
                },
                // 亏损加
                lossAdd(){
                    if(this.list.loss_value<this.nowPri){
                        this.list.loss_value= (parseFloat(this.list.loss_value) + 0.01).toFixed(2)
                    }else{
                        return false;
                    }
                },
                // 自动
                switchd(e){
                    if($(e.currentTarget).hasClass("on")){
                        $(e.currentTarget).removeClass("on")
                        this.list.defer = 2
                    }else{
                        $(e.currentTarget).addClass("on")
                        this.list.defer = 1
                    }
                    
                },
                // 创建策略
                submit(){
                    if (!$(".ploy-bottom h2").hasClass("on")|| !$(".play-bei li").hasClass("on")) {
                        alert("请阅读相关协议并选择推荐金额！")
                        return false;
                    }
                    if (this.list.stock_number=='0') {
                        alert("请买入股数！")
                        return false;
                    }
                    Vue.delete(this.list,'trade');

                    // this.list.market_value = (this.list.stock_number * this.nowPri).toFixed(2)
                    let _this = this;
                        axios.get('/api/mobile/createStrategyContest',{params:Object.assign(_this.list,account)})
                        .then(function (res) {
                            if(res.data.status =='1'){
                                alert("添加成功！")
                                window.location.href="/index/trade/simulation.html"
                            }else{
                                alert(res.data.msg)
                            }
                        })
                        .catch(function (error) {
                            console.log(error);
                        });
                },
                // 详细信息
                stockDetail_tobuy(){
                        let _this = this;
                        axios.post('/api/mobile/stockDetail_tobuy',Object.assign(account,{stock_code:getSearchString('stock_code')}))
                        .then(function (res) {
                            if(res.data.status =='200'){
                                _this.nowPri = res.data.list.nowPri
                                _this.list.buy_price = res.data.list.nowPri
                                // _this.bs()
                                setTimeout(function(){
                                    $(".play-bei li").eq(7).addClass("on")
                                    _this.tuijian = $(".play-bei li").eq(7).find("span").text();
                                    // _this.list.double_value = _this.tuijian;
                                    _this.algorithm()
                                },10)
                            }
                        })
                        .catch(function (error) {
                            console.log(error);
                        });
                    },
                // 算法
                algorithm(){
                    this.list.surplus_value =  (this.nowPri *1.2).toFixed(2)
                    this.list.stock_number =(this.tuijian / this.nowPri) - ((this.tuijian / this.nowPri) % 100) 
                    // this.loss = ((this.moneyone - ( this.list.stock_number * this.nowPri*0.03))/this.list.stock_number/this.nowPri*100).toFixed(2)
                    this.list.loss_value =( (90)*this.nowPri/100).toFixed(2)
                    // this.list.buy_poundage = this.tuijian*this.poundage

                },
                account_money(){
                    let _this = this;
                    axios.post('/api/mobile/account_money',account)
                    .then(function (res) {
                        if(res.data.status =='1'){
                            _this.analog_money = res.data.list.analog_money
                        }
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
                }
            }
        })
        vm.title= msd.stock_name+msd.stock_code
        vm.list = Object.assign(vm.list,msd)
        </script>


    </body>
</html>