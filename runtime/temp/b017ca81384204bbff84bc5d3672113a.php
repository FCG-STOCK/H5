<?php if (!defined('THINK_PATH')) exit(); /*a:1:{s:74:"/home/wwwroot/m/application/index/view/strategy/optional_contest_info.html";i:1548925628;}*/ ?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta http-equiv="content-language" content="zh-CN" />
        <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no" />
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black" />
        <meta name="format-detection" content="telephone=no" />
        <meta name="keywords" content="" />
        <meta name="description" content="" />
        <meta name="author" content="令克网络-高端网站建设-https://www.link-web.cn/" />
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
        <title>赢在策略</title>
        <link href="/public/static/img/favicon.ico" rel="shortcut icon">
        <link rel="stylesheet" href="/public/static/css/reset.css">
        <link rel="stylesheet" href="/public/static/css/style.css">
        <script src="/public/static/js/jquery-1.11.3.js"></script>
        <script src="/public/static/js/html5.js"></script>
        <script src="/public/static/js/adaptive-version2.js"></script>
        <script src="/public/static/js/jquery.easing.1.3.js"></script>
        <script src="/public/static/js/jquery.transit.js"></script>
        <script src="/public/static/js/jquery.lazyload.js"></script>
    </head>
    <body>
        <!-- <div class="includeDom" include="inc/header.html" data-intro="index"></div> -->
        <div class="content-box data">
            
            <div class="optional-info-con">
                <div class="content">
                    <div class="optional-info-tit f-cb">
                        <div class="left fl">
                            <h2 id="stockname"><?php echo $res['name']; ?></h2>
                            <p id="stockid"><?php echo $res['gid']; ?></p>
                        </div>
                        <div class="center fl">
                            <p <?php if($res['bs'] == 2): ?> style="color:#26b41b" <?php endif; ?> id="danjia"><?php echo $res['nowPri']; ?></p>
                            <?php if($res['bs'] == 2): ?> 
                            <p style="color:#26b41b"><?php echo $res['increase']; ?> <?php echo $res['increPer']; ?>%</p>
                            <?php else: ?>
                            <p >+<?php echo $res['increase']; ?> +<?php echo $res['increPer']; ?>%</p>
                            <?php endif; ?>
                            
                            
                        </div>
                        <a href="javascript:;" class="fr info-box">查看行情</a>
                    </div>
                    <div class="optional-info-bb">
                        
                        <div class="con f-cb">
                            <div class="tit fl">交易金额</div>
                            <div class="b-con" >
                                <a href="javascript:;" class="two hover">1000</a>
                                <a href="javascript:;" class="three">2000</a>
                                <a href="javascript:;" class="four">3000</a>
                                <a href="javascript:;" class="five">5000</a>
                                <a href="javascript:;" class="six">8000</a>
                                <a href="javascript:;" class="seven">10000</a>
                                <a href="javascript:;" class="eight">20000</a>
                                <a href="javascript:;" class="night">30000</a>
                            </div>
                        </div>
                    </div>
                    <div class="optional-info-linepic">
                        <div class="title">分时</div>
                        <div class="cont" id="Kline-div">
                           <div id="m-line" class="m-line"></div>
                        </div>
                    </div>
                    <div class="optional-info-ib">
                        <div class="title f-cb">
                            <h2>买入数量</h2>
                            <div class="fl box">
                                <h3 id="gushu">200股</h3>
                                <p id="shizhi">(市值:3996元,资金利用率82.07%)</p>
                            </div>
                        </div>
                        <dl class="con f-cb">
                            <dd>
                                <h2>止盈：<span class="col">涨至</span></h2>
                                <span class="input-wrapper">
                                    <i class="input-minus bbt red" onclick="zydown()">-</i>
                                    <input type="text" value="25.5" id="up" oninput="value=value.replace(/[^\d.]/g,'')" style="color: #d63834;" disabled="disabled">
                                    <i class="input-plus bbt red" onclick="zyup()">+</i>
                                </span>
                                <p>上涨<span class="red" id="zybfs">20%</span>发起平仓</p>
                            </dd>
                            <dd>
                                <h2>止损：<span class="col">跌至</span></h2>
                                <span class="input-wrapper">
                                    <i class="input-minus bbt green" onclick="zsdown()">-</i>
                                    <input type="text" value="13.33" id="down" oninput="value=value.replace(/[^\d.]/g,'')" disabled="disabled">
                                    <i class="input-plus bbt green" onclick="zsup()">+</i>
                                </span>
                                <p>下跌<span class="green" id="zsbfs">10%</span>发起平仓</p>
                            </dd>
                        </dl>
                    </div>
                    <div class="optional-info-it">预计可赚 <span class="red" id="shouru">643.8 </span>元</div>
                    
                    <div class="optional-info-bototm mncl">
                        <h2>合计：<span class="red" id="zfy">500.00</span> 元 <div class="fr">模拟策略余额：<span class="red" id="analog_money"><?php echo $analog_money; ?></span> 元</div></h2>
                        <input type="submit" name="" class="submit hover" value="创建模拟策略" onclick="setcel()" id="chuangjian">
                        <label class="check"><input type="checkbox" name="" value="" checked="checked">我已同意阅读并同意<a href="/HD(T+1).pdf">《相关协议》</a></label>
                    </div>
                </div>
            </div>
        </div>
        

        <input type="hidden"  id="hid_shizhi" value="">
        <div class="submit-success" style="display: none;"></div>
        <!-- <div class="includeDom" include="inc/footer.html"></div> -->
        <script src="/public/static/js/main.js"></script>
        <script src="/public/static/js/echarts.min.js"></script>
        <script src="/public/static/js/tmpData.js"></script>
        <script src="/public/static/js/k-line.js"></script>
        <script src="/public/static/js/jquery.cookie.js"></script>
        <script>
            $(function () {
                


                

                
                /*计算一些数值*/

                //翻倍后的信用金
                var fbmoney = $(".b-con .hover").html();
                   
                //单价
                var danjia = Number($("#danjia").html());

                //推荐买入股数 翻倍后的信用金/单价 向下取整(只能是100的倍数)
                var gushu = (Math.floor(fbmoney/danjia/100))*100;

                //市值=推荐买入股数*单价
                var shizhi = Math.round(gushu*danjia);//number的精度原因用四舍五入
                $("#gushu").html(gushu+"股");
                
                  
                //资金利用率
                var bfs = (Math.floor(((shizhi/fbmoney)*100)*10))/10; 
                
                $("#shizhi").html("(市值"+shizhi+"元,资金利用率"+bfs+"%)");
                //隐藏域里的市值
                $("#hid_shizhi").val(shizhi);

                //止盈价格 止损价格 取小数点后两位
                $("#up").val((Math.floor((danjia*1.2)*100))/100);
                
                  
                if(gushu==0){
                    $("#zsbfs").html("0%");
                    $("#down").val("0%");            
                }else{
                    //止损价格百分数  = 10%
                    var zsbfs  = 0.1;
                    zsbfs = (zsbfs*100).toFixed();
                    $("#zsbfs").html(zsbfs+"%");

                    //止损价格 = （1-止损百分数）*现价
                    $("#down").val(((1-zsbfs/100)*danjia).toFixed(2));            
                }
                  
                //预计可赚
                var zhiyin = Number($("#up").val());
                var chazhi = zhiyin-danjia;
                var yjkz = Math.round(gushu*chazhi);//四舍五入           
                $("#shouru").html(yjkz);
             
                //总费用
                var zfy = $(".b-con  .hover").html();

                $("#zfy").html(zfy);

                /*计算结束*/



                $(".b-con a").click(function(){  

                    $(this).addClass('hover').siblings("a").removeClass('hover');   

                    /*计算一些数值*/

                    //翻倍后的信用金
                    var fbmoney = $(".b-con .hover").html();
                       
                    //单价
                    var danjia = Number($("#danjia").html());

                    //推荐买入股数 翻倍后的信用金/单价 向下取整(只能是100的倍数)
                    var gushu = (Math.floor(fbmoney/danjia/100))*100;

                    //市值=推荐买入股数*单价
                    var shizhi = Math.round(gushu*danjia);//number的精度原因用四舍五入
                    $("#gushu").html(gushu+"股");
                    
                      
                    //资金利用率
                    var bfs = (Math.floor(((shizhi/fbmoney)*100)*10))/10; 
                    
                    $("#shizhi").html("(市值"+shizhi+"元,资金利用率"+bfs+"%)");
                    //隐藏域里的市值
                    $("#hid_shizhi").val(shizhi);

                    //止盈价格 止损价格 取小数点后两位
                    $("#up").val((Math.floor((danjia*1.2)*100))/100);
                    
                      
                    if(gushu==0){
                        $("#zsbfs").html("0%");
                        $("#down").val("0%");            
                    }else{
                        //止损价格百分数  = 0.1
                        var zsbfs  = 0.1;
                        zsbfs = (zsbfs*100).toFixed();
                        $("#zsbfs").html(zsbfs+"%");

                        //止损价格 = （1-止损百分数）*现价
                        $("#down").val(((1-zsbfs/100)*danjia).toFixed(2));            
                    }
                      
                    //预计可赚
                    var zhiyin = Number($("#up").val());
                    var chazhi = zhiyin-danjia;
                    var yjkz = Math.round(gushu*chazhi);//四舍五入           
                    $("#shouru").html(yjkz);
                 
                    //技术服务费
                    var fwf = fbmoney*0.0025;
                    $("#fwf").html(fwf);

                    //判断是否使用红包
                    if($("#redpacket").val()==''){
                        //总费用
                        var zfy = Number($("#jianru").val()*1000)+Number(fwf);
                    }else{
                        //总费用
                        var zfy = Number($("#jianru").val()*1000)+Number(fwf)-10;
                    }

                    //总费用
                    var zfy = $(".b-con  .hover").html();

                    $("#zfy").html(zfy);

                    /*计算结束*/
                    
                })

                

                $('.optional-info-tit .info-box').click(function(){
                    if ($(this).hasClass('hover')) {
                        $(this).removeClass('hover').html("查看行情");
                        $(this).parents(".content").find('.optional-info-linepic').height(0)
                    }else{
                        $(this).addClass('hover').html("关闭行情");
                        $(this).parents(".content").find('.optional-info-linepic').height("auto")
                    }
                })

                $('.optional-info-bototm label input').click(function(){
                    if ($(this).is(":checked")) {
                        $(this).parent("label").addClass('check');
                        $(this).parents(".optional-info-bototm").find('.submit').addClass('hover')
                        $(".optional-info-bototm input[type='submit']").attr("onclick","setcel()");
                        
                    }else{
                        $(this).parent("label").removeClass('check');
                        $(this).parents(".optional-info-bototm").find('.submit').removeClass('hover')
                        
                        $(".optional-info-bototm input[type='submit']").removeAttr("onclick");
                    }
                })
            })
        </script>

        <!--选择止盈止损-->
        <script type="text/javascript">
            
            function zyup(){
                var danjia = $("#danjia").html();
                var bjnum = Number(danjia)*1.2;
                var ponum = Number($("#up").val());
                if(ponum>=bjnum){
                  $("#zyup").css("cursor","not-allowed");
                  $("#zyup").removeAttr("onclick");
                }else{
                  $("#zydown").attr("onclick","zydown()");
                  $("#zydown").css("cursor","pointer");
                  $("#zyup").css("cursor","pointer");
                  $("#zyup").attr("onclick","zyup()");
                  
                  var a = Number($("#up").val());   
                  var b = 0.01;
                  var vb =a+b;
                  vb = Math.round(vb * 100) / 100;
          
                  $("#up").val(vb);
                  var zybfs = parseInt(((vb-danjia)/danjia)*100);
                  $("#zybfs").html(zybfs+"%");
                  //预计可赚
                  var gushu = $("#gushu").html();      
                  gushu = Number(gushu.substr(0,gushu.length-1));
                  var zhiyin = Number($("#up").val());
                  var chazhi = zhiyin-danjia;
                  var yjkz = Math.round(gushu*chazhi);//四舍五入           
                  $("#shouru").html(yjkz);
                }    
            
            }
          
            
          
            function zydown(){
                var danjia = $("#danjia").html();
                var bjnum = Number(danjia);
                var ponum = Number($("#up").val());
                if(ponum<=bjnum){
                  $("#zydown").css("cursor","not-allowed");
                  $("#zydown").removeAttr("onclick");
                }else{
                  $("#zyup").attr("onclick","zyup()");
                  $("#zyup").css("cursor","pointer");
          
                  
                  var vb = Number($("#up").val())-0.01;
                  vb =  (Math.floor(vb*100))/100;
                  $("#up").val(vb);
                  var zybfs = parseInt(((vb-danjia)/danjia)*100);
                  $("#zybfs").html(zybfs+"%");
                //预计可赚
                  var gushu = $("#gushu").html();      
                  gushu = Number(gushu.substr(0,gushu.length-1));
                  var zhiyin = Number($("#up").val());
                  var chazhi = zhiyin-danjia;
                  var yjkz = Math.round(gushu*chazhi);//四舍五入           
                  $("#shouru").html(yjkz);
                }    
            
            }
              
              

            function zsup(){
                var danjia = $("#danjia").html();
                var bjnum = Number(danjia);
                var ponum = Number($("#down").val());
                if(ponum>=bjnum){
                  $("#zsup").css("cursor","not-allowed");
                  $("#zsup").removeAttr("onclick");
                }else{
                  $("#zsdown").attr("onclick","zsdown()");
                  $("#zsdown").css("cursor","pointer");
                  $("#zsup").css("cursor","pointer");
                  $("#zsup").attr("onclick","zsup()");
                  
                  var a = Number($("#down").val());   
                  var b = 0.01;
                  var vb =a+b;
                  vb = Math.round(vb * 100) / 100;
                  $("#down").val(vb);
                  var zsbfs = parseInt(((danjia-vb)/danjia)*100);
                  $("#zsbfs").html(zsbfs+"%");
                }
            
            
            }
          
          
            function zsdown(){
                var danjia = $("#danjia").html();
                
                var fbmoney = $(".b-con .hover").html();
                      
              var reg = new RegExp("[\\u4E00-\\u9FFF]+","g");
              if(reg.test(fbmoney)){
                fbmoney = fbmoney.substr(0,fbmoney.length-2);
                fbmoney = Number(fbmoney*10000);
              }              
                   
                 //推荐买入股数 翻倍后的信用金/单价 向下取整(只能是100的倍数)
                 var gushu = (Math.floor(fbmoney/danjia/100))*100;
                 //市值=推荐买入股数*单价
                 var shizhi = Math.round(gushu*danjia);
              
                 //止损价格百分数  =[信用金-(市值*10%)]/股票数/现价
                var xymoney = $("#jianru").val()*1000;
                var zsbfs  = (xymoney-shizhi*0.1)/gushu/danjia;
                zsbfs = zsbfs.toFixed(2);
              
                var bjnum = (Number(danjia)*(1-zsbfs));
                var ponum = Number($("#down").val());
                if(ponum<=bjnum){
                  $("#zsdown").css("cursor","not-allowed");
                  $("#zsdown").removeAttr("onclick");
                }else{
                  $("#zsup").attr("onclick","zsup()");
                  $("#zsup").css("cursor","pointer");
                  $("#zsdown").css("cursor","pointer");
                  $("#zsdown").attr("onclick","zsdown()");
                  var vb = Number($("#down").val())-0.01;
                  vb =  (Math.floor(vb*100))/100;
                  $("#down").val(vb);
                  var zsbfs = parseInt(((danjia-vb)/danjia)*100);
                  $("#zsbfs").html(zsbfs+"%");
                }
            
            
            }

        </script>


        <!--提交-->
        <script type="text/javascript">
            function setcel(){
                var market_value = $("#hid_shizhi").val();//市值
                if(market_value==0){
                    submitsuccess("市值不能为0");
                    return;
                }
                if(market_value>500000){
                    submitsuccess("单笔策略市值超过最大额度50万");
                    return;       
                }

                 

                // if(Number($("#zfy").html())>Number($("#tactics_balance").html())){
                //     submitsuccess("策略余额不足");
                //     return; 
                // }
                     
                var account = $.cookie('username');//账户
                
                var stock_name = $("#stockname").html();//股票名称
                var stock_code = $("#stockid").html();//股票代码
                var stock_number = $("#gushu").html().substr(0,$("#gushu").html().length-1);//买入股数
                var credit = $(".b-con .hover").html();;//信用金
               
                var buy_price = $("#danjia").html();//买入价格
                var surplus_value = $("#up").val();//止盈价格
                var loss_value = $("#down").val();//止损价格
                
              
                $.ajax({
                    url: "/API/info/createStrategyContest",
                    data: { 
                            account:account,                                                         
                            stock_name:stock_name,
                            stock_code:stock_code,
                            stock_number:stock_number,
                            credit:credit,                           
                            buy_price:buy_price,
                            surplus_value:surplus_value,
                            loss_value:loss_value,
                            
                              },
                    dataType: "json",
                    method:"post", 
                    success: function (data) {
                        // alert('ajax调用成功');
                        var dataObj = JSON.parse(data);
                        if(dataObj.status==1){
                            submitsuccess("正在为您申购请稍后...");
                            $("#chuangjian").removeClass('hover');                       
                            $("#chuangjian").removeAttr("onclick");  
                            setTimeout(function () { 
                                submitsuccess("恭喜您创建策略成功");
                            },3000);   
                            setTimeout(function () { window.location.href = '/index/strategy/strategy'; }, 4000);//延迟1秒后跳转 
                        }
                        if(dataObj.status==1100){
                            
                            submitsuccess("模拟策略余额不足，请先充值"); 
                            return;
                                            
                        }
                                       
                        
                      
                        if(dataObj.status==1101){
                            submitsuccess("所有策略市值超过最大额度150万");
                            return;
                                            
                        }
                      
                        if(dataObj.status==1200){
                            
                            submitsuccess("非交易时间禁止创建");
                            return;
                                            
                        }
                                    
                      
                    }
                })                                        
               
              
              

                
            }



        </script> 

    </body>
</html>