<?php if (!defined('THINK_PATH')) exit(); /*a:3:{s:62:"D:\xampp\htdocs\peiqi/application/index\view\price\quotes.html";i:1551857901;s:66:"D:\xampp\htdocs\peiqi\application\index\view\price\inc\header.html";i:1551860770;s:66:"D:\xampp\htdocs\peiqi\application\index\view\index\inc\footer.html";i:1552012658;}*/ ?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta http-equiv="content-language" content="zh-CN" />
        <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no" />
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black" />
        <meta name="format-detection" content="telephone=no" />
        <meta name="keywords" content="" />
        <meta name="description" content="" />
        <meta name="author" content="令克网络-高端网站建设-https://www.link-web.cn/" />
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
        <title>赢在策略</title>
        <link href="/public/static/img/favicon.ico" rel="shortcut icon">
        <link rel="stylesheet" href="/public/static/css/reset.css">
        <link rel="stylesheet" href="/public/static/css/style.css">
        <link rel="stylesheet" href="/public/static/css/price.css">
        <script src="/public/static/js/jquery-1.11.3.js"></script>
        <script src="/public/static/js/html5.js"></script>
        <script src="/public/static/js/adaptive-version2.js"></script>
        <script src="/public/static/js/jquery.easing.1.3.js"></script>
        <script src="/public/static/js/jquery.transit.js"></script>
        <script src="/public/static/js/jquery.lazyload.js"></script>
    </head>
    <body>
        <div class="includeDom"  data-id="1">
            <header class="price-header">
    <ul class="f-cb">
        <li><a href="/index/price/index.html">自选</a></li>
        <li><a href="/index/price/quotes.html">行情</a></li>
    </ul>
    <div class="price-search" onclick="window.location.href='/index/index/optional_search.html'"></div>
</header>
<script>
    var id = $('header').parent(".includeDom").data('id');
    $('header li').eq(id).addClass('hover');
</script>

        </div>
        <div class="welcome-list">
            <ul class="f-cb">
                <li>
                    <h2>上证指数</h2>
                    <h3><span class="ico red" id="shang_nowPoint">2747.2285</span></h3>
                    <div class="f-cb">
                        <span class="fl num red" id="shang_nowPrice">49.164</span>
                        <span class="fr num red" id="shang_diff_rate">0.5%</span>
                    </div>
                </li>
                <li>
                    <h2>深证指数</h2>
                    <h3><span class="ico red" id="shen_nowPoint">2747.2285</span></h3>
                    <div class="f-cb">
                        <span class="fl num red" id="shen_nowPrice">49.164</span>
                        <span class="fr num red" id="shen_diff_rate">0.5%</span>
                    </div>
                </li>
                <li>
                    <h2>创业版</h2>
                    <h3><span class="ico red" id="chuang_nowPoint">2747.2285</span></h3>
                    <div class="f-cb">
                        <span class="fl num red" id="chuang_nowPrice">49.164</span>
                        <span class="fr num red" id="chuang_diff_rate">0.5%</span>
                    </div>
                </li>
            </ul>
        </div>
        <div class="quotes-swiper">
            <h2 class="title">热门股票</h2>
            <!-- Swiper -->
            <div class="swiper-container w94">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <h2>新股开板</h2>
                        <span>2.52%</span>
                        <p>立华股份<i>0.49%</i></p>
                    </div>
                    <div class="swiper-slide">
                        <h2>新股开板</h2>
                        <span>2.52%</span>
                        <p>立华股份<i>0.49%</i></p>
                    </div>
                    <div class="swiper-slide">
                        <h2>新股开板</h2>
                        <span>2.52%</span>
                        <p>立华股份<i>0.49%</i></p>
                    </div>
                    <div class="swiper-slide">
                        <h2>新股开板</h2>
                        <span class="green">2.52%</span>
                        <p>立华股份<i class="green">0.49%</i></p>
                    </div>
                </div>
                <!-- Add Arrows -->
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
            </div>
        </div>
        <ul class="quotes-list">
            <li>
                <h2 class="title">涨幅榜</h2>
                <dl>
                    <dd>
                        <a href="/index/price/info.html?stock_code=sz000633&stock_name=中科新材" class="f-cb">
                            <div class="qulist-left">
                                <h2>美的集团</h2>
                                <p>SZ000333</p>
                            </div>
                            <p class="green">0.5%</p>
                            <p class="green">5.51</p>
                        </a>
                    </dd>
                    <dd>
                        <a href="/index/price/info.html?stock_code=sz000633&stock_name=中科新材" class="f-cb">
                            <div class="qulist-left">
                                <h2>美的集团</h2>
                                <p>SZ000333</p>
                            </div>
                            <p>0.5%</p>
                            <p>5.51</p>
                        </a>
                    </dd>
                </dl>
            </li>
            <li>
                <h2 class="title">跌幅榜</h2>
                <dl>
                    <dd>
                        <a href="/index/price/info.html?stock_code=sz000633&stock_name=中科新材" class="f-cb">
                            <div class="qulist-left">
                                <h2>美的集团</h2>
                                <p>SZ000333</p>
                            </div>
                            <p>0.5%</p>
                            <p>5.51</p>
                        </a>
                    </dd>
                    <dd>
                        <a href="/index/price/info.html?stock_code=sz000633&stock_name=中科新材" class="f-cb">
                            <div class="qulist-left">
                                <h2>美的集团</h2>
                                <p>SZ000333</p>
                            </div>
                            <p>0.5%</p>
                            <p>5.51</p>
                        </a>
                    </dd>
                </dl>
            </li>
            <li>
                <h2 class="title">快速涨幅</h2>
                <dl>
                    <dd>
                        <a href="/index/price/info.html?stock_code=sz000633&stock_name=中科新材" class="f-cb">
                            <div class="qulist-left">
                                <h2>美的集团</h2>
                                <p>SZ000333</p>
                            </div>
                            <p>0.5%</p>
                            <p>5.51</p>
                        </a>
                    </dd>
                    <dd>
                        <a href="/index/price/info.html?stock_code=sz000633&stock_name=中科新材" class="f-cb">
                            <div class="qulist-left">
                                <h2>美的集团</h2>
                                <p>SZ000333</p>
                            </div>
                            <p>0.5%</p>
                            <p>5.51</p>
                        </a>
                    </dd>
                </dl>
            </li>
        </ul>
        <div class="includeDom"  data-id="1">
            <footer>
    <ul class="f-cb">
        <!-- <li>
            <a href="/index/index/index.html">
                <div class="pic-box">
                    <img src="/public/static/img/footer01.png" alt="" class="bg">
                    <img src="/public/static/img/footer01_h.png" alt="" class="pic">
                </div>
                <h2>首页</h2>
            </a>
        </li>
        <li>
            <a href="/index/strategy/strategy.html">
                <div class="pic-box">
                    <img src="/public/static/img/footer02.png" alt="" class="bg">
                    <img src="/public/static/img/footer02_h.png" alt="" class="pic">
                </div>
                <h2>策略</h2>
            </a>
        </li>
        <li>
            <a href="/index/ranking/rankingList.html">
                <div class="pic-box">
                    <img src="/public/static/img/footer03.png" alt="" class="bg">
                    <img src="/public/static/img/footer03_h.png" alt="" class="pic">
                </div>
                <h2>排行</h2>
            </a>
        </li>
        <li>
            <a href="/index/center/personal.html">
                <div class="pic-box">
                    <img src="/public/static/img/footer04.png" alt="" class="bg">
                    <img src="/public/static/img/footer04_h.png" alt="" class="pic">
                </div>
                <h2>我的</h2>
            </a>
        </li> -->
        <li>
            <a href="/index/index/index.html">
                <div class="pic-box">
                    <img src="/public/static/img/img_7.png" alt="" class="bg">
                    <img src="/public/static/img/img_8.png" alt="" class="pic">
                </div>
                <h2>首页</h2>
            </a>
        </li>
        <li>
            <a href="/index/price/index.html">
                <div class="pic-box">
                    <img src="/public/static/img/img_9.png" alt="" class="bg">
                    <img src="/public/static/img/img_10.png" alt="" class="pic">
                </div>
                <h2>行情</h2>
            </a>
        </li>
        <li>
            <a href="/index/trade/index.html">
                <div class="pic-box">
                    <img src="/public/static/img/img_11.png" alt="" class="bg">
                    <img src="/public/static/img/img_12.png" alt="" class="pic">
                </div>
                <h2>交易</h2>
            </a>
        </li>
        <li>
            <a href="/index/news/index.html">
                <div class="pic-box">
                    <img src="/public/static/img/img_13.png" alt="" class="bg">
                    <img src="/public/static/img/img_14.png" alt="" class="pic">
                </div>
                <h2>社区</h2>
            </a>
        </li>
        <li>
            <a href="/index/center/personal.html">
                <div class="pic-box">
                    <img src="/public/static/img/img_15.png" alt="" class="bg">
                    <img src="/public/static/img/img_16.png" alt="" class="pic">
                </div>
                <h2>我的</h2>
            </a>
        </li>
    </ul>
    <div class="loading">
        <img src="/public/static/img/logo.png" alt="" class="logo">
    </div>
</footer>
<script>
    jQuery(document).ready(function() {
        $('.loading').addClass("active");
    })
    var id = $('footer').parent(".includeDom").data('id');
    $('footer li').eq(id).addClass('hover');
</script>
        </div>
        <script src="/public/static/js/main.js"></script>
        <link rel="stylesheet" type="text/css" href="/public/static/swiper/swiper.css">
        <script src="/public/static/swiper/swiper.js"></script>
        <script type="text/javascript">
            reflash();
            //首页大盘股指实时行情_批量  5秒刷新一次
            //执行ajax
            function reflash(){                
                $.ajax({
                    url: "/api/index/grailindex",
                    dataType: "json",
                    async:true,
                    success: function relash(data) {
                        // alert('ajax调用成功');
                        var dataObj = JSON.parse(data);
                        if(dataObj.status==1){
                            var res = dataObj.list.showapi_res_body;
                            //上证指数
                            var shang_nowPoint = (res.indexList[0].nowPoint);
                            var shang_nowPrice = (parseFloat(res.indexList[0].nowPrice)).toFixed(2);
                            var shang_diff_rate = (parseFloat(res.indexList[0].diff_rate)).toFixed(2);
                            if(shang_diff_rate<0){
                                $("#shang_nowPoint").html(shang_nowPoint);
                                $("#shang_nowPoint").removeClass("red");
                                $("#shang_nowPoint").addClass("green");

                                $("#shang_nowPrice").html(shang_nowPrice);
                                $("#shang_nowPrice").removeClass("red");
                                $("#shang_nowPrice").addClass("green");

                                $("#shang_diff_rate").html(shang_diff_rate+"%");
                                $("#shang_diff_rate").removeClass("red");
                                $("#shang_diff_rate").addClass("green");

                            }else{
                                $("#shang_nowPoint").html(shang_nowPoint);
                                $("#shang_nowPrice").html(shang_nowPrice);
                                $("#shang_diff_rate").html(shang_diff_rate+"%");
                            }
                            

                            //深圳指数
                            var shen_nowPoint = res.indexList[1].nowPoint;
                            var shen_nowPrice =(parseFloat(res.indexList[1].nowPrice)).toFixed(2);
                            var shen_diff_rate = (parseFloat(res.indexList[1].diff_rate)).toFixed(2);

                            if(shen_diff_rate<0){
                                $("#shen_nowPoint").html(shen_nowPoint);
                                $("#shen_nowPoint").removeClass("red");
                                $("#shen_nowPoint").addClass("green");

                                $("#shen_nowPrice").html(shen_nowPrice);
                                $("#shen_nowPrice").removeClass("red");
                                $("#shen_nowPrice").addClass("green");
                                
                                $("#shen_diff_rate").html(shang_diff_rate+"%");
                                $("#shen_diff_rate").removeClass("red");
                                $("#shen_diff_rate").addClass("green");

                            }else{
                                $("#shen_nowPoint").html(shen_nowPoint);
                                $("#shen_nowPrice").html(shen_nowPrice);
                                $("#shen_diff_rate").html(shen_diff_rate+"%");
                            }

                            //创业板
                            var chuang_nowPoint = res.indexList[2].nowPoint;
                            var chuang_nowPrice = (parseFloat(res.indexList[2].nowPrice)).toFixed(2);
                            var chuang_diff_rate = (parseFloat(res.indexList[2].diff_rate)).toFixed(2);
                            if(chuang_diff_rate<0){
                                $("#chuang_nowPoint").html(chuang_nowPoint);
                                $("#chuang_nowPoint").removeClass("red");
                                $("#chuang_nowPoint").addClass("green");

                                $("#chuang_nowPrice").html(chuang_nowPrice);
                                $("#chuang_nowPrice").removeClass("red");
                                $("#chuang_nowPrice").addClass("green");
                                
                                $("#chuang_diff_rate").html(chuang_diff_rate+"%");
                                $("#chuang_diff_rate").removeClass("red");
                                $("#chuang_diff_rate").addClass("green");

                            }else{
                                $("#chuang_nowPoint").html(chuang_nowPoint);
                                $("#chuang_nowPrice").html(chuang_nowPrice);
                                $("#chuang_diff_rate").html(chuang_diff_rate+"%");
                            }
                        }else{
                            alert("获取数据失败");
                        }
                    }
                })
                    
                /* 等待五秒后再次请求数据 */
                window.setTimeout("reflash()", 5000);
            }

        </script>
        <script>
            var swiper = new Swiper('.swiper-container', {
                slidesPerView: 3,
                navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
                },
            });
            $(function(){
                $(".quotes-list li").eq(0).addClass("on").find("dl").slideDown(200)
                $(".quotes-list li").click(function(){
                    if (!$(this).hasClass("on")) {
                        $(this).addClass("on").siblings("li").removeClass("on")
                        $(this).find("dl").slideDown(200).parents("li").siblings("li").find("dl").slideUp(200)
                    }
                })
            })
        </script>
    </body>
</html>
