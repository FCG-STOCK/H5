<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:67:"/home/stock/h5/application/index/view/center/personal_recharge.html";i:1557999148;s:59:"/home/stock/h5/application/index/view/index/inc/footer.html";i:1554779792;}*/ ?>
<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="content-language" content="zh-CN" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />
    <meta name="format-detection" content="telephone=no" />
    <meta name="keywords" content="" />
    <meta name="description" content="" />
      
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
    <title>充值</title>
    <link href="/public/static/img/favicon.ico" rel="shortcut icon">
    <link rel="stylesheet" href="/public/static/css/reset.css">
    <link rel="stylesheet" href="/public/static/css/style.css">
    <link rel="stylesheet" href="/public/static/awesome/css/font-awesome.css">
    <script src="/public/static/js/jquery-1.11.3.js"></script>
    <script src="/public/static/js/html5.js"></script>
    <script src="/public/static/js/adaptive-version2.js"></script>
    <script src="/public/static/js/jquery.easing.1.3.js"></script>
    <script src="/public/static/js/jquery.transit.js"></script>
    <script src="/public/static/js/jquery.lazyload.js"></script>
  <style type="text/css">
          .personal-rechangelist{
            width:100%;
            background:#fff;
            padding:0 3%;
          }
          .personal-rechangelist li{
            width:100%;
            padding-right: 2%;
            display:block;
            border-top:1px solid #eee;
            background:url('jt.png') no-repeat right center;
            background-size:0.11rem auto;
          }
          .personal-rechangelist li a{
            display:block;
            font-size:0.28rem;
            color:#333;
            line-height:0.88rem;
          }
    .personal-rechangelist li:first-child{border-top:none}
        </style>
</head>
<body>
    <!-- <div class="includeDom" include="inc/header.html" data-intro="index"></div> -->
    <div class="content-box data">
        <?php if($res['is_true'] != 1): ?>
        <div class="guide-top">
            <a href="javascript:history.back(-1);" class="back"><i class="fa fa-angle-left"></i></a>
            <span class="data-fonts">未实名认证，请先实名认证</span>
        </div>
        <div>
            <a href="/index/center/personal_attr.html" class="changeUrl">实名认证</a>
            <?php elseif($have_card == 2): ?>
            <div class="guide-top">
                <a href="javascript:history.back(-1);" class="back"><i class="fa fa-angle-left"></i></a>
                <span class="data-fonts">未绑定银行卡，请先绑定</span>
            </div>
            <div>
                <a href="/index/center/personal_addcard.html" class="changeUrl">绑定银行卡</a>

                <?php else: ?>
                <div class=" guide-top ">
                     <a href="javascript:history.back(-1);" class="back"><i class="fa fa-angle-left"></i></a>
                    <span class="data-fonts">充值</span>
                </div>
                <!--<div class="personal-cash" style="padding-top: 0.2rem;">
                    <form action="javascript:;" id="form_papadding-top: 0.2rem;ypsw">
                        <label class="ico2"><input type="text" name="" placeholder="请输入充值金额" id="money"></label>
                        <input type="submit" name="" value="确认" id="recharge">
                    </form>
                </div>-->
                    
              <ul class="f-cb personal-rechangelist">
                <li><a href="personal_rechargeapply.html?type=1">支付宝充值</a></li>
                <li><a href="personal_rechargeapply.html?type=2">微信充值</a></li>
                <li><a href="personal_rechargecard.html">银行卡充值</a></li>
              </ul>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <div class="submit-success" style="display: none;"></div>
    <div class="includeDom" data-id="4">
        <footer>
    <ul class="f-cb">
        <!-- <li>
            <a href="/index/index/index.html">
                <div class="pic-box">
                    <img src="/public/static/img/footer01.png" alt="" class="bg">
                    <img src="/public/static/img/footer01_h.png" alt="" class="pic">
                </div>
                <h2>首页</h2>
            </a>
        </li>
        <li>
            <a href="/index/strategy/strategy.html">
                <div class="pic-box">
                    <img src="/public/static/img/footer02.png" alt="" class="bg">
                    <img src="/public/static/img/footer02_h.png" alt="" class="pic">
                </div>
                <h2>策略</h2>
            </a>
        </li>
        <li>
            <a href="/index/ranking/rankingList.html">
                <div class="pic-box">
                    <img src="/public/static/img/footer03.png" alt="" class="bg">
                    <img src="/public/static/img/footer03_h.png" alt="" class="pic">
                </div>
                <h2>排行</h2>
            </a>
        </li>
        <li>
            <a href="/index/center/personal.html">
                <div class="pic-box">
                    <img src="/public/static/img/footer04.png" alt="" class="bg">
                    <img src="/public/static/img/footer04_h.png" alt="" class="pic">
                </div>
                <h2>我的</h2>
            </a>
        </li> -->
        <li>
            <a href="/index/index/index.html">
                <div class="pic-box">
                    <img src="/public/static/img/img_7.png" alt="" class="bg">
                    <img src="/public/static/img/img_8.png" alt="" class="pic">
                </div>
                <h2>首页</h2>
            </a>
        </li>
        <li>
            <a href="/index/price/quotes.html">
                <div class="pic-box">
                    <img src="/public/static/img/img_9.png" alt="" class="bg">
                    <img src="/public/static/img/img_10.png" alt="" class="pic">
                </div>
                <h2>行情</h2>
            </a>
        </li>
        <li>
            <a href="/index/trade/index.html">
                <div class="pic-box">
                    <img src="/public/static/img/img_11.png" alt="" class="bg">
                    <img src="/public/static/img/img_12.png" alt="" class="pic">
                </div>
                <h2>交易</h2>
            </a>
        </li>
        <li>
            <a href="/index/news/index.html">
                <div class="pic-box">
                    <img src="/public/static/img/img_13.png" alt="" class="bg">
                    <img src="/public/static/img/img_14.png" alt="" class="pic">
                </div>
                <h2>资讯</h2>
            </a>
        </li>
        <li>
            <a href="/index/center/personal.html">
                <div class="pic-box">
                    <img src="/public/static/img/img_15.png" alt="" class="bg">
                    <img src="/public/static/img/img_16.png" alt="" class="pic">
                </div>
                <h2>我的</h2>
            </a>
        </li>
    </ul>
    <div class="loading">
            <div class="loadings">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div> 
        <!-- <img src="/public/static/img/logo.png" alt="" class="logo"> -->
    </div>
</footer>
<script>
    var id = $('footer').parent(".includeDom").data('id');
    $('footer li').eq(id).addClass('hover');
</script>
    </div>
    <script src="/public/static/js/main.js"></script>
    <script src="/public/static/js/jquery.cookie.js"></script>
    <script>
        $("#recharge").click(function () {
            var money = $("#money").val();
            if (money == '') {
                submitsuccess('请输入充值金额');
                return;
            }
            if (money == 1) {
                submitsuccess('请输入其他充值金额');
                return;
            }
            $(function () {
                $.ajax({
                    url: "<?php echo $apiurl; ?>/index/pay/chargeMoney",
                    data: {
                        money: money,
                        account: $.cookie('username')
                    },
                    dataType: "html",
                    method: "get",
                    success: function (data) {
                        $("body").html(data);
                    }
                })
            })


        })
    </script>
</body>
</html>