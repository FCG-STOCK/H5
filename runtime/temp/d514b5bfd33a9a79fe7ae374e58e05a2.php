<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:75:"/www/wwwroot/celue2.0.h5.solingke.cn/application/index/view/news/index.html";i:1557999094;s:81:"/www/wwwroot/celue2.0.h5.solingke.cn/application/index/view/index/inc/footer.html";i:1554779792;}*/ ?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta http-equiv="content-language" content="zh-CN" />
        <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no" />
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black" />
        <meta name="format-detection" content="telephone=no" />
        <meta name="keywords" content="" />
        <meta name="description" content="" />
         
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
        <title>资讯</title>
        <link href="/public/static/img/favicon.ico" rel="shortcut icon">
        <link rel="stylesheet" href="/public/static/css/reset.css">
        <link rel="stylesheet" href="/public/static/css/style.css">
        <link rel="stylesheet" href="/public/static/css/news.css">
        <link rel="stylesheet" href="/public/static/awesome/css/font-awesome.css">
        <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
        <script src="/public/static/js/vue.js"></script>
        <script src="/public/static/js/jquery-1.11.3.js"></script>
        <script src="/public/static/js/html5.js"></script>
        <script src="/public/static/js/adaptive-version2.js"></script>
        <script src="/public/static/js/jquery.easing.1.3.js"></script>
        <script src="/public/static/js/jquery.transit.js"></script>
        <script src="/public/static/js/jquery.lazyload.js"></script>
    </head>
    <body>
        <div id="app">
            <div class="guide-top m0">
                
                <!-- <a href="javascript:history.back(-1);"></a> -->
                资讯
            </div>
            <div class="swiper-container rank-top">
                <ul class=" swiper-wrapper">
                    <li class="swiper-slide" v-for="(item,index) in rank" @click="rankChange(item,$event)" :class="index==0?'on':''" v-cloak>{{item.name}}</li>
                </ul>
            </div>
          <div>
          <div class="zanwu" v-if="list.length==0"></div>
            <div v-else>
            <ul class="news-list">
                <li v-for="(item,index) in list" v-cloak>
                    <a :href="'/index/index/news_info.html?id='+item.id" class="f-cb">
                        <div class="news_list_left">
                            <img :src="item.picurl" alt="">
                        </div>
                        <div class="news_list_right">
                            <h2 v-cloak>{{item.title}}</h2>
                            <p v-cloak><span>最新</span>{{item.create_time}}<i>{{item.times}}</i></p>
                        </div>
                    </a>
                </li>
            </ul>
            </div>
          </div>
            
        </div>
        <div class="includeDom"  data-id="3">
            <footer>
    <ul class="f-cb">
        <!-- <li>
            <a href="/index/index/index.html">
                <div class="pic-box">
                    <img src="/public/static/img/footer01.png" alt="" class="bg">
                    <img src="/public/static/img/footer01_h.png" alt="" class="pic">
                </div>
                <h2>首页</h2>
            </a>
        </li>
        <li>
            <a href="/index/strategy/strategy.html">
                <div class="pic-box">
                    <img src="/public/static/img/footer02.png" alt="" class="bg">
                    <img src="/public/static/img/footer02_h.png" alt="" class="pic">
                </div>
                <h2>策略</h2>
            </a>
        </li>
        <li>
            <a href="/index/ranking/rankingList.html">
                <div class="pic-box">
                    <img src="/public/static/img/footer03.png" alt="" class="bg">
                    <img src="/public/static/img/footer03_h.png" alt="" class="pic">
                </div>
                <h2>排行</h2>
            </a>
        </li>
        <li>
            <a href="/index/center/personal.html">
                <div class="pic-box">
                    <img src="/public/static/img/footer04.png" alt="" class="bg">
                    <img src="/public/static/img/footer04_h.png" alt="" class="pic">
                </div>
                <h2>我的</h2>
            </a>
        </li> -->
        <li>
            <a href="/index/index/index.html">
                <div class="pic-box">
                    <img src="/public/static/img/img_7.png" alt="" class="bg">
                    <img src="/public/static/img/img_8.png" alt="" class="pic">
                </div>
                <h2>首页</h2>
            </a>
        </li>
        <li>
            <a href="/index/price/quotes.html">
                <div class="pic-box">
                    <img src="/public/static/img/img_9.png" alt="" class="bg">
                    <img src="/public/static/img/img_10.png" alt="" class="pic">
                </div>
                <h2>行情</h2>
            </a>
        </li>
        <li>
            <a href="/index/trade/index.html">
                <div class="pic-box">
                    <img src="/public/static/img/img_11.png" alt="" class="bg">
                    <img src="/public/static/img/img_12.png" alt="" class="pic">
                </div>
                <h2>交易</h2>
            </a>
        </li>
        <li>
            <a href="/index/news/index.html">
                <div class="pic-box">
                    <img src="/public/static/img/img_13.png" alt="" class="bg">
                    <img src="/public/static/img/img_14.png" alt="" class="pic">
                </div>
                <h2>资讯</h2>
            </a>
        </li>
        <li>
            <a href="/index/center/personal.html">
                <div class="pic-box">
                    <img src="/public/static/img/img_15.png" alt="" class="bg">
                    <img src="/public/static/img/img_16.png" alt="" class="pic">
                </div>
                <h2>我的</h2>
            </a>
        </li>
    </ul>
    <div class="loading">
            <div class="loadings">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div> 
        <!-- <img src="/public/static/img/logo.png" alt="" class="logo"> -->
    </div>
</footer>
<script>
    var id = $('footer').parent(".includeDom").data('id');
    $('footer li').eq(id).addClass('hover');
</script>
        </div>
        <script src="/public/static/js/main.js"></script>
        <script src="/public/static/swiper/swiper.js"></script>
        <link rel="stylesheet" type="text/css" href="/public/static/swiper/swiper.css">
        <script>

            jQuery(document).ready(function() {
                $('.loading').addClass("active");
            })
        </script>
        <script>
            $(function(){
                var swiper = new Swiper('.swiper-container', {
                  slidesPerView: 6,
                  pagination: {
                      clickable: true,
                  },
                  });
            })
        var vm=new Vue({
            el:'#app',
            data() {
                return {
                    num:'3',
                    rank:[
                        {
                            name:"头条",
                            cid:"1"
                        },
                        {
                            name:"股票",
                            cid:"2"
                        },
                        {
                            name:"理财",
                            cid:"3"
                        },
                        {
                            name:"基金",
                            cid:"4"
                        },
                        {
                            name:"私募",
                            cid:"5"
                        },
                        {
                            name:"研报",
                            cid:"7"
                        }
                    ],
                    list:[
                        {
                            img:'/public/static/img/img_43.png',
                            name:"投资需要耐心",
                            time:"2018-07-04",
                            num:"2185"
                        },
                        {
                            img:'/public/static/img/img_43.png',
                            name:"策略的分析维度与关键要素策略的分析维度与关键要素策略的分析维度与关键要素策略的分析维度与关键要素",
                            time:"2018-07-04",
                            num:"2185"
                        },
                        {
                            img:'/public/static/img/img_43.png',
                            name:"投资需要耐心",
                            time:"2018-07-04",
                            num:"2185"
                        },
                        {
                            img:'/public/static/img/img_43.png',
                            name:"策略的分析维度与关键要素策略的分析维度与关键要素",
                            time:"2018-07-04",
                            num:"2185"
                        },
                        {
                            img:'/public/static/img/img_43.png',
                            name:"策略的分析维度与关键要素策略的分析维度与关键要素",
                            time:"2018-07-04",
                            num:"2185"
                        },
                        {
                            img:'/public/static/img/img_43.png',
                            name:"策略的分析维度与关键要素策略的分析维度与关键要素",
                            time:"2018-07-04",
                            num:"2185"
                        },
                        
                    ]
                };
            },
            mounted() {
                this.newsCateList(1)
            },
            methods: {
                rankChange(item,e){
                    if (!$(e.target).hasClass("on")) {
                        $(e.target).addClass("on").siblings("li").removeClass("on");
                        this.newsCateList(item.cid)
                    }
                },
                newsCateList(obj){
                    let _this = this;
                    axios.post('/api/mobile/newsCateList',{cid:obj})
                    .then(function (res) {
                        if(res.data.status =='1'){
                            _this.list = res.data.list
                        }
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
                }

            }
        })
        var flagPage=true;
        var NewUrl = 'ajax/honor_ajax'; 
        var p = 1; //记录第几页
        var allpage = vm.num;
        console.log(allpage)
        $(window).scroll( function(event){
        //当内容滚动到底部时加载新的内容 100当距离最底部100个像素时开始加载. 
        if ($(this).scrollTop() + $(window).height() + 10 >= $(document).height() && $(this).scrollTop() > 10 && flagPage==true) {
            var _this = $(this);
            console.log(allpage)
            flagPage=false;
                // $.ajax({
                // url: NewUrl,
                // data: { page: p, reg: allpage },
                // cache: false,
                // dataType: 'html',
                // beforeSend: function(){
                //     $(".morebox").html("加载更多...");
                // },
                // success: function (html) {
                //     $('.listb .sec-list .ulbox ul').append(html);
                //     p += 1; //下一页
                //     console.log(p);
                //     if (p > allpage) {
                //         $('.morebox').hide();
                //         flagPage=false;
                //     }else{
                //         setTimeout(function(){
                //                flagPage=true;
                //             },500)
                //         } 
                //     }
                // });
            return false;
          }
        });
        </script>
    </body>
</html>