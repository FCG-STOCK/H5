<?php if (!defined('THINK_PATH')) exit(); /*a:1:{s:64:"/home/wwwroot/m/application/index/view/center/personal_cash.html";i:1550147872;}*/ ?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta http-equiv="content-language" content="zh-CN" />
        <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no" />
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black" />
        <meta name="format-detection" content="telephone=no" />
        <meta name="keywords" content="" />
        <meta name="description" content="" />
        <meta name="author" content="令克网络-高端网站建设-https://www.link-web.cn/" />
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
        <title>赢在策略</title>
        <link href="/public/static/img/favicon.ico" rel="shortcut icon">
        <link rel="stylesheet" href="/public/static/css/reset.css">
        <link rel="stylesheet" href="/public/static/css/style.css">
        <script src="/public/static/js/jquery-1.11.3.js"></script>
        <script src="/public/static/js/html5.js"></script>
        <script src="/public/static/js/adaptive-version2.js"></script>
        <script src="/public/static/js/jquery.easing.1.3.js"></script>
        <script src="/public/static/js/jquery.transit.js"></script>
        <script src="/public/static/js/jquery.lazyload.js"></script>
    </head>
    <body>
        <div class="includeDom" include="inc/header.html" data-intro="index"></div>
        <div class="content-box data">
            <div class="personal-title">提现</div>
          	<?php if($arr == 1): ?>
            <div class="personal-cash">
                <form action="javascript:;" id="form_paypsw">
                    <label class="ico1 marb"><?php echo $bankcart['bank_num']; ?></label>
                    <label class="ico2"><input type="text" name="" placeholder="请输入提现金额" id="money"></label>
                    <label class="ico4"><input type="password" name="" placeholder="请输入提现密码" id="payPassword_rsainput" maxlength="6" minlength="6" ></label>
                    <input type="submit" name="" onclick="cash()" value="确认提现">
                    <!-- <div id="payPassword_container" class="alieditContainer clearfix" data-busy="0">
                        <h2>输入交易密码</h2>
                        <div class="six-password">
                            <input class="i-text sixDigitPassword" id="payPassword_rsainput" type="password" autocomplete="off" required="required" value="" name="payPassword_rsainput" data-role="sixDigitPassword" tabindex="" maxlength="6" minlength="6" aria-required="true">
                            <div tabindex="0" class="sixDigitPassword-box">
                                <i><b></b></i>
                                <i><b></b></i>
                                <i><b></b></i>
                                <i><b></b></i>
                                <i><b></b></i>
                                <i><b></b></i>
                                <span id="cardwrap" data-role="cardwrap"></span>
                            </div>
                        </div>
                    </div> -->
                </form>
            </div>
          	<?php else: ?>
          	<div class="content-box data">
            	<span class="data-fonts">未绑定银行卡，请先绑定</span>
        	</div>
            <?php endif; ?>
        </div>
        <div class="includeDom" include="inc/footer.html" data-id="3"></div>
        <script src="/public/static/js/main.js"></script>
        <script src="/public/static/js/jquery-validate.js"></script>
        <script src="/public/static/js/validate_into.js"></script>
        <script>
            // $('.personal-cash input[type="submit"]').click(function() {
            //     $('#payPassword_container').addClass('hover');
            // })
        </script>
        <script type="text/javascript">
            //设置昵称
            function cash(){
                if($("#money").val().length==0){
                    alert('提现金额不能为空');
                    //submitsuccess("提现金额不能为空");
                    return;
                }
                if($("#payPassword_rsainput").val().length==0){
                    alert('提现密码不能为空');
                    //submitsuccess("提现密码不能为空");
                    return;
                }
                $(function(){
                    $.ajax({
                        url: "/API/index/extractMoney",
                        data: {
                            money: $("#money").val(),
                            trade_pwd: $("#payPassword_rsainput").val()
                        },
                        dataType: "json",
                        method:"post",
                        async:true,
                        success: function (data) {
                            // alert('ajax调用成功');
                            var dataObj = JSON.parse(data);
                            if(dataObj.status==1){

                                alert('提现申请成功');
                                setTimeout(function() { window.location.href='/index/center/personal_account'},1000);

                            }else{
                                alert(dataObj.msg);
                            }
                        }
                    })
                })
            }
        </script>
    </body>
</html>