<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:80:"/www/wwwroot/peiqi.solingke.cn/application/index/view/trade/trade_hold_info.html";i:1554274484;s:75:"/www/wwwroot/peiqi.solingke.cn/application/index/view/index/inc/footer.html";i:1554104025;}*/ ?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta http-equiv="content-language" content="zh-CN" />
        <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no" />
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black" />
        <meta name="format-detection" content="telephone=no" />
        <meta name="keywords" content="" />
        <meta name="description" content="" />
        <meta name="author" content="令克网络-高端网站建设-https://www.link-web.cn/" />
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
        <title>配齐策略</title>
        <link href="/public/static/img/favicon.ico" rel="shortcut icon">
        <link rel="stylesheet" href="/public/static/css/reset.css">
        <link rel="stylesheet" href="/public/static/css/style.css">
        <link rel="stylesheet" href="/public/static/css/trade.css">
        <link rel="stylesheet" href="/public/static/awesome/css/font-awesome.css">
        <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
        <script src="/public/static/js/vue.js"></script>
        <script src="/public/static/js/jquery-1.11.3.js"></script>
        <script src="/public/static/js/html5.js"></script>
        <script src="/public/static/js/adaptive-version2.js"></script>
        <script src="/public/static/js/jquery.easing.1.3.js"></script>
        <script src="/public/static/js/jquery.transit.js"></script>
        <script src="/public/static/js/jquery.lazyload.js"></script>
    </head>
    <body>
        <div class="content-box">
            <div class="guide-top ">
                <a href="javascript:history.back(-1);" class="back"><i class="fa fa-angle-left"></i></a>
                股票详情
            </div>
            <div class="trade-hold-info w94 f-cb" id="app">
                <h3>{{list.stock_name}}</h3>
                <p>({{list.stock_code}})</p>
                <h2 :class="((list.sell_price - list.buy_price)*list.stock_number).toFixed(0) < '0'?'green':'red'">{{((list.sell_price - list.buy_price)*list.stock_number).toFixed(0)}}</h2>
                <h4>买入:<span class="bb">{{list.buy_price}}</span>　卖出:<span class="bb">{{list.sell_price}}</span></h4>
                <span class="type process end"></span><!-- end表示交易完成process表示持仓中 -->
                <dl class="f-cb">
                    <dd>策略盈亏<span class="fonts fr " :class="((list.sell_price - list.buy_price)*list.stock_number).toFixed(0) < '0'?'green':'red'">{{((list.sell_price - list.buy_price)*list.stock_number).toFixed(0)}}</span></dd>
                    <dd>交易股数<span class="fonts fr">{{list.stock_number}}</span></dd>
                    <dd>买入价<span class="fonts fr">{{list.buy_price}}</span></dd>
                    <dd>卖出价<span class="fonts fr">{{list.sell_price}}</span></dd>
                    <dd>买入时间<span class="fonts fr">{{list.buy_time}}</span></dd>
                    <dd>卖出时间<span class="fonts fr">{{list.sell_time}}</span></dd>
                    <dd>卖出类型<span class="fonts fr">
                        <span v-if="list.sell_type=='1'">主动卖出</span>
                        <span v-else-if="list.sell_type=='2'">达到日期</span>
                        <span v-else-if="list.sell_type=='3'">触发止盈</span>
                        <span v-else>触发止损</span>
                    </span></dd>
                    <!-- <dd>卖出类型<span class="fonts fr">700</span></dd> -->
                </dl>
            </div>
        </div>
        <div class="includeDom"  data-id="2">
                <footer>
    <ul class="f-cb">
        <!-- <li>
            <a href="/index/index/index.html">
                <div class="pic-box">
                    <img src="/public/static/img/footer01.png" alt="" class="bg">
                    <img src="/public/static/img/footer01_h.png" alt="" class="pic">
                </div>
                <h2>首页</h2>
            </a>
        </li>
        <li>
            <a href="/index/strategy/strategy.html">
                <div class="pic-box">
                    <img src="/public/static/img/footer02.png" alt="" class="bg">
                    <img src="/public/static/img/footer02_h.png" alt="" class="pic">
                </div>
                <h2>策略</h2>
            </a>
        </li>
        <li>
            <a href="/index/ranking/rankingList.html">
                <div class="pic-box">
                    <img src="/public/static/img/footer03.png" alt="" class="bg">
                    <img src="/public/static/img/footer03_h.png" alt="" class="pic">
                </div>
                <h2>排行</h2>
            </a>
        </li>
        <li>
            <a href="/index/center/personal.html">
                <div class="pic-box">
                    <img src="/public/static/img/footer04.png" alt="" class="bg">
                    <img src="/public/static/img/footer04_h.png" alt="" class="pic">
                </div>
                <h2>我的</h2>
            </a>
        </li> -->
        <li>
            <a href="/index/index/index.html">
                <div class="pic-box">
                    <img src="/public/static/img/img_7.png" alt="" class="bg">
                    <img src="/public/static/img/img_8.png" alt="" class="pic">
                </div>
                <h2>首页</h2>
            </a>
        </li>
        <li>
            <a href="/index/price/index.html">
                <div class="pic-box">
                    <img src="/public/static/img/img_9.png" alt="" class="bg">
                    <img src="/public/static/img/img_10.png" alt="" class="pic">
                </div>
                <h2>行情</h2>
            </a>
        </li>
        <li>
            <a href="/index/trade/index.html">
                <div class="pic-box">
                    <img src="/public/static/img/img_11.png" alt="" class="bg">
                    <img src="/public/static/img/img_12.png" alt="" class="pic">
                </div>
                <h2>交易</h2>
            </a>
        </li>
        <li>
            <a href="/index/news/index.html">
                <div class="pic-box">
                    <img src="/public/static/img/img_13.png" alt="" class="bg">
                    <img src="/public/static/img/img_14.png" alt="" class="pic">
                </div>
                <h2>资讯</h2>
            </a>
        </li>
        <li>
            <a href="/index/center/personal.html">
                <div class="pic-box">
                    <img src="/public/static/img/img_15.png" alt="" class="bg">
                    <img src="/public/static/img/img_16.png" alt="" class="pic">
                </div>
                <h2>我的</h2>
            </a>
        </li>
    </ul>
    <div class="loading">
            <div class="loadings">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div> 
        <!-- <img src="/public/static/img/logo.png" alt="" class="logo"> -->
    </div>
</footer>
<script>
    var id = $('footer').parent(".includeDom").data('id');
    $('footer li').eq(id).addClass('hover');
</script>
            </div>
        <script src="/public/static/js/main.js"></script>
        <script>
            var  ids = getSearchString("id");
            var vm=new Vue({
                el:'#app',
                
                data() {
                    return {
                        list:{}
                    };
                },
                
                mounted() {
                    this.checkStrategyHistory()
                },
                watch: {
                },
                methods: {
                    // 历史策略
                    checkStrategyHistory(){
                        let _this = this;
                        let obj = {account:account.account,id:ids}
                        axios.post('/api/mobile/checkStrategyHistoryContestdetails',obj)
                        .then(function (res) {
                            if(res.data.status =='1'){
                                _this.list = res.data.list
                            }
                        })
                        .catch(function (error) {
                            console.log(error);
                        });
                    },
                },
                components: {
                },
            })
        </script>
    </body>
</html>