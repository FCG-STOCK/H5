<?php if (!defined('THINK_PATH')) exit(); /*a:1:{s:74:"/www/wwwroot/peiqi.solingke.cn/application/index/view/center/use_info.html";i:1554270721;}*/ ?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta http-equiv="content-language" content="zh-CN" />
        <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no" />
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black" />
        <meta name="format-detection" content="telephone=no" />
        <meta name="keywords" content="" />
        <meta name="description" content="" />
        <meta name="author" content="令克网络-高端网站建设-https://www.link-web.cn/" />
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
        <title>配齐策略</title>
        <link href="/public/static/img/favicon.ico" rel="shortcut icon">
        <link rel="stylesheet" href="/public/static/css/reset.css">
        <link rel="stylesheet" href="/public/static/css/style.css">
        <link rel="stylesheet" href="/public/static/css/search.css">
        <link rel="stylesheet" href="/public/static/awesome/css/font-awesome.css">
        <style>body{background: #f3f5f7;}</style>
        <script src="/public/static/js/jquery-1.11.3.js"></script>
        <script src="/public/static/js/html5.js"></script>
        <script src="/public/static/js/adaptive-version2.js"></script>
        <script src="/public/static/js/jquery.easing.1.3.js"></script>
        <script src="/public/static/js/jquery.transit.js"></script>
        <script src="/public/static/js/jquery.lazyload.js"></script>
    </head>
    <body>
        <div class="guide-top">
            <a href="javascript:history.back(-1);" class="back"><i class="fa fa-angle-left"></i></a>
            使用规则
        </div>
        <div class="content-box">
            <div class="useinfo">
                <div class="useinfo-content">
                    <h2>体验券的使用说明</h2>
                    <p>平台所有跟投的计划均为保本信息，计划若出现穿仓，亏损到跟投人的本金，由平台跌幅给跟投本人本金及利息。</p>
                    <h2>操盘券的注意事项</h2>
                    <p>平台所有跟投的计划均为保本信息，计划若出现穿仓，亏损到跟投人的本金，由平台跌幅给跟投本人本金及利息。</p>
                    <h2>跟投券的注意事项</h2>
                    <p>平台所有跟投的计划均为保本信息，计划若出现穿仓，亏损到跟投人的本金，由平台跌幅给跟投本人本金及利息。
                        <br/><span class="usezhu">*活动的最终解释权归平台所有
                        </span>
                    </p>
                </div>
            </div>
        </div>
        <div class="includeDom footer" include="inc/footer.html" data-id="3"></div>
        <script src="/public/static/js/main.js"></script>
    </body>
</html>