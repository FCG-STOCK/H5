<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:76:"/wwwroot/h5.hdcelue.com/application/index/view/center/personal_recharge.html";i:1551104848;s:68:"/wwwroot/h5.hdcelue.com/application/index/view/index/inc/footer.html";i:1546598458;}*/ ?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta http-equiv="content-language" content="zh-CN" />
        <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no" />
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black" />
        <meta name="format-detection" content="telephone=no" />
        <meta name="keywords" content="" />
        <meta name="description" content="" />
        <meta name="author" content="令克网络-高端网站建设-https://www.link-web.cn/" />
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
        <title>恒达资讯H5</title>
        <link href="/public/static/img/favicon.ico" rel="shortcut icon">
        <link rel="stylesheet" href="/public/static/css/reset.css">
        <link rel="stylesheet" href="/public/static/css/style.css">
        <script src="/public/static/js/jquery-1.11.3.js"></script>
        <script src="/public/static/js/html5.js"></script>
        <script src="/public/static/js/adaptive-version2.js"></script>
        <script src="/public/static/js/jquery.easing.1.3.js"></script>
        <script src="/public/static/js/jquery.transit.js"></script>
        <script src="/public/static/js/jquery.lazyload.js"></script>
    </head>
    <body>
        <!-- <div class="includeDom" include="inc/header.html" data-intro="index"></div> -->
        <div class="content-box data">
            <div class="personal-title">充值</div>
          	<?php if($res['is_true'] == 1): ?>
            <div class="personal-cash">
               <form action="javascript:;" id="form_paypsw">
                    <label class="ico1 marb"></label>
                 	<label class="ico2"><input type="text" id="bank_num" name="" placeholder="请输入银行卡号" value="<?php echo $bank_num; ?>" <?php if($bank_num == ''): ?> disabled <?php endif; ?>></label>
                    <label class="ico2"><input type="text" id="money" name="" placeholder="请输入充值金额"></label>
                    <input type="submit" name="" onclick="chongzhi()" value="确认">
                </form>
              	<!-- <img src='/public/static/img/charge_ewm.jpg' style="width:80%;margin:20px auto 0;display:block">-->
            </div>
          	<?php else: ?>
          	<div class="content-box data">
            	<span class="data-fonts">未实名认证，请先实名认证</span>
        	<div>
          	<?php endif; ?>
          
        </div>
        <div class="includeDom"  data-id="3">
            <footer>
    <ul class="f-cb">
        <li>
            <a href="/index/index/index.html">
                <div class="pic-box">
                    <img src="/public/static/img/footer01.png" alt="" class="bg">
                    <img src="/public/static/img/footer01_h.png" alt="" class="pic">
                </div>
                <h2>首页</h2>
            </a>
        </li>
        <li>
            <a href="/index/strategy/strategy.html">
                <div class="pic-box">
                    <img src="/public/static/img/footer02.png" alt="" class="bg">
                    <img src="/public/static/img/footer02_h.png" alt="" class="pic">
                </div>
                <h2>策略</h2>
            </a>
        </li>
        <li>
            <a href="/index/ranking/rankingList.html">
                <div class="pic-box">
                    <img src="/public/static/img/footer03.png" alt="" class="bg">
                    <img src="/public/static/img/footer03_h.png" alt="" class="pic">
                </div>
                <h2>排行</h2>
            </a>
        </li>
        <li>
            <a href="/index/center/personal.html">
                <div class="pic-box">
                    <img src="/public/static/img/footer04.png" alt="" class="bg">
                    <img src="/public/static/img/footer04_h.png" alt="" class="pic">
                </div>
                <h2>我的</h2>
            </a>
        </li>
    </ul>
</footer>
<script>
    var id = $('footer').parent(".includeDom").data('id');
    $('footer li').eq(id).addClass('hover');
</script>
        </div>
       <script src="/public/static/js/main.js"></script>
        <script type="text/javascript">
            //充值
            function chongzhi(){
                if($("#bank_num").val().length==0){
                    submitsuccess("银行卡号不能为空");
                    return;
                }
                if($("#money").val().length==0){
                    submitsuccess("充值金额不能为空");
                    return;
                }
                $(function(){
                  	
                    $.ajax({
                      	
                        url: "/api/index/chargeMoney",
                        data: { 
                            bank_num: $("#bank_num").val(),
                            money: $("#money").val()
                        },
                        dataType: "html",
                        method:"post",
                        async:true,
                        success: function (data) {
                            

                                $('body').html(data);

                           
                        }
                    })
                })
            }
        </script>
    </body>
</html>