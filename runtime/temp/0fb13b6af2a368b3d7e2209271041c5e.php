<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:74:"/wwwroot/h5.hdcelue.com/application/index/view/ranking/ranking_record.html";i:1546679058;s:68:"/wwwroot/h5.hdcelue.com/application/index/view/index/inc/footer.html";i:1546598458;}*/ ?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta http-equiv="content-language" content="zh-CN" />
        <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no" />
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black" />
        <meta name="format-detection" content="telephone=no" />
        <meta name="keywords" content="" />
        <meta name="description" content="" />
        <meta name="author" content="令克网络-高端网站建设-https://www.link-web.cn/" />
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
        <title>恒达资讯H5</title>
        <link href="/public/static/img/favicon.ico" rel="shortcut icon">
        <link rel="stylesheet" href="/public/static/css/reset.css">
        <link rel="stylesheet" href="/public/static/css/style.css">
        <script src="/public/static/js/jquery-1.11.3.js"></script>
        <script src="/public/static/js/html5.js"></script>
        <script src="/public/static/js/adaptive-version2.js"></script>
        <script src="/public/static/js/jquery.easing.1.3.js"></script>
        <script src="/public/static/js/jquery.transit.js"></script>
        <script src="/public/static/js/jquery.lazyload.js"></script>
    </head>
    <body>
        <div class="content-box">
            <div class="panking-record">
                <div class="personal-title"><?php echo $list['nick_name']; ?>的战绩</div>
                <div class="personal-record-top">
                    <div class="record-to-tit"><span class="pic"></span>总收益</div>
                    <div class="record-to-fonts"><?php echo $list['all']; ?></div>
                    <ul class="record-top-list f-cb">
                        <li>
                            <p>日收益率</p>
                            <p><?php echo $list['day']; ?></p>
                        </li>
                        <li>
                            <p>周收益率</p>
                            <p><?php echo $list['week']; ?></p>
                        </li>
                        <li>
                            <p>月收益率</p>
                            <p><?php echo $list['month']; ?></p>
                        </li>
                    </ul>
                    <div class="record-top-mc">模拟大赛总排名：第<?php echo $list['ranking']; ?>名
                        <?php if($list['is_subscribe'] == 2): ?>
                        <a href="javascript:;" class="subscribe fr">订阅</a>
                        <?php else: ?>
                        <a href="javascript:;" class="subscribe fr hover">取消订阅</a>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="personal-record-bot">
                    <div class="title f-cb">
                        <span class="pic fl" style="background: url(<?php echo $list['headurl']; ?>) no-repeat center;"></span>
                        <span class="name fl"><?php echo $list['nick_name']; ?></span>
                        <span class="num fr hover"><?php echo $list['subscribe_sum']; ?></span>
                    </div>
                    <ul class="list f-cb">
                        <li class="hover">持仓</li>
                        <li>记录</li>
                    </ul>
                </div>
            </div>
            <?php if($list['is_subscribe'] == 1): ?>
            <input type="hidden" id="is_subscribe" value="2">
            <?php else: ?>
            <input type="hidden" id="is_subscribe" value="1">
            <?php endif; ?>
            <input type="hidden" id="subscribe_account" value="<?php echo $list['subscribe_account']; ?>">
            <input type="hidden" id="subscribe_nick_name" value="<?php echo $list['subscribe_nick_name']; ?>">
            <input type="hidden" id="trues" value="<?php echo $list['trues']; ?>">
            <div class="panking-record-list">
                <ul class="lists">
                    <?php if(is_array($list['buy_record']) || $list['buy_record'] instanceof \think\Collection || $list['buy_record'] instanceof \think\Paginator): if( count($list['buy_record'])==0 ) : echo "" ;else: foreach($list['buy_record'] as $key=>$vo): ?>
                    <li>
                        <div class="title f-cb">
                            <?php if($vo['income'] > 0): ?>
                            <div class="tt up fl">盈</div>
                            <?php else: ?>
                            <div class="tt down fl">亏</div>
                            <?php endif; ?>
                            <div class="two fl">
                                <h2><?php echo $vo['stock_name']; ?></h2>
                                <p>(<?php echo $vo['stock_code']; ?>)</p>
                            </div>
                            <div class="three fl"><span class="red"><?php echo $vo['stock_number']; ?></span>股</div>
                            <a href="<?php echo url('strategy/optional_info',['gid'=>$vo['stock_code']]); ?>" class="fr links">跟买</a>
                        </div>
                        <dl class="con f-cb">
                            <dd><span class="bb">策略止盈</span><?php echo $vo['surplus_value']; ?></dd>
                            <dd><span class="bb">策略止损</span><?php echo $vo['loss_value']; ?></dd>
                            <dd><span class="bb">买入价</span><?php echo $vo['buy_price']; ?></dd>
                            <dd><span class="bb">卖出价</span>持仓中</dd>
                            <dd><span class="bb">买入时间</span><?php echo $vo['buy_time']; ?></dd>
                            <dd><span class="bb">卖出时间</span>持仓中</dd>
                        </dl>
                    </li>
                    <?php endforeach; endif; else: echo "" ;endif; ?>
                </ul>
                <ul class="lists">
                    <?php if(is_array($list['record']) || $list['record'] instanceof \think\Collection || $list['record'] instanceof \think\Paginator): if( count($list['record'])==0 ) : echo "" ;else: foreach($list['record'] as $key=>$vo): ?>
                    <li>
                        <div class="title f-cb">
                            <?php if($vo['truegetprice'] > 0): ?>
                            <div class="tt up fl">盈</div>
                            <?php else: ?>
                            <div class="tt down fl">亏</div>
                            <?php endif; ?>
                            <div class="two fl">
                                <h2><?php echo $vo['stock_name']; ?></h2>
                                <p>(<?php echo $vo['stock_code']; ?>)</p>
                            </div>
                            <div class="three fl"><span class="red"><?php echo $vo['stock_number']; ?></span>股</div>
                        </div>
                        <dl class="con f-cb">
                            <dd><span class="bb">策略止盈</span><?php echo $vo['surplus_value']; ?></dd>
                            <dd><span class="bb">策略止损</span><?php echo $vo['loss_value']; ?></dd>
                            <dd><span class="bb">买入价</span><?php echo $vo['buy_price']; ?></dd>
                            <dd><span class="bb">卖出价</span><?php echo $vo['sell_price']; ?></dd>
                            <dd><span class="bb">买入时间</span><?php echo $vo['buy_time']; ?></dd>
                            <dd><span class="bb">卖出时间</span><?php echo $vo['sell_time']; ?></dd>
                        </dl>
                    </li>
                    <?php endforeach; endif; else: echo "" ;endif; ?>
                </ul>
            </div>
        </div>
        <!--<div class="includeDom" include="inc/footer.html" data-id="2"></div>-->
        <div class="includeDom" data-intro="scroll" data-id="2">
            <footer>
    <ul class="f-cb">
        <li>
            <a href="/index/index/index.html">
                <div class="pic-box">
                    <img src="/public/static/img/footer01.png" alt="" class="bg">
                    <img src="/public/static/img/footer01_h.png" alt="" class="pic">
                </div>
                <h2>首页</h2>
            </a>
        </li>
        <li>
            <a href="/index/strategy/strategy.html">
                <div class="pic-box">
                    <img src="/public/static/img/footer02.png" alt="" class="bg">
                    <img src="/public/static/img/footer02_h.png" alt="" class="pic">
                </div>
                <h2>策略</h2>
            </a>
        </li>
        <li>
            <a href="/index/ranking/rankingList.html">
                <div class="pic-box">
                    <img src="/public/static/img/footer03.png" alt="" class="bg">
                    <img src="/public/static/img/footer03_h.png" alt="" class="pic">
                </div>
                <h2>排行</h2>
            </a>
        </li>
        <li>
            <a href="/index/center/personal.html">
                <div class="pic-box">
                    <img src="/public/static/img/footer04.png" alt="" class="bg">
                    <img src="/public/static/img/footer04_h.png" alt="" class="pic">
                </div>
                <h2>我的</h2>
            </a>
        </li>
    </ul>
</footer>
<script>
    var id = $('footer').parent(".includeDom").data('id');
    $('footer li').eq(id).addClass('hover');
</script>
        </div>
        <div class="submit-success" style="display: none;"></div>
        <script src="/public/static/js/main.js"></script>
        <script>
            $('.panking-record-list .lists').eq(0).show();
            $('.personal-record-bot .list li').click(function() {
                $(this).addClass('hover').siblings('li').removeClass('hover');
                $('.panking-record-list .lists').eq($(this).index()).stop().fadeIn().siblings('.lists').hide();
            })

            $('.record-top-mc .subscribe').click(function() {
                $.ajax({
                    url: "/index/ranking/createSubscribe",
                    data:{
                        trues:$("#trues").val(),
                        subscribe_account:$("#subscribe_account").val(),
                        subscribe_nick_name:$("#subscribe_nick_name").val(),
                        is_subscribe:$("#is_subscribe").val()
                    },
                    dataType: "json",
                    success: function (data) {
//                         alert('ajax调用成功');
                        var dataObj = JSON.parse(data);
                        if(dataObj.status==1){
                            submitsuccess(dataObj.msg);
                            if ($(this).hasClass('hover')) {
                                $(this).removeClass('hover').html("订阅");
                            }else{
                                $(this).addClass('hover').html("取消订阅");
                            }
                            setTimeout(function() { window.location.reload()},1000);
                        }
                    }
                })
            })
        </script>
    </body>
</html>