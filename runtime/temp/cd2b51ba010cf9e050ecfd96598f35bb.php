<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:75:"/www/wwwroot/celue2.0.h5.solingke.cn/application/index/view/price/info.html";i:1558601387;s:85:"/www/wwwroot/celue2.0.h5.solingke.cn/application/index/view/price/inc/infoheader.html";i:1554296400;}*/ ?>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta http-equiv="content-language" content="zh-CN" />
        <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no" />
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black" />
        <meta name="format-detection" content="telephone=no" />
        <meta name="keywords" content="" />
        <meta name="description" content="" />
        <meta name="author" content="   " />
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
        <title>股票行情</title>
        <link href="/public/static/img/favicon.ico" rel="shortcut icon">
        <link rel="stylesheet" href="/public/static/css/reset.css">
        <link rel="stylesheet" href="/public/static/css/style.css">
        <link rel="stylesheet" href="/public/static/css/price.css"> 
        <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
        <script src="/public/static/js/vue.js"></script>
        <script src="/public/static/js/jquery-1.11.3.js"></script>
        <script src="/public/static/js/html5.js"></script>
        <script src="/public/static/js/adaptive-version2.js"></script>
        <script src="/public/static/js/jquery.easing.1.3.js"></script>
        <script src="/public/static/js/jquery.transit.js"></script>
        <script src="/public/static/js/jquery.lazyload.js"></script>
        <style>.price-search{top:0.09rem}
       </style>
    </head>
    <body>
        <div class="includeDom"  data-id="0">
            <link rel="stylesheet" href="/public/static/awesome/css/font-awesome.css">
<div class='por'>
    <div class="guide-top" id="infoheader">
            <a href="javascript:history.back(-1);" class="back"><i class="fa fa-angle-left"></i></a>
                <!-- <a href="javascript:history.back(-1);"></a> -->
            行情
            <div style="position: absolute;top: 0;right: 3%;">
                <div v-if="show=='2'" class="price-search" onclick="window.location.href='/index/index/optional_search.html'"></div>
                <div v-else class="price-search" onclick="window.location.href='/index/index/optional_search_trade.html'"></div>
            </div>
    </div>
</div>
<div class="loading">
  <div class="loadings">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
    <!--<img src="/public/static/img/logo.png" alt="" class="logo">-->
</div>
<script>
    jQuery(document).ready(function() {
        $('.loading').addClass("active");
    })
</script>
<script>
    function getSearchString(key) {
        let Url = window.location.search;
        var str = Url;
        str = str.substring(1, str.length);
        var arr = str.split("&");
        var obj = new Object();
        for (var i = 0; i < arr.length; i++) {
            var tmp_arr = arr[i].split("=");
            obj[decodeURIComponent(tmp_arr[0])] = decodeURIComponent(tmp_arr[1]);
        }
        return obj[key];
    }
    let ns=getSearchString("trade");
new Vue({
el:'#infoheader',
    data() {
        return {
            show:2
        };
    },
    mounted() {
        if(ns=='1'){
            this.show=1;
        }else{
            this.show=2;
        }
    },
    methods: {
    },
    
})
</script>
        </div>
        <div class="content-box bb" id="app">
            <div class="all-date">
                <div class="date-title" ><span id="he_name" v-cloak>{{list.name}}{{list.code}}</span>
                     <!-- <a onclick="add_zixuan()" class="add"></a> -->
                    </div>
                <div class="date-center">
                    <div class="date-center-top w94">
                        <h1 id="danjia" v-cloak>{{list.nowPri}}</h1>
                        <div class="red-font" v-cloak>
                            <a href="javascript:;" id="increase"  v-cloak :class="list.increase<0?'green':'red'">{{list.increase}}</a>
                            <a href="javascript:;" id="increper" v-cloak :class="list.increPer<0?'green':'red'" >{{list.increPer}}%</a>
                        </div>
                        <ul class="lists f-cb">
                            <li>
                                <h2 id="jinkai" v-cloak>{{list.todayopen}}</h2>
                                <p>今开</p>
                            </li>
                            <li>
                                <h2 id="zuoshou" v-cloak>{{list.yestodayshou}}</h2>
                                <p>昨收</p>
                            </li>
                            <li>
                                <h2 id="chengjiaoliang" v-cloak>{{(list.chengjiaoliang/10000).toFixed(2)}}万</h2>
                                <p>成交量</p>
                            </li>
                            <li>
                                <h2 id="shizhi" v-cloak>{{list.zongshizhi}}亿</h2>
                                <p>市值</p>
                            </li>
                        </ul>
                    </div>
                    <ul class="date-lists f-cb w94" style="text-align: center;">
                        <li style="width: 28%;">
                            <p>最高 <span class="fr" id="zuigao" v-cloak>{{list.zuigao}}</span></p>
                            <p>最低 <span class="fr" id="zuidi" v-cloak>{{list.zuidi}}</span></p>
                        </li>
                        <li style="width: 28%;">
                            <p>涨停价 <span class="fr" id="ztj" v-cloak>{{(list.upstopprice)}}</span></p>
                            <p>跌停价 <span class="fr" id='dtj' v-cloak>{{(list.downstopprice)}}</span></p>
                        </li>
                        <li style="width: 28%;">
                            <p>市盈率 <span class="fr" id="zhiyinglv" v-cloak>{{list.shiyinglv}}</span></p>
                            <p>市净率 <span class="fr" id="shijinlv" v-cloak>{{list.shijinglv}}</span></p>
                        </li>
                    </ul>
                </div>
                <ul class="date-list-title f-cb">
                    <li class="hover">分时</li>
                    <li>日K</li>
                    <li>周K</li>
                    <li>月K</li>
                </ul>
                <div class="date-list-con">
                    <div class="content">
                        <div class="line w94">
                            <div class="zanwu">
                            </div>
                            <div class="m-line" id="m-line"></div>
                        </div>
                        <div class="tts-box f-cb">
                            
                            <div class="box">
                                <h2>买盘档</h2>
                                <ul class="f-cb">
                                    <li><span id="maid5" v-cloak>{{list.buyFivePri==''?'--':list.buyFivePri}}</span><span class="fonts">买5</span><span class="num" id="mai5" v-cloak>{{list.buyFive==''?'--':list.buyFive}}</span></li>
                                    <li><span id="maid4" v-cloak>{{list.buyFourPri==''?'--':list.buyFourPri}}</span><span class="fonts">买4</span><span class="num" id="mai4" v-cloak>{{list.buyFour==''?'--':list.buyFour}}</span></li>
                                    <li><span id="maid3" v-cloak>{{list.buyThreePri==''?'--':list.buyThreePri}}</span><span class="fonts">买3</span><span class="num" id="mai3" v-cloak>{{list.buyThree==''?'--':list.buyThree}}</span></li>
                                    <li><span id="maid2" v-cloak>{{list.buyTwoPri==''?'--':list.buyTwoPri}}</span><span class="fonts">买2</span><span class="num" id="mai2" v-cloak>{{list.buyTwo==''?'--':list.buyTwo}}</span></li>
                                    <li><span id="maid1" v-cloak>{{list.buyOnePri==''?'--':list.buyOnePri}}</span><span class="fonts">买1</span><span class="num" id="mai1" v-cloak>{{list.buyOne==''?'--':list.buyOne}}</span></li>
                                </ul>
                            </div>
                          <div class="box">
                                <h2>卖盘档</h2>
                                <ul class="f-cb">
                                    <li><span id="selld5" v-cloak>{{list.sellFivePri==''?'--':list.sellFivePri}}</span><span class="fonts">卖5</span><span class="num" id="sell5" v-cloak>{{list.sellFive==''?'--':list.sellFive}}</span></li>
                                    <li><span id="selld4" v-cloak>{{list.sellFourPri==''?'--':list.sellFourPri}}</span><span class="fonts">卖4</span><span class="num" id="sell4" v-cloak>{{list.sellFour==''?'--':list.sellFour}}</span></li>
                                    <li><span id="selld3" v-cloak>{{list.sellThreePri==''?'--':list.sellThreePri}}</span><span class="fonts">卖3</span><span class="num" id="sell3" v-cloak>{{list.sellThree==''?'--':list.sellThree}}</span></li>
                                    <li><span id="selld2" v-cloak>{{list.sellTwoPri==''?'--':list.sellTwoPri}}</span><span class="fonts">卖2</span><span class="num" id="sell2" v-cloak>{{list.sellTwo==''?'--':list.sellTwo}}</span></li>
                                    <li><span id="selld1" v-cloak>{{list.sellOnePri==''?'--':list.sellOnePri}}</span><span class="fonts">卖1</span><span class="num" id="sell1" v-cloak>{{list.sellOne==''?'--':list.sellOne}}</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="content">
                         <div class="line w94">
                            <div class="k-content" id="k-content1"></div>
                        </div>
                    </div>
                    <div class="content">
                         <div class="line w94">
                            <div class="k-content" id="k-content2"></div>
                        </div>
                    </div>
                    <div class="content">
                         <div class="line w94">
                            <div class="k-content" id="k-content3"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="date-links">
            <a href="javascript:;" class="trades" onclick="ploy(this)">创建策略</a>
            <a href="javascript:;" onclick="add_zixuan()" id="add_zixuan">添加自选</a>
        </div>
        <div class="submit-success on" style="display: none;"></div>
        <input type="hidden" id="stock_code" value="sz000633">
        <input type="hidden" id="stock_name" value="中科新材">
        <script src="/public/static/js/main.js"></script>
        <script src="/public/static/js/echarts.min.js"></script>
        <!-- <script src="/public/static/js/tmpData.js"></script> -->
        <script src="/public/static/js/k-line.js"></script>
        <script src="/public/static/js/jquery.cookie.js"></script>
        <script>
            $(function(){
                let ns=getSearchString("trade");
                let searchs=search() 
                let t=date("09:30:00")
                var timestamp1 =  (new Date()).valueOf(); 
                if(timestamp1>t){
                    if(ns=='1'){
                        $('.date-links a').eq(0).attr('href','/index/trade/ploy.html?stock_code='+searchs.stock_code+'&stock_name='+searchs.stock_name+'&trade=1')
                    }else{
                        $('.date-links a').eq(0).attr('href','/index/strategy/ploy.html?stock_code='+searchs.stock_code+'&stock_name='+searchs.stock_name)
                    }
                }else{
                    // submitsuccess("非交易时间禁止创建策略！")
                } 
            })
                localStorage.removeItem("packet")  
            var vm=new Vue({
                el:'#app',
                props: {
                },
                data:function() {
                    return {
                        list:{},
                        hide:true
                    };
                },
                computed: {
                },
                mounted:function() {
                    this.stockDetail_tobuy()
                },
                methods: {
                    stockDetail_tobuy(){
                        var stock_code =  getSearchString('stock_code');
                        var _this=this;
                        axios.get('/api/Mobile/stockDetail_tobuy?stock_code='+stock_code)
                        .then(function (res) {
                            if(res.data.status!=200){
                                _this.hide = false;
                                // submitsuccess("该股票已被风控，无法购买"); 
                                $('.date-links a').eq(0).attr('href','javascript:;')
                                return;         
                            }else{
                                _this.list = res.data.list
                            }
                        })
                        .catch(function (error) {
                            console.log(error);
                        });
                    }
                },
            })
            </script>
        <script type="text/javascript">
                localStorage.removeItem("packet")                           
                
                //     var stock_code =  getSearchString('stock_code');
                //     $.ajax({
                //         url: "/api/Mobile/stockDetail_tobuy?stock_code="+stock_code,
                //         dataType: "json",
                //         success: function (data) {
                //                 // alert('ajax调用成功');
                //             var dataObj = JSON.parse(data);
                //             if(dataObj.status!=200){
                //                 submitsuccess("该股票已被风控，无法购买"); 
                //                 $('.date-links a').eq(0).attr('href','javascript:;')
                //                 return;         
                //             }
                //             if(dataObj.status==200){
                //             //判断正负值
                //             var increase2 = dataObj.list.increase;
                //             var increPer2 = dataObj.list.increPer;
                //             if(increase2<0){
                //                 //截取小数点后两位
                //                 var increase = increase2.substr(0,increase2.indexOf(".")+3);
                //                 $("#increase").removeClass("red");
                //                 $("#increase").addClass("green");
                                
                //             }else{
                //                 //截取小数点后两位
                //                 var increase = "+"+increase2.substr(0,increase2.indexOf(".")+3);
                //                 $("#increase").removeClass("green");
                //                 $("#increase").addClass("red");
                //                 $("#danjia").css("color","#fc5146");                            
                //             }
                //             $("#increase").html(increase2);
                            
                //             if(increPer2<0){
                //                 var increPer = increPer2+"%";                   
                //             }else{
                //                 var increPer = "+"+increPer2+"%";                                 
                //             }
                //             $("#increase").html(increase2);
                //             var danjia = dataObj.list.nowPri;
                //             $("#danjia").html(danjia);
                //             //涨停价
                //             var ztj = (danjia*1.1).toString();
                //             ztj =  ztj.substr(0,ztj.indexOf(".")+3);
                //             $("#ztj").html(ztj);
                //             //跌停价
                //             var dtj = (danjia*0.9).toString();
                //             dtj = dtj.substr(0,dtj.indexOf(".")+3);
                //             $("#dtj").html(dtj);
                //             //买一到买五
                //             var maid1  = dataObj.list.buyOnePri;
                //             maid1 = maid1.substr(0,maid1.indexOf(".")+3);
                //             $("#maid1").html(maid1);

                //             var maid2  = dataObj.list.buyTwoPri;
                //             maid2 = maid2.substr(0,maid2.indexOf(".")+3);
                //             $("#maid2").html(maid2);

                //             var maid3  = dataObj.list.buyThreePri;
                //             maid3 = maid3.substr(0,maid3.indexOf(".")+3);
                //             $("#maid3").html(maid3);

                //             var maid4  = dataObj.list.buyFourPri;
                //             maid4 = maid4.substr(0,maid4.indexOf(".")+3);
                //             $("#maid4").html(maid4); 

                //             var maid5  = dataObj.list.buyFivePri;
                //             maid5 = maid5.substr(0,maid5.indexOf(".")+3);
                //             $("#maid5").html(maid5);

                //             //买一到买五大
                //             var mai1  = dataObj.list.buyOne;
                //             mai1 = mai1.substr(0,mai1.length-2);
                //             $("#mai1").html(mai1);

                //             var mai2  = dataObj.list.buyTwo;
                //             mai2 = mai2.substr(0,mai2.length-2);
                //             $("#mai2").html(mai2);

                //             var mai3  = dataObj.list.buyThree;
                //             mai3 = mai3.substr(0,mai3.length-2);
                //             $("#mai3").html(mai3);

                //             var mai4  = dataObj.list.buyFour;
                //             mai4 = mai4.substr(0,mai4.length-2);
                //             $("#mai4").html(mai4);

                //             var mai5  = dataObj.list.buyFive;
                //             mai5 = mai5.substr(0,mai5.length-2);
                //             $("#mai5").html(mai5);

                //             //卖一到买五
                //             var selld1  = dataObj.list.sellOnePri;
                //             selld1 = selld1.substr(0,selld1.indexOf(".")+3);
                //             $("#selld1").html(selld1);

                //             var selld2  = dataObj.list.sellTwoPri;
                //             selld2 = selld2.substr(0,selld2.indexOf(".")+3);
                //             $("#selld2").html(selld2);

                //             var selld3  = dataObj.list.sellThreePri;
                //             selld3 = selld3.substr(0,selld3.indexOf(".")+3);
                //             $("#selld3").html(selld3);

                //             var selld4  = dataObj.list.sellFourPri;
                //             selld4 = selld4.substr(0,selld4.indexOf(".")+3);
                //             $("#selld4").html(selld4); 

                //             var selld5  = dataObj.list.sellFivePri;
                //             selld5 = selld5.substr(0,selld5.indexOf(".")+3);
                //             $("#selld5").html(selld5);


                //             //卖一到买大
                //             var sell1  = dataObj.list.sellOne;
                //             sell1 = sell1.substr(0,sell1.length-2);
                //             $("#sell1").html(sell1);

                //             var sell2  = dataObj.list.sellTwo;
                //             sell2 = sell2.substr(0,sell2.length-2);
                //             $("#sell2").html(sell2);

                //             var sell3  = dataObj.list.sellThree;
                //             sell3 = sell3.substr(0,sell3.length-2);
                //             $("#sell3").html(sell3);

                //             var sell4  = dataObj.list.sellFour;
                //             sell4 = sell4.substr(0,sell4.length-2);
                //             $("#sell4").html(sell4); 

                //             var sell5  = dataObj.list.sellFive;
                //             sell5 = sell5.substr(0,sell5.length-2);
                //             $("#sell5").html(sell5);

                //             //今开
                //             var jinkai = dataObj.list.todayopen;
                //             $("#jinkai").html(jinkai);

                //             //昨收
                //             var zuoshou = dataObj.list.yestodayshou;
                //             $("#zuoshou").html(zuoshou);

                //             //成交量
                //             var chengjiaoliang = dataObj.list.chengjiaoliang;
                //             chengjiaoliang = (Number(chengjiaoliang)/10000).toFixed(2);
                //             $("#chengjiaoliang").html(chengjiaoliang+'万');

                //             //市值
                //             var shizhi = dataObj.list.zongshizhi;
                //             $("#shizhi").html(shizhi+'亿');

                //                 //最高
                //                 var zuigao = dataObj.list.zuigao;
                //                 $("#zuigao").html(zuigao);

                //                 //最低
                //                 var zuidi = dataObj.list.zuidi;
                //                 $("#zuidi").html(zuidi);

                //                 //市盈率
                //                 var zhiyinglv = dataObj.list.shiyinglv;
                //                 $("#zhiyinglv").html(zhiyinglv);

                //                 //市净率 
                //                 var shijinlv = dataObj.list.shijinglv;
                //                 $("#shijinlv").html(shijinlv);

                //                 var stock_name = dataObj.list.name;//股票名字
                //                 $("#stock_name").val(stock_name);//存隐藏域
                //                 $("#he_name").html(stock_name+stock_code);




                //             }                     
                //         }
                //     })                           
                //     })
                //添加自选
                function add_zixuan(){
                        if(getCookie('username')){
                        }else{
                            window.location.href="/index/index/login.html "
                        }
                    if($("#add_zixuan").text()=="删除自选"){
                        $.ajax({
                            url: "/api/mobile/deleteChooseStock",
                            data: Object.assign(account,{ stock_code: getSearchString('stock_code')}),
                            dataType: "json",
                            method:"post", 
                            success: function (data) {
                                var dataObj = JSON.parse(data);
                                if(dataObj.status==1){
                                    submitsuccess('已删除我的自选股');
                                    $("#add_zixuan").text("添加自选")
                                }else{
                                    alert(dataObj.msg)
                                }                     
                            }
                        })  
                    }else{
                        $.ajax({
                            url: "/api/mobile/addChooseStock",
                            data: Object.assign(account,{ stock_code: getSearchString('stock_code'),  
                                    stock_name:getSearchString('stock_name')}),
                            dataType: "json",
                            method:"post", 
                            success: function (data) {
                                var dataObj = JSON.parse(data);
                                if(dataObj.status==1){
                                    submitsuccess('已添入我的自选股');
                                    $(this).addClass('hover');
                                    $("#add_zixuan").text("删除自选")
                                }else{
                                    alert(dataObj.msg)
                                }                     
                            }
                        })  
                    }
                }
                function ifChooseStock(){
                  $.ajax({
                        url: "/api/mobile/ifChooseStock",
                        data: Object.assign(account,{ stock_code: getSearchString('stock_code'),  
                                stock_name:getSearchString('stock_name')}),
                        dataType: "json",
                        method:"post", 
                        success: function (data) {
                            var dataObj = JSON.parse(data);
                            if(dataObj.status==1){
                                $("#add_zixuan").text("删除自选")
                            }else{
                                $("#add_zixuan").text("添加自选")
                            }   
                                            
                        }
                    })  
                }
                if(getCookie('username')){
                    ifChooseStock()
                }

        </script>
        <script>
            
            function ploy(e){
                let ns=getSearchString("trade");
                if($(e)[0].href=='javascript:;'){
                    let t=date("09:30:00")
                    var timestamp1 =  (new Date()).valueOf(); 
                    if(timestamp1>t){
                    }else{
                        if(vm.hide==false){
                            submitsuccess("该股已被系统风控，无法购买！")
                        }else{
                            submitsuccess("非交易时间禁止创建策略！")
                        }
                    }
                }
            }
            function submitsuccess(msg){
                $('.submit-success').html(msg);
                $('.submit-success').fadeIn();
                setTimeout(function(){
                    $('.submit-success').fadeOut();
                },1000);
            }
            function Kday(){
                $.ajax({
                    url: "/api/Mobile/Kday",
                    data: {
                        code:getSearchString('stock_code')
                    },
                    dataType: "json",
                    method:"post",
                    success: function (data) {
                        var dataObj = JSON.parse(data);
                        if(dataObj.status==1){
                            var len = dataObj.list.length;
                            var kdata1 = [];
                            for(var i=0;i<len;i++){
                                kdata1[i] = new Array();
    
                                kdata1[i][0] = dataObj.list[i].time;
                                kdata1[i][1] = dataObj.list[i].begin;
                                kdata1[i][2] = dataObj.list[i].close;
                                kdata1[i][3] = dataObj.list[i].high;
                                kdata1[i][4] = dataObj.list[i].low;
                                kdata1[i][5] = dataObj.list[i].volumn;
    
    
                            }
                            var kChart1 = echarts.init(document.getElementById('k-content1'));
                            kChart1.setOption(initKOption(kdata1));
                        }
                    }
                })
            }
            function Kweek(){
                $.ajax({
                    url: "/api/Mobile/Kweek",
                    data: {
                        code: getSearchString('stock_code')
                    },
                    dataType: "json",
                    method:"post",
                    success: function (data) {
                        var dataObj = JSON.parse(data);
                        if(dataObj.status==1){
                            var len = dataObj.list.length;
                            var kdata2 = [];
                            for(var i=0;i<len;i++){
                                kdata2[i] = new Array();
                                kdata2[i][0] = dataObj.list[i].time;
                                kdata2[i][1] = dataObj.list[i].begin;
                                kdata2[i][2] = dataObj.list[i].close;
                                kdata2[i][3] = dataObj.list[i].high;
                                kdata2[i][4] = dataObj.list[i].low;
                                kdata2[i][5] = dataObj.list[i].volumn;
                            }
                            var kChart2 = echarts.init(document.getElementById('k-content2'));
                            kChart2.setOption(initKOption(kdata2));
                        }
                    }
                })
            }
            function Kmonth(){
                $.ajax({
                    url: "/api/Mobile/Kmonth",
                    data: {
                        code: getSearchString('stock_code')
                    },
                    dataType: "json",
                    method:"post",
                    success: function (data) {
                        var dataObj = JSON.parse(data);
                        if(dataObj.status==1){
                            var len = dataObj.list.length;
                            var kdata3 = [];
                            for(var i=0;i<len;i++){
                                kdata3[i] = new Array();
    
                                kdata3[i][0] = dataObj.list[i].time;
                                kdata3[i][1] = dataObj.list[i].begin;
                                kdata3[i][2] = dataObj.list[i].close;
                                kdata3[i][3] = dataObj.list[i].high;
                                kdata3[i][4] = dataObj.list[i].low;
                                kdata3[i][5] = dataObj.list[i].volumn;
                           }
                            var kChart3 = echarts.init(document.getElementById('k-content3'));
                            kChart3.setOption(initKOption(kdata3));
                        }
                    }
                })
            }
            function minhour(){
                $.ajax({
                  url: "/api/Mobile/minhour",
                  data: {
                      code: getSearchString('stock_code')
                  },
                  dataType: "json",
                  method:"post",
                  success: function (data) {
                      var dataObj = JSON.parse(data);
                      if(dataObj.status==1){
                          var len = dataObj.list.length;
                          var mdata = [];
                          mdata['data'] = new Array();
                          for(var i=0;i<len;i++){
                              mdata['data'][i] = new Array();
                              mdata['data'][i][0] = dataObj.list[i].time;
                              mdata['data'][i][1] = dataObj.list[i].price;
                              mdata['data'][i][2] = dataObj.list[i].junjia;
                              mdata['data'][i][3] = dataObj.list[i].volumn;
                              mdata['yestclose'] = dataObj.para;
                          }
                          var mChart = echarts.init(document.getElementById('m-line'));
                          mChart.setOption(initMOption(mdata,'us'));
                      }
                  }
              })
            }
            $(function(){
           
               
                //var mChart = echarts.init(document.getElementById('m-line'));
                //mChart.setOption(initMOption(mdata,'us'));

//                var kChart1 = echarts.init(document.getElementById('k-content1'));
//                kChart1.setOption(initKOption(kdata1));

//                var kChart2 = echarts.init(document.getElementById('k-content2'));
//                kChart2.setOption(initKOption(kdata2));
//
//                var kChart3 = echarts.init(document.getElementById('k-content3'));
//                kChart3.setOption(initKOption(kdata3));

                $('.date-list-con .content').eq(0).height("auto");

                $('.date-list-title li').click(function(){
                    $(this).addClass('hover').siblings('li').removeClass('hover');
                    $('.date-list-con .content').eq($(this).index()).height("auto").siblings('.content').height(0);
                })

            })
            let interval;
            // kLineFour();
            kLine()
            // function kLineFour(){
            //     // interval=setInterval('kLineFour()',2000)
            // }
            // 设置开盘交易时间 每天9:30
            function date(news){
                var myDate = new Date();
                var year=myDate.getFullYear();
                var month= myDate.getMonth()+1;
                var date=myDate.getDate()
                var hours=myDate.getHours(); //获取当前小时数(0-23) 
                var min=myDate.getMinutes(); //获取当前分钟数(0-59) 
                let newdate = year+'/'+month+'/'+date+' '+news;
                let d = new Date(newdate)
                let t =  (d).valueOf();
                return t;
            }
            function kLine(){
                let t=date("09:30:00")
                var timestamp1 =  (new Date()).valueOf(); 
                    if(timestamp1>t){
                        console.log(222)
                        $('.date-links a').eq(0).attr('href','JavaScript:;')
                        minhour();
                        $(".zanwu").remove()
                    }else{
                        $("#m-line").remove()
                    }
                // minhour();
                Kday();
                Kweek();
                Kmonth();
                clearInterval(interval);
             }
        </script>
        



    </body>
</html>