<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:67:"/home/wwwroot/m/application/index/view/center/personal_addcard.html";i:1550059642;s:60:"/home/wwwroot/m/application/index/view/index/inc/footer.html";i:1548925626;}*/ ?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta http-equiv="content-language" content="zh-CN" />
        <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no" />
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black" />
        <meta name="format-detection" content="telephone=no" />
        <meta name="keywords" content="" />
        <meta name="description" content="" />
        <meta name="author" content="令克网络-高端网站建设-https://www.link-web.cn/" />
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
        <title>赢在策略</title>
        <link href="/public/static/img/favicon.ico" rel="shortcut icon">
        <link rel="stylesheet" href="/public/static/css/reset.css">
        <link rel="stylesheet" href="/public/static/css/style.css">
        <script src="/public/static/js/jquery-1.11.3.js"></script>
        <script src="/public/static/js/html5.js"></script>
        <script src="/public/static/js/adaptive-version2.js"></script>
        <script src="/public/static/js/jquery.easing.1.3.js"></script>
        <script src="/public/static/js/jquery.transit.js"></script>
        <script src="/public/static/js/jquery.lazyload.js"></script>
    </head>
    <body>
        <div class="content-box data">
            <div class="personal-title">我的银行卡</div>
            <div class="personal-attr bb">
                <label class="f-cb">
                    <span class="tit">银行卡</span>
                    <input type="number" name="" placeholder="请输入银行卡" id="bank_num" pattern="\d*"/>
                </label>
                <label class="f-cb">
                    <span class="tit">银行卡所属省</span>
                    <input type="text" name="" placeholder="请输入省" id="province">
                </label>
                <label class="f-cb">
                    <span class="tit">银行卡所属市</span>
                    <input type="text" name="" placeholder="请输入银行所属市" id="city">
                </label>
                <label class="f-cb">
                    <span class="tit">开户行</span>
                    <input type="text" name="" placeholder="请输入开户行" id="branchname">
                </label>
                <input type="submit" name="" value="确认上传" onclick="addbankcard()">
                <p class="tts">该卡可作为快捷支付及提现使用，暂不支持在线换卡，如若需要换卡请致电客服</p>
            </div>
        </div>
        <div class="includeDom"  data-id="3">
            <footer>
    <ul class="f-cb">
        <li>
            <a href="/index/index/index.html">
                <div class="pic-box">
                    <img src="/public/static/img/footer01.png" alt="" class="bg">
                    <img src="/public/static/img/footer01_h.png" alt="" class="pic">
                </div>
                <h2>首页</h2>
            </a>
        </li>
        <li>
            <a href="/index/strategy/strategy.html">
                <div class="pic-box">
                    <img src="/public/static/img/footer02.png" alt="" class="bg">
                    <img src="/public/static/img/footer02_h.png" alt="" class="pic">
                </div>
                <h2>策略</h2>
            </a>
        </li>
        <li>
            <a href="/index/ranking/rankingList.html">
                <div class="pic-box">
                    <img src="/public/static/img/footer03.png" alt="" class="bg">
                    <img src="/public/static/img/footer03_h.png" alt="" class="pic">
                </div>
                <h2>排行</h2>
            </a>
        </li>
        <li>
            <a href="/index/center/personal.html">
                <div class="pic-box">
                    <img src="/public/static/img/footer04.png" alt="" class="bg">
                    <img src="/public/static/img/footer04_h.png" alt="" class="pic">
                </div>
                <h2>我的</h2>
            </a>
        </li>
    </ul>
</footer>
<script>
    var id = $('footer').parent(".includeDom").data('id');
    $('footer li').eq(id).addClass('hover');
</script>
        </div>
        <script src="/public/static/js/main.js"></script>
        <script src="/public/static/js/jquery.cookie.js"></script>


        <script type="text/javascript">
            function addbankcard(){
                $(function(){
                    $.ajax({
                        url: "/API/index/addbankcard",
                        data: { account: $.cookie("username"),
                            bank_num:$("#bank_num").val(),
                            province:$("#province").val(),
                            city:$("#city").val(),
                            branchname:$("#branchname").val()
                        },
                        dataType: "json",
                        method:"post",
                        success: function (data) {
                            // alert('ajax调用成功');
                            var dataObj = JSON.parse(data);
                            if(dataObj.status==1){
                                alert("银行卡添加成功");
                                submitsuccess("银行卡添加成功");
                                setTimeout(function () { window.location.href='/index/center/personal_card' }, 1000);//延迟1秒后关闭

                            }else{
                                submitsuccess(dataObj.msg);
                            }
                        }
                    })
                })
            }




        </script>
    </body>
</html>