<?php if (!defined('THINK_PATH')) exit(); /*a:3:{s:72:"/www/wwwroot/peiqi.solingke.cn/application/index/view/strategy/ploy.html";i:1554708426;s:79:"/www/wwwroot/peiqi.solingke.cn/application/index/view/price/inc/ployhrader.html";i:1554258032;s:75:"/www/wwwroot/peiqi.solingke.cn/application/index/view/index/inc/footer.html";i:1554779792;}*/ ?>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta http-equiv="content-language" content="zh-CN" />
        <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no" />
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black" />
        <meta name="format-detection" content="telephone=no" />
        <meta name="keywords" content="" />
        <meta name="description" content="" />
        <meta name="author" content="令克网络-互联网系统解决方案服务商-https://www.link-web.cn/" />
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
        <title>创建策略</title>
        <link href="/public/static/img/favicon.ico" rel="shortcut icon">
        <link rel="stylesheet" href="/public/static/css/reset.css">
        <link rel="stylesheet" href="/public/static/css/style.css">
        <link rel="stylesheet" href="/public/static/css/price.css">
        <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
        <script src="/public/static/js/vue.js"></script>
        <script src="/public/static/js/jquery-1.11.3.js"></script>
        <script src="/public/static/js/html5.js"></script>
        <script src="/public/static/js/adaptive-version2.js"></script>
        <script src="/public/static/js/jquery.easing.1.3.js"></script>
        <script src="/public/static/js/jquery.transit.js"></script>
        <script src="/public/static/js/jquery.lazyload.js"></script>
        <style>.price-search{top:0.09rem}.shade-work button{color:#e6453d}</style>
        <!-- <script src="https://cdn.bootcss.com/vConsole/3.2.0/vconsole.min.js"></script> -->

    </head>
    <body>
        <div id="app">
            <div class="includeDom"  data-id='0'>
                <link rel="stylesheet" href="/public/static/awesome/css/font-awesome.css">
<div class='por ploy-header'>
    <div class="guide-top">
        <!-- <a href="/index/price/index.html"></a> -->
        <a href="/index/price/index.html" class="back"><i class="fa fa-angle-left"></i></a>
        {{title}}
        <div class="price-search" onclick="window.location.href='/index/index/optional_search.html'">切换</div>
    </div>
    <div class="ploy-list">
            策略
        <ul class="f-cb">
            <li><a :href="'/index/strategy/ploy.html?stock_code='+list.stock_code+'&stock_name='+list.stock_name">T+1</a></li>
            <li><a :href="'/index/strategy/ployD.html?stock_code='+list.stock_code+'&stock_name='+list.stock_name">T+D</a></li>
        </ul>
    </div>
</div>
            </div>
            <div >   
                <div class="m20">
                    <div class="ploy-money">
                        投入信用金
                        <div clas="por">
                            <div>
                                <input type="tel" id="moneys" value="1" @input="inputs(money,$event)">
                                <span class="numd"  v-text="moneyone"></span> 
                            </div>
                            元
                        </div>
                        <p>请输入1000的整数倍</p>
                    </div>
                    <div class="play-bei">
                        <h2>推荐金额</h2>
                        <ul class='f-cb'>
                            <li v-for='item in bei' @click="changes(item,$event)"> <span>{{item*moneyone/10000}}</span> 万</li>
                        </ul>
                    </div>
                    <div class="play-num on">
                            买入数量
                            <p>{{list.stock_number}}股(市值:{{(list.stock_number * nowPri/10000).toFixed(2)}}万,资金利用率: {{(list.stock_number * nowPri / tuijian *100).toFixed(2)}} %)</p>
                    </div>
                    <div class="play-loss">
                        <ul>
                            <li>
                                止盈
                                <p>涨至 <span class="red">{{proNum}}%</span> 发起平仓</p>
                                <div class="f-cb">
                                    <span @click="profitLess($event)"   class="red">-</span>
                                    <input type="text" disabled v-model="list.surplus_value" style="color:#d63834">
                                    <span @click="profitAdd($event)"    class="red">+</span>
                                </div>
                            </li>
                            <li>
                                止损
                                <p>下跌 <span class="green">{{loss}}%</span> 发起平仓</p>
                                <div class="f-cb">
                                    <span @click="lossLess($event)">-</span>
                                    <input type="text" disabled v-model="list.loss_value" style="color:#279d1b">
                                    <span @click="lossAdd($event)">+</span>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="m20 ">
                    <div class="play-num">
                        持仓时间
                        <p>1天<span></span></p>
                    </div>
                    <!-- <div class="play-num">
                        自动递延
                        <b @click="switchd($event)" class="on"></b>
                    </div> -->
                    <div class="play-num">
                        优惠
                        <p><a href="/index/center/personal_packet.html"> <i>{{nameText}}</i>{{name}}元</a></p>
                    </div>
                </div>
                <div class="m20">
                    <div class="play-num">
                            预计可赚
                        <p>{{((list.surplus_value - nowPri)*list.stock_number).toFixed(2)}}元</p>
                    </div>
                    <div class="play-num">
                      服务费<span class="on"><i>创建策略时实时扣除，包含证券交易费（印花税、过户费等），此费用只在创建策略时收取，具体收费以下单页面为准。</i></span>
                        <p>{{list.buy_poundage}}元</p>
                    </div>
                  <div class="play-num">
                      递延费<span class="on"><i>策略起始交易日后的第2个交易日开始按交易日收取，具体收费以下单页面为准。</i></span>
                        <p>{{(tuijian*defer).toFixed(2)}}元</p>
                    </div>
                    <div class="play-num">
                            合计
                        <p>{{parseFloat(moneyone) + list.buy_poundage -(-name)}}元</p>
                    </div>
                    <div class="play-num">
                            策略余额
                        <p>{{tactics_balance}}元</p>
                    </div>
                </div>
                <div class="ploy-bottom">
                    <h2>我已阅读并同意<a href="/addOne.html">《相关协议》</a></h2>
                    <a href="javascript:;" @click="submit($event)">创建策略</a>
                </div>
            </div>
        </div>
        <div class="shade" style="">
          <div class="shade-work">
            <i></i>
            <p>是否卖出万 科Ａsz000002?</p> 
            <div>
              <button>确定</button>
            </div>
          </div>
      	</div>
      <div class="submit-success on" style="display: none;"></div>
        <div style="height:1.2rem"></div>
        <div class="includeDom"  data-id="1">
            <footer>
    <ul class="f-cb">
        <!-- <li>
            <a href="/index/index/index.html">
                <div class="pic-box">
                    <img src="/public/static/img/footer01.png" alt="" class="bg">
                    <img src="/public/static/img/footer01_h.png" alt="" class="pic">
                </div>
                <h2>首页</h2>
            </a>
        </li>
        <li>
            <a href="/index/strategy/strategy.html">
                <div class="pic-box">
                    <img src="/public/static/img/footer02.png" alt="" class="bg">
                    <img src="/public/static/img/footer02_h.png" alt="" class="pic">
                </div>
                <h2>策略</h2>
            </a>
        </li>
        <li>
            <a href="/index/ranking/rankingList.html">
                <div class="pic-box">
                    <img src="/public/static/img/footer03.png" alt="" class="bg">
                    <img src="/public/static/img/footer03_h.png" alt="" class="pic">
                </div>
                <h2>排行</h2>
            </a>
        </li>
        <li>
            <a href="/index/center/personal.html">
                <div class="pic-box">
                    <img src="/public/static/img/footer04.png" alt="" class="bg">
                    <img src="/public/static/img/footer04_h.png" alt="" class="pic">
                </div>
                <h2>我的</h2>
            </a>
        </li> -->
        <li>
            <a href="/index/index/index.html">
                <div class="pic-box">
                    <img src="/public/static/img/img_7.png" alt="" class="bg">
                    <img src="/public/static/img/img_8.png" alt="" class="pic">
                </div>
                <h2>首页</h2>
            </a>
        </li>
        <li>
            <a href="/index/price/quotes.html">
                <div class="pic-box">
                    <img src="/public/static/img/img_9.png" alt="" class="bg">
                    <img src="/public/static/img/img_10.png" alt="" class="pic">
                </div>
                <h2>行情</h2>
            </a>
        </li>
        <li>
            <a href="/index/trade/index.html">
                <div class="pic-box">
                    <img src="/public/static/img/img_11.png" alt="" class="bg">
                    <img src="/public/static/img/img_12.png" alt="" class="pic">
                </div>
                <h2>交易</h2>
            </a>
        </li>
        <li>
            <a href="/index/news/index.html">
                <div class="pic-box">
                    <img src="/public/static/img/img_13.png" alt="" class="bg">
                    <img src="/public/static/img/img_14.png" alt="" class="pic">
                </div>
                <h2>资讯</h2>
            </a>
        </li>
        <li>
            <a href="/index/center/personal.html">
                <div class="pic-box">
                    <img src="/public/static/img/img_15.png" alt="" class="bg">
                    <img src="/public/static/img/img_16.png" alt="" class="pic">
                </div>
                <h2>我的</h2>
            </a>
        </li>
    </ul>
    <div class="loading">
            <div class="loadings">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div> 
        <!-- <img src="/public/static/img/logo.png" alt="" class="logo"> -->
    </div>
</footer>
<script>
    var id = $('footer').parent(".includeDom").data('id');
    $('footer li').eq(id).addClass('hover');
</script>
        </div>
        <div class="loading">
            <div class="loadings">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>
        <script src="/public/static/js/main.js"></script>
        <!-- <script src="/public/static/js/jquery.cookie.js"></script> -->
        <script>
            
            var id = $('.ploy-header').parent(".includeDom").data('id');
            $('.ploy-list li').eq(id).addClass('hover');
            $(function(){
                $(".ploy-bottom h2").click(function () {
                    if (!$(this).hasClass("on")) {
                        $(this).addClass("on")
                    }else{
                        $(this).removeClass("on")
                    }
                })
               $(".play-num span.on").click(function(){
          		var text = $(this).find("i").text();
              	$(".shade-work p").text(text);
              	$(".shade").fadeIn(0)
            	})
            $(".shade-work button").click(function(){
              $(this).parents(".shade").fadeOut(0)
            })
            })
         
            let msd=search()
            function submitsuccess(msg){
                $('.submit-success').html(msg);
                $('.submit-success').fadeIn();
                setTimeout(function(){
                    $('.submit-success').fadeOut();
                },1000);
            }
            var vm=new Vue({
            el:'#app',
            props: {
            },
            data() {
                return {
                    title:"美的集团SZ000333",
                    money:"1000",
                    moneyone:"",
                    bei:[],
                    loss:'',
                    profit:'14.3',
                    name:"0.00",
                    nameText:'',
                    nowPri:"",
                    tuijian:"",
                    lossNum:"",
                  	proNum:"20",
                  defer:"",
                  poundage:"",
                    list:{
                        market_value:"",
                        double_value:"",
                        strategy_type:"1",
                        stock_name:"",
                        stock_code:"",
                        stock_number:"",
                        credit:"",
                        buy_price:"",
                        buy_poundage:"",
                        surplus_value:"",
                        loss_value:"",
                        defer:1,
                        packet_id:""
                    },
                    tactics_balance:'',
                    allow_kuisun:''
                };
            },
            computed: {
            },
            mounted() {
                if (localStorage.getItem('packet')) {
                    let packet = JSON.parse(localStorage.getItem('packet'))
                    this.name="-"+packet.money
                    this.nameText = packet.packet_name
                    this.list.packet_id = packet.id
                }
                this.stockDetail_tobuy();
                this.account_money()
                
                this.moneyone = this.money
                this.list.credit = this.moneyone
            },
            methods: {
                // 选择金额
                inputs(val,e){
                    if($(e.target)[0].value==''){
                        this.list.surplus_value =  (this.nowPri *1.2).toFixed(2)
                        this.list.stock_number =0
                        this.loss = "00.00"
                        this.list.loss_value ="0.00"
                        this.moneyone = 0*10000;
                        return false;
                    }
                    this.moneyone = $(e.target)[0].value*1000;
                    this.list.credit = this.moneyone
                    let nums = this.bei[$(".play-bei li.on").index()]
                    this.tuijian= (this.moneyone*nums);
                    this.list.double_value = this.tuijian;
                    this.algorithm()
                },
                // 选择倍数
                changes(item,e){
                    e.preventDefault();
                    if($(e.currentTarget).hasClass("on")){
                    }else{
                        
                        $(e.currentTarget).addClass("on").siblings().removeClass("on")
                        this.tuijian= $(e.currentTarget).find("span").text()*10000;
                        this.list.double_value = this.tuijian;
                        this.algorithm()
                    }
                },
                // 盈利减
                profitLess(e){
                  this.list.surplus_value=parseFloat(this.list.surplus_value);                 
                  this.nowPri=parseFloat(this.nowPri);

                    if(this.list.surplus_value>this.nowPri){
                        this.list.surplus_value= (this.list.surplus_value-0.01).toFixed(2);
                        $(".play-loss li.red div span").css("color","#d63834")
                      	this.proNum = (((this.list.surplus_value - this.nowPri)/this.nowPri)*100).toFixed(2)
                    }else{
                      	$(e.currentTarget).css("color","#999")
                        return false;
                        
                    }
                },
                // 盈利加
                profitAdd(e){
                    let num = (this.nowPri *1.5).toFixed(2)
                    if(this.list.surplus_value<num){
                        this.list.surplus_value= (parseFloat(this.list.surplus_value) + 0.01).toFixed(2)
                        $(".play-loss li.red div span").css("color","#d63834")
                      this.proNum = (((this.list.surplus_value - this.nowPri)/this.nowPri)*100).toFixed(2)
                    }else{
                        $(e.currentTarget).css("color","#999")
                        return false;
                    }
                },
                // 亏损减
                lossLess(e){
                    //let less = ((100 - this.loss)* this.nowPri/100).toFixed(2);
                  //console.log((100 - this.loss)* this.nowPri)
                    if(this.list.loss_value>this.lossNum){
                        $(".play-loss li:last-child div span").css("color","#5db12a")
                        this.list.loss_value= (this.list.loss_value-0.01).toFixed(2);
                        this.loss = (((this.nowPri - this.list.loss_value)/this.nowPri)*100).toFixed(2)
                    }else{
                        $(e.currentTarget).css("color","#999")
                        return false;
                    }
                },
                // 亏损加
                lossAdd(e){
                    if(this.list.loss_value<this.nowPri){
                        $(".play-loss li:last-child div span").css("color","#5db12a")
                        this.list.loss_value= (parseFloat(this.list.loss_value) + 0.01).toFixed(2);
                      	this.loss = (((this.nowPri - this.list.loss_value)/this.nowPri)*100).toFixed(2)
                    }else{
                        $(e.currentTarget).css("color","#999")
                        return false;
                    }
                },
                // 自动
                switchd(e){
                    if($(e.currentTarget).hasClass("on")){
                        $(e.currentTarget).removeClass("on")
                        this.list.defer = 2
                    }else{
                        $(e.currentTarget).addClass("on")
                        this.list.defer = 1
                    }
                    
                },
                // 创建策略
                submit(e){
                    if (!$(".ploy-bottom h2").hasClass("on")|| !$(".play-bei li").hasClass("on")|| $("#moneys").val()=='') {
                        alert("请阅读相关协议并选择推荐金额！")
                        return false;
                    }
                    if($(e.currentTarget).hasClass("on")){
                        return false;
                    }
                    $(e.currentTarget).addClass("on");
                    var md = $(e.currentTarget)
                    this.list.market_value = (this.list.stock_number * this.nowPri).toFixed(2)
                    let _this = this;
                        axios.post('/api/mobile/createStrategy',Object.assign(_this.list,account))
                        .then(function (res) {
                            if(res.data.status =='1'){
                                submitsuccess("正在为您撮合中...")
                                setTimeout(() => {
                                    submitsuccess("恭喜您撮合成功")
                                }, 3000);
                                setTimeout(() => {
                                    sessionStorage.removeItem('packet');
                                    md.removeClass("on")
                                    window.location.href="/index/trade/index.html"
                                }, 4000);
                            }else{
                                md.removeClass("on")
                                submitsuccess(res.data.msg)
                            }
                        })
                        .catch(function (error) {
                            console.log(error);
                        });
                },
                // 详细信息
                stockDetail_tobuy(){
                        let _this = this;
                        axios.post('/api/mobile/stockDetail_tobuy',Object.assign(account,{stock_code:getSearchString('stock_code')}))
                        .then(function (res) {
                            if(res.data.status =='200'){
                                _this.nowPri = res.data.list.nowPri
                                _this.list.buy_price = res.data.list.nowPri
                                _this.hide()
                                
                            }
                        })
                        .catch(function (error) {
                            console.log(error);
                        });
                    },
                // 倍数
                bs(){
                    let _this = this;
                    axios.post('/api/mobile/bs')
                    .then(function (res) {
                        if(res.data.status =='1'){
                            let bsd = []
                            for (let index = 0; index < res.data.list.times.length; index++) {
                                bsd.push(res.data.list.times[index])
                            }
                            _this.bei =bsd;
                            setTimeout(function(){
                                $(".play-bei li").eq(_this.bei.length-1).addClass("on")
                                _this.tuijian = $(".play-bei li").eq(_this.bei.length-1).find("span").text()*10000;
                                _this.list.double_value = _this.tuijian;
                                _this.poundage = res.data.list.poundage
                         		_this.defer = res.data.list.defer
                                _this.algorithm()
                            },10)
                        }
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
                },
                // 算法
                algorithm(){
                    this.list.surplus_value =  (this.nowPri *1.2).toFixed(2)
                    this.list.stock_number =(this.tuijian / this.nowPri) - ((this.tuijian / this.nowPri) % 100) ;
                    if((this.list.stock_number * this.nowPri) =='0'){
                        this.loss = 0
                        this.list.loss_value ="0.00"
                    }else{
                        this.loss_value =((this.list.stock_number * this.nowPri - this.moneyone*this.allow_kuisun)/this.list.stock_number).toFixed(2);
                        this.loss = (((this.nowPri - this.loss_value)/this.nowPri)*100).toFixed(2)
                    }
                    if(this.list.stock_number==0){
                        this.list.loss_value ="0.00"
                    }else{
                      this.list.loss_value =((this.list.stock_number * this.nowPri - this.moneyone*this.allow_kuisun)/this.list.stock_number).toFixed(2);
                    }
                  this.lossNum = ((this.list.stock_number * this.nowPri - this.moneyone*this.allow_kuisun)/this.list.stock_number).toFixed(2);
                    //this.list.loss_value =( (100 - this.loss)*this.nowPri/100).toFixed(2)
                    this.list.buy_poundage = this.tuijian*this.poundage
                },
                account_money(){
                    let _this = this;
                    axios.post('/api/mobile/account_money',account)
                    .then(function (res) {
                        if(res.data.status =='1'){
                            _this.tactics_balance = res.data.list.tactics_balance
                        }
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
                },
                hide(){
                    let _this = this;
                    axios.post('/api/mobile/hide',account)
                    .then(function (res) {
                        if(res.data.status =='1'){
                            _this.allow_kuisun = res.data.list.allow_kuisun
                            _this.bs()
                        }
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
                }
            }
        })
        vm.title= msd.stock_name+msd.stock_code
        vm.list = Object.assign(vm.list,msd)
        </script>
        <!-- <script>
            // 初始化
            var vConsole = new VConsole();
            console.log('Hello world');
          </script> -->
    </body>
</html>