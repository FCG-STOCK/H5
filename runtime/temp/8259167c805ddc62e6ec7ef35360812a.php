<?php if (!defined('THINK_PATH')) exit(); /*a:3:{s:75:"/wwwroot/h5.hdcelue.com/application/index/view/center/personal_account.html";i:1546509430;s:68:"/wwwroot/h5.hdcelue.com/application/index/view/index/inc/header.html";i:1546673568;s:68:"/wwwroot/h5.hdcelue.com/application/index/view/index/inc/footer.html";i:1546598458;}*/ ?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta http-equiv="content-language" content="zh-CN" />
        <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no" />
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black" />
        <meta name="format-detection" content="telephone=no" />
        <meta name="keywords" content="" />
        <meta name="description" content="" />
        <meta name="author" content="令克网络-高端网站建设-https://www.link-web.cn/" />
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
        <title>恒达资讯H5</title>
        <link href="/public/static/img/favicon.ico" rel="shortcut icon">
        <link rel="stylesheet" href="/public/static/css/reset.css">
        <link rel="stylesheet" href="/public/static/css/style.css">
        <script src="/public/static/js/jquery-1.11.3.js"></script>
        <script src="/public/static/js/html5.js"></script>
        <script src="/public/static/js/adaptive-version2.js"></script>
        <script src="/public/static/js/jquery.easing.1.3.js"></script>
        <script src="/public/static/js/jquery.transit.js"></script>
        <script src="/public/static/js/jquery.lazyload.js"></script>
    </head>
    <body>
        <!-- <div class="includeDom" include="inc/header.html" data-intro="index"></div> -->
        <!-- <div class="includeDom" data-intro="index">
            <header class="f-cb">
    <form action="###" method="get" accept-charset="utf-8">
        <input type="text" name="" placeholder="搜索股票代码/拼音首字母" id="search">
    </form>
    <a href="/index/index/information.html" class="info-links active">
        <span class="red-box"></span>
    </a>
    <ul class="search-over" id="header-search">
        <li><a href="javascript:;">sz000001</a></li>
        <li><a href="javascript:;">sz000002</a></li>
    </ul>
</header>
<div class="loading">
    <img src="/public/static/img/logo.png" alt="" class="logo">
</div>
<script>
    $(function(){
        jQuery(document).ready(function() {

            $('.loading').addClass("active");
        })
        var intro = $('header').parent(".includeDom").data('intro');
        if (intro == "scroll") {
            $(window).load(function() {
                var heih = $(".banner").height();
                $(window).scroll(function () {
                    if($(window).scrollTop() > heih){
                        $('header').addClass('active')
                    }else{
                        $('header').removeClass('active')
                    }
                })
            });
        }else{
            $('header').addClass('active')
        }



        $("#search").focus(function(){

            window.location.href='/index/strategy/search_stock';

        })




        //搜索
        // $("#search").keyup(function(){
        //     $('#header-search').fadeIn(500);
        //     setTimeout(function(){
        //         $('#header-search li').each(function(){
        //             var index = $(this).index();
        //             $(this).delay(index *100).animate({left:0,opacity:1},300);
        //         })
        //     },300)

        // })
    })
</script>
        </div> -->
        <div class="content-box data">
            <div class="personal-title">我的账户</div>
            <div class="account-top">
                <div class="w94">
                    <h2>恒达资讯钱包</h2>
                    <div class="center f-cb">
                        <h1><?php echo $res['balance']; ?></h1>
                        <div class="fr links">
                            <a href="personal_cash.html">提现</a>
                            <a href="personal_recharge.html">充值</a>
                        </div>
                    </div>
                    <p>钱包中的资金需转入策略余额才能使用</p>
                </div>
            </div>
            <div class="account-center">
                <div class="w94 f-cb">
                    <div class="fl">
                        <h2>策略余额</h2>
                        <p><?php echo $res['tactics_balance']; ?></p>
                        <div class="links f-cb">
                            <a href="personal_into.html">转入</a>
                            <a href="personal_out.html">转出</a>
                        </div>
                    </div>
                    <div class="fr bb">
                        <h2>冻结信用金</h2>
                        <p><?php echo $res['frozen_money']; ?></p>
                    </div>
                </div>
            </div>
            <div class="personal-data-list">
                <a href="personal_water.html">钱包流水 <span class="fr tss jt"></span></a>
                <a href="personal_celuewater.html">策略交易流水 <span class="fr tss jt"></span></a>
            </div>
        </div>
        <!-- <div class="includeDom" include="inc/footer.html" data-id="3"></div> -->
        <div class="includeDom"  data-id="3">
            <footer>
    <ul class="f-cb">
        <li>
            <a href="/index/index/index.html">
                <div class="pic-box">
                    <img src="/public/static/img/footer01.png" alt="" class="bg">
                    <img src="/public/static/img/footer01_h.png" alt="" class="pic">
                </div>
                <h2>首页</h2>
            </a>
        </li>
        <li>
            <a href="/index/strategy/strategy.html">
                <div class="pic-box">
                    <img src="/public/static/img/footer02.png" alt="" class="bg">
                    <img src="/public/static/img/footer02_h.png" alt="" class="pic">
                </div>
                <h2>策略</h2>
            </a>
        </li>
        <li>
            <a href="/index/ranking/rankingList.html">
                <div class="pic-box">
                    <img src="/public/static/img/footer03.png" alt="" class="bg">
                    <img src="/public/static/img/footer03_h.png" alt="" class="pic">
                </div>
                <h2>排行</h2>
            </a>
        </li>
        <li>
            <a href="/index/center/personal.html">
                <div class="pic-box">
                    <img src="/public/static/img/footer04.png" alt="" class="bg">
                    <img src="/public/static/img/footer04_h.png" alt="" class="pic">
                </div>
                <h2>我的</h2>
            </a>
        </li>
    </ul>
</footer>
<script>
    var id = $('footer').parent(".includeDom").data('id');
    $('footer li').eq(id).addClass('hover');
</script>
        </div>
        <script src="/public/static/js/main.js"></script>
    </body>
</html>