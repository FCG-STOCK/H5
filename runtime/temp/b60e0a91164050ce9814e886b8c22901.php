<?php if (!defined('THINK_PATH')) exit(); /*a:3:{s:54:"/home/stock/h5/application/index/view/price/index.html";i:1558595580;s:59:"/home/stock/h5/application/index/view/price/inc/header.html";i:1554779654;s:59:"/home/stock/h5/application/index/view/index/inc/footer.html";i:1554779792;}*/ ?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta http-equiv="content-language" content="zh-CN" />
        <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no" />
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black" />
        <meta name="format-detection" content="telephone=no" />
        <meta name="keywords" content="" />
        <meta name="description" content="" />
         
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
        <title>策略</title>
        <link href="/public/static/img/favicon.ico" rel="shortcut icon">
        <link rel="stylesheet" href="/public/static/css/reset.css">
        <link rel="stylesheet" href="/public/static/css/style.css">
        <link rel="stylesheet" href="/public/static/css/price.css">
        <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
        <script src="/public/static/js/vue.js"></script>
        <script src="/public/static/js/jquery-1.11.3.js"></script>
        <script src="/public/static/js/html5.js"></script>
        <script src="/public/static/js/adaptive-version2.js"></script>
        <script src="/public/static/js/jquery.easing.1.3.js"></script>
        <script src="/public/static/js/jquery.transit.js"></script>
        <script src="/public/static/js/jquery.lazyload.js"></script>
    </head>
    <body>
        <!-- <div class="includeDom" include="inc/footer.html" data-id="3"></div> -->
        <div class="includeDom"  data-id="1">
            <link rel="stylesheet" href="/public/static/awesome/css/font-awesome.css">
<script src="/public/static/js/main.js"></script>
<header class="price-header">
	<a href="javascript:history.back(-1);" class="back"><i class="fa fa-angle-left"></i></a>
    <ul class="f-cb">
       <li><a href="/index/price/quotes.html">行情</a></li>
        <li><a href="/index/price/index.html">自选</a></li>
    </ul>
    <div class="price-search" onclick="window.location.href='/index/index/optional_search.html'"></div>
</header>
<script>
    var id = $('header').parent(".includeDom").data('id');
    $('header li').eq(id).addClass('hover');
    
</script>

        </div>
        <div id="app">
            <div class="welcome-list">
                <ul class="f-cb">
                    <li>
                        <h2>上证指数</h2>
                        <h3><span class="ico red" id="shang_nowPoint">--</span></h3>
                        <div class="f-cb">
                            <span class="fl num red" id="shang_nowPrice">--</span>
                            <span class="fr num red" id="shang_diff_rate">--</span>
                        </div>
                    </li>
                    <li>
                        <h2>深证指数</h2>
                        <h3><span class="ico red" id="shen_nowPoint">--</span></h3>
                        <div class="f-cb">
                            <span class="fl num red" id="shen_nowPrice">--</span>
                            <span class="fr num red" id="shen_diff_rate">--</span>
                        </div>
                    </li>
                    <li>
                        <h2>创业板指</h2>
                        <h3><span class="ico red" id="chuang_nowPoint">--</span></h3>
                        <div class="f-cb">
                            <span class="fl num red" id="chuang_nowPrice">--</span>
                            <span class="fr num red" id="chuang_diff_rate">--</span>
                        </div>
                    </li>
                </ul>
            </div>
            <div >
                <div v-if="list.length==0" class="w94 node">
                    <div class="zanwu"></div>
                </div>
                <div v-else class="price-table">
                    <div class="table-top">
                        <p>
                            <span>股票</span>
                            <span>当前价</span>
                            <span>涨跌幅</span>
                            <span>1</span>
                        </p>
                    </div>
                    <div class="table-work">
                        <p v-for="(item,index) in list" :key="index" v-cloak>
                            <a :href="'/index/price/info.html?stock_code='+item.stock_code+'&stock_name='+item.stock_name" v-cloak>
                                <span>
                                    <i v-cloak>{{item.stock_name}}</i>
                                    <span v-cloak>{{item.stock_code}}</span>
                                </span>
                                <span v-cloak :class="item.rate_price < '0'?'green':'red'">{{item.nowprice}}</span>
                                <span v-cloak :class="item.rate_price < '0'?'green':'red'">{{item.rate}}</span>
                            </a>
                            <span>
                                    <a class="del" href="javascript:;" @click="del(item.stock_code)"></a>
                            </span>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="submit-success on"></div>
        <div class="includeDom"  data-id="1">
            <footer>
    <ul class="f-cb">
        <!-- <li>
            <a href="/index/index/index.html">
                <div class="pic-box">
                    <img src="/public/static/img/footer01.png" alt="" class="bg">
                    <img src="/public/static/img/footer01_h.png" alt="" class="pic">
                </div>
                <h2>首页</h2>
            </a>
        </li>
        <li>
            <a href="/index/strategy/strategy.html">
                <div class="pic-box">
                    <img src="/public/static/img/footer02.png" alt="" class="bg">
                    <img src="/public/static/img/footer02_h.png" alt="" class="pic">
                </div>
                <h2>策略</h2>
            </a>
        </li>
        <li>
            <a href="/index/ranking/rankingList.html">
                <div class="pic-box">
                    <img src="/public/static/img/footer03.png" alt="" class="bg">
                    <img src="/public/static/img/footer03_h.png" alt="" class="pic">
                </div>
                <h2>排行</h2>
            </a>
        </li>
        <li>
            <a href="/index/center/personal.html">
                <div class="pic-box">
                    <img src="/public/static/img/footer04.png" alt="" class="bg">
                    <img src="/public/static/img/footer04_h.png" alt="" class="pic">
                </div>
                <h2>我的</h2>
            </a>
        </li> -->
        <li>
            <a href="/index/index/index.html">
                <div class="pic-box">
                    <img src="/public/static/img/img_7.png" alt="" class="bg">
                    <img src="/public/static/img/img_8.png" alt="" class="pic">
                </div>
                <h2>首页</h2>
            </a>
        </li>
        <li>
            <a href="/index/price/quotes.html">
                <div class="pic-box">
                    <img src="/public/static/img/img_9.png" alt="" class="bg">
                    <img src="/public/static/img/img_10.png" alt="" class="pic">
                </div>
                <h2>行情</h2>
            </a>
        </li>
        <li>
            <a href="/index/trade/index.html">
                <div class="pic-box">
                    <img src="/public/static/img/img_11.png" alt="" class="bg">
                    <img src="/public/static/img/img_12.png" alt="" class="pic">
                </div>
                <h2>交易</h2>
            </a>
        </li>
        <li>
            <a href="/index/news/index.html">
                <div class="pic-box">
                    <img src="/public/static/img/img_13.png" alt="" class="bg">
                    <img src="/public/static/img/img_14.png" alt="" class="pic">
                </div>
                <h2>资讯</h2>
            </a>
        </li>
        <li>
            <a href="/index/center/personal.html">
                <div class="pic-box">
                    <img src="/public/static/img/img_15.png" alt="" class="bg">
                    <img src="/public/static/img/img_16.png" alt="" class="pic">
                </div>
                <h2>我的</h2>
            </a>
        </li>
    </ul>
    <div class="loading">
            <div class="loadings">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div> 
        <!-- <img src="/public/static/img/logo.png" alt="" class="logo"> -->
    </div>
</footer>
<script>
    var id = $('footer').parent(".includeDom").data('id');
    $('footer li').eq(id).addClass('hover');
</script>
        </div>

        <script src="/public/static/js/main.js"></script>
        <script type="text/javascript">
        
            reflash();

            //首页大盘股指实时行情_批量  5秒刷新一次
            //执行ajax
            function reflash(){                
                $.ajax({
                    url: "/api/index/grailindex",
                    dataType: "json",
                    async:true,
                    success: function relash(data) {
                        // alert('ajax调用成功');
                        var dataObj = JSON.parse(data);
                        if(dataObj.status==1){
                            var res = dataObj.list.showapi_res_body;
                            //上证指数
                            var shang_nowPoint = (parseFloat(res.indexList[0].nowPoint)).toFixed(2);
                            var shang_nowPrice = (parseFloat(res.indexList[0].nowPrice)).toFixed(2);
                            var shang_diff_rate = (parseFloat(res.indexList[0].diff_rate)).toFixed(2);
                            if(shang_diff_rate<0){
                                $("#shang_nowPoint").html(shang_nowPoint);
                                $("#shang_nowPoint").removeClass("red");
                                $("#shang_nowPoint").addClass("green");

                                $("#shang_nowPrice").html(shang_nowPrice);
                                $("#shang_nowPrice").removeClass("red");
                                $("#shang_nowPrice").addClass("green");

                                $("#shang_diff_rate").html(shang_diff_rate+"%");
                                $("#shang_diff_rate").removeClass("red");
                                $("#shang_diff_rate").addClass("green");

                            }else{
                                $("#shang_nowPoint").html(shang_nowPoint);
                                $("#shang_nowPrice").html(shang_nowPrice);
                                $("#shang_diff_rate").html(shang_diff_rate+"%");
                            }
                            

                            //深圳指数
                            var shen_nowPoint = (parseFloat(res.indexList[1].nowPoint)).toFixed(2);
                            var shen_nowPrice =(parseFloat(res.indexList[1].nowPrice)).toFixed(2);
                            var shen_diff_rate = (parseFloat(res.indexList[1].diff_rate)).toFixed(2);

                            if(shen_diff_rate<0){
                                $("#shen_nowPoint").html(shen_nowPoint);
                                $("#shen_nowPoint").removeClass("red");
                                $("#shen_nowPoint").addClass("green");

                                $("#shen_nowPrice").html(shen_nowPrice);
                                $("#shen_nowPrice").removeClass("red");
                                $("#shen_nowPrice").addClass("green");
                                
                                $("#shen_diff_rate").html(shang_diff_rate+"%");
                                $("#shen_diff_rate").removeClass("red");
                                $("#shen_diff_rate").addClass("green");

                            }else{
                                $("#shen_nowPoint").html(shen_nowPoint);
                                $("#shen_nowPrice").html(shen_nowPrice);
                                $("#shen_diff_rate").html(shen_diff_rate+"%");
                            }

                            //创业板
                            var chuang_nowPoint = (parseFloat(res.indexList[2].nowPoint)).toFixed(2);
                            var chuang_nowPrice = (parseFloat(res.indexList[2].nowPrice)).toFixed(2);
                            var chuang_diff_rate = (parseFloat(res.indexList[2].diff_rate)).toFixed(2);
                            if(chuang_diff_rate<0){
                                $("#chuang_nowPoint").html(chuang_nowPoint);
                                $("#chuang_nowPoint").removeClass("red");
                                $("#chuang_nowPoint").addClass("green");

                                $("#chuang_nowPrice").html(chuang_nowPrice);
                                $("#chuang_nowPrice").removeClass("red");
                                $("#chuang_nowPrice").addClass("green");
                                
                                $("#chuang_diff_rate").html(chuang_diff_rate+"%");
                                $("#chuang_diff_rate").removeClass("red");
                                $("#chuang_diff_rate").addClass("green");

                            }else{
                                $("#chuang_nowPoint").html(chuang_nowPoint);
                                $("#chuang_nowPrice").html(chuang_nowPrice);
                                $("#chuang_diff_rate").html(chuang_diff_rate+"%");
                            }
                        }else{
                            alert("获取数据失败");
                        }
                    }
                })
                    
                /* 等待五秒后再次请求数据 */
                window.setTimeout("reflash()", 5000);
            }

        </script>
        <script>
        new Vue({
            el:'#app',
            data() {
                return {
                    list:[]
                };
            },
            created() {
            },
            mounted() {
                this.myChooseStock()
            },
            methods: {
                // 查看我的自选
                myChooseStock(){
                    let _this = this;
                    axios.post('/api/mobile/myChooseStock',account)
                    .then(function (res) {
                       if(res.data.status =='1'){
                            _this.list = res.data.list
                       }
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
                },
                // 删除某个我的自选
                del(stock_code){
                    let _this = this;
                    axios.post('/api/mobile/deleteChooseStock',Object.assign(account,{stock_code:stock_code}))
                    .then(function (res) {
                       if(res.data.status =='1'){
                            submitsuccess("删除成功！")
                            // _this.list = res.data.list
                            _this.myChooseStock()
                       }
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
                }
            }
        })
        </script>
    </body>
</html>
