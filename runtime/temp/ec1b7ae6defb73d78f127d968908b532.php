<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:60:"D:\xampp\htdocs\peiqi/application/index\view\trade\info.html";i:1552008389;s:66:"D:\xampp\htdocs\peiqi\application\index\view\index\inc\footer.html";i:1552012658;}*/ ?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta http-equiv="content-language" content="zh-CN" />
        <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no" />
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black" />
        <meta name="format-detection" content="telephone=no" />
        <meta name="keywords" content="" />
        <meta name="description" content="" />
        <meta name="author" content="令克网络-高端网站建设-https://www.link-web.cn/" />
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
        <title>赢在策略</title>
        <link href="/public/static/img/favicon.ico" rel="shortcut icon">
        <link rel="stylesheet" href="/public/static/css/reset.css">
        <link rel="stylesheet" href="/public/static/css/style.css">
        <link rel="stylesheet" href="/public/static/css/trade.css">
        <script src="/public/static/js/jquery-1.11.3.js"></script>
        <script src="/public/static/js/html5.js"></script>
        <script src="/public/static/js/adaptive-version2.js"></script>
        <script src="/public/static/js/jquery.easing.1.3.js"></script>
        <script src="/public/static/js/jquery.transit.js"></script>
        <script src="/public/static/js/jquery.lazyload.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/vue@2.6.8/dist/vue.js"></script>
    </head>
    <body>
        <div id="app">
            <div class="guide-top m0 boxs"> 
                <a href="javascript:history.back(-1);"></a>
                个人战绩
            </div>
            <div class="rank-info-header">
                <img src="/public/static/img/img_35.png" alt="">
                <p>31110191 <span>排行：1</span></p>
                <!-- <a href="javascript:;"></a> -->
                <a href="javascript:;" class="on"></a>
            </div>
            <div class="rank-info-poa">
                <div>
                    <h2>19.41% <span>总收益</span></h2>
                    <ul>
                        <li>日：<span>0%</span></li>
                        <li>周：<span>0%</span></li>
                        <li>月：<span>19.41%</span></li>
                    </ul>
                </div>
            </div>
            <ul class="rank-top rank-info-top">
                <li v-for="(item,index) in rank" @click="rankChange(item,$event)" :class="index==0?'on':''">{{item.name}}</li>
            </ul>
            <ul class="rank-info-list">
                <li>
                    <div class="rank_info_list_top">
                        <span >亏</span>
                        <div>
                            <h2>美的集团</h2>
                            <p>(SZ000333)</p>
                        </div>
                        <a href="/index/price/ploy.html">
                                跟买
                        </a>
                    </div>
                    <div class="rank_info_bottom">
                        <div class="f-cb">
                            <p>浮动盈亏<span class="green">231.00</span></p>
                            <p>买入价<span>25.02</span></p>
                        </div>
                        <div class="f-cb">
                            <p>交易股数<span>700</span></p>
                            <p>卖出价<span>25.35</span></p>
                        </div>
                        <p>买入时间<span>2019-01-08 13:46</span></p>
                        <p>卖出时间<span>2019-01-09 13:46</span></p>
                    </div>
                </li>
                <li>
                    <div class="rank_info_list_top">
                        <span class="on">亏</span>
                        <div>
                            <h2>美的集团</h2>
                            <p>(SZ000333)</p>
                        </div>
                        <a href="/index/price/ploy.html">
                                跟买
                        </a>
                    </div>
                    <div class="rank_info_bottom">
                        <div class="f-cb">
                            <p>浮动盈亏<span class="red">231.00</span></p>
                            <p>买入价<span>25.02</span></p>
                        </div>
                        <div class="f-cb">
                            <p>交易股数<span>700</span></p>
                            <p>卖出价<span>25.35</span></p>
                        </div>
                        <p>买入时间<span>2019-01-08 13:46</span></p>
                        <p>卖出时间<span>2019-01-09 13:46</span></p>
                    </div>
                </li>
            </ul>
        </div>
        <div style="height:1.2rem"></div>
        <div class="includeDom"  data-id="2">
            <footer>
    <ul class="f-cb">
        <!-- <li>
            <a href="/index/index/index.html">
                <div class="pic-box">
                    <img src="/public/static/img/footer01.png" alt="" class="bg">
                    <img src="/public/static/img/footer01_h.png" alt="" class="pic">
                </div>
                <h2>首页</h2>
            </a>
        </li>
        <li>
            <a href="/index/strategy/strategy.html">
                <div class="pic-box">
                    <img src="/public/static/img/footer02.png" alt="" class="bg">
                    <img src="/public/static/img/footer02_h.png" alt="" class="pic">
                </div>
                <h2>策略</h2>
            </a>
        </li>
        <li>
            <a href="/index/ranking/rankingList.html">
                <div class="pic-box">
                    <img src="/public/static/img/footer03.png" alt="" class="bg">
                    <img src="/public/static/img/footer03_h.png" alt="" class="pic">
                </div>
                <h2>排行</h2>
            </a>
        </li>
        <li>
            <a href="/index/center/personal.html">
                <div class="pic-box">
                    <img src="/public/static/img/footer04.png" alt="" class="bg">
                    <img src="/public/static/img/footer04_h.png" alt="" class="pic">
                </div>
                <h2>我的</h2>
            </a>
        </li> -->
        <li>
            <a href="/index/index/index.html">
                <div class="pic-box">
                    <img src="/public/static/img/img_7.png" alt="" class="bg">
                    <img src="/public/static/img/img_8.png" alt="" class="pic">
                </div>
                <h2>首页</h2>
            </a>
        </li>
        <li>
            <a href="/index/price/index.html">
                <div class="pic-box">
                    <img src="/public/static/img/img_9.png" alt="" class="bg">
                    <img src="/public/static/img/img_10.png" alt="" class="pic">
                </div>
                <h2>行情</h2>
            </a>
        </li>
        <li>
            <a href="/index/trade/index.html">
                <div class="pic-box">
                    <img src="/public/static/img/img_11.png" alt="" class="bg">
                    <img src="/public/static/img/img_12.png" alt="" class="pic">
                </div>
                <h2>交易</h2>
            </a>
        </li>
        <li>
            <a href="/index/news/index.html">
                <div class="pic-box">
                    <img src="/public/static/img/img_13.png" alt="" class="bg">
                    <img src="/public/static/img/img_14.png" alt="" class="pic">
                </div>
                <h2>社区</h2>
            </a>
        </li>
        <li>
            <a href="/index/center/personal.html">
                <div class="pic-box">
                    <img src="/public/static/img/img_15.png" alt="" class="bg">
                    <img src="/public/static/img/img_16.png" alt="" class="pic">
                </div>
                <h2>我的</h2>
            </a>
        </li>
    </ul>
    <div class="loading">
        <img src="/public/static/img/logo.png" alt="" class="logo">
    </div>
</footer>
<script>
    jQuery(document).ready(function() {
        $('.loading').addClass("active");
    })
    var id = $('footer').parent(".includeDom").data('id');
    $('footer li').eq(id).addClass('hover');
</script>
        </div>
        <div class="loading">
            <img src="/public/static/img/logo.png" alt="" class="logo">
        </div>
        <script src="/public/static/js/main.js"></script>
        <script>
        jQuery(document).ready(function() {
            $('.loading').addClass("active");
        })
        </script>
        <script>
        new Vue({
            el:'#app',
            data() {
                return {
                    rank:[
                        {
                            name:"持仓",
                            url:"1111111"
                        },
                        {
                            name:"记录",
                            url:"1111111"
                        },
                    ]
                
                };
            },
            mounted() {
            },
            methods: {
                rankChange(item,e){
                    if (!$(e.target).hasClass("on")) {
                        $(e.target).addClass("on").siblings("li").removeClass("on")
                    }
                }
            }
        })
        </script>
    </body>
</html>