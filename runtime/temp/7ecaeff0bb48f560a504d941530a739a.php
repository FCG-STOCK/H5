<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:94:"/www/wwwroot/celue2.0.h5.solingke.cn/application/index/view/center/personal_rechargeapply.html";i:1557999094;s:81:"/www/wwwroot/celue2.0.h5.solingke.cn/application/index/view/index/inc/footer.html";i:1554779792;}*/ ?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta http-equiv="content-language" content="zh-CN" />
        <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no" />
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black" />
        <meta name="format-detection" content="telephone=no" />
        <meta name="keywords" content="" />
        <meta name="description" content="" />
         
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
        <title>充值</title>
        <link href="/public/static/img/favicon.ico" rel="shortcut icon">
    	<link rel="stylesheet" href="/public/static/css/reset.css">
    	<link rel="stylesheet" href="/public/static/css/style.css">
      <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
        <script src="/public/static/js/clipboard.min.js"></script>
        <link rel="stylesheet" href="/public/static/awesome/css/font-awesome.css">
        <script src="/public/static/js/jquery-1.11.3.js"></script>
        <script src="/public/static/js/html5.js"></script>
        <script src="/public/static/js/adaptive-version2.js"></script>
        <script src="/public/static/js/jquery.easing.1.3.js"></script>
        <script src="/public/static/js/jquery.transit.js"></script>
        <script src="/public/static/js/jquery.lazyload.js"></script>
    </head>
    <body>
        <div class="content-box" id="app">
          <div class=" guide-top ">
            <a href="javascript:history.back(-1);" class="back"><i class="fa fa-angle-left"></i></a>
            <span class="data-fonts">{{title}}充值</span>
          </div>
          <div class="rechangecard f-cb">
              <div class="top f-cb">
                  <h2>打开{{title}}扫码支付</h2>
                  <div class="apply-picture">
                      <img :src="erpic" alt="" />
                  </div>
              </div>
              <div class="bot">
                  <h2>支付成功后填写下方信息</h2>
                  <label class="f-cb">
                    <span class="tit">付款账号</span>
                    <input type="text" name="" value="" placeholder="请输入你的付款账号" v-model="account">
                  </label>
                  <label class="f-cb">
                    <span class="tit">付款人</span>
                    <input type="text" name="" value="" placeholder="请输入付款方姓名" v-model="name">
                  </label>
                  <label class="f-cb">
                    <span class="tit">付款金额</span>
                    <input type="text" name="" value="" placeholder="请输入你的付款金额" v-model="price" oninput = "value=value.replace(/[^\d]/g,'')">
                  </label>
                  <label class="f-cb">
                    <span class="tit">手机号</span>
                    <span class="con">{{telphone}}</span>
                  </label>
                  <!--<label class="f-cb">
                    <span class="tit">验证码</span>
                    <input type="text" name="" value="" placeholder="请输入验证码" v-model="card">
                    <input type="button" @click="getcard" id="code" value="获取验证码">
                  </label>-->
                  <label class="file-b f-cb">
                      <span class="tit">上传凭证</span>
                      <div class="file-box">
                          <img :src="filepic" alt="" />
                          <input type="file" name="" @change="changepicture($event)">
                      </div>
                  </label>
                  <input type="submit" name="" value="确认提交" @click="submit">
                  <div class="ts-box">
                      <h3>关于转账到账时间说明：</h3>
                      <p>1、工作日17点30分之前转账的，承诺在资金到账后的半小时内完成充值；</p>
                      <p>2、工作日17点30分以后转账的，承诺在第二个工作日的早上9点完成充值；</p>
                      <p>3、非工作日转账的，承诺在下一工作日的早上9点完成充值；</p>
                      <p>4、请使用实名认证后对应姓名的银行卡进行转账；</p>
                      <p>5、如未能及时充值，请联系客服：**********。</p>
                  </div>
              </div>
          </div>
        </div>
        <div class="submit-success" style="display: none;"></div>
        <div class="includeDom"  data-id="4">
            <footer>
    <ul class="f-cb">
        <!-- <li>
            <a href="/index/index/index.html">
                <div class="pic-box">
                    <img src="/public/static/img/footer01.png" alt="" class="bg">
                    <img src="/public/static/img/footer01_h.png" alt="" class="pic">
                </div>
                <h2>首页</h2>
            </a>
        </li>
        <li>
            <a href="/index/strategy/strategy.html">
                <div class="pic-box">
                    <img src="/public/static/img/footer02.png" alt="" class="bg">
                    <img src="/public/static/img/footer02_h.png" alt="" class="pic">
                </div>
                <h2>策略</h2>
            </a>
        </li>
        <li>
            <a href="/index/ranking/rankingList.html">
                <div class="pic-box">
                    <img src="/public/static/img/footer03.png" alt="" class="bg">
                    <img src="/public/static/img/footer03_h.png" alt="" class="pic">
                </div>
                <h2>排行</h2>
            </a>
        </li>
        <li>
            <a href="/index/center/personal.html">
                <div class="pic-box">
                    <img src="/public/static/img/footer04.png" alt="" class="bg">
                    <img src="/public/static/img/footer04_h.png" alt="" class="pic">
                </div>
                <h2>我的</h2>
            </a>
        </li> -->
        <li>
            <a href="/index/index/index.html">
                <div class="pic-box">
                    <img src="/public/static/img/img_7.png" alt="" class="bg">
                    <img src="/public/static/img/img_8.png" alt="" class="pic">
                </div>
                <h2>首页</h2>
            </a>
        </li>
        <li>
            <a href="/index/price/quotes.html">
                <div class="pic-box">
                    <img src="/public/static/img/img_9.png" alt="" class="bg">
                    <img src="/public/static/img/img_10.png" alt="" class="pic">
                </div>
                <h2>行情</h2>
            </a>
        </li>
        <li>
            <a href="/index/trade/index.html">
                <div class="pic-box">
                    <img src="/public/static/img/img_11.png" alt="" class="bg">
                    <img src="/public/static/img/img_12.png" alt="" class="pic">
                </div>
                <h2>交易</h2>
            </a>
        </li>
        <li>
            <a href="/index/news/index.html">
                <div class="pic-box">
                    <img src="/public/static/img/img_13.png" alt="" class="bg">
                    <img src="/public/static/img/img_14.png" alt="" class="pic">
                </div>
                <h2>资讯</h2>
            </a>
        </li>
        <li>
            <a href="/index/center/personal.html">
                <div class="pic-box">
                    <img src="/public/static/img/img_15.png" alt="" class="bg">
                    <img src="/public/static/img/img_16.png" alt="" class="pic">
                </div>
                <h2>我的</h2>
            </a>
        </li>
    </ul>
    <div class="loading">
            <div class="loadings">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div> 
        <!-- <img src="/public/static/img/logo.png" alt="" class="logo"> -->
    </div>
</footer>
<script>
    var id = $('footer').parent(".includeDom").data('id');
    $('footer li').eq(id).addClass('hover');
</script>
        </div>
        <!--<script src="/public/h5static/js/main.js"></script>-->
      	<script src="/public/h5static/js/jquery.cookie.js"></script>
        <script type="text/javascript">
        $(function(){
          new Vue({
            el:'#app',
             data:{
                erpic:"",
                dates:'',
                title:"",
                telphone:"",
                filepic:"",
                coderefid:"",
                account:"",
                name:"",
                price:"",
                card:"",
                type:"",
             },
            mounted:function(){
              this.erwiema();
            },
            methods: {
              erwiema:function(){
                var _this = this;
                $.ajax({
                    type: "post",
                    url: baseUrl+"/index/pay/ewm_info",
                    success: function (res) {
                        res  =JSON.parse(res)
                        if(res.status=='1'){
                            function getUrlParam(name) {
                                var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"); //构造一个含有目标参数的正则表达式对象
                                var r = window.location.search.substr(1).match(reg);  //匹配目标参数
                                if (r != null) return unescape(r[2]); return null; //返回参数值
                            }

                            _this.telphone = $.cookie("username")
                            _this.type = getUrlParam("type");
                            if (_this.type == 1) {
                              _this.erpic = res.list.alipay_ewm;
                              _this.title = "支付宝";
                            }else{
                              _this.erpic = res.list.wx_ewm;
                              _this.title = "微信";
                            }
                        }

                    }

                });
              },
              changepicture:function(e){
                var _this = this;
                var box = $(e.target);
                var FileData=box[0].files[0];
                var form = new FormData();
                form.append('voucher[]',FileData);
                console.log(FileData);
                $.ajax({
                      contentType: false,    //不可缺
                       processData: false,    //不可缺
                      type: "post",
                      url: baseUrl+"/index/pay/upload_voucher",
                      data:form,
                      dataType: "json",
                      success: function (res) {
                          if(res.status=='1'){
                            _this.filepic = res.list
                          }

                      }

                  });
              },
              getcard:function(){
                var _this = this;
                $.ajax({
                      type: "post",
                      url: baseUrl+"/index/pay/sendMessage",
                      data:{
                        mobile:_this.telphone,
                      },
                      dataType: "json",
                      success: function (res) {
                          if(res.status=='1'){
                              _this.coderefid=res.list
                              var code = $("#code");
                              code.attr("disabled","disabled");
                              var time = 60;
                              var set=setInterval(function(){
                                  code.val(""+--time+"秒");
                              }, 1000);
                              console.log(time)
                              setTimeout(function(){
                                  code.attr("disabled",false).val("获取验证码");
                                  clearInterval(set);
                              }, 60000);
                          }

                      }

                  });
              },
              submit:function(){
                var _this = this;
                if (_this.account == "") {
                  alert("请输入你的付款账号");
                  return false;
                };

                if (_this.name == "") {
                  alert("请输入付款方姓名");
                  return false;
                };

                if (_this.price == "") {
                  alert("请输入你的付款金额");
                  return false;
                };

                //if (_this.card == "") {
                  //alert("请输入验证码");
                  //return false;
               // };

                if (_this.filepic == "") {
                  alert("请上传凭证");
                  return false;
                };
                $.ajax({
                      type: "post",
                      url: baseUrl+"/index/pay/comfire_recharge",
                      data:{
                        account:_this.telphone,
                        trade_price:_this.price,
                        detailed:_this.filepic,
                        pay_account:_this.account,
                        pay_username:_this.name,
                        trade_type:_this.type,
                        //yzm:_this.card,
                      },
                      dataType: "json",
                      success: function (res) {
                          if(res.status=='1'){
                              alert(res.msg);
                          }else{
                            alert(res.msg);
                          }

                      }

                  });
              }
            }

          })
           //复制指定的文字
                var clipboard = new Clipboard('.copy');

                clipboard.on('success', function(e) {
                    alert("复制成功")
                });
        })
        </script>
    </body>
</html>