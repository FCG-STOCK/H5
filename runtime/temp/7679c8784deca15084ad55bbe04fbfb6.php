<?php if (!defined('THINK_PATH')) exit(); /*a:3:{s:62:"D:\xampp\htdocs\peiqi/application/index\view\price\ploy_d.html";i:1551947782;s:70:"D:\xampp\htdocs\peiqi\application\index\view\price\inc\ployhrader.html";i:1551925380;s:66:"D:\xampp\htdocs\peiqi\application\index\view\index\inc\footer.html";i:1551927778;}*/ ?>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta http-equiv="content-language" content="zh-CN" />
        <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no" />
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black" />
        <meta name="format-detection" content="telephone=no" />
        <meta name="keywords" content="" />
        <meta name="description" content="" />
        <meta name="author" content="令克网络-互联网系统解决方案服务商-https://www.link-web.cn/" />
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
        <title>股票行情</title>
        <link href="/public/static/img/favicon.ico" rel="shortcut icon">
        <link rel="stylesheet" href="/public/static/css/reset.css">
        <link rel="stylesheet" href="/public/static/css/style.css">
        <link rel="stylesheet" href="/public/static/css/price.css">
        <script src="/public/static/js/jquery-1.11.3.js"></script>
        <script src="/public/static/js/html5.js"></script>
        <script src="/public/static/js/adaptive-version2.js"></script>
        <script src="/public/static/js/jquery.easing.1.3.js"></script>
        <script src="/public/static/js/jquery.transit.js"></script>
        <script src="/public/static/js/jquery.lazyload.js"></script>
        <style>.price-search{top:0.09rem}</style>
        <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    </head>
    <body>
        <div id="app">
            <div class="includeDom"  data-id='1'>
                <div class='por ploy-header'>
    <div class="guide-top">
        <a href="javascript:history.back(-1);"></a>
        {{title}}
        <div class="price-search" onclick="window.location.href='/index/index/optional_search.html'">切换</div>
    </div>
    <div class="ploy-list">
            策略
        <ul class="f-cb">
            <li><a href="/index/price/ploy.html">T+1</a></li>
            <li><a href="/index/price/ployD.html">T+D</a></li>
        </ul>
    </div>
</div>
            </div>
            <div class="m20">
                <div class="ploy-money">
                    投入信用金
                    <div>
                        <input type="tel" v-model="money" @blur="inputs(money)">万元
                    </div>
                    <p>请输入10000的整数倍</p>
                </div>
                <div class="play-bei">
                    <h2>推荐金额</h2>
                    <ul class='f-cb'>
                        <li v-for='item in bei' @click="changes(item,$event)">{{(item*money).toFixed(0)}}万元</li>
                    </ul>
                </div>
                <div class="play-num">
                        买入数量
                        <p>20000股(市值:194800，资金利用率:97.4%)</p>
                </div>
                <div class="play-loss">
                    <ul>
                        <li class="red">
                            止盈
                            <p>涨至 <span class="green">10%</span> 发起平仓</p>
                            <div class="f-cb">
                                <span @click="profitLess">-</span>
                                <input type="text" disabled v-model="profit">
                                <span @click="profitAdd">+</span>
                            </div>
                        </li>
                        <li>
                            止损
                            <p>下跌 <span class="green">10%</span> 发起平仓</p>
                            <div class="f-cb">
                                <span @click="lossLess">-</span>
                                <input type="text" disabled v-model="loss">
                                <span @click="lossAdd">+</span>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="m20 ">
                <div class="play-num">
                    持仓时间
                    <p>0天<span>(若未申请递延次日14:50分将自动卖出股票)</span></p>
                </div>
                <div class="play-num">
                    自动递延
                    <b @click="switchd($event)"></b>
                </div>
                <div class="play-num">
                    优惠
                    <p><a href="/index/center/personal_packet.html">{{name}}</a></p>
                </div>
            </div>
            <div class="m20">
                <div class="play-num">
                        预计可赚
                    <p>390.00元</p>
                </div>
                <div class="play-num">
                        服务费
                    <p>5.00元</p>
                </div>
                <div class="play-num">
                        合计
                    <p>1005.00元</p>
                </div>
                <div class="play-num">
                        余额
                    <p>0.00元</p>
                </div>
            </div>
            <div class="ploy-bottom">
                <h2>我已同意阅读并同意<a href="">《相关协议》</a></h2>
                <a href="javascript:;" @click="submit">创建策略</a>
            </div>
        </div>
        <div style="height:1.2rem"></div>
        <div class="includeDom"  data-id="1">
            <footer>
    <ul class="f-cb">
        <!-- <li>
            <a href="/index/index/index.html">
                <div class="pic-box">
                    <img src="/public/static/img/footer01.png" alt="" class="bg">
                    <img src="/public/static/img/footer01_h.png" alt="" class="pic">
                </div>
                <h2>首页</h2>
            </a>
        </li>
        <li>
            <a href="/index/strategy/strategy.html">
                <div class="pic-box">
                    <img src="/public/static/img/footer02.png" alt="" class="bg">
                    <img src="/public/static/img/footer02_h.png" alt="" class="pic">
                </div>
                <h2>策略</h2>
            </a>
        </li>
        <li>
            <a href="/index/ranking/rankingList.html">
                <div class="pic-box">
                    <img src="/public/static/img/footer03.png" alt="" class="bg">
                    <img src="/public/static/img/footer03_h.png" alt="" class="pic">
                </div>
                <h2>排行</h2>
            </a>
        </li>
        <li>
            <a href="/index/center/personal.html">
                <div class="pic-box">
                    <img src="/public/static/img/footer04.png" alt="" class="bg">
                    <img src="/public/static/img/footer04_h.png" alt="" class="pic">
                </div>
                <h2>我的</h2>
            </a>
        </li> -->
        <li>
            <a href="/index/index/index.html">
                <div class="pic-box">
                    <img src="/public/static/img/img_7.png" alt="" class="bg">
                    <img src="/public/static/img/img_8.png" alt="" class="pic">
                </div>
                <h2>首页</h2>
            </a>
        </li>
        <li>
            <a href="/index/price/index.html">
                <div class="pic-box">
                    <img src="/public/static/img/img_9.png" alt="" class="bg">
                    <img src="/public/static/img/img_10.png" alt="" class="pic">
                </div>
                <h2>行情</h2>
            </a>
        </li>
        <li>
            <a href="/index/trade/index.html">
                <div class="pic-box">
                    <img src="/public/static/img/img_11.png" alt="" class="bg">
                    <img src="/public/static/img/img_12.png" alt="" class="pic">
                </div>
                <h2>交易</h2>
            </a>
        </li>
        <li>
            <a href="">
                <div class="pic-box">
                    <img src="/public/static/img/img_13.png" alt="" class="bg">
                    <img src="/public/static/img/img_14.png" alt="" class="pic">
                </div>
                <h2>社区</h2>
            </a>
        </li>
        <li>
            <a href="/index/center/personal.html">
                <div class="pic-box">
                    <img src="/public/static/img/img_15.png" alt="" class="bg">
                    <img src="/public/static/img/img_16.png" alt="" class="pic">
                </div>
                <h2>我的</h2>
            </a>
        </li>
    </ul>
    <div class="loading">
        <img src="/public/static/img/logo.png" alt="" class="logo">
    </div>
</footer>
<script>
    jQuery(document).ready(function() {
        $('.loading').addClass("active");
    })
    var id = $('footer').parent(".includeDom").data('id');
    $('footer li').eq(id).addClass('hover');
</script>
        </div>
        <div class="loading">
            <img src="/public/static/img/logo.png" alt="" class="logo">
        </div>
        <script>
        jQuery(document).ready(function() {
            $('.loading').addClass("active");
        })
        </script>
        <script src="/public/static/js/main.js"></script>
        <script src="/public/static/js/jquery.cookie.js"></script>
        <script>
            var id = $('.ploy-header').parent(".includeDom").data('id');
            $('.ploy-list li').eq(id).addClass('hover');
            $(function(){
                $(".ploy-bottom h2").click(function () {
                    if (!$(this).hasClass("on")) {
                        $(this).addClass("on")
                    }else{
                        $(this).removeClass("on")
                    }
                })
            })
        new Vue({
        el:'#app',
            props: {
            },
            data() {
                return {
                    title:"美的集团SZ000333",
                    money:"1",
                    bei:[2,3,4,5,6,7,8,9],
                    loss:'13.27',
                    profit:'14.3',
                    name:""
                };
            },
            computed: {
            },
            created() {
            },
            mounted() {
                if (sessionStorage.getItem('packet')) {
                    let packet = JSON.parse(sessionStorage.getItem('packet'))
                    this.name=packet.money
                }
            },
            methods: {
                // 选择金额
                inputs(val){
                    if(val<1){
                        this.money="1"
                    }else if(val>200000){
                        this.money="200000"
                    }else if(val=="NaN"){
                        this.money="1"
                        // this.pzzj=0
                    }else{
                        this.money=(val - ((val)%1)) 
                    }
                },
                // 选择倍数
                changes(item,e){
                    e.preventDefault();
                    if($(e.currentTarget).hasClass("on")){
                        $(e.currentTarget).removeClass("on")
                        // this.pzzj = item*0;
                    }else{
                        $(e.currentTarget).addClass("on").siblings().removeClass("on")
                        // this.pzzj = item*(this.money)
                    }
                },
                // 盈利减
                profitLess(){
                    if(this.profit>(13.08)){
                        this.profit= (this.profit-0.01).toFixed(2)
                    }else{
                        return false;
                    }
                },
                // 盈利加
                profitAdd(){
                    let num =(13.08*1.1).toFixed(2);
                    if(this.profit<num){
                        this.profit= (parseFloat(this.profit) + 0.01).toFixed(2)
                    }else{
                        return false;
                    }
                },
                // 亏损减
                lossLess(){
                    if(this.loss>(13.08)){
                        this.loss= (this.loss-0.01).toFixed(2)
                    }else{
                        return false;
                    }
                },
                // 亏损加
                lossAdd(){
                    let num =(13.08*1.1).toFixed(2);
                    if(this.loss<num){
                        this.loss= (parseFloat(this.loss) + 0.01).toFixed(2)
                    }else{
                        return false;
                    }
                },
                // 自动
                switchd(e){
                    console.log(e)
                    if($(e.currentTarget).hasClass("on")){
                        $(e.currentTarget).removeClass("on")
                    }else{
                        $(e.currentTarget).addClass("on")
                    }
                    
                },
                // 创建策略
                submit(){
                    if (!$(".ploy-bottom h2").hasClass("on")|| !$(".play-bei li").hasClass("on")) {
                        alert("请阅读相关协议并选择推荐金额！")
                        return false;
                    }
                    window.location.href="/index/trade/index.html"
                }
            }
        })
        </script>


    </body>
</html>