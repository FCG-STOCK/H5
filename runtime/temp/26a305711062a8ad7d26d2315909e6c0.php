<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:66:"D:\xampp\htdocs\peiqi/application/index\view\trade\simulation.html";i:1552009023;s:66:"D:\xampp\htdocs\peiqi\application\index\view\index\inc\footer.html";i:1552012658;}*/ ?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta http-equiv="content-language" content="zh-CN" />
        <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no" />
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black" />
        <meta name="format-detection" content="telephone=no" />
        <meta name="keywords" content="" />
        <meta name="description" content="" />
        <meta name="author" content="令克网络-高端网站建设-https://www.link-web.cn/" />
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
        <title>赢在策略</title>
        <link href="/public/static/img/favicon.ico" rel="shortcut icon">
        <link rel="stylesheet" href="/public/static/css/reset.css">
        <link rel="stylesheet" href="/public/static/css/style.css">
        <link rel="stylesheet" href="/public/static/css/trade.css">
        <script src="/public/static/js/jquery-1.11.3.js"></script>
        <script src="/public/static/js/html5.js"></script>
        <script src="/public/static/js/adaptive-version2.js"></script>
        <script src="/public/static/js/jquery.easing.1.3.js"></script>
        <script src="/public/static/js/jquery.transit.js"></script>
        <script src="/public/static/js/jquery.lazyload.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    </head>
    <body>
        <div id="app">
                <div class="trader-header">
                        模拟
                        <a href="/index/trade/index.html">交易</a>
                    </div>
                    <div class="trade_money">
                        <div class="trade_money_add f-cb">
                            <div class="trad_m_add_left">
                                <p>总市值</p>
                                <h2>{{money}}</h2>
                            </div>
                            <a href="/index/trade/subdy.html">我的订阅</a>
                        </div>
                        <ul class="f-cb">
                            <li>
                                <h2>浮动盈亏</h2>
                                <p>0.00</p>
                            </li>
                            <li>
                                <h2>保证金</h2>
                                <p>0.00</p>
                            </li>
                            <li>
                                <h2>可用资金</h2>
                                <p>0.00</p>
                            </li>
                        </ul>
                    </div>
                    <ul class="trade-href ">
                        <li>
                            <a href="/index/index/optional_search_trade.html">
                                <img src="/public/static/img/img_27.png" alt="">
                                <p>创建策略</p>
                            </a>
                        </li>
                        <li>
                            <a href="">
                                <img src="/public/static/img/img_28.png" alt="">
                                <p>资金记录</p>
                            </a>
                        </li>
                        <li>
                            <a href="/index/trade/rank.html">
                                <img src="/public/static/img/img_33.png" alt="">
                                <p>高手排行</p>
                            </a>
                        </li>
                        <li>
                            <a href="/index/trade/experts.html">
                                <img src="/public/static/img/img_34.png" alt="">
                                <p>高手推荐</p>
                            </a>
                        </li>
                    </ul>
            <ul class="trade-coll ">
                <li class="on">持仓策略</li>
                <li>模拟记录</li>
            </ul>
            <div class="w94 trade-collspace">
                <ul>
                    <li>
                        <a href="javascript:;">
                            <h2>美的集团(SZ000333) <p>2018-06-22</p></h2>
                            <dl>
                                <dd>
                                    <span class="red">5.0</span>
                                    <i>浮动盈亏</i>
                                </dd>
                                <dd>
                                    <span>1400</span>
                                    <i>交易股数</i>
                                </dd>
                                <dd>
                                    <span>5000</span>
                                    <i>信用金</i>
                                </dd>
                                <dd>
                                    <span>5000</span>
                                    <i>冻结资产</i>
                                </dd>
                            </dl>
                            <div class="trade-hide">
                                <ol>
                                    <li>买入: <span>9.65</span> </li>
                                    <li>现价: <span>9.65</span> </li>
                                    <li>止盈: <span class="red">9.66</span></li>
                                    <li>止损: <span class="green">9.66</span></li>
                                </ol>
                                <div class="">   
                                    <i>当前盈利</i>
                                    <p>上涨<span class="red">0.83</span> 元即将到触发止盈</p>
                                </div>
                                <p class="cell-center">
                                    <span class="sell">卖出</span>
                                    <span class="automac">自动递延</span>
                                    <span class="modify">修改止损</span>
                                </p>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:;">
                            <h2>美的集团(SZ000333) <p>2018-06-22</p></h2>
                            <dl>
                                <dd>
                                    <span class="green">5.0</span>
                                    <i>浮动盈亏</i>
                                </dd>
                                <dd>
                                    <span>1400</span>
                                    <i>交易股数</i>
                                </dd>
                                <dd>
                                    <span>5000</span>
                                    <i>信用金</i>
                                </dd>
                                <dd>
                                    <span>5000</span>
                                    <i>冻结资产</i>
                                </dd>
                            </dl>
                            <div class="trade-hide">
                                <ol>
                                    <li>买入: <span>9.65</span> </li>
                                    <li>现价: <span>9.65</span> </li>
                                    <li>止盈: <span class="red">9.66</span></li>
                                    <li>止损: <span class="green">9.66</span></li>
                                </ol>
                                <div class="">   
                                    <i class="on">当前亏损</i>
                                    <p>下跌<span class="green">0.83</span> 元即将到触发止跌</p>
                                </div>
                                <p class="cell-center">
                                    <span class="sell">卖出</span>
                                    <span class="automac">自动递延</span>
                                    <span class="modify">修改止损</span>
                                </p>
                            </div>
                        </a>
                    </li>
                </ul>
                <ul>
                    <li>
                        <a href="/index/strategy/strategy_simulation_info.html?id=10">
                            <h2>美的集团(SZ000333) <p>2018-06-22</p></h2>
                            <dl>
                                <dd>
                                    <span class="red">5.0</span>
                                    <i>浮动盈亏</i>
                                </dd>
                                <dd>
                                    <span>1400</span>
                                    <i>交易股数</i>
                                </dd>
                                <dd>
                                    <span>5000</span>
                                    <i>信用金</i>
                                </dd>
                                <dd>
                                    <span>5000</span>
                                    <i>冻结资产</i>
                                </dd>
                            </dl>
                        </a>
                    </li>
                    <li>
                        <a href="/index/strategy/strategy_simulation_info.html?id=10">
                            <h2>美的集团(SZ000333) <p>2018-06-22</p></h2>
                            <dl>
                                <dd>
                                    <span class="green">5.0</span>
                                    <i>浮动盈亏</i>
                                </dd>
                                <dd>
                                    <span>1400</span>
                                    <i>交易股数</i>
                                </dd>
                                <dd>
                                    <span>5000</span>
                                    <i>信用金</i>
                                </dd>
                                <dd>
                                    <span>5000</span>
                                    <i>冻结资产</i>
                                </dd>
                            </dl>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="includeDom"  data-id="2">
            <footer>
    <ul class="f-cb">
        <!-- <li>
            <a href="/index/index/index.html">
                <div class="pic-box">
                    <img src="/public/static/img/footer01.png" alt="" class="bg">
                    <img src="/public/static/img/footer01_h.png" alt="" class="pic">
                </div>
                <h2>首页</h2>
            </a>
        </li>
        <li>
            <a href="/index/strategy/strategy.html">
                <div class="pic-box">
                    <img src="/public/static/img/footer02.png" alt="" class="bg">
                    <img src="/public/static/img/footer02_h.png" alt="" class="pic">
                </div>
                <h2>策略</h2>
            </a>
        </li>
        <li>
            <a href="/index/ranking/rankingList.html">
                <div class="pic-box">
                    <img src="/public/static/img/footer03.png" alt="" class="bg">
                    <img src="/public/static/img/footer03_h.png" alt="" class="pic">
                </div>
                <h2>排行</h2>
            </a>
        </li>
        <li>
            <a href="/index/center/personal.html">
                <div class="pic-box">
                    <img src="/public/static/img/footer04.png" alt="" class="bg">
                    <img src="/public/static/img/footer04_h.png" alt="" class="pic">
                </div>
                <h2>我的</h2>
            </a>
        </li> -->
        <li>
            <a href="/index/index/index.html">
                <div class="pic-box">
                    <img src="/public/static/img/img_7.png" alt="" class="bg">
                    <img src="/public/static/img/img_8.png" alt="" class="pic">
                </div>
                <h2>首页</h2>
            </a>
        </li>
        <li>
            <a href="/index/price/index.html">
                <div class="pic-box">
                    <img src="/public/static/img/img_9.png" alt="" class="bg">
                    <img src="/public/static/img/img_10.png" alt="" class="pic">
                </div>
                <h2>行情</h2>
            </a>
        </li>
        <li>
            <a href="/index/trade/index.html">
                <div class="pic-box">
                    <img src="/public/static/img/img_11.png" alt="" class="bg">
                    <img src="/public/static/img/img_12.png" alt="" class="pic">
                </div>
                <h2>交易</h2>
            </a>
        </li>
        <li>
            <a href="/index/news/index.html">
                <div class="pic-box">
                    <img src="/public/static/img/img_13.png" alt="" class="bg">
                    <img src="/public/static/img/img_14.png" alt="" class="pic">
                </div>
                <h2>社区</h2>
            </a>
        </li>
        <li>
            <a href="/index/center/personal.html">
                <div class="pic-box">
                    <img src="/public/static/img/img_15.png" alt="" class="bg">
                    <img src="/public/static/img/img_16.png" alt="" class="pic">
                </div>
                <h2>我的</h2>
            </a>
        </li>
    </ul>
    <div class="loading">
        <img src="/public/static/img/logo.png" alt="" class="logo">
    </div>
</footer>
<script>
    jQuery(document).ready(function() {
        $('.loading').addClass("active");
    })
    var id = $('footer').parent(".includeDom").data('id');
    $('footer li').eq(id).addClass('hover');
</script>
        </div>
        <script src="/public/static/js/main.js"></script>
        <script>
            $(function() {
                $(".trade-coll li").click(function(){
                    let index = $(this).index()
                    $(this).addClass("on").siblings().removeClass("on")
                    $(".trade-collspace ul").eq(index).fadeIn(0).siblings("ul").fadeOut(0)
                })
                $(".trade-collspace ul").eq(0).fadeIn(0)
                $(".trade-collspace ul>li").eq(0).addClass("on").find(".trade-hide").slideDown(200)
                $(".trade-collspace ul>li").click(function(){
                    $(this).addClass("on").find(".trade-hide").slideDown(200).parents("li").siblings("li").removeClass("on").find(".trade-hide").slideUp(200)
                })
            })
        new Vue({
        el:'#app',
            props: {
            },
            data() {
                return {
                    money:"0.00"
                };
            },
            computed: {
            },
            created() {
            },
            mounted() {
            },
            methods: {
            },
            components: {
            },
        })
        </script>
    </body>
</html>
