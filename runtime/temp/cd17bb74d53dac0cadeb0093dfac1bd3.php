<?php if (!defined('THINK_PATH')) exit(); /*a:1:{s:54:"/home/stock/H5/application/index/view/index/login.html";i:1559271730;}*/ ?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta http-equiv="content-language" content="zh-CN" />
        <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no" />
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black" />
        <meta name="format-detection" content="telephone=no" />
        <meta name="keywords" content="" />
        <meta name="description" content="" />
        <meta name="author" content=" " />
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
        <title>登录</title>
        <link href="/public/static/img/favicon.ico" rel="shortcut icon">
        <link rel="stylesheet" href="/public/static/css/reset.css">
        <link rel="stylesheet" href="/public/static/css/style.css">
        <link rel="stylesheet" href="/public/static/awesome/css/font-awesome.css">
        <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
        <script src="/public/static/js/jquery-1.11.3.js"></script>
        <script src="/public/static/js/html5.js"></script>
        <script src="/public/static/js/adaptive-version2.js"></script>
        <script src="/public/static/js/jquery.easing.1.3.js"></script>
        <script src="/public/static/js/jquery.transit.js"></script>
        <script src="/public/static/js/jquery.lazyload.js"></script>
    </head>
    <body>
        <div class="guide-top m0 loginon">
            <a href="javascript:history.back(-1);" class="back"><i class="fa fa-angle-left"></i></a>
            登录
        </div>
        <div class="loginpic">
            <img src="/public/static/img/loginpic.png" alt="">
        </div>
        
        <div class="content-loreq login">
            <div class="w94 f-cb">
                <form  method="get" accept-charset="utf-8">
                    <label class="ico1">
                        <input type="number" name="" pattern="\d*"/ value="" placeholder="手机号" class="user">
                    </label>
                    <label class="ico2">
                        <input type="password" name="" value="" placeholder="密码" class="pwr">
                        <span class="clickable"></span>
                    </label>
                    <input type="button" name="" value="登录" onclick="login()" id="logincss">
                    <div class="links f-cb">
                        <a href="register.html" class="fl">注册</a>
                        <a href="forget.html" class="fr">忘记密码？</a>
                    </div>
                </form>
            </div>
            <div class="ts">登录即代表阅读并同意<a class="red" href="/index/index/imformation.html">服务条款</a></div>
        </div>
        <div class="submit-success" style="display: none;"></div>
        <script src="/public/static/js/main.js"></script>
        <script src="/public/static/js/ajax/index.js"></script>
        <script>
        $(function(){
            $(".content-loreq label .clickable").click(function () {
                if ($(this).hasClass('hover')) {
                    $(this).removeClass('hover');
                    $(this).siblings('input').attr("type","password")
                }else{
                    $(this).addClass('hover');
                    $(this).siblings('input').attr("type","text")
                }
            })
        })
        </script>
    </body>
</html>