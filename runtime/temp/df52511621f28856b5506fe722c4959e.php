<?php if (!defined('THINK_PATH')) exit(); /*a:1:{s:64:"D:\xampp\htdocs\peiqi/application/index\view\index\register.html";i:1551775939;}*/ ?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta http-equiv="content-language" content="zh-CN" />
        <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no" />
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black" />
        <meta name="format-detection" content="telephone=no" />
        <meta name="keywords" content="" />
        <meta name="description" content="" />
        <meta name="author" content="令克网络-高端网站建设-https://www.link-web.cn/" />
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
        <title>赢在策略</title>
        <link href="/public/static/img/favicon.ico" rel="shortcut icon">
        <link rel="stylesheet" href="/public/static/css/reset.css">
        <link rel="stylesheet" href="/public/static/css/style.css">
        <script src="/public/static/js/jquery-1.11.3.js"></script>
        <script src="/public/static/js/html5.js"></script>
        <script src="/public/static/js/adaptive-version2.js"></script>
        <script src="/public/static/js/jquery.easing.1.3.js"></script>
        <script src="/public/static/js/jquery.transit.js"></script>
        <script src="/public/static/js/jquery.lazyload.js"></script>
    </head>
    <body>
        <div class="content-loreq register">
            <div class="w80 f-cb">
                <h1>注册</h1>
                <div class="title">欢迎来到赢在策略</div>
                <form action="javascript:;" method="get" accept-charset="utf-8">
                    <label class="ico0">
                        <input type="text" name="" value="" placeholder="用户名" id="name">
                    </label>
                    <label class="ico1">
                        <input type="number" name="" value="" placeholder="手机号" id="username" pattern="\d*"/>
                    </label>
                    <div class="f-cb">
                        <label class="ico3 code fl">
                            <input type="number" name="" value="" placeholder="验证码" id="verify" pattern="\d*"/>
                        </label>
                        <input type="button" class="btn fr" id="seconds" value="获取验证码" onclick="yzm()">
                    </div>
                    <label class="ico1">
                        <input type="number" name="" value="" placeholder="邀请码（必填）" id="inviter_tel" pattern="\d*"/>
                    </label>
                    <label class="ico2">
                        <input type="password" name="" value="" placeholder="密码" id="passworde">
                    </label>
                    <input type="submit" name="" value="注册" class="res" onclick="register()">
                </form>
            </div>
            <div class="ts">注册即代表阅读并同意<span class="red" onclick="window.location.href='/YZ(Service).pdf';">服务条款</span></div>
        </div>
        <div class="submit-success" style="display: none;"></div>
        <script src="/public/static/js/main.js"></script>
        <script src="/public/static/js/ajax/index.js"></script>

    </body>
</html>