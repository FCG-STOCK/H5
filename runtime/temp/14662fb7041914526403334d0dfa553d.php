<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:69:"/www/wwwroot/peiqi.solingke.cn/application/index/view/trade/rank.html";i:1554802418;s:75:"/www/wwwroot/peiqi.solingke.cn/application/index/view/index/inc/footer.html";i:1554779792;}*/ ?>
﻿<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta http-equiv="content-language" content="zh-CN" />
        <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no" />
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black" />
        <meta name="format-detection" content="telephone=no" />
        <meta name="keywords" content="" />
        <meta name="description" content="" />
        <meta name="author" content="令克网络-高端网站建设-https://www.link-web.cn/" />
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
        <title>策略2.0</title>
        <link href="/public/static/img/favicon.ico" rel="shortcut icon">
        <link rel="stylesheet" href="/public/static/css/reset.css">
        <link rel="stylesheet" href="/public/static/css/style.css">
        <link rel="stylesheet" href="/public/static/css/trade.css">
        <link rel="stylesheet" href="/public/static/awesome/css/font-awesome.css">
        <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
        <script src="/public/static/js/vue.js"></script>
        <script src="/public/static/js/jquery-1.11.3.js"></script>
        <script src="/public/static/js/html5.js"></script>
        <script src="/public/static/js/adaptive-version2.js"></script>
        <script src="/public/static/js/jquery.easing.1.3.js"></script>
        <script src="/public/static/js/jquery.transit.js"></script>
        <script src="/public/static/js/jquery.lazyload.js"></script>
    </head>
    <body>
        <div id="app">
            <div class="guide-top m0">
                <a href="javascript:history.back(-1);" class="back"><i class="fa fa-angle-left"></i></a>
                <!-- <a href="javascript:history.back(-1);"></a> -->
                高手排行
            </div>
            <ul class="rank-top">
                <li v-for="(item,index) in rank" @click="rankChange(item,$event)" :class="index==0?'on':''">{{item.name}}</li>
            </ul>
            <div >
                <div v-if="list.length==0" style="padding:0 3%;font-size:0.28rem;text-align:center;margin-top:1.2rem"> 
                    <div class="zanwu"></div>  
                </div>
                <ul class="rank-list" v-else> 
                    <li v-for="(item,index) in list" :key="index">
                        <a :href="'/index/trade/info.html?id='+item.id+'&trues='+item.trues+'&aid='+item.aid">
                            <i :class="index=='0'?'one':(index=='1'?'two':(index=='2'?'three':''))">{{index+1}}</i>
                            <img :src="item.headurl" alt="">
                            {{item.nick_name}}
                            <p>总收益率 <span>{{item.profit}}</span></p>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="includeDom"  data-id="2">
            <footer>
    <ul class="f-cb">
        <!-- <li>
            <a href="/index/index/index.html">
                <div class="pic-box">
                    <img src="/public/static/img/footer01.png" alt="" class="bg">
                    <img src="/public/static/img/footer01_h.png" alt="" class="pic">
                </div>
                <h2>首页</h2>
            </a>
        </li>
        <li>
            <a href="/index/strategy/strategy.html">
                <div class="pic-box">
                    <img src="/public/static/img/footer02.png" alt="" class="bg">
                    <img src="/public/static/img/footer02_h.png" alt="" class="pic">
                </div>
                <h2>策略</h2>
            </a>
        </li>
        <li>
            <a href="/index/ranking/rankingList.html">
                <div class="pic-box">
                    <img src="/public/static/img/footer03.png" alt="" class="bg">
                    <img src="/public/static/img/footer03_h.png" alt="" class="pic">
                </div>
                <h2>排行</h2>
            </a>
        </li>
        <li>
            <a href="/index/center/personal.html">
                <div class="pic-box">
                    <img src="/public/static/img/footer04.png" alt="" class="bg">
                    <img src="/public/static/img/footer04_h.png" alt="" class="pic">
                </div>
                <h2>我的</h2>
            </a>
        </li> -->
        <li>
            <a href="/index/index/index.html">
                <div class="pic-box">
                    <img src="/public/static/img/img_7.png" alt="" class="bg">
                    <img src="/public/static/img/img_8.png" alt="" class="pic">
                </div>
                <h2>首页</h2>
            </a>
        </li>
        <li>
            <a href="/index/price/quotes.html">
                <div class="pic-box">
                    <img src="/public/static/img/img_9.png" alt="" class="bg">
                    <img src="/public/static/img/img_10.png" alt="" class="pic">
                </div>
                <h2>行情</h2>
            </a>
        </li>
        <li>
            <a href="/index/trade/index.html">
                <div class="pic-box">
                    <img src="/public/static/img/img_11.png" alt="" class="bg">
                    <img src="/public/static/img/img_12.png" alt="" class="pic">
                </div>
                <h2>交易</h2>
            </a>
        </li>
        <li>
            <a href="/index/news/index.html">
                <div class="pic-box">
                    <img src="/public/static/img/img_13.png" alt="" class="bg">
                    <img src="/public/static/img/img_14.png" alt="" class="pic">
                </div>
                <h2>资讯</h2>
            </a>
        </li>
        <li>
            <a href="/index/center/personal.html">
                <div class="pic-box">
                    <img src="/public/static/img/img_15.png" alt="" class="bg">
                    <img src="/public/static/img/img_16.png" alt="" class="pic">
                </div>
                <h2>我的</h2>
            </a>
        </li>
    </ul>
    <div class="loading">
            <div class="loadings">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div> 
        <!-- <img src="/public/static/img/logo.png" alt="" class="logo"> -->
    </div>
</footer>
<script>
    var id = $('footer').parent(".includeDom").data('id');
    $('footer li').eq(id).addClass('hover');
</script>
        </div>
        <div class="loading">
            <img src="/public/static/img/logo.png" alt="" class="logo">
        </div>
        <script src="/public/static/js/main.js"></script>
        
        <script>
        new Vue({
            el:'#app',
            data() {
                return {
                    rank:[
                        {
                            name:"总收益",
                            url:"/api/mobile/allrankingList",
                            title:"暂无总收益排行",
                            list:[]
                        },
                        {
                            name:"日收益",
                            url:"/api/mobile/dayrankingList",
                            title:"暂无日收益排行",
                            list:[]
                        },
                        {
                            name:"周收益",
                            url:"/api/mobile/weekrankingList",
                            title:"暂无周收益排行",
                            list:[]
                        },
                        {
                            name:"月收益",
                            url:"/api/mobile/monthrankingList",
                            title:"暂无月收益排行",
                            list:[]
                        }
                    ],
                    title:"",
                    list:[],
                };
            },
            mounted() {
                this.allrankingLis();
                setTimeout(() => {
                    this.dayrankingList()
                    this.weekrankingList()
                    this.monthrankingList()
                }, 500);
            },
            methods: {
                rankChange(item,e){
                    if (!$(e.target).hasClass("on")) {
                        $(e.target).addClass("on").siblings("li").removeClass("on")
                        this.list = item.list;
                        this.title = item.title
                    }
                },
                allrankingLis(){
                    let _this =this;
                    axios.post('/api/mobile/allrankingList',account)
                    .then(function (res) {
                        if(res.data.status=='1'){
                            _this.rank[0].list =res.data.list;
                            _this.list =res.data.list;
                            _this.title =_this.rank[0].title 
                        }
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
                },
                dayrankingList(){
                    let _this =this;
                    axios.post('/api/mobile/dayrankingList',account)
                    .then(function (res) {
                        if(res.data.status=='1'){
                            _this.rank[1].list =res.data.list

                        }
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
                },
                weekrankingList(){
                    let _this =this;
                    axios.post('/api/mobile/weekrankingList',account)
                    .then(function (res) {
                        if(res.data.status=='1'){
                            _this.rank[2].list =res.data.list

                        }
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
                },
                monthrankingList(){
                    let _this =this;
                    axios.post('/api/mobile/monthrankingList',account)
                    .then(function (res) {
                        if(res.data.status=='1'){
                            _this.rank[3].list =res.data.list
                        }
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
                }
            }
        })
        </script>
    </body>
</html>