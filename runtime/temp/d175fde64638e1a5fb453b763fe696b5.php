<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:69:"/www/wwwroot/peiqi.solingke.cn/application/index/view/trade/info.html";i:1554802418;s:75:"/www/wwwroot/peiqi.solingke.cn/application/index/view/index/inc/footer.html";i:1554779792;}*/ ?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta http-equiv="content-language" content="zh-CN" />
        <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no" />
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black" />
        <meta name="format-detection" content="telephone=no" />
        <meta name="keywords" content="" />
        <meta name="description" content="" />
        <meta name="author" content="令克网络-高端网站建设-https://www.link-web.cn/" />
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
        <title>策略2.0</title>
        <link href="/public/static/img/favicon.ico" rel="shortcut icon">
        <link rel="stylesheet" href="/public/static/css/reset.css">
        <link rel="stylesheet" href="/public/static/css/style.css">
        <link rel="stylesheet" href="/public/static/css/trade.css">
        <link rel="stylesheet" href="/public/static/awesome/css/font-awesome.css">
        <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
        <script src="/public/static/js/vue.js"></script>
        <script src="/public/static/js/jquery-1.11.3.js"></script>
        <script src="/public/static/js/html5.js"></script>
        <script src="/public/static/js/adaptive-version2.js"></script>
        <script src="/public/static/js/jquery.easing.1.3.js"></script>
        <script src="/public/static/js/jquery.transit.js"></script>
        <script src="/public/static/js/jquery.lazyload.js"></script>
    </head>
    <body>
        <div id="app">
            <div class="guide-top m0 boxs"> 
                <a href="javascript:history.back(-1);" class="back"><i class="fa fa-angle-left"></i></a>
                <!-- <a href="javascript:history.back(-1);"></a> -->
                个人战绩
            </div>
            <div class="rank-info-header">
                <img :src="list.headurl" alt="">
                <p>{{list.nick_name}} <span>排行：{{!list.ranking==''?list.ranking:'--'}}</span></p>
                <!-- <a href="javascript:;"></a> -->
                <a href="javascript:;" :class="list.is_subscribe=='1'?'on':''" @click="createS"></a>
            </div>
            <div class="rank-info-poa">
                <div>
                    <h2>{{list.all}} <span>总收益</span></h2>
                    <ul>
                        <li>日：<span>{{list.day}}</span></li>
                        <li>周：<span>{{list.week}}</span></li>
                        <li>月：<span>{{list.month}}</span></li>
                    </ul>
                </div>
            </div>
            <ul class="rank-top rank-info-top">
                <li v-for="(item,index) in rank" @click="rankChange(item,$event)" :class="index==0?'on':''">{{item.name}}</li>
            </ul>
            <div>
                <div class="hide">
                    <div v-if="buy_record==''" class="zanwu"></div>
                    <ul class="rank-info-list" v-else>
                        <li  v-for="(item,index) in buy_record" :key="index">
                            <div class="rank_info_list_top">
                                    <span :class="item.income < '0'?'':'on'">{{item.getprice<'0'?'亏':'盈'}}</span>
                                    <div>
                                        <h2>{{item.stock_name}}</h2>
                                        <p>({{item.stock_code}})</p>
                                    </div>
                                    <i><b>{{item.stock_number}}</b>股</i>
                                    <div>
                                        <a v-if="trues=='1'" :href="'/index/strategy/ploy.html?stock_code='+item.stock_code+'&stock_name='+item.stock_name">
                                            跟买
                                        </a>
                                        <a v-else :href="'/index/trade/ploy.html?stock_code='+item.stock_code+'&stock_name='+item.stock_name+'&trade=1'">
                                            跟买
                                        </a>
                                    </div>
                            </div>
                            <div class="rank_info_bottom">
                                <div class="f-cb">
                                    <p>策略止盈<span >{{item.surplus_value}}</span></p>
                                    <p>买入价<span>{{item.buy_price}}</span></p>
                                </div>
                                <div class="f-cb">
                                        <p>策略止损<span>{{item.loss_value}}</span></p>
                                    <p>卖出价<span>{{item.sell_price}}</span></p>
                                </div>
                                <p>买入时间<span>{{item.buy_time}}</span></p>
                                <p>卖出时间<span>--</span></p>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="hide">
                    <div v-if="record==''" class="zanwu"></div>
                    <ul class="rank-info-list" v-else>
                        <li  v-for="(item,index) in record" :key="index">
                            <div class="rank_info_list_top">
                                <span :class="item.getprice < '0'?'':'on'">{{item.getprice<'0'?'亏':'盈'}}</span>
                                <div>
                                    <h2>{{item.stock_name}}</h2>
                                    <p>({{item.stock_code}})</p>
                                </div>
                                <i><b>{{item.stock_number}}</b>股</i>
                            </div>
                            <div class="rank_info_bottom">
                                <div class="f-cb">
                                    <p>策略止盈<span >{{item.surplus_value}}</span></p>
                                    <p>买入价<span>{{item.buy_price}}</span></p>
                                </div>
                                <div class="f-cb">
                                        <p>策略止损<span>{{item.loss_value}}</span></p>
                                    <p>卖出价<span>{{item.sell_price}}</span></p>
                                </div>
                                <p>买入时间<span>{{item.buy_time}}</span></p>
                                <p>卖出时间<span>{{item.sell_time}}</span></p>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div style="height:1.2rem"></div>
        <div class="includeDom"  data-id="2">
            <footer>
    <ul class="f-cb">
        <!-- <li>
            <a href="/index/index/index.html">
                <div class="pic-box">
                    <img src="/public/static/img/footer01.png" alt="" class="bg">
                    <img src="/public/static/img/footer01_h.png" alt="" class="pic">
                </div>
                <h2>首页</h2>
            </a>
        </li>
        <li>
            <a href="/index/strategy/strategy.html">
                <div class="pic-box">
                    <img src="/public/static/img/footer02.png" alt="" class="bg">
                    <img src="/public/static/img/footer02_h.png" alt="" class="pic">
                </div>
                <h2>策略</h2>
            </a>
        </li>
        <li>
            <a href="/index/ranking/rankingList.html">
                <div class="pic-box">
                    <img src="/public/static/img/footer03.png" alt="" class="bg">
                    <img src="/public/static/img/footer03_h.png" alt="" class="pic">
                </div>
                <h2>排行</h2>
            </a>
        </li>
        <li>
            <a href="/index/center/personal.html">
                <div class="pic-box">
                    <img src="/public/static/img/footer04.png" alt="" class="bg">
                    <img src="/public/static/img/footer04_h.png" alt="" class="pic">
                </div>
                <h2>我的</h2>
            </a>
        </li> -->
        <li>
            <a href="/index/index/index.html">
                <div class="pic-box">
                    <img src="/public/static/img/img_7.png" alt="" class="bg">
                    <img src="/public/static/img/img_8.png" alt="" class="pic">
                </div>
                <h2>首页</h2>
            </a>
        </li>
        <li>
            <a href="/index/price/quotes.html">
                <div class="pic-box">
                    <img src="/public/static/img/img_9.png" alt="" class="bg">
                    <img src="/public/static/img/img_10.png" alt="" class="pic">
                </div>
                <h2>行情</h2>
            </a>
        </li>
        <li>
            <a href="/index/trade/index.html">
                <div class="pic-box">
                    <img src="/public/static/img/img_11.png" alt="" class="bg">
                    <img src="/public/static/img/img_12.png" alt="" class="pic">
                </div>
                <h2>交易</h2>
            </a>
        </li>
        <li>
            <a href="/index/news/index.html">
                <div class="pic-box">
                    <img src="/public/static/img/img_13.png" alt="" class="bg">
                    <img src="/public/static/img/img_14.png" alt="" class="pic">
                </div>
                <h2>资讯</h2>
            </a>
        </li>
        <li>
            <a href="/index/center/personal.html">
                <div class="pic-box">
                    <img src="/public/static/img/img_15.png" alt="" class="bg">
                    <img src="/public/static/img/img_16.png" alt="" class="pic">
                </div>
                <h2>我的</h2>
            </a>
        </li>
    </ul>
    <div class="loading">
            <div class="loadings">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div> 
        <!-- <img src="/public/static/img/logo.png" alt="" class="logo"> -->
    </div>
</footer>
<script>
    var id = $('footer').parent(".includeDom").data('id');
    $('footer li').eq(id).addClass('hover');
</script>
        </div>
        <div class="submit-success on" style="display: none;"></div>
        <div class="loading">
            <img src="/public/static/img/logo.png" alt="" class="logo">
        </div>
        <script src="/public/static/js/main.js"></script>
        <script>

        var search = search();
        function submitsuccess(msg){
            $('.submit-success').html(msg);
            $('.submit-success').fadeIn();
            setTimeout(function(){
                $('.submit-success').fadeOut();
            },1000);
        }
        new Vue({
            el:'#app',
            data() {
                return {
                    rank:[
                        {
                            name:"持仓",
                            url:"1"
                        },
                        {
                            name:"记录",
                            url:"2"
                        },
                    ],
                    list:{},
                    buy_record:[],
                    record:[],
                    trues:'',
                    nick_name :''
                };
            },
            mounted() {
                this.subscribeInfo();
                $(".hide").eq(0).fadeIn(0)
                // this.slide = rank[0].list;
                this.trues = search.trues
            },
            methods: {
                subscribeInfo(){
                    let _this =this;
                    axios.post('/api/mobile/subscribeInfo',Object.assign(search,account))
                    .then(function (res) {
                        if(res.data.status=='1'){
                            _this.list = res.data.list;
                            _this.buy_record = res.data.list.buy_record
                            _this.record = res.data.list.record;
                            _this.nick_name = res.data.list.nick_name  
                        }
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
                },
                rankChange(item,e){
                    if (!$(e.target).hasClass("on")) {
                        $(e.target).addClass("on").siblings("li").removeClass("on");
                        $(".hide").eq($(e.target).index()).fadeIn(0).siblings(".hide").fadeOut(0)
                    }
                },
                createS(){
                    let _this=this;
                    submitsuccess('订阅中...')
                    if(_this.list.is_subscribe== 1){
                        _this.list.is_subscribe=2;
                    }else{
                        _this.list.is_subscribe=1;
                    }
                    axios.get('/api/mobile/createSubscribe',{params:Object.assign(search,account,{subscribe_nick_name:_this.nick_name,is_subscribe:_this.list.is_subscribe})})
                    .then(function (res) {
                        if(res.data.status=='1'){
                            submitsuccess('订阅成功')
                            setTimeout(() => {
                                _this.subscribeInfo()
                            }, 1000);
                            _this.list = res.data.list;
                            _this.buy_record = res.data.list.buy_record
                            _this.record = res.data.list.record
                        }else{
                            submitsuccess('订阅')

                        }
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
                }
            }
        })
        </script>
    </body>
</html>