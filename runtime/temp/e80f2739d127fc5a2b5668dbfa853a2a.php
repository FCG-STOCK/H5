<?php if (!defined('THINK_PATH')) exit(); /*a:1:{s:70:"/home/stock/h5/application/index/view/index/optional_search_trade.html";i:1557999502;}*/ ?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta http-equiv="content-language" content="zh-CN" />
        <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no" />
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black" />
        <meta name="format-detection" content="telephone=no" />
        <meta name="keywords" content="" />
        <meta name="description" content="" />
        <meta name="author" content="   " />
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
        <title>股票搜索</title>
        <link href="favicon.ico" rel="shortcut icon">
        <link rel="stylesheet" href="/public/static/css/reset.css">
        <link rel="stylesheet" href="/public/static/css/style.css">
        <link rel="stylesheet" href="/public/static/css/search.css">
        <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
        <script src="/public/static/js/vue.js"></script>
        <script src="/public/static/js/jquery-1.11.3.js"></script>
        <script src="/public/static/js/html5.js"></script>
        <script src="/public/static/js/adaptive-version2.js"></script>
        <script src="/public/static/js/jquery.easing.1.3.js"></script>
        <script src="/public/static/js/jquery.transit.js"></script>
        <script src="/public/static/js/jquery.lazyload.js"></script>
    </head>
    <body>
        <div class="content-box">
            <div class="optional-search f-cb">
            <form>
                <input type="text" placeholder="搜索股票代码/拼音首字母" id="searchKey"></input>
            </form>
            <a href="javascript:history.back(-1);"><p class="cancel">返回</p></a>
            </div>
            <div id="app">
                <div class="search-content">
                    <div class="search-hot">
                        <div class="stock">热门股票</div>
                        <ul>
                            <li class="f-cb" v-for='(item,index) in list' :key="index" v-cloak>
                                <a href="javascript:;" @click="jump(item.stock_code,item.stock_name)" >
                                    <div class="mm">{{item.stock_name}}<span>({{item.stock_code}})</span></div>
                                    <div class="ms"><h2 :class="item.rate_price < '0'?'green':'red'">{{item.nowprice}}</h2></div>
                                    <div class="ms">{{item.rate_price}}</div>
                                    <div class="ms">{{item.rate}}</div>
                                </a>
                            </li>
                           
                        </ul>
                    </div>
                    <div>
                        <div></div>
                    </div>
                    <div class="search-history" >
                        <div class="stock f-cb">
                            <h2>历史记录</h2>
                            <p><a href="javascript:;" class="delete" onclick="delall()">清空</a></p>
                        </div>
                        <ul>
                            <li class="f-cb" v-for="(item,index) in para" v-cloak>
                                <a  href="javascript:;" @click="jump(item.stock_code,item.stock_name)" >
                                    <div class="mm">{{item.stock_name}}<span>({{item.stock_code}})</span></div>
                                    <div class="ms"><h2 :class="item.rate_price < '0'?'green':'red'">{{item.nowprice}}</h2></div>
                                    <div class="ms">{{item.rate}}</div>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="tosearch">
                <ul class="center" id="searchover">
                    <!-- <li class="f-cb">
                        <a href="javascript:">
                            <div class="mm">美的集团<span>(SZ000333)</span></div>
                            <div class="ms"><h2 class="red">5.51</h2></div>
                            <div class="ms">+0.42</div>
                            <div class="ms">+0.01%</div>
                        </a>
                    </li>
                    <li class="f-cb">
                        <a href="javascript:">
                            <div class="mm">美的集团<span>(SZ000333)</span></div>
                            <div class="ms"><h2 class="red">5.51</h2></div>
                            <div class="ms">+0.42</div>
                            <div class="ms">+0.01%</div>
                        </a>
                    </li>
                    <li class="f-cb">
                        <a href="javascript:">
                            <div class="mm">美的集团<span>(SZ000333)</span></div>
                            <div class="ms"><h2 class="green">5.51</h2></div>
                            <div class="ms">+0.42</div>
                            <div class="ms">+0.01%</div>
                        </a>
                    </li> -->
                </ul>
            </div>

        </div>
        <div class="submit-success" style="display: none;"></div>
        <script src="/public/static/js/main.js"></script>
        <script src="/public/static/js/jquery.cookie.js"></script>
        <script>
            $('.search-history .stock .delete').click(function () {
                    $(this).parents(".search-history").find("ul").remove();
                })
            $('.optional-search input').click(function () {
                $('.tosearch').fadeIn();
                setTimeout(function(){
                    $('.tosearch .center').stop().animate({'center':0},300)
                },100)
            })
            // $('.optional-search .cancel').click(function () {
            //     $('.tosearch .center').stop().animate({'center':'-50%'},800)
            //     setTimeout(function(){
            //         $('.tosearch').fadeOut();
            //     },200)
            //     var search = '';
            //     $('.optional-search input').val(search);
            // })
        </script>

        <script type="text/javascript">
        	//通过网上接口，来搜索股票
			//键入触发事件
	        $('#searchKey').keyup(function(){
	        	//如果输入的值为空就刷新当前页面
	            var content = ($('#searchKey').val()).toUpperCase();
	            if(!content){
	                location.reload();
	            }
	        	$.ajax({
	                url: "/api/mobile/search_stock_api",
	                data: { content: $('#searchKey').val()
	                          },
	                dataType: "json",
	                method:"post", 
	                success: function (data) {
	                    // alert('ajax调用成功');
	                    var dataObj = JSON.parse(data);
	                    if(dataObj.status==1){
	                    	var colStr = '';
	                    	for(var i=0,len=dataObj.list.length;i<len;i++){
                                if (dataObj.list[i].rate_price<0) {
                                    colStr+= '<li class="f-cb">'+
                                            '<a href="javascript:" onclick="jump(\''+dataObj.list[i].stock_code+'\',\''+dataObj.list[i].stock_name+'\')";>'+
                                                '<div class="mm">'+dataObj.list[i].stock_name+'<span>('+dataObj.list[i].stock_code+')</span></div>'+
                                                '<div class="ms"><h2 class="green">'+dataObj.list[i].nowprice+'</h2></div>'+
                                                '<div class="ms " style="color:green">'+dataObj.list[i].rate_price+'</div>'+
                                                '<div class="ms" style="color:green">'+dataObj.list[i].rate+'</div>'+
                                            '</a>'+
                                          '</li>';
                                }
                                else{
				                colStr+= '<li class="f-cb">'+
						                    '<a href="javascript:"  onclick="jump(\''+dataObj.list[i].stock_code+'\',\''+dataObj.list[i].stock_name+'\')"; >'+
						                        '<div class="mm">'+dataObj.list[i].stock_name+'<span>('+dataObj.list[i].stock_code+')</span></div>'+
						                        '<div class="ms"><h2 class="red">'+dataObj.list[i].nowprice+'</h2></div>'+
						                        '<div class="ms">'+dataObj.list[i].rate_price+'</div>'+
						                        '<div class="ms">'+dataObj.list[i].rate+'</div>'+
						                    '</a>'+
				               			  '</li>';
                                }


				            }

				            $('#searchover').html(colStr);

	                    }                      
	                }
	            })                     
	        });



           	//先判断用户查询的股票是否停盘，再让他购买,并且添加历史搜索记录
            function jump(stock_code,stock_name){
                $.ajax({
                    url: "/api/mobile/addHistoryStock",
                    dataType: "json",
                    data:{account:$.cookie('username'),
                        stock_code:stock_code,
                        stock_name,stock_name},
                    success: function (data) {
                        // alert('ajax调用成功');
                        var dataObj = JSON.parse(data);
                        if(dataObj.status==1){
                            window.location.href='/index/price/info.html?stock_code='+stock_code+'&stock_name='+stock_name+'&trade=1';
                        }else{
                           submitsuccess("该股票已限制购买");
                        }
                    }
                })
        //        $.ajax({
        //            url: "/api/search/thischeckstockstop_api?gid="+stock_code,
        //            //dataType: "json",
        //            success: function (data) {
        //                // alert('ajax调用成功');
        //                //var dataObj = JSON.parse(data);
        //
        //                if(data!=200){
        //                    submitsuccess("该股票已限制购买");
        //                    return;
        //                }
        //
        //            }
        //        })
            }
            
            $('.search-history .stock .delete').click(function () {
                $(this).parents(".search-history").find("ul").remove();
            })



        </script>
        <script>
        var vm=new Vue({
            el:'#app',
            props: {
            },
            data() {
                return {
                    list:[],
                    para:[]
                };
            },
            mounted() {
                this.search_stock_list()
            },
            methods: {
                search_stock_list(){
                    let _this = this;
                    axios.post('/api/mobile/search_stock_list',account)
                    .then(function (res) {
                       if(res.data.status =='1'){
                            _this.list = res.data.list
                            _this.para = res.data.para
                       }
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
                },
                jump(stock_code,stock_name){
                    $.ajax({
                        type:"post",
                    url: "/api/mobile/addHistoryStock",
                    dataType: "json",
                    data:{account:account.account,
                        stock_code:stock_code,
                        stock_name,stock_name},
                    success: function (data) {
                        // alert('ajax调用成功');
                        var dataObj = JSON.parse(data);
                        if(dataObj.status==1){
                            window.location.href='/index/price/info.html?stock_code='+stock_code+'&stock_name='+stock_name+'&trade=1';
                        }else{
                           submitsuccess("该股票已限制购买");
                        }
                    }
                })
                }
            },
        })
        
        function delall(){
            $.ajax({
                url: "/api/mobile/deleteAllHistoryStock",
                dataType: "json",
                data:{account:$.cookie('username'),},
                success: function (data) {
                    // alert('ajax调用成功');
                    var dataObj = JSON.parse(data);
                    if(dataObj.status==1){
                        submitsuccess("清空成功"); 
                        vm.search_stock_list()
                        //setTimeout(function () { window.location.reload()}, 2000);//延迟1秒后关闭         
                    }
                }
            })
        }
        </script>
    </body>
</html>