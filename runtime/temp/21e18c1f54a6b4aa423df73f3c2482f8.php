<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:95:"/www/wwwroot/peiqi.solingke.cn/application/index/view/strategy/strategy_simulation_contest.html";i:1554273497;s:75:"/www/wwwroot/peiqi.solingke.cn/application/index/view/index/inc/footer.html";i:1554104025;}*/ ?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta http-equiv="content-language" content="zh-CN" />
        <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no" />
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black" />
        <meta name="format-detection" content="telephone=no" />
        <meta name="keywords" content="" />
        <meta name="description" content="" />
        <meta name="author" content="令克网络-高端网站建设-https://www.link-web.cn/" />
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
        <title>配齐策略</title>
        <link href="/public/static/img/favicon.ico" rel="shortcut icon">
        <link rel="stylesheet" href="/public/static/css/reset.css">
        <link rel="stylesheet" href="/public/static/css/style.css">
        <link rel="stylesheet" href="/public/static/awesome/css/font-awesome.css">
        <script src="/public/static/js/jquery-1.11.3.js"></script>
        <script src="/public/static/js/html5.js"></script>
        <script src="/public/static/js/adaptive-version2.js"></script>
        <script src="/public/static/js/jquery.easing.1.3.js"></script>
        <script src="/public/static/js/jquery.transit.js"></script>
        <script src="/public/static/js/jquery.lazyload.js"></script>
    </head>
    <body>
        <!--<div class="includeDom" include="inc/header.html" data-intro="index"></div>-->
        <div class="content-box data">
            <div class="personal-title">
                <a href="javascript:history.back(-1);" class="back"><i class="fa fa-angle-left"></i></a>
                历史记录
            </div>
            <div class="simulation-top f-cb">
                <div class="co2"><?php echo $celue['arr']['allStrategyNum']; ?>笔</div>
                <div class="co2"><?php echo $celue['arr']['winStrategyNum']; ?>笔</div>
                <div class="co2">成功率：<span class="co8"><?php echo $celue['arr']['rate']; ?></span></div>
                <div class="co8">总策略</div>
                <div class="co8">盈利策略</div>
                <div class="co2">总盈利：
                    <?php if($celue['arr']['win'] >= 0): ?>
                    <span class="red"><?php echo $celue['arr']['win']; ?>元</span>
                    <?php else: ?>
                    <span class="gree"><?php echo $celue['arr']['win']; ?>元</span>
                    <?php endif; ?>
                </div>
            </div>
            <ul class="simulation-bot f-cb">
                <?php if(is_array($celue['list']) || $celue['list'] instanceof \think\Collection || $celue['list'] instanceof \think\Paginator): if( count($celue['list'])==0 ) : echo "" ;else: foreach($celue['list'] as $key=>$vo): ?>
                <li>
                    <a href="<?php echo url('strategy_simulation_contest_info',['id'=>$vo['id']]); ?>">
                        <div class="title"><?php echo $vo['stock_name']; ?>(<?php echo $vo['stock_code']; ?>)<span class="day"><?php echo $vo['buy_time']; ?></span></div>
                        <dl class="con f-cb">
                            <dd><?php echo $vo['credit']; ?></dd>
                            <dd><?php echo $vo['stock_number']; ?></dd>
                            <dd class="green"><?php echo $vo['getprice']; ?></dd>
                            <?php if($vo['getprice'] >= 0): ?>
                            <dd><?php echo $vo['truegetprice']; ?></dd>
                            <?php else: ?>
                            <dd><?php echo $vo['dropprice']; ?></dd>
                            <?php endif; ?>
                            <dd>交易本金</dd>
                            <dd>交易股数</dd>
                            <dd>交易盈亏</dd>
                            <?php if($vo['getprice'] >= 0): ?>
                            <dd>盈利分配</dd>
                            <?php else: ?>
                            <dd>亏损赔付</dd>
                            <?php endif; ?>
                        </dl>
                    </a>
                </li>
                <?php endforeach; endif; else: echo "" ;endif; ?>
            </ul>
        </div>
        <!--<div class="includeDom" include="inc/footer.html" data-id="1"></div>-->
        <div class="includeDom" data-intro="scroll" data-id="1">
            <footer>
    <ul class="f-cb">
        <!-- <li>
            <a href="/index/index/index.html">
                <div class="pic-box">
                    <img src="/public/static/img/footer01.png" alt="" class="bg">
                    <img src="/public/static/img/footer01_h.png" alt="" class="pic">
                </div>
                <h2>首页</h2>
            </a>
        </li>
        <li>
            <a href="/index/strategy/strategy.html">
                <div class="pic-box">
                    <img src="/public/static/img/footer02.png" alt="" class="bg">
                    <img src="/public/static/img/footer02_h.png" alt="" class="pic">
                </div>
                <h2>策略</h2>
            </a>
        </li>
        <li>
            <a href="/index/ranking/rankingList.html">
                <div class="pic-box">
                    <img src="/public/static/img/footer03.png" alt="" class="bg">
                    <img src="/public/static/img/footer03_h.png" alt="" class="pic">
                </div>
                <h2>排行</h2>
            </a>
        </li>
        <li>
            <a href="/index/center/personal.html">
                <div class="pic-box">
                    <img src="/public/static/img/footer04.png" alt="" class="bg">
                    <img src="/public/static/img/footer04_h.png" alt="" class="pic">
                </div>
                <h2>我的</h2>
            </a>
        </li> -->
        <li>
            <a href="/index/index/index.html">
                <div class="pic-box">
                    <img src="/public/static/img/img_7.png" alt="" class="bg">
                    <img src="/public/static/img/img_8.png" alt="" class="pic">
                </div>
                <h2>首页</h2>
            </a>
        </li>
        <li>
            <a href="/index/price/index.html">
                <div class="pic-box">
                    <img src="/public/static/img/img_9.png" alt="" class="bg">
                    <img src="/public/static/img/img_10.png" alt="" class="pic">
                </div>
                <h2>行情</h2>
            </a>
        </li>
        <li>
            <a href="/index/trade/index.html">
                <div class="pic-box">
                    <img src="/public/static/img/img_11.png" alt="" class="bg">
                    <img src="/public/static/img/img_12.png" alt="" class="pic">
                </div>
                <h2>交易</h2>
            </a>
        </li>
        <li>
            <a href="/index/news/index.html">
                <div class="pic-box">
                    <img src="/public/static/img/img_13.png" alt="" class="bg">
                    <img src="/public/static/img/img_14.png" alt="" class="pic">
                </div>
                <h2>资讯</h2>
            </a>
        </li>
        <li>
            <a href="/index/center/personal.html">
                <div class="pic-box">
                    <img src="/public/static/img/img_15.png" alt="" class="bg">
                    <img src="/public/static/img/img_16.png" alt="" class="pic">
                </div>
                <h2>我的</h2>
            </a>
        </li>
    </ul>
    <div class="loading">
            <div class="loadings">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div> 
        <!-- <img src="/public/static/img/logo.png" alt="" class="logo"> -->
    </div>
</footer>
<script>
    var id = $('footer').parent(".includeDom").data('id');
    $('footer li').eq(id).addClass('hover');
</script>
        </div>
        <script src="/public/static/js/main.js"></script>
    </body>
</html>