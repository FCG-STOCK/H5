<?php

class Stock 
{

	protected $API = '47.110.147.206/Api/';//交易
  
  	protected $URL = '39.97.187.213/Api/';//行情

	function querydata($IP,$Version,$YybID,$AccountNo,$TradeAccount,$JyPassword,$Port,$TxPassword,$Category){
		
		$api = $this->API;//接口地址

		$url = $api.'QueryData?IP='.$IP.'&Version='.$Version.'&YybID='.$YybID.'&AccountNo='.$AccountNo.'&TradeAccount='.$TradeAccount.'&JyPassword='.$JyPassword.'&Port='.$Port.'&TxPassword='.$TxPassword.'&Category='.$Category;
		$res = $this->https_request($url,$header='',$data='');//CURL获取数据
		return $res;

	}


	function querydatas($IP,$Version,$YybID,$AccountNo,$TradeAccount,$JyPassword,$Port,$TxPassword,$Category,$Count){
		
		$api = $this->API;//接口地址

		$url = $api.'QueryDatas?IP='.$IP.'&Version='.$Version.'&YybID='.$YybID.'&AccountNo='.$AccountNo.'&TradeAccount='.$TradeAccount.'&JyPassword='.$JyPassword.'&Port='.$Port.'&TxPassword='.$TxPassword.'&Category='.$Category.'&Count='.$Count;
		$res = $this->https_request($url,$header='',$data='');//CURL获取数据
		
		return $res;
	}



	function sendorder($IP,$Version,$YybID,$AccountNo,$TradeAccount,$JyPassword,$Port,$TxPassword,$Category,$PriceType,$Gddm,$Zqdm,$Price,$Quantity){
		
		$api = $this->API;//接口地址

		$url = $api.'SendOrder?IP='.$IP.'&Version='.$Version.'&YybID='.$YybID.'&AccountNo='.$AccountNo.'&TradeAccount='.$TradeAccount.'&JyPassword='.$JyPassword.'&Port='.$Port.'&TxPassword='.$TxPassword.'&Category='.$Category.'&PriceType='.$PriceType.'&Gddm='.$Gddm.'&Zqdm='.$Zqdm.'&Price='.$Price.'&Quantity='.$Quantity;
		$res = $this->https_request($url,$header='',$data='');//CURL获取数据
		return $res;

	}


	function cancelorder($IP,$Version,$YybID,$AccountNo,$TradeAccount,$JyPassword,$Port,$TxPassword,$ExchangeID,$hth){
		
		$api = $this->API;//接口地址

		$url = $api.'CancelOrder?IP='.$IP.'&Version='.$Version.'&YybID='.$YybID.'&AccountNo='.$AccountNo.'&TradeAccount='.$TradeAccount.'&JyPassword='.$JyPassword.'&Port='.$Port.'&TxPassword='.$TxPassword.'&ExchangeID='.$ExchangeID.'&hth='.$hth;
		$res = $this->https_request($url,$header='',$data='');//CURL获取数据
		return $res;

	}


	// CURL
	function https_request($url,$header=NULL,$data=null){
	    $curl = curl_init();
	    curl_setopt($curl, CURLOPT_URL,$url);
	    if(!empty($header)){
	        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
	    }
	    if (!empty($data)){
	        curl_setopt($curl,CURLOPT_POST,1);
	        curl_setopt($curl,CURLOPT_POSTFIELDS,$data);
	    }
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	    $output = curl_exec($curl);
	    curl_close($curl);
	    return $output;
	}


	
  		//股票行情
	function Hq($gid){
		
		$api = $this->URL;//接口地址

		$url = $api.'Hq?gid='.$gid;

		$res = $this->https_request($url,$header='',$data='');//CURL获取数据
		return $res;

	}
  
  
  	//股票行情(正式)
	function Hq_real($gid){
		
		$api = $this->URL;//接口地址

		$url = $api.'Hq_real?gid='.$gid;

		$res = $this->https_request($url,$header='',$data='');//CURL获取数据
		return $res;

	}
  
  
  	//批量获取股票现价
	function Hq_real_list($gid){
		
		$api = $this->URL;//接口地址

		$url = $api.'Hq_real_list?gid='.$gid;
		
		$res = $this->https_request($url,$header='',$data='');//CURL获取数据
		return $res;

	}
  
  	
  
  
  
  	//批量股票行情
	function Hq_list($gid){
		
		$api = $this->URL;//接口地址

		$url = $api.'Hq_list?gid='.$gid;

		$res = $this->https_request($url,$header='',$data='');//CURL获取数据
		return $res;

	}
  
  
  	//大盘股指批量行情1
	function grailindex1(){
		
		$api = $this->URL;//接口地址

		$url = $api.'Grailindex1';

		$res = $this->https_request($url,$header='',$data='');//CURL获取数据
		return $res;

	}
  
  
  	//大盘股指批量行情2
	function grailindex2(){
		
		$api = $this->URL;//接口地址

		$url = $api.'Grailindex2';

		$res = $this->https_request($url,$header='',$data='');//CURL获取数据
		return $res;

	}
  
  
  	//大盘股指批量行情3
	function grailindex3(){
		
		$api = $this->URL;//接口地址

		$url = $api.'Grailindex3';

		$res = $this->https_request($url,$header='',$data='');//CURL获取数据
		return $res;

	}



	//日K图
	function Kday($gid){
		
		$api = $this->URL;//接口地址

		$url = $api.'Kday?gid='.$gid;
		
		$res = $this->https_request($url,$header='',$data='');//CURL获取数据
		return $res;

	}


	//月K图
	function Kmonth($gid){
		
		$api = $this->URL;//接口地址

		$url = $api.'Kmonth?gid='.$gid;
		
		$res = $this->https_request($url,$header='',$data='');//CURL获取数据
		return $res;

	}

	//周K图
	function Kweek($gid){
		
		$api = $this->URL;//接口地址

		$url = $api.'Kweek?gid='.$gid;
		
		$res = $this->https_request($url,$header='',$data='');//CURL获取数据
		return $res;

	}


	//分时图
	function Minhour($gid){
		
		$api = $this->URL;//接口地址

		$url = $api.'Minhour?code='.$gid;
		
		$res = $this->https_request($url,$header='',$data='');//CURL获取数据
		return $res;

	}


	//判断股票是否停盘
	function Stop($gid){
		
		$api = $this->URL;//接口地址

		$url = $api.'Stop?gid='.$gid;
		
		$res = $this->https_request($url,$header='',$data='');//CURL获取数据
		return $res;

	}
  
  
  
  	//股票模糊搜索
	function Tosearch($gid){
		
		$api = $this->URL;//接口地址

		$url = $api.'Tosearch?gid='.$gid;
		
		$res = $this->https_request($url,$header='',$data='');//CURL获取数据
		return $res;

	}
  
  	//涨幅
	function zhangfu(){
		
		$api = $this->URL;//接口地址

		$url = $api.'zhangfu';
		
		$res = $this->https_request($url,$header='',$data='');//CURL获取数据
		return $res;

	}
  
  	//跌幅
	function diefu(){
		
		$api = $this->URL;//接口地址

		$url = $api.'diefu';
		
		$res = $this->https_request($url,$header='',$data='');//CURL获取数据
		return $res;

	}
  


}